<?php

/**
 * Product:       Xtento_OrderExport
 * ID:            W4rEZtZ3+0mfVAiovVO105wawPm4tL6y8TbFEn5dSf4=
 * Last Modified: 2015-09-10T15:24:06+00:00
 * File:          app/code/Xtento/OrderExport/Block/Adminhtml/Destination.php
 * Copyright:     Copyright (c) XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

namespace Xtento\OrderExport\Block\Adminhtml;

class Destination extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_addButtonLabel = __('Add New Destination');
        parent::_construct();
    }
}
