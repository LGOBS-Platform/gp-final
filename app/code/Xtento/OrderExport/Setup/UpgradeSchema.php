<?php

/**
 * Product:       Xtento_OrderExport
 * ID:            W4rEZtZ3+0mfVAiovVO105wawPm4tL6y8TbFEn5dSf4=
 * Last Modified: 2017-09-19T11:53:16+00:00
 * File:          app/code/Xtento/OrderExport/Setup/UpgradeSchema.php
 * Copyright:     Copyright (c) XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

namespace Xtento\OrderExport\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

/**
 * @codeCoverageIgnore
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $connection = $setup->getConnection();

        if (version_compare($context->getVersion(), '2.3.7', '<')) {
            $connection->addIndex(
                $setup->getTable('xtento_orderexport_profile_history'),
                $setup->getIdxName('xtento_orderexport_profile_history', ['entity_id']),
                ['entity_id']
            );
        }

        $setup->endSetup();
    }
}
