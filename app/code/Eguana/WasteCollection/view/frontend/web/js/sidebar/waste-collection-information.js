define(['jquery', 'ko'], function($,ko) {
    return function () {
        var wasteCollectionInformation = $('input[name=waste-household-collection]:checked').val();
        return {
            getWasteCollectionInformation: function () {
                var wasteCollectionChecked = jQuery('input[name=waste-household-collection]:checked').val();
                return wasteCollectionChecked ? wasteCollectionChecked : '';
            }
        }
    }
});
