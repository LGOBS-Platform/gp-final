define([
    'jquery',
    'mage/utils/wrapper',
    'Magento_Checkout/js/model/quote'
], function ($, wrapper, quote) {
    'use strict';

    return function (setShippingInformationAction) {

        return wrapper.wrap(setShippingInformationAction, function (originalAction) {
            var shippingAddress = quote.shippingAddress();
            if (shippingAddress['extension_attributes'] === undefined) {
                shippingAddress['extension_attributes'] = {};
            }

            // shippingAddress['extension_attributes']['waste_household_collection'] = jQuery('[name="waste_household_collection"]').val();
            shippingAddress['extension_attributes']['waste_household_collection'] = jQuery('input[name=waste-household-collection]:checked').val();
            return originalAction();
        });
    };
});