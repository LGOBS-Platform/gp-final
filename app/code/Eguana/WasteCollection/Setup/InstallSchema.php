<?php
/**
 * Created by PhpStorm.
 * User: sonia
 * Date: 18. 11. 7
 * Time: 오후 9:33
 */

namespace Eguana\WasteCollection\Setup;

use Magento\Framework\DB\Ddl\Table;

/**
 * Class InstallSchema
 * @package Eguana\WasteCollection\Setup
 */
class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{

    /**
     *
     *
     * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
     * @param \Magento\Framework\Setup\ModuleContextInterface $context
     * @return void
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(\Magento\Framework\Setup\SchemaSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        $installer->getConnection()->addColumn(
            $setup->getTable('sales_order_grid'),
            'waste_household_collection',
            [
                'type' => Table::TYPE_TEXT,
                'comment' => 'Waste Household Collection'
            ]
        );

        $installer->endSetup();
    }
}