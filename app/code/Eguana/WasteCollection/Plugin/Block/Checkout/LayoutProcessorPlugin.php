<?php
/**
 * Created by PhpStorm.
 * User: sonia
 * Date: 18. 11. 7
 * Time: 오후 10:01
 */
namespace Eguana\WasteCollection\Plugin\Block\Checkout;

use Magento\Quote\Model\QuoteRepository;

class LayoutProcessorPlugin
{

    /**
     * @var \Magento\Checkout\Model\Session
     */
    private $checkoutSession;

    public function __construct(\Magento\Checkout\Model\Session $checkoutSession)
    {
        $this->checkoutSession = $checkoutSession;
    }


    public function afterProcess(\Magento\Checkout\Block\Checkout\LayoutProcessor $subject,  $jsLayout)
    {
        if (!$this->getVisibility()) {
            return $jsLayout;
        }

        $shippingConfiguration = &$jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
        ['children']['shippingAddress']['children'];
        //Checks if shipping step available.
        if (isset($shippingConfiguration)) {
            $shippingConfiguration['shippingAdditional'] = [
                'component' => 'uiComponent',
                'displayArea' => 'shippingAdditional',
                'children' => [
                    'waste-colloection-radio' => [
                        'component' => 'uiComponent',
                        'displayArea' => 'waste-colloection-radio',
                        'config' => [
                            'customScope' =>'shippingAddress',
                            'template' => 'Eguana_WasteCollection/waste-colloection-radio',
                        ],
                        'dataScope' => 'shippingAddress.waste_household_collection',
                        'provider' => 'checkoutProvider',
                        'visible' => true,
                        'sortOrder' => 200,
                    ]
                ]
            ];
        }
        return $jsLayout;
    }

    /**
     * @return bool
     */
    protected function getVisibility()
    {
        $quote = $this->checkoutSession->getQuote();
        if ($quote->getStore()->getCode() !== 'it'){
            return false;
        }
//        $weight = 0;
//        foreach($quote->getAllVisibleItems() as $item) {
//            $weight += ($item->getWeight() * $item->getQty()) ;
//        }
//        return $weight > 22.9 ? true : false;
        return true;
    }

}