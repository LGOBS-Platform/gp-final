<?php
/**
 * Created by PhpStorm.
 * User: sonia
 * Date: 18. 11. 8
 * Time: 오전 3:14
 */
namespace Eguana\WasteCollection\Plugin\Api;


use Magento\Quote\Model\QuoteRepository;

class ShippingInformationManagementInterfacePlugin
{
    /**
     * @var QuoteRepository
     */
    private $quoteRepository;

    public function __construct(QuoteRepository $quoteRepository)
    {
        $this->quoteRepository = $quoteRepository;
    }


    public function beforeSaveAddressInformation(\Magento\Checkout\Api\ShippingInformationManagementInterface $subject, $cartId, \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation)
    {
        $extAttributes = $addressInformation->getShippingAddress()->getExtensionAttributes();
        $wasteHouseholdCollection = $extAttributes->getWasteHouseholdCollection();
        $quote = $this->quoteRepository->getActive($cartId);
        $quote->setData('waste_household_collection',$wasteHouseholdCollection);
    }
}