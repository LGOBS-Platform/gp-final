<?php
/**
 * Created by PhpStorm.
 * User: sonia
 * Date: 18. 11. 9
 * Time: 오전 10:00
 */

namespace Eguana\WasteCollection\Observer;


use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class ProcessOrderPlace implements ObserverInterface
{

    /**
     * @param Observer $observer
     * @return $this
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $observer->getEvent()->getQuote();
        /** @var \Magento\Sales\Model\Order $order */
        $order = $observer->getEvent()->getOrder();
        $order->setData('waste_household_collection',$quote->getData('waste_household_collection'));

        return $this;

    }
}