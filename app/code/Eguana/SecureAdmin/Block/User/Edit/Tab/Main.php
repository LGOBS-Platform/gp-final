<?php
/**
 * Created by PhpStorm.
 * User: sonia
 * Date: 18. 9. 15
 * Time: 오후 7:24
 */

namespace Eguana\SecureAdmin\Block\User\Edit\Tab;


/**
 * Class Main Overrides \Magento\Backend\Block\System\Account\Edit\Form to add verify rules for security
 * when create a new user
 * @package Eguana\SecureAdmin\Block\User\Edit\Tab
 */
class Main extends \Magento\User\Block\User\Edit\Tab\Main
{
    /**
     * Add password input fields
     *
     * @param \Magento\Framework\Data\Form\Element\Fieldset $fieldset
     * @param string $passwordLabel
     * @param string $confirmationLabel
     * @param bool $isRequired
     * @return void
     */
    protected function _addPasswordFields(
        \Magento\Framework\Data\Form\Element\Fieldset $fieldset,
        $passwordLabel,
        $confirmationLabel,
        $isRequired = false
    ) {
        $requiredFieldClass = $isRequired ? ' required-entry' : '';
        $fieldset->addField(
            'password',
            'password',
            [
                'name' => 'password',
                'label' => $passwordLabel,
                'id' => 'customer_pass',
                'title' => $passwordLabel,
                'class' => 'input-text validate-admin-password validate-admin-password-match-user-username validate-admin-password-continued-number' . $requiredFieldClass,
                'required' => $isRequired
            ]
        );
        $fieldset->addField(
            'confirmation',
            'password',
            [
                'name' => 'password_confirmation',
                'label' => $confirmationLabel,
                'id' => 'confirmation',
                'title' => $confirmationLabel,
                'class' => 'input-text validate-cpassword' . $requiredFieldClass,
                'required' => $isRequired
            ]
        );
    }

}