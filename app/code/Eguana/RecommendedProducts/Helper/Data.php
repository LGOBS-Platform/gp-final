<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-08-24
 * Time: 오후 7:12
 */

namespace Eguana\RecommendedProducts\Helper;


use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\StoreManagerInterface;

class Data extends AbstractHelper
{
    protected $_storeManager;

    public function __construct(
        Context $context,
        StoreManagerInterface $storeManagerInterface
    )
    {
        $this->_storeManager = $storeManagerInterface;
        parent::__construct($context);
    }

    public function getRecommendedEnabled(){

        return $this->getRecommendedGeneralConfig('enabled');
    }

    public function getRecommendedCategroyId(){
        return $this->getRecommendedGeneralConfig('category_id');
    }

    public function getRecommendedProductSize(){
        return $this->getRecommendedGeneralConfig('product_size');
    }

    public function getRecommendedGeneralConfig($path){
        $storeManager = $this->_storeManager;
        return $this->scopeConfig->getValue('recommended_products/general/'.$path , $storeManager::CONTEXT_STORE  , $storeManager->getStore()->getCode());
    }
}