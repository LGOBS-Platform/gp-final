<?php

/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-08-24
 * Time: 오후 5:48
 */
namespace Eguana\RecommendedProducts\Block;

use Eguana\RecommendedProducts\Helper\Data;
use Magento\Catalog\Model\CategoryRepository;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\ResourceModel\Category\Collection;
use \Magento\Framework\View\Element\Template;

class ListBlock extends Template
{
    protected $_categoryCollectionFactory;

    protected $_categoryRepository;

    protected $_recommendedHelper;

    protected $_info;

    public function __construct(
        Template\Context $context,
        CategoryRepository $categoryRepository,
        CollectionFactory $categoryCollectionFactory,
        Data $recommendedHelper,
        array $data = []
    )
    {
        $this->_categoryCollectionFactory = $categoryCollectionFactory;
        $this->_categoryRepository = $categoryRepository;
        $this->_recommendedHelper = $recommendedHelper;
        parent::__construct($context, $data);
    }

    public function isEnabled(){
        return $this->_recommendedHelper->getRecommendedEnabled();
    }

    public function getCategoryInfo(){

        if(is_null($this->_info)){
            $this->setCategoryInfo();
        }

        return $this->_info;
    }

    protected function setCategoryInfo(){
        $info = [];
        $recommended = $this->getRecommendedCategory();

        /**
         * @var $collection Collection
         * @var $item Category
         */

        $collection = $this->getChildCategoryCollection(explode(',',$recommended->getChildren()));

        foreach ($collection as $item){
            $info[] = [
                'name' => $item->getName(),
                'id' => $item->getId(),
                'html' => $this->getCategoryProduct($item->getId())
            ];
        }

        $this->_info = $info;
    }

    public function getCategoryProduct($categoryId){

        /** @var $block \Eguana\RecommendedProducts\Block\Product\ListProduct */
        $block = $this->getLayout()->createBlock('Eguana\RecommendedProducts\Block\Product\ListProduct');
        $block->setRecommendedPageSize($this->_recommendedHelper->getRecommendedProductSize());
        $block->setCategoryId($categoryId)
            ->setTemplate('Eguana_RecommendedProducts::product/list.phtml');

        return $block->toHtml();

    }

    protected function getChildCategoryCollection($categoryIds){

        /**
         * @var $collection Collection
         * @var $item Category
         */
        $collection = $this->_categoryCollectionFactory->create();

        $collection
            ->addAttributeToFilter( 'entity_id' , [ 'in' => $categoryIds])
            ->addAttributeToSort('position')
            ->addAttributeToSelect('name');

        return $collection;
    }

    public function getRecommendedCategory(){
        return $this->_categoryRepository->get($this->_recommendedHelper->getRecommendedCategroyId());
    }
}