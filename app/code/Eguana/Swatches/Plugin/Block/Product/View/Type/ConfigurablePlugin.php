<?php
/**
 * Created by PhpStorm.
 * User: sonia
 * Date: 18. 9. 12
 * Time: 오전 6:25
 */
namespace Eguana\Swatches\Plugin\Block\Product\View\Type;

/**
 * Class ConfigurablePlugin
 * @package Eguana\Swatches\Plugin\Block\Product\View\Type
 */
class ConfigurablePlugin
{
    /**
     * @var \Magento\Framework\Json\EncoderInterface
     */
    private $jsonEncoder;
    /**
     * @var \Magento\Framework\Json\DecoderInterface
     */
    private $jsonDecoder;

    /**
     * ConfigurablePlugin constructor.
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param \Magento\Framework\Json\DecoderInterface $jsonDecoder
     */
    public function __construct(
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Framework\Json\DecoderInterface $jsonDecoder
    ) {
        $this->jsonEncoder = $jsonEncoder;
        $this->jsonDecoder = $jsonDecoder;
    }

    /**
     * @param \Magento\ConfigurableProduct\Block\Product\View\Type\Configurable $subject
     * @param $result
     * @return string
     */
    public function afterGetJsonConfig(
        \Magento\ConfigurableProduct\Block\Product\View\Type\Configurable $subject,
        $result
    ) {
        $result = $this->jsonDecoder->decode($result);

        foreach ($subject->getAllowProducts() as $product) {
            $result['sku'][$product->getId()][] = $product->getData('sku');
            $result['model_name'][$product->getId()][] = $product->getData('model_name');
            $result['user_friendly_name'][$product->getId()][] = $product->getData('user_friendly_name');
        }

        return $this->jsonEncoder->encode($result);
    }
}