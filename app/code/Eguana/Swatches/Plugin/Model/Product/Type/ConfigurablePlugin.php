<?php
/**
 * Created by PhpStorm.
 * User: sonia
 * Date: 18. 9. 12
 * Time: 오전 6:30
 */
namespace Eguana\Swatches\Plugin\Model\Product\Type;

/**
 * Class ConfigurablePlugin
 * @package Eguana\Swatches\Plugin\Model\Product\Type
 */
class ConfigurablePlugin
{
    /**
     * @param \Magento\ConfigurableProduct\Model\Product\Type\Configurable $subject
     * @param $result
     * @return \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable\Product\Collection
     */
    public function afterGetUsedProductCollection(
        \Magento\ConfigurableProduct\Model\Product\Type\Configurable $subject,
        $result
    ) {
        /** @var \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable\Product\Collection $result */
        $result->addAttributeToSelect(['sku','model_name','user_friendly_name']);
        return $result;
    }
}