<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2018-08-24
 * Time: 오후 7:19
 */

namespace Eguana\Attributes\Plugin\Checkout\Model;

use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Catalog\Model\ProductRepository as ProductRepository;

class DefaultConfigProviderPlugin extends \Magento\Framework\Model\AbstractModel
{
    protected $checkoutSession;

    protected $_productRepository;

    /**
     * DefaultConfigProviderPlugin constructor.
     * @param CheckoutSession $checkoutSession
     * @param ProductRepository $productRepository
     */
    public function __construct(
        CheckoutSession $checkoutSession,
        ProductRepository $productRepository
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->_productRepository = $productRepository;
    }

    /**
     * @param \Magento\Checkout\Model\DefaultConfigProvider $subject
     * @param array $result
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function afterGetConfig(
        \Magento\Checkout\Model\DefaultConfigProvider $subject,
        array $result
    ) {
        $items = $result['totalsData']['items'];
        foreach ($items as $index => $item) {
            $quoteItem = $this->checkoutSession->getQuote()->getItemById($item['item_id']);
            $product = $this->_productRepository->getById($quoteItem->getProduct()->getId());
            $result['quoteItemData'][$index]['sales_model_code'] = $product->getSalesModelCode();
            $result['quoteItemData'][$index]['model_name'] = $product->getModelName();
            $result['quoteItemData'][$index]['user_friendly_name'] = $product->getUserFriendlyName();
        }
        return $result;
    }
}