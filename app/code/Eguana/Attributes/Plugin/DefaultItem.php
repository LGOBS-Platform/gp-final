<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2018-08-24
 * Time: 오후 6:43
 */

namespace Eguana\Attributes\Plugin;

use Magento\Quote\Model\Quote\Item;

class DefaultItem
{
    public function aroundGetItemData($subject, \Closure $proceed, Item $item)
    {
        $data = $proceed($item);
        $product = $item->getProduct();

        $atts = [
            'sales_model_code' => $product->getSalesModelCode(),
            'model_name' => $product->getModelName(),
            'user_friendly_name' => $product->getUserFriendlyName()
        ];

        return array_merge($data, $atts);
    }
}