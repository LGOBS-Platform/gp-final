<?php
/**
 * Created by PhpStorm.
 * User: abbas
 * Date: 8/7/18
 * Time: 1:36 PM
 */

namespace Eguana\Attributes\Setup;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Type;
use Magento\Catalog\Model\Resource\Eav\Attribute;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;
use Magento\Catalog\Setup\CategorySetupFactory;


/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
    /**
     * EAV setup factory
     *
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    private $attributeSetFactory;
    private $categorySetupFactory;
    protected $_attributeSetCollection;
    /**
     * Init
     *
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(EavSetupFactory $eavSetupFactory, AttributeSetFactory $attributeSetFactory, CategorySetupFactory $categorySetupFactory,
    \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $attributeSetCollection)
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->attributeSetFactory = $attributeSetFactory;
        $this->categorySetupFactory = $categorySetupFactory;
        $this->_attributeSetCollection = $attributeSetCollection;
        /* assign object to class global variable for use in other class methods */
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $textAttributes = array(
            'locale_code'=>'Locale Code',
            'super_category_id'=>'Super Category ID',
            'super_category_name'=>'Super Category Name',
            'super_category_eng_name'=>'Super Category English Name',
            'category_id'=>'Category ID',
            'category_name'=>'Category Name',
            'category_eng_name'=>'Category English Name',
            'sub_category_id1'=>'Sub Category 1 ID',
            'sub_category_name1'=>'Sub Category 1 Name',
            'sub_category_eng_name1'=>'Sub Category 1 English Name',
            'sub_category_id2'=>'Sub Category 2 ID',
            'sub_category_name2'=>'Sub Category 2 Name',
            'sub_category_eng_name2'=>'Sub Category 2 English Name',
            'sub_category_id3'=>'Sub Category 3 ID',
            'sub_category_name3'=>'Sub Category 3 Name',
            'sub_category_eng_name3'=>'Sub Category 3 English Name',
            'sub_category_id4'=>'Sub Category 4 ID',
            'sub_category_name4'=>'Sub Category 4 Name',
            'sub_category_eng_name4'=>'Sub Category 4 English Name',
            'sub_category_id5'=>'Sub Category 5 ID',
            'sub_category_name5'=>'Sub Category 5 Name',
            'sub_category_eng_name5'=>'Sub Category 5 English Name',
            'model_id'=>'Model ID',
            'model_name'=>'Model Name',
            'sales_model_code'=>'Sales Model Code',
            'sales_suffix_code'=>'Sales Suffix Code',
            'product_level1_code'=>'Product Level 1 Code',
            'product_level2_code'=>'Product Level 2 Code',
            'product_level3_code'=>'Product Level 3 Code',
            'color_code'=>'Color Code',
            'user_friendly_name'=>'User Friendly Name',
            'title_name'=>'Color Code',
            'full_model_name'=>'Full Model Name',
            'bullet_feature1'=>'Bullet Feature 1',
            'bullet_feature2'=>'Bullet Feature 2',
            'bullet_feature3'=>'Bullet Feature 3',
            'bullet_feature4'=>'Bullet Feature 4',
            'bullet_feature5'=>'Bullet Feature 5',
            'bullet_feature6'=>'Bullet Feature 6',
            'model_status_code'=>'Model Status Code',
            'url'=>'URL',
            'level1_spec_id'=>'Level 1 Spec ID',
            'level1_spec_name'=>'Level 1 Spec Name',
            'level2_spec_id'=>'Level 2 Spec ID',
            'level2_spec_name'=>'Level 2 Spec Name',
            'spec_value_id'=>'Spec Value ID',
            'spec_value_name'=>'Spec Value Name',
        );

        $dateTimeAttributes = array(
            'creation_date'=>'Creation Date',
            'last_update_date'=>'Last Update Date',
            'model_release_date'=>'Model Release Date'

        );

        $decimalAttributes = array(
            'inch_code'=>'Inch Code',
            'promotion_price'=>'Promotion_Price'

        );

        $booleanAttributes = array(
            'msrp_code'=>'MSRP' //Just msrp was not working so I set the msrp_code

        );

        $wysiwygAttributes = array(
            'model_desc'=>'Model Description',
            //'features'=>'Features',
            'specifications'=>'Specifications',

        );

        $setup->startSetup();

        // TO CREATE ATTRIBUTE SETS
        $attributeSets = array('Refrigerator', 'Washing Machine', 'TV', 'CAV', 'MNT', 'MC');
        $categorySetup = $this->categorySetupFactory->create(['setup' => $setup]);

        foreach ($attributeSets as $attributeSetName) {
            $currentAttributeSetId = $this->getAttributeSetId($attributeSetName);
            if(!$currentAttributeSetId) {
                $attributeSet = $this->attributeSetFactory->create();
                $entityTypeId = $categorySetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);
                $attributeSetId = $categorySetup->getDefaultAttributeSetId($entityTypeId);
                $data = [
                    'attribute_set_name' => $attributeSetName,
                    'entity_type_id' => $entityTypeId,
                    'sort_order' => 200,
                ];
                $attributeSet->setData($data);
                $attributeSet->validate();
                $attributeSet->save();
                $attributeSet->initFromSkeleton($attributeSetId);
                $attributeSet->save();
                $autosettingsTabName = 'LG Attributes';
                $categorySetup->addAttributeGroup($entityTypeId, $attributeSet->getId(), $autosettingsTabName, 60);
                $autosettingsTabName = 'GERP Attributes';
                $categorySetup->addAttributeGroup($entityTypeId, $attributeSet->getId(), $autosettingsTabName, 70);
                $autosettingsTabName = 'Features';
                $categorySetup->addAttributeGroup($entityTypeId, $attributeSet->getId(), $autosettingsTabName, 80);
            }

        }

        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        foreach ($textAttributes as $code=>$textAttribute)
        {
            $this->addTextField($eavSetup, $code, $textAttribute);
        }

        foreach ($dateTimeAttributes as $code=>$dateAttribute)
        {
            $this->addDateTimeField($eavSetup, $code, $dateAttribute);
        }

        foreach ($decimalAttributes as $code=>$decimalAttribute)
        {
            $this->addDecimalField($eavSetup, $code, $decimalAttribute);
        }

        foreach ($booleanAttributes as $code=>$booleanAttribute)
        {
            $this->addBooleanField($eavSetup, $code, $booleanAttribute);
        }

        foreach ($wysiwygAttributes as $code=>$wysiwygAttribute)
        {
            $this->addWysiwygField($eavSetup, $code, $wysiwygAttribute);
        }

        for ($i = 1; $i <= 20; $i++)
        {
            $this->addWysiwygField($eavSetup, 'feature_'.$i, 'Feature '.$i, 'Features');
        }

        $gerpAttributes = array(
            'model_no'=>'Model No',
            'rrp_price'=>'RRP Price',
            'selling_price'=>'Selling Price'
        );

        foreach ($gerpAttributes as $code=>$textAttribute)
        {
            $this->addTextField($eavSetup, $code, $textAttribute, 'GERP Attributes');
        }

        $setup->endSetup();
    }

    private function getAttributeSetId($attributeSetName)
    {
        $attributeSet = $this->_attributeSetCollection->create()->addFieldToSelect(
            '*'
        )->addFieldToFilter(
            'attribute_set_name',
            $attributeSetName
        );
        $attributeSetId = 0;
        foreach($attributeSet as $attr):
            $attributeSetId = $attr->getAttributeSetId();
        endforeach;
        return $attributeSetId;
    }

    private function addTextField($eavSetup, $attributeCode, $attributeName, $group = 'LG Attributes')
    {
        /**
         * Add attributes to the eav/attribute
         */
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            $attributeCode,
            [
                'type' => 'text',
                'backend' => '',
                'frontend' => '',
                'label' => $attributeName,
                'input' => 'text',
                'class' => '',
                'source' => '',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => '',
                'searchable' => true,
                'filterable' => true,
                'comparable' => false,
                'visible_on_front' => true,
                'used_in_product_listing' => true,
                'unique' => false,
                'apply_to' => 'simple,configurable,virtual,bundle,downloadable,grouped',
                'group'=> $group
            ]
        );
    }

    private function addDateTimeField($eavSetup, $attributeCode, $attributeName)
    {
        /**
         * Add attributes to the eav/attribute
         */
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            $attributeCode,
            [
                'type' => 'datetime',
                'frontend' => '',
                'label' => $attributeName,
                'input' => 'date',
                'backend' => \Magento\Eav\Model\Entity\Attribute\Backend\Datetime::class,
                'class' => '',
                'source' => '',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => '',
                'searchable' => true,
                'filterable' => true,
                'comparable' => false,
                'visible_on_front' => true,
                'used_in_product_listing' => true,
                'unique' => false,
                'apply_to' => 'simple,configurable,virtual,bundle,downloadable,grouped',
                'group'=> 'LG Attributes'
            ]
        );

    }

    private function addDecimalField($eavSetup, $attributeCode, $attributeName)
    {
        /**
         * Add attributes to the eav/attribute
         */
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            $attributeCode,
            [
                'type' => 'decimal',
                'frontend' => '',
                'label' => $attributeName,
                'input' => 'text',
                'backend' => '',
                'class' => '',
                'source' => '',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => 0,
                'searchable' => true,
                'filterable' => true,
                'comparable' => true,
                'visible_on_front' => true,
                'used_in_product_listing' => true,
                'unique' => false,
                'apply_to' => 'simple,configurable,virtual,bundle,downloadable,grouped',
                'group'=> 'LG Attributes'
            ]
        );
    }

    private function addBooleanField($eavSetup, $attributeCode, $attributeName)
    {
        $eavSetup->removeAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            $attributeCode);
        /**
         * Add attributes to the eav/attribute
         */
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            $attributeCode,
            [
                'type' => 'text',
                'frontend' => '',
                'label' => $attributeName,
                'input' => 'select',
                'backend' => '',
                'class' => '',
                'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => 0,
                'searchable' => true,
                'filterable' => true,
                'comparable' => true,
                'visible_on_front' => true,
                'used_in_product_listing' => true,
                'unique' => false,
                'apply_to' => 'simple,configurable,virtual,bundle,downloadable,grouped',
                'group'=> 'LG Attributes'
            ]
        );

    }

    private function addWysiwygField($eavSetup, $attributeCode, $attributeName, $group='LG Attributes')
    {
        /**
         * Add attributes to the eav/attribute
         */
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            $attributeCode,
            [
                'type' => 'text',
                'frontend' => '',
                'label' => $attributeName,
                'input' => 'textarea',
                'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
                'class' => '',
                'source' => '',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => true,
                'used_in_product_listing' => false,
                'wysiwyg_enabled' => true,
                'unique' => false,
                'apply_to' => 'simple,configurable,virtual,bundle,downloadable,grouped',
                'group'=> $group
            ]
        );

    }
}