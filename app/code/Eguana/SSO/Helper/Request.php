<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-09-05
 * Time: 오전 10:26
 */

namespace Eguana\SSO\Helper;


use Eguana\SSO\Model\Cache\Type;
use Eguana\SSO\Model\RestClient;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Stdlib\CookieManagerInterface;

class Request extends Data
{
    protected $_restClient;

    protected $_cookieManager;

    protected $_registry;

    protected $_cookieMetaFactory;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        Type $ssoCache,
        SessionHelper $sessionHelper,
        \Eguana\SSO\Logger\SSO $ssoLogger,
        Security $securityHelper,
        RestClient $restClient,
        Registry $registry,
        CookieMetadataFactory $cookieMetadataFactory,
        CookieManagerInterface $cookieManagerInterface
    )
    {
        $this->_registry = $registry;
        $this->_restClient = $restClient;
        $this->_cookieManager = $cookieManagerInterface;
        $this->_cookieMetaFactory = $cookieMetadataFactory;
        parent::__construct($context, $ssoCache, $sessionHelper,$ssoLogger,$securityHelper);
    }

    protected function registry($key,$value=null){

        if(is_null($value)){
            return $this->_registry->registry($key);
        }

        $this->_registry->register($key,$value);
        return $value;
    }

    public function getSsoUserData(){

        $this->log(__METHOD__,"START");
        $response = false;

        $validateRequest = $this->validateSessionRequest();

        if ($validateRequest && isset($validateRequest->success) && $validateRequest->success == 'true') {

            $this->log(__METHOD__."SSO DATA VALIDATE SUCCESS",null,true);
            $response = $this->getUserProfileRequest();
            $this->log(__METHOD__."SSO DATA GET PROFILE SUCCESS",null,true);
        }else{
            $this->log(__METHOD__."SSO DATA VALIDATE FAIL",null,true);
        }
        $this->log(__METHOD__, "END", true);

        return $response;
    }

    public function validateSessionRequest(){

        $this->log(__METHOD__,"START");

        $uri = $this->getRequestConfig('uri');
        $path = $this->getRequestConfig('validate_path');

        $lgSessionId = $this->_cookieManager->getCookie($this->getSSOGeneralConfig('dot_com_session_key'));
        $lgSessionId = str_replace(" ", "+", $lgSessionId);

        $response = $this->registry($path);

        if(is_null($response)){

            $this->log(__METHOD__,"URI : ".$uri);
            $this->log(__METHOD__,"PATH : ".$path);
            $this->log(__METHOD__,"LG SESSION ID : ".$lgSessionId);

            if(!$lgSessionId){
                $this->log(__METHOD__,"DATA NULL .",true);
                return false;
            }

            $data = $this->dataToRequestXml('ValidateSessionRequest',['lgSessionId' => $lgSessionId]);
            $this->log(__METHOD__,"Request Data : ".$data,true);

            $response = $this->restCall($uri,$path,$data);
            $this->registry($path,$response);
        }

        return $response;

    }

    public function getUserProfileRequest(){

        $this->log(__METHOD__,"START");

        $uri = $this->getRequestConfig('uri');
        $path = $this->getRequestConfig('profile_path');

        $response = $this->registry($path);

        if(is_null($response)) {

            $lgSessionId = $this->_cookieManager->getCookie($this->getSSOGeneralConfig('dot_com_session_key'));
            $lgSessionId = str_replace(" ", "+", $lgSessionId);
            $obsReferenceId = $this->getSessionHelper()->getSsoSession()->getReferenceId();

            $this->log(__METHOD__, "URI : " . $uri);
            $this->log(__METHOD__, "PATH : " . $path);
            $this->log(__METHOD__, "LG SESSION ID : " . $lgSessionId);
            $this->log(__METHOD__, "OBS REFERENCE ID : " . $obsReferenceId,true);

            if (!$lgSessionId || !$obsReferenceId) {
                $this->log(__METHOD__, "DATA NULL .", true);
                return false;
            }

            $data = $this->dataToRequestXml('GetUserProfileRequest', ['lgSessionId' => $lgSessionId, 'obsReferenceId' => $obsReferenceId]);
            $this->log(__METHOD__, "Request Data : " . $data, true);

            $response = $this->restCall($uri,$path,$data);
            $this->registry($path,$response);
        }

        return $response;
    }

    public function logoutRequest(){

        $this->log(__METHOD__,"START");

        $uri = $this->getRequestConfig('uri');
        $path = $this->getRequestConfig('logout_path');


        $response = $this->registry($path);

        if(is_null($response)) {

            $lgSessionId = $this->_cookieManager->getCookie($this->getSSOGeneralConfig('dot_com_session_key'));
            $lgSessionId = str_replace(" ", "+", $lgSessionId);
            $this->log(__METHOD__,"URI : ".$uri);
            $this->log(__METHOD__,"PATH : ".$path);
            $this->log(__METHOD__,"LG SESSION ID : ".$lgSessionId);

            if(!$lgSessionId){
                $this->log(__METHOD__,"LG SESSION ID NULL .",true);
                return false;
            }

            $data = $this->dataToRequestXml('LGSessionIdExpiredRequest',['lgSessionId' => $lgSessionId]);
            $this->log(__METHOD__,"Request Data : ".$data,true);

            $response = $this->restCall($uri,$path,$data);
            $this->registry($path,$response);
            $cookieMetadata = $this->_cookieMetaFactory->createCookieMetadata()->setPath('/');
            $this->_cookieManager->deleteCookie($this->getSSOGeneralConfig('dot_com_session_key'),$cookieMetadata);
        }

        return $response;
    }

    public function dataToRequestXml($headerName,$data){
        $xml = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?>'."<ns1:$headerName/>",LIBXML_NOERROR,false,'ns1',true);
        $xml->addAttribute('xmlns:xmlns:ns1','http://www.lg.com');
        foreach ($data as $key => $value){
            $xml->addChild($key,$value);
        }

        return $xml->asXML();

    }

    protected function restCall($uri,$path,$data){

        $this->log(__METHOD__,"START");
        $this->log(__METHOD__,$uri);
        $this->log(__METHOD__,$data);
        $this->log(__METHOD__,base64_encode($this->getAuthKey()));

        $lgSessionCookieName = $this->getSSOGeneralConfig('dot_com_session_key');
        $lgSessionId = $this->_cookieManager->getCookie($lgSessionCookieName);
        $lgSessionId = str_replace(" ", "+", $lgSessionId);

        $header = [
            'Content-Type: text/xml',
            'authKey: '.base64_encode($this->getAuthKey()),
        ];

        if($lgSessionId != '')
            $header[] = 'Cookie: '.$lgSessionCookieName.'='.$lgSessionId;
        $ch=curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $uri.$path);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60); //timeout in seconds
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        $response = curl_exec($ch);
        $error = curl_error($ch);
        curl_close($ch);

        $this->log(__METHOD__,$response);
        $this->log(__METHOD__,$error);

        if($response != '' && $error == ''){
            $this->log(__METHOD__,'SUCCESS',true);
            return simplexml_load_string($response);
        }
        $this->log(__METHOD__,'FAIL',true);

        return false;

    }

    public function getRestClient(){
        return $this->_restClient;
    }


}