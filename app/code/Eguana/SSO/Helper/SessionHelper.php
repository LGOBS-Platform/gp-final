<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-09-04
 * Time: 오전 10:59
 */

namespace Eguana\SSO\Helper;

use Magento\Customer\Model\Visitor;
use Magento\Framework\App\Helper\AbstractHelper;

class SessionHelper extends AbstractHelper
{
    const PHP_SESSION_COOKIE_KIY = 'PHPSESSID';

    protected $_checkoutSession;

    protected $_ssoSession;

    protected $_customerSession;

    protected $_sessionSaveHandler;

    protected $_date;

    protected $_visitorCollection;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Checkout\Model\Session $checkoutSession,
        Visitor $visitorModel,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Eguana\SSO\Model\Session $ssoSession,
        \Magento\Framework\Session\SaveHandler $sessionSaveHandler,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Model\ResourceModel\Visitor\Collection $visitorCollection
    )
    {
        $this->_checkoutSession = $checkoutSession;
        $this->_ssoSession = $ssoSession;
        $this->_customerSession = $customerSession;
        $this->_sessionSaveHandler = $sessionSaveHandler;
        $this->_date = $date;
        $this->_visitorCollection = $visitorCollection;
        parent::__construct($context);
    }

    /** @return \Magento\Checkout\Model\Session */
    public function getCheckoutSession(){
        return $this->_checkoutSession;
    }

    /** @return \Magento\Customer\Model\Session */
    public function getCustomerSession(){
        return $this->_customerSession;
    }

    /** @return \Eguana\SSO\Model\Session */
    public function getSsoSession(){
        return $this->_ssoSession;
    }

    /**
     * @param $sessionId string
     * @return boolean
     */
    public function validateSession($sessionId){

        $result = true;
        $expireDate = $this->_date->gmtTimestamp() - $this->scopeConfig->getValue('web/cookie/cookie_lifetime');
        $visitorData = $this->_visitorCollection
            ->addFieldToFilter('session_id',['eq' => $sessionId])
            ->addFieldToFilter('last_visit_at',['gteq' => $expireDate])
            ->getFirstItem();

        if(!$visitorData){
            $this->_sessionSaveHandler->destroy($sessionId);
            $result = false;
        }

        return $result;
    }

    /**
     * @param $sessionId string
     * @return boolean
     */
    public function sessionDestory($sessionId){
        return $this->_sessionSaveHandler->destroy($sessionId);
    }

    /**
     * @param $sessionId string
     * @return array
     */
    public function getSessionDataByID($sessionId){

        $sessionData = $this->_sessionSaveHandler->read($sessionId);

        $sessionData = explode('_session_',$sessionData);


        $data = [];
        $nextDataKey = null;
        $currentData = null;

        for ($sessionCounter = 1 ; $sessionCounter < count($sessionData); $sessionCounter++){

            $splitData = explode('|',$sessionData[$sessionCounter]);


            $sessionCount = count($splitData);

            for($sessionDataCounter = 0 ; $sessionDataCounter < $sessionCount; $sessionDataCounter++){

                if($sessionDataCounter == 0){
                    $nextDataKey = $splitData[$sessionDataCounter];
                }elseif($sessionDataCounter+1 == $sessionCount)
                    break;

                $split = strrpos($splitData[$sessionDataCounter+1],'}') +1;

                if($split !== false){
                    $currentData = unserialize(substr($splitData[$sessionDataCounter+1],0,$split));
                    $data[$nextDataKey] = $currentData;
                    $nextDataKey = substr($splitData[$sessionDataCounter+1],$split);
                }
                if(!$nextDataKey)
                    continue;

                $data[$nextDataKey] = $currentData;
            }
        }

        return $data;

    }

}