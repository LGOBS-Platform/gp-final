<?php

/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-08-27
 * Time: 오전 10:00
 */

namespace Eguana\SSO\Helper;

use Eguana\SSO\Model\Cache\Type;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Model\Session;
use \Magento\Framework\App\Helper\Context;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Model\Customer as CustomerModel;
use Magento\Customer\Model\CustomerExtractor;
use Magento\Customer\Model\ResourceModel\CustomerRepository;
use Eguana\SSO\Model\Session as SsoSession;
use Magento\Quote\Model\QuoteRepository;

class Customer extends Data
{

    protected $_customerModel;

    /** @var \Magento\Customer\Model\CustomerExtractor */
    protected $_customerExtractor;

    /** \Magento\Customer\Model\AccountManagement */
    protected $_accountManagement;

    protected $_customerRepository;

    /**
     * @var \Magento\Framework\Stdlib\Cookie\PhpCookieManager
     */
    protected $_cookieMetadataManager;

    protected $_customerGroupFactory;

    protected $_storeManager;

    protected $_orderCollection;

    protected $_quoteRepository;

    protected $_refreshHelper;

    protected $_quoteFactory;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        Type $ssoCache,
        SessionHelper $sessionHelper,
        \Eguana\SSO\Logger\SSO $ssoLogger,
        Security $securityHelper,
        CustomerModel $customerModel,
        CustomerExtractor $customerExtractor,
        \Magento\Customer\Model\GroupFactory $customerGroupFactory,
        AccountManagementInterface $accountManagement,
        CustomerRepository $customerRepository,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Sales\Model\ResourceModel\Order\Collection $orderCollection,
        \Eguana\MiniCartRefresh\Helper\Data $refreshHelper,
        QuoteRepository $quoteRepository,
        \Magento\Quote\Model\QuoteFactory $quoteFactory
    )
    {
        $this->_customerModel = $customerModel;
        $this->_customerExtractor = $customerExtractor;
        $this->_accountManagement = $accountManagement;
        $this->_customerRepository = $customerRepository;
        $this->_customerGroupFactory = $customerGroupFactory;
        $this->_storeManager = $storeManager;
        $this->_orderCollection = $orderCollection;
        $this->_refreshHelper = $refreshHelper;
        $this->_quoteRepository = $quoteRepository;
        $this->_quoteFactory = $quoteFactory;
        parent::__construct($context,$ssoCache,$sessionHelper,$ssoLogger,$securityHelper);
    }

    public function customerDelete($referenceId){

        $this->log(__METHOD__,$referenceId);

        $result = true;
        $code = '00';
        $errorMsg = '';

        /** @var $customerGroup \Magento\Customer\Model\Group */
        $customer = $this->getCustomer($referenceId);


        if($this->customerProcessingOrderCheck($customer)){

            if($groupChangeResult = $this->changeCustomerGroup($customer,self::CUSTOMER_GROUP_DELETE_PENDING) !== true){
                $result = false;
                $code = '01';
                $errorMsg = $groupChangeResult;
            }

        }else{
            $result = false;
            $code = '02';
            $errorMsg = __('The customer\'s order is  processing.');
        }


        $this->log(__METHOD__,"RESULT : ".$result);
        $this->log(__METHOD__,"CODE : ".$code);
        $this->log(__METHOD__,"ERROR MSG : ".$errorMsg,true);

        return [
            'result' => $result,
            'code' => $code,
            'errorMsg' => $errorMsg
        ];
    }

    /**
     * @param $customer \Magento\Customer\Model\Customer
     * @return boolean
     */
    protected function customerProcessingOrderCheck($customer){

        $processingOrderCount  = $this->_orderCollection
            ->addFieldToFilter('state', ['eq' => 'processing'])
            ->addFieldToFilter('customer_id',['eq' => $customer->getId()])->count();

        if($processingOrderCount > 0){
            return false;
        }

        return true;
    }

    /**
     * @param $customer \Magento\Customer\Model\Customer
     * @param $groupCode string
     * @return boolean | string
     */
    protected function changeCustomerGroup($customer,$groupCode){

        $customer->setGroupId($this->getGroupIdByCode($groupCode));

        try{
            $customer->save();
        }catch (\Exception $e){
            return $e->getMessage();
        }

        return true;
    }

    protected function getGroupIdByCode($groupCode){

        $customerGroup = $this->_customerGroupFactory->create();
        $customerGroup->load($groupCode,'customer_group_code');

        return $customerGroup->getId();
    }

    public function loginRejectCustomerGroupInfo(){

        return [
            $this->getGroupIdByCode(self::CUSTOMER_GROUP_DELETE_PENDING) => self::CUSTOMER_GROUP_DELETE_PENDING,
            $this->getGroupIdByCode(self::CUSTOMER_GROUP_DELETE) => self::CUSTOMER_GROUP_DELETE
        ];
    }

    /**
     * @param $sessionId string | null
     * @return Session | boolean
     * @throws \Exception
     */
    public function logout($sessionId){
        $sessionData = $this->getSessionHelper()->getSessionDataByID($sessionId);
        $this->log(__METHOD__,$sessionId,true);
        $this->log(__METHOD__,$sessionData,true);


        if(isset($sessionData['default']) && isset($sessionData['default']['visitor_data']) &&
            isset($sessionData['default']['visitor_data']['customer_id'])){
            return $this->getSessionHelper()->sessionDestory($sessionId);
        }

        return false;
    }

    /**
     * @param $obsReferenceId string | null
     * @param $guestSessionId string | null
     * @return Session | boolean
     * @throws \Exception
     */
    public function login($obsReferenceId,$guestSessionId = null){

        $this->log(__METHOD__,"OBS REFERENCE ID : ".$obsReferenceId);
        $this->log(__METHOD__,"GUEST SESSION ID : ".$guestSessionId);

        if(!$obsReferenceId)
            throw new \Exception(__('reference id is require.'));

        $customer = $this->getCustomer($obsReferenceId);

        if(!$customer->getId()){
            $this->log(__METHOD__,"CUSTOMER SIGN UP");

            $bindData = $this->_bindCustomerData($obsReferenceId);
            $this->ssoSessionSet($obsReferenceId);
            $customer = $this->_customerCreate($bindData);

            if(!$customer->getId()){
                $this->log(__METHOD__,"Customer Sign Up Fail.",true);
                throw new \Exception(__('Customer Sign Up Fail.'));
            }

            $this->log(__METHOD__,"Customer Sign Up SUCESS.");

        }else{
            $this->log(__METHOD__,"CUSTOMER SIGN IN");
            $customer = $this->_customerRepository->getById($customer->getId());
        }

        $session = $this->_login($customer,$guestSessionId,$obsReferenceId);

        if(!$session->getCustomerId()){
            $this->log(__METHOD__,"Customer Sign In Fail.",true);
            throw new \Exception(__('Customer Sign In Fail.'));
        }

        $this->_refreshHelper->setRefresh();

        $this->log(__METHOD__,"LOGIN SESSION ID : ".$session->getSessionId(),true);

        return $session;
    }

    /**
     * @param $sessionId string
     * @return boolean
     */
    public function sessionValidate($sessionId){

        $this->log(__METHOD__,"SESSION ID : ".$sessionId,true);

        return $this->getSessionHelper()->validateSession($sessionId);
    }

    /**
     *  @param $customer CustomerInterface
     *  @param $guestSessionId string
     *  @return Session
     */
    protected function _login($customer , $guestSessionId = null,$obsReferenceId){

        $customerSession = $this->getSessionHelper()->getCustomerSession();
        $customerSession->setCustomerDataAsLoggedIn($customer);
        $customerSession->regenerateId();
        $this->ssoSessionSet($obsReferenceId);

        if(!is_null($guestSessionId))
            $this->customerCartAddGuestProduct($customer->getId(),$guestSessionId);

        return $customerSession;
    }

    /**
     *  @param $guestSessionId string
     */
    protected function customerCartAddGuestProduct($customerId,$guestSessionId){
        $this->log(__METHOD__,'CUSTOMER ID : '.$customerId);
        $this->log(__METHOD__,'GUEST SESSION ID : '.$guestSessionId,true);
        $sessionData = $this->getSessionHelper()->getSessionDataByID($guestSessionId);

        if(isset($sessionData['default']) && isset($sessionData['default']['visitor_data']) &&
            isset($sessionData['default']['visitor_data']['quote_id'])){
            $this->loadCustomerQuote($customerId,$sessionData['default']['visitor_data']['quote_id']);
        }
    }

    /**
     * Load data for customer quote and merge with current quote
     *
     * @return $this
     */
    public function loadCustomerQuote($customerId,$currentQuoteId)
    {
        $this->log(__METHOD__,'START',true);
        $checkoutSession = $this->_sessionHelper->getCheckoutSession();
        $this->log(__METHOD__,'1',true);
        try {
            $customerQuote = $this->_quoteRepository->get($checkoutSession->getQuoteId());
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            $customerQuote = $this->_quoteFactory->create();
        }
        $this->log(__METHOD__,'2',true);

        $customerQuote->setStoreId($this->_storeManager->getStore()->getId());
        $currentQuote = $this->_quoteRepository->get($currentQuoteId);
        $this->log(__METHOD__,'3',true);

        if ($customerQuote->getId() && $currentQuoteId != $customerQuote->getId()) {
            if ($currentQuoteId) {
                $this->_quoteRepository->save(
                    $customerQuote->merge($this->_quoteRepository->get($currentQuoteId))->collectTotals()
                );
            }

            $checkoutSession->setQuoteId($customerQuote->getId());

            if ($currentQuote) {
                $this->_quoteRepository->delete($currentQuote);
            }
            $checkoutSession->setQuoteId($customerQuote->getId());

        }
        $this->log(__METHOD__,'END',true);

        return $this;
    }

    protected function ssoSessionSet($referenceId){

        $ssoSession = $this->getSessionHelper()->getSsoSession();

        $ssoSession->setReferenceId($referenceId);

    }

    protected function _customerCreate($customerData){

        $request = $this->_getRequest();
        $request->setParams(
            $customerData
        );
        $customer = $this->_customerExtractor->extract('customer_account_create', $request);
        $customer->setGroupId($customerData['group_id']);


        $customer = $this->_accountManagement
            ->createAccount($customer, $customerData['password']);
        $this->_eventManager->dispatch(
            'customer_register_success',
            ['account_controller' => $customer, 'customer' => $customer]
        );

        return $customer;
    }

    /**
     * @param string $referenceId
     * @return array
     */
    protected function _bindCustomerData($referenceId){

        $currentStore = $this->_storeManager->getStore();

        $customerData = [
            'group_id' => self::SSO_CUSTOMER_SIGN_UP_GROUP_ID,
            'reference_id' => $referenceId,
            'store_code' => $currentStore->getCode(),
            'store_id' => $currentStore->getId(),
            'website_id' => $currentStore->getWebsiteId(),
            'firstname' => $referenceId,
            'lastname' => $referenceId,
            'email' => sprintf("%s@lg.com",$referenceId),
            'password' => 'LG'.md5(time())
        ];

        return $customerData;
    }


    /**
     * @param string $referenceId
     * @return CustomerModel | boolean
     */
    protected function getCustomer($referenceId){

        /**
         * @var \Magento\Customer\Model\ResourceModel\Customer\Collection $customerCollection
         * @var \Magento\Customer\Model\Customer $customer
         */
        $customerCollection = $this->_customerModel->getCollection();
        $customer = $customerCollection->addAttributeToFilter(self::CUSTOMER_SSO_ID,['eq' => $referenceId])
            ->getFirstItem();

        return $customer;
    }

}