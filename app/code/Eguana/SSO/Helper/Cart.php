<?php

/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-08-27
 * Time: 오전 10:00
 */

namespace Eguana\SSO\Helper;


use Eguana\SSO\Model\Cache\Type;
use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\FilterGroupBuilder;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Catalog\Model\Product as ProductModel;

class Cart extends Data
{
    const SSO_CART_CACHE_PREFIX_KEY = 'SSO_CART_';

    protected $_cart;

    protected $_storeManager;

    protected $_productRepository;

    protected $_quoteRepository;

    protected $_cookieManager;

    protected $_searchCriteria;

    protected $_filterBuilder;

    protected $_filterGroupBuilder;

    protected $_productModel;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        Type $ssoCache,
        SessionHelper $sessionHelper,
        \Eguana\SSO\Logger\SSO $ssoLogger,
        Security $securityHelper,
        \Magento\Checkout\Model\Cart $cart,
        StoreManagerInterface $storeManagerInterface,
        \Magento\Framework\Stdlib\Cookie\PhpCookieManager $cookieManager,
        ProductRepository $productRepository,
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria,
        FilterBuilder $filterBuilder,
        FilterGroupBuilder $filterGroupBuilder,
        ProductModel $productModel
    )
    {
        $this->_cart = $cart;
        $this->_storeManager = $storeManagerInterface;
        $this->_productRepository = $productRepository;
        $this->_cookieManager = $cookieManager;
        $this->_searchCriteria = $searchCriteria;
        $this->_filterBuilder = $filterBuilder;
        $this->_filterGroupBuilder = $filterGroupBuilder;
        $this->_productModel = $productModel;
        parent::__construct($context, $ssoCache, $sessionHelper,$ssoLogger,$securityHelper);
    }

    public function cartAdd($modelId,$qty){

        $result = false;
        $message = null;

        $this->log(__METHOD__,"PRODUCT MODEL ID : ".$modelId);
        $this->log(__METHOD__,"PRODUCT QTY : ".$qty);

        try {

            $product = $this->getProductByModelId($modelId);

            if(!$product)
                throw new \Magento\Framework\Exception\LocalizedException(__('Product Model Not found.'));

            $this->log(__METHOD__,"PRODUCT SKU : ".$product->getSku());

            $this->_cart->addProduct($product, [
                'qty' => $qty
            ]);

            $quoteItems = $this->_cart->getQuote()->getItems();

            if(count($quoteItems) > 0){
                foreach ($quoteItems as $quoteItem) {

                    $productData = $this->_productModel->load($product->getId());
                    if($productData->getData('product_from')){
                        $quoteItem->setData('product_from',$productData->getData('product_from'));
                    }
                    if($productData->getData('delivery_type')){
                        $quoteItem->setData('delivery_type',$productData->getData('delivery_type'));
                    }
                    if($productData->getData('product_carrier')){
                        $quoteItem->setData('product_carrier',$productData->getData('product_carrier'));
                    }
                }
            }

            $this->_cart->save();
            /**
             * @todo remove wishlist observer \Magento\Wishlist\Observer\AddToCart
             */
            $this->_eventManager->dispatch(
                'checkout_cart_add_product_complete',
                ['product' => $product, 'request' => $this->_getRequest()->setParams([
                    'product' => $product->getId(),
                    'qty' => $qty
                ])]
            );

            if (!$this->_cart->getQuote()->getHasError()) {
                $result = true;
                $message = __('You added %1 to your shopping cart.',$product->getName());

                $customerHelper = $this->getSessionHelper()->getCustomerSession();

                if(!$customerHelper->isLoggedIn())
                    $this->_cookieManager->deleteCookie($this->getSSOGeneralConfig('dot_com_session_key'));
            }

        }catch (NoSuchEntityException $e){
            $this->log(__METHOD__,"Error Message : ".$e->getMessage(),true);
            $message = __('Product Not found.');
        }catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->log(__METHOD__,"Error Message : ".$e->getMessage(),true);
            $message = __($e->getMessage());
        } catch (\Exception $e) {
            $this->log(__METHOD__,"Error Message : ".$e->getMessage(),true);
            $message = __('We can\'t add this item to your shopping cart right now.');
        }

        $this->log(__METHOD__,"RESULT : ".$result);
        $this->log(__METHOD__,"Message : ".$message,true);

        return array_merge([
            'result' => $result,
            'message' => $message->render()
        ],$this->cartInfo());
    }

    public function getProductByModelId($modelId){

        $search = $this->_productRepository->getList($this->_searchCriteria->setFilterGroups(
            [$this->_filterGroupBuilder->addFilter(
                $this->_filterBuilder->setField('model_id')
                    ->setConditionType('eq')
                    ->setValue($modelId)->create()
            )->create()]
        )->setPageSize(1)->setCurrentPage(1));

        if($search->getTotalCount() > 0){
            foreach ($search->getItems() as $item){
                return $item;
            }
        }

        return false;
    }

    /**
     * @param $cacheSkip null | boolean
     * @return array
     */
    public function cartInfo($cacheSkip = false){


        $this->log(__METHOD__,"CACHE SKIP : ".$cacheSkip);

        $checkoutSession = $this->getSessionHelper()->getCheckoutSession();
        $cacheData = $this->_ssoCache->load(self::SSO_CART_CACHE_PREFIX_KEY.$checkoutSession->getSessionId());

        if($cacheSkip || !$cacheData){

            $this->log(__METHOD__,"NO CACHING");

            $data = [];


            $quote = $checkoutSession->getQuote();

            $data['totalQty'] = is_null($quote->getItemsQty())?0:round($quote->getItemsQty(),0);

            $this->_ssoCache->save(serialize($data),self::SSO_CART_CACHE_PREFIX_KEY.$checkoutSession->getSessionId());
            $cacheData = $data;

        }else{
            $this->log(__METHOD__,"CACHING");
            $cacheData = unserialize($cacheData);
        }

        $log[] = $cacheData;
        $this->log(__METHOD__,$log,true);

        return $cacheData;
    }

}