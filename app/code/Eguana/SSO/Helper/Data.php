<?php

/**
 * Created by Eguana.
 * User: jason
 * Date: 2018-08-27
 * Time: 오전 9:59
 */

namespace Eguana\SSO\Helper;

use Eguana\SSO\Model\Cache\Type;
use \Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{
    const CUSTOMER_SSO_ID = 'reference_id';

    const CUSTOMER_SSO_EMAIL = 'email';

    const CUSTOMER_SSO_FIRST_NAME = 'firstname';

    const CUSTOMER_SSO_LAST_NAME = 'lastname';

    const CUSTOMER_GROUP_DELETE_PENDING = 'Delete Pending';

    const CUSTOMER_GROUP_DELETE = 'Delete';

    const SSO_CUSTOMER_SIGN_UP_GROUP_ID = 1;
    
    const PHP_SESSION_COOKIE_KIY = 'PHPSESSID';

    const AUTH_TIME_INTERVAL = 300;

    const DOT_COM_ENCRYPT_FREFIX_DATA = 'lgepl_prd';

    protected $_sessionHelper;

    protected $_ssoCache;

    protected $_ssoLogger;

    protected $_securityHelper;

    protected $_logData;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        Type $ssoCache,
        SessionHelper $sessionHelper,
        \Eguana\SSO\Logger\SSO $ssoLogger,
        Security $securityHelper
    )
    {
        $this->_sessionHelper = $sessionHelper;
        $this->_ssoCache = $ssoCache;
        $this->_ssoLogger = $ssoLogger;
        $this->_securityHelper = $securityHelper;
        parent::__construct($context);
    }

    /** @return SessionHelper */
    public function getSessionHelper(){
        return $this->_sessionHelper;
    }

    /** @return Type */
    public function getSsoCache(){
        return $this->_ssoCache;
    }

    public function auth(){

        $result = false;


        $encryptData = $this->_getRequest()->getHeader('authKey');
        $this->log(__METHOD__,$encryptData,true);

        if($this->getSecurityConfig('enable')){
            
            $decrypt = $this->_securityHelper->decrypt(base64_decode($encryptData),$this->getSsoPrivateKey());
            $decryptTime = $this->_securityHelper->decryptToTimestamp($decrypt);
            $currentTime = $this->_securityHelper->getCurrentKoreaTimestamp();
            $authPlusTime = $decryptTime + self::AUTH_TIME_INTERVAL;
            $authMinusTime = $decryptTime - self::AUTH_TIME_INTERVAL;

            $this->log(__METHOD__,'DECRYPT : '.$decrypt);
            $this->log(__METHOD__,'DECRYPT TIME : '.$decryptTime);
            $this->log(__METHOD__,'PLUS TIME'.$authPlusTime);
            $this->log(__METHOD__,'MINUS TIME : '.$authMinusTime);
            $this->log(__METHOD__,'CURRENT TIME : '.$currentTime,true);

            if($authMinusTime < $currentTime && $currentTime < $authPlusTime)
                $result = true;

        }else{
            $result = true;
        }

        return $result;
    }

    public function getAuthKey(){

        $encryptData[] = self::DOT_COM_ENCRYPT_FREFIX_DATA;
        $encryptData[] = $this->_securityHelper->getCurrentKoreaDate();

        return $this->_securityHelper->encrypt(implode(',',$encryptData),$this->getDotComPublicKey());
    }

    public function getSSOConfig($path){
        return $this->scopeConfig->getValue('sso/'.$path);
    }

    public function getSSOGeneralConfig($path){
        return $this->getSSOConfig('general/'.$path);
    }

    protected function getSecurityConfig($path){
        return $this->getSSOConfig('security/'.$path);
    }

    public function getRequestConfig($path){
        return $this->getSSOConfig('request/'.$path);
    }

    public function getLinkConfig($path){
        return $this->getSSOConfig('link/'.$path);
    }

    public function getLinkReturnType(){
        return $this->getLinkConfig('return_type');
    }

    public function getLinkUri(){
        return $this->getLinkConfig('uri');
    }

    public function getSignInLink(){
        return $this->getLinkConfig('sign_in');
    }

    public function getChangeAccountLink(){
        return $this->getLinkConfig('change_account');
    }

    public function getChangePasswordLink(){
        return $this->getLinkConfig('change_password');
    }

    public function getSignUpLink(){
        return $this->getLinkConfig('sign_up');
    }

    public function getLogoutLink(){
        return $this->getLinkConfig('logout');
    }

    protected function getSsoPublicKey(){
        return $this->getKeyFile($this->getSecurityConfig('sso_pub'));
    }

    protected function getSsoPrivateKey(){
        return $this->getKeyFile($this->getSecurityConfig('sso_private'));
    }

    protected function getDotComPublicKey(){
        return $this->getKeyFile($this->getSecurityConfig('dot_com_pub'));
    }

    protected function getKeyFile($filename){
        return $this->_securityHelper->getKeyFile($filename);
    }

    public function log($key,$message = null ,$write = false){

        if($write && $this->getSSOGeneralConfig('log_enable')){
            $this->_logData[$key][] = $message;
            $this->_ssoLogger->info($key,$this->_logData[$key]);
            unset($this->_logData[$key]);
        }else{
            $this->_logData[$key][] = $message;
        }

    }

}