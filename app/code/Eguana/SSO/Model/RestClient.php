<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-09-05
 * Time: 오전 9:51
 */

namespace Eguana\SSO\Model;


class RestClient
{
    const REQUEST_TIME_OUT_SECOND = 3;

    protected $_restClient;

    protected $_uri;

    protected $_path;

    protected $_data;

    public function __construct(
        \Zend_Rest_Client $restClient
    )
    {
        $this->_restClient = $restClient;
    }

    /**
     * @throws \Zend_Http_Client_Exception
     * @throws \Exception
     * @return \Zend_Rest_Client
     */
    public function execute()
    {
        if($this->_validate()){

            $restClient = $this->getRestClient();
            $restClient->setUri($this->getUri());
            $restClient->getHttpClient()->setConfig(['timeout' => self::REQUEST_TIME_OUT_SECOND]);

            $restClient->restPost(
                $this->getPath(),
                $this->getData()
            );

            return $restClient;
        }
    }

    /**
     * @throws \Exception
     * @return boolean
     */
    protected function _validate(){

        if( ! $this->getUri()) {
            throw new \Exception(
                __('uri required.')
            );
        }

        if( ! $this->getPath()) {
            throw new \Exception(
                __('path required.')
            );
        }

        if( ! $this->getData()) {
            throw new \Exception(
                __('data required.')
            );
        }

        return true;
    }

    /** @return $this */
    public function clean(){

        $this->setUri(null);

        $this->setPath(null);

        $this->setData(null);

        return $this;
    }

    /**
     * @return \Zend_Rest_Client
     */
    public function getRestClient(){ return $this->_restClient; }

    public function setUri($uri){ $this->_uri = $uri; return $this; }
    public function getUri(){ return $this->_uri; }

    public function setPath($path){ $this->_path = $path; return $this; }
    public function getPath(){ return $this->_path; }

    public function setData($data){ $this->_data = $data; return $this; }
    public function getData(){ return $this->_data; }

}