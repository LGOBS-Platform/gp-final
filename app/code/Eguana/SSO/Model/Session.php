<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-08-28
 * Time: 오후 3:23
 */

namespace Eguana\SSO\Model;


use Eguana\SSO\Helper\Customer;
use Magento\Framework\Session\SessionManager;

class Session extends SessionManager
{

    public function getReferenceId(){
        return $this->getData(Customer::CUSTOMER_SSO_ID);
    }

    public function setReferenceId($referenceId){
        $this->storage->setData(Customer::CUSTOMER_SSO_ID,$referenceId);
        return $this;
    }

    public function getEmail(){
        return $this->getData(Customer::CUSTOMER_SSO_EMAIL);
    }

    public function setEmail($email){
        $this->storage->setData(Customer::CUSTOMER_SSO_EMAIL,$email);
        return $this;
    }

    public function getFirstname(){
        return $this->getData(Customer::CUSTOMER_SSO_FIRST_NAME);
    }

    public function setFirstname($firstname){
        $this->storage->setData(Customer::CUSTOMER_SSO_FIRST_NAME,$firstname);
        return $this;
    }

    public function getLastname(){
        return $this->getData(Customer::CUSTOMER_SSO_LAST_NAME);
    }

    public function setLastname($lastname){
        $this->storage->setData(Customer::CUSTOMER_SSO_LAST_NAME,$lastname);
        return $this;
    }

    public function ssoDataSet($ssoData){
        $this->setReferenceId(strval($ssoData->obsReferenceId));
        $this->setEmail(strval($ssoData->userInfo->emailAddr));
        $this->setFirstname(strval($ssoData->userInfo->firstName));
        $this->setLastname(strval($ssoData->userInfo->lastName));
    }

}