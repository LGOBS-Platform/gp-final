<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-08-28
 * Time: 오후 3:44
 */

namespace Eguana\SSO\Observer;

use Eguana\SSO\Model\Session;
use Magento\Customer\Model\Customer;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class CustomerSaveBefore implements ObserverInterface
{

    public function execute(Observer $observer)
    {
        /** @var Customer $customer*/
        $customer = $observer->getCustomer();

        if(!$customer->isObjectNew()){
            $referenceId = $customer->getData(\Eguana\SSO\Helper\Customer::CUSTOMER_SSO_ID);
            if($referenceId){
                $customer->setData(
                    \Eguana\SSO\Helper\Customer::CUSTOMER_SSO_EMAIL,
                    sprintf("%s@lg.com",$referenceId)
                );

                $customer->setData(
                    \Eguana\SSO\Helper\Customer::CUSTOMER_SSO_FIRST_NAME,
                    $referenceId
                );


                $customer->setData(
                    \Eguana\SSO\Helper\Customer::CUSTOMER_SSO_LAST_NAME,
                    $referenceId
                );
            }else{
            }
        }
    }
}