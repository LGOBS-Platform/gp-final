<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-08-28
 * Time: 오후 3:44
 */

namespace Eguana\SSO\Observer;

use Eguana\SSO\Helper\Cart;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class CartAddProductComplete implements ObserverInterface
{
    protected $_cartHelper;

    public function __construct(
        Cart $cartHelper
    )
    {
        $this->_cartHelper = $cartHelper;
    }

    public function execute(Observer $observer)
    {
        $this->_cartHelper->cartInfo(true);
    }

}