<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-08-28
 * Time: 오후 3:44
 */

namespace Eguana\SSO\Observer;

use Eguana\SSO\Helper\Cart;
use Eguana\SSO\Helper\Request;
use Eguana\SSO\Model\Session;
use Magento\Customer\Model\Customer;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class CustomerLoadAfter implements ObserverInterface
{
    protected $_ssoSession;

    protected $_requestHelper;

    protected $_secetionDataRefreshHelper;

    protected $_cartHelper;

    protected $_httpContext;

    public function __construct(
        Session $ssoSession,
        \Eguana\MiniCartRefresh\Helper\Data $secetionDataRefreshHelper,
        Request $requestHelper,
        \Magento\Framework\App\Http\Context $context,
        Cart $cartHelper
    )
    {
        $this->_ssoSession = $ssoSession;
        $this->_secetionDataRefreshHelper = $secetionDataRefreshHelper;
        $this->_requestHelper = $requestHelper;
        $this->_cartHelper = $cartHelper;
        $this->_httpContext = $context;
    }

    public function execute(Observer $observer)
    {
        $requestHelper = $this->_requestHelper;
        $requestHelper->log(__METHOD__,"START");
        /** @var Customer $customer*/
        $customer = $observer->getCustomer();
        $ssoSession = $this->getSsoSession();

        if($customer->getId()){

            if($ssoSession->getReferenceId() == ''){
                $requestHelper->log(__METHOD__,"SSO DATA EMPTY");
                $customerReferenceId = $customer->getData(\Eguana\SSO\Helper\Customer::CUSTOMER_SSO_ID);
                $ssoSession->setReferenceId($customerReferenceId);
            }

            $requestHelper->log(__METHOD__,"IS CUSTOMER");
            $requestHelper->log(__METHOD__,"REFERENCE ID : ".$ssoSession->getReferenceId());

            if(!$ssoSession->getEmail()){
                $requestHelper->log(__METHOD__,"SSO DATA EMPTY");
                $ssoData = $this->_requestHelper->getSsoUserData();

                if($ssoData !== false){
                    $requestHelper->log(__METHOD__,"SSO DATA TRUE");
                    $requestHelper->log(__METHOD__,$ssoData->__toString());
                    $ssoSession->ssoDataSet($ssoData);
                    $this->_cartHelper->cartInfo(true);
                    $this->_secetionDataRefreshHelper->setRefresh();
                }else{
                    $requestHelper->log(__METHOD__,"SSO DATA FAIL");
                }
            }

            if($ssoSession->getEmail())
                $customer->setData(\Eguana\SSO\Helper\Customer::CUSTOMER_SSO_EMAIL,$ssoSession->getEmail());
            if($ssoSession->getFirstname())
                $customer->setData(\Eguana\SSO\Helper\Customer::CUSTOMER_SSO_FIRST_NAME,$ssoSession->getFirstname());
            if($ssoSession->getLastname())
                $customer->setData(\Eguana\SSO\Helper\Customer::CUSTOMER_SSO_LAST_NAME,$ssoSession->getLastname());

            $this->_secetionDataRefreshHelper->setRefresh();
            $this->_httpContext->setValue('customer_logged_in',true,false);
        }else{
            $requestHelper->log(__METHOD__,"IS NOT CUSTOMER");
        }

        $requestHelper->log(__METHOD__,"END",true);
    }

    protected function getSsoSession(){
        return $this->_ssoSession;
    }

}