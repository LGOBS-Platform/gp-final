<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-08-28
 * Time: 오후 3:44
 */

namespace Eguana\SSO\Observer;

use Eguana\SSO\Helper\Cart;
use Eguana\SSO\Helper\Customer;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class CustomerLoginBefore implements ObserverInterface
{
    protected $_customerHelper;

    public function __construct(
        Customer $customerHelper
    )
    {
        $this->_customerHelper = $customerHelper;
    }

    public function execute(Observer $observer)
    {
        /** @var $customer \Magento\Customer\Model\Customer */
        $customer = $observer->getCustomer();

        if(isset($this->_customerHelper->loginRejectCustomerGroupInfo()[$customer->getGroupId()]))
            throw new \Exception(__('Customer does not exist.'));

    }

}