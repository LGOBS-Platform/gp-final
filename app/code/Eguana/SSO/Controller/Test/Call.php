<?php
/**
 * Created by Eguana.
 * User: jason
 * Date: 2018-08-27
 * Time: 오전 9:47
 */

namespace Eguana\SSO\Controller\Test;

use Eguana\SSO\Helper\Cart;
use Eguana\SSO\Helper\Customer;
use Eguana\SSO\Helper\Request;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultFactory;
use Eguana\SSO\Controller\AbstractAction;

class Call extends AbstractAction
{

    public function execute()
    {
        echo base64_encode($this->getCustomerHelper()->getAuthKey());
    }

}