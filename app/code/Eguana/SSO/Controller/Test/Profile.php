<?php
/**
 * Created by Eguana.
 * User: jason
 * Date: 2018-08-27
 * Time: 오전 9:47
 */

namespace Eguana\SSO\Controller\Test;


use Eguana\SSO\Controller\AbstractAction;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\Raw;
use Magento\Framework\Controller\ResultFactory;

class Profile extends AbstractAction
{
    public function _execute()
    {
        $this->getCustomerHelper()->log(__METHOD__);
        $requestData = $this->getRequestRawData();
        
        /**@var $resultPage Json */
        $sessionId = $requestData->lgSessionId;
        $referenceId = $requestData->obsReferenceId;

        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_RAW)
            ->setContents(sprintf('<?xml version="1.0" encoding="UTF-8"?>
<ns1:GetUserProfileResponse xmlns:ns1="http://www.lg.com">
 <obsReferenceId>%s</obsReferenceId>
 <userInfo>
<firstName>%s</firstName>
<lastName>%s</lastName>
<emailAddr>%s</emailAddr>
</userInfo>
</ns1:GetUserProfileResponse>
',$sessionId,$referenceId.'-data',$referenceId.'-data',$referenceId.'-data@lg.com'))
            ->setHeader('Accept','application/xml');

        return $resultPage;
    }

    protected function getRequestRawData(){
        return simplexml_load_string(file_get_contents("php://input"));
    }

}