<?php
/**
 * Created by Eguana.
 * User: jason
 * Date: 2018-08-27
 * Time: 오전 9:47
 */

namespace Eguana\SSO\Controller\Test;


use Eguana\SSO\Controller\AbstractAction;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\Raw;
use Magento\Framework\Controller\ResultFactory;

class Validate extends AbstractAction
{
    public function _execute()
    {
        $this->getCustomerHelper()->log(__METHOD__);

        $result = 'true';
        /**@var $resultPage Json */

        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_RAW)
            ->setContents(sprintf('<?xml version="1.0" encoding="UTF-8"?>
<ns1:ValidateSessionResponse xmlns:ns1="http://www.lg.com">
 <success>%s</success>
</ns1:ValidateSessionResponse>
',$result))->setHeader('Accept','application/xml');

        return $resultPage;
    }

}