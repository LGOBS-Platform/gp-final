<?php
/**
 * Created by Eguana.
 * User: jason
 * Date: 2018-08-27
 * Time: 오전 9:47
 */

namespace Eguana\SSO\Controller\Test;


use Eguana\SSO\Controller\AbstractAction;
use Eguana\SSO\Helper\Cart;
use Eguana\SSO\Helper\Customer;
use Magento\Framework\App\Action\Context;

class Login extends AbstractAction
{

    /**
     * @var \Magento\Framework\Stdlib\Cookie\PhpCookieManager
     */
    protected $_cookieMetadataManager;

    protected $_cookieMetadataFactory;

    public function __construct(
        Context $context,
        Cart $cartHelper,
        Customer $customerHelper,
        \Magento\Framework\Stdlib\Cookie\PhpCookieManager $phpCookieManager,
        \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory,
        \Magento\Framework\Webapi\Rest\Response\Renderer\Xml $xmlResult
    )
    {
        $this->_cookieMetadataManager = $phpCookieManager;
        $this->_cookieMetadataFactory = $cookieMetadataFactory;
        parent::__construct($context, $cartHelper, $customerHelper, $xmlResult);
    }

    public function execute()
    {
        $obsReferenceId = 'test'.date('YmdHis');
        $this->getCustomerHelper()->login($obsReferenceId);
        $requestData = $this->getRequest();

        $returnUrl = $requestData->getParam('returnUrl');

        $publicCookieMetadata = $this->_cookieMetadataFactory->createPublicCookieMetadata()
            ->setDuration(315360000)
            ->setPath('/')
            ->setSecure($this->getRequest()->isSecure())
            ->setHttpOnly(false);

        $this->_cookieMetadataManager->setPublicCookie($this->_customerHelper->getSSOGeneralConfig('dot_com_session_key'),$obsReferenceId,$publicCookieMetadata);

        return $this->_redirect($returnUrl);
    }

}