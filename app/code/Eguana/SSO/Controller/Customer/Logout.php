<?php
/**
 * Created by Eguana.
 * User: jason
 * Date: 2018-08-27
 * Time: 오전 9:47
 */

namespace Eguana\SSO\Controller\Customer;


use Eguana\SSO\Controller\AbstractAction;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\Raw;
use Magento\Framework\Controller\ResultFactory;

class LogOut extends AbstractAction
{
    protected function _execute()
    {
        $result = [
            'result' => false,
            'errorMsg' => ''
        ];
        $customerHelper = $this->getCustomerHelper();
        $requestData = $this->getRequestRawData();
        $sessionId = $requestData[$customerHelper::PHP_SESSION_COOKIE_KIY];

        try{
            $result['result'] = $customerHelper->logout($sessionId);
        }catch (\Exception $e){
            $result['errorMsg'] = $e->getMessage();
        }

        return $this->getResultPage($result);
    }

}