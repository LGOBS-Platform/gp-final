<?php
/**
 * Created by Eguana.
 * User: jason
 * Date: 2018-08-27
 * Time: 오전 9:50
 */

namespace Eguana\SSO\Controller\Customer;


use Eguana\SSO\Controller\AbstractAction;

class Validate extends AbstractAction
{
    protected function _execute()
    {
        $result = [];
        $customerHelper = $this->getCustomerHelper();
        $requestData = $this->getRequestRawData();
        $sessionId = $requestData[$customerHelper::PHP_SESSION_COOKIE_KIY];

        try{
            $result['result'] = $customerHelper->sessionValidate($sessionId);
        }catch (\Exception $e){
            $result['result'] = false;
        }

        return $this->getResultPage($result);
    }

}