<?php
/**
 * Created by Eguana.
 * User: jason
 * Date: 2018-08-27
 * Time: 오전 9:47
 */

namespace Eguana\SSO\Controller\Customer;


use Eguana\SSO\Controller\AbstractAction;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\Raw;
use Magento\Framework\Controller\ResultFactory;

class Login extends AbstractAction
{
    protected function _execute()
    {
        $customerHelper = $this->getCustomerHelper();
        $customerHelper->log(__METHOD__,'START',true);
        $requestData = $this->getRequestRawData();
        $customerHelper->log(__METHOD__,$requestData,true);
        $obsReferenceId = $requestData['obsReferenceId'];
        $customerHelper->log(__METHOD__,$obsReferenceId,true);
        $guestSessionId = isset($requestData[$customerHelper::PHP_SESSION_COOKIE_KIY])?$requestData[$customerHelper::PHP_SESSION_COOKIE_KIY] : null;
        $customerHelper->log(__METHOD__,$guestSessionId,true);

        $result = [
            $customerHelper::PHP_SESSION_COOKIE_KIY => '',
            'errorMsg' => ''
        ];
        $customerHelper->log(__METHOD__,$result,true);

        try{
            $session = $customerHelper->login($obsReferenceId,$guestSessionId);
            $customerHelper->log(__METHOD__,'success',true);
            $result[$customerHelper::PHP_SESSION_COOKIE_KIY] = $session->getSessionId();
            $customerHelper->log(__METHOD__,$result[$customerHelper::PHP_SESSION_COOKIE_KIY],true);
        }catch (\Exception $e){
            $result['errorMsg'] = $e->getMessage();
        }
        $customerHelper->log(__METHOD__,'end',true);

        return $this->getResultPage($result);
    }

}