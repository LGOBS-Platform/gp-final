<?php
/**
 * Created by Eguana.
 * User: jason
 * Date: 2018-08-27
 * Time: 오전 9:51
 */

namespace Eguana\SSO\Controller\Customer;


use Eguana\SSO\Controller\AbstractAction;

class Delete extends AbstractAction
{
    protected function _execute()
    {
        $customerHelper = $this->getCustomerHelper();
        $customerHelper->log(__METHOD__,__METHOD__);
        $requestData = $this->getRequestRawData();
        $obsReferenceId = $requestData['obsReferenceId'];
        $customerHelper->log(__METHOD__,'DELETE BEFORE',true);
        return $this->getResultPage($customerHelper->customerDelete($obsReferenceId));
    }

}