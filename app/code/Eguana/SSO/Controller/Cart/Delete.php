<?php
/**
 * Created by Kenneth.
 * Date: 2018-12-03
 * Time: 오전 11:52
 */

namespace Eguana\SSO\Controller\Cart;

use Magento\Framework\Controller\ResultFactory;

class Delete extends \Magento\Checkout\Controller\Cart\Delete
{

    public function execute()
    {
         if (!$this->_formKeyValidator->validate($this->getRequest())) {
            return $this->resultRedirectFactory->create()->setPath('*/*/');
        }

        $id = (int)$this->getRequest()->getParam('id');
        $quoteItem = null;

        if ($id) {
            try {
                    $quoteItem = $this->cart->getQuote()->getItemById($id);
                    if (!$quoteItem) {
                        $this->messageManager->addError(__("We can't find the quote item."));
                        return $this->resultFactory->create(ResultFactory::TYPE_REDIRECT)->setPath('checkout/cart');
                    }
                    $this->cart->removeItem($id)->save();
            } catch (\Exception $e) {
                    $this->messageManager->addError(__('We can\'t remove the item.'));
                    $this->_objectManager->get(\Psr\Log\LoggerInterface::class)->critical($e);
            }
        }

        $defaultUrl = $this->_objectManager->create(\Magento\Framework\UrlInterface::class)->getUrl('*/*');
        return $this->resultRedirectFactory->create()->setUrl($this->_redirect->getRedirectUrl($defaultUrl));

    }

}