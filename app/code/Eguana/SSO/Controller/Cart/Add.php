<?php
/**
 * Created by Eguana.
 * User: jason
 * Date: 2018-08-27
 * Time: 오전 9:51
 */

namespace Eguana\SSO\Controller\Cart;

use Eguana\SSO\Controller\AbstractAction;

class Add extends AbstractAction
{
    public function execute()
    {
        $modelId = $this->getRequest()->getParam('sku');
        
        $filter = new \Zend_Filter_LocalizedToNormalized(
            ['locale' => $this->_objectManager->get(
                \Magento\Framework\Locale\ResolverInterface::class
            )->getLocale()]
        );
        $qty = $filter->filter(1);
        
        return $this->getResultPage($this->getCartHelper()->cartAdd($modelId,$qty));
    }

}