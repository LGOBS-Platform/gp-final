<?php
/**
 * Created by Eguana.
 * User: jason
 * Date: 2018-08-27
 * Time: 오전 9:51
 */

namespace Eguana\SSO\Controller\Cart;

use Eguana\SSO\Controller\AbstractAction;

class Info extends AbstractAction
{
    public function execute()
    {
        return $this->getResultPage($this->getCartHelper()->cartInfo());
    }

}