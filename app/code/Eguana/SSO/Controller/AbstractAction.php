<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-08-27
 * Time: 오전 10:03
 */

namespace Eguana\SSO\Controller;


use Eguana\SSO\Helper\Cart;
use Eguana\SSO\Helper\Customer;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultFactory;

abstract class AbstractAction extends Action
{
    protected $_cartHelper;

    protected $_customerHelper;

    protected $_xmlResult;

    public function __construct(
        Context $context,
        Cart $cartHelper,
        Customer $customerHelper,
        \Magento\Framework\Webapi\Rest\Response\Renderer\Xml $xmlResult
    )
    {
        $this->_cartHelper = $cartHelper;
        $this->_customerHelper = $customerHelper;
        $this->_xmlResult = $xmlResult;
        parent::__construct($context);
    }

    public function execute()
    {
        $result['result'] = false;
        $result['errorMsg'] = __('Authentication failed.');

        if($this->_authCheck()){
            return $this->_execute();
        }

        return $this->getResultPage($result);
    }

    protected function _execute(){

    }

    protected function getRequestRawData(){
        return json_decode(file_get_contents("php://input"),true);
    }

    /** @return boolean */
    protected function _authCheck(){

        $helper = $this->_customerHelper;
        return $helper->auth();
    }

    /**
     * @param $data array
     * @return Json
     */
    protected function getResultPage($data){

        /**@var $resultPage Json */

//        $xml = $this->_xmlResult;
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_JSON);
//            ->setContents($xml->render($data))
//            ->setHeader('Accept','application/xml');
        $resultPage->setData($data);

        return $resultPage;
    }

    /** @return Customer */
    protected function getCustomerHelper(){
        return $this->_customerHelper;
    }

    /** @return Cart*/
    protected function getCartHelper(){
        return $this->_cartHelper;
    }

}