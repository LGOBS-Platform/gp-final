<?php
namespace Eguana\SSO\Controller\Account;

use Eguana\SSO\Helper\Request;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ObjectManager;

class Logout extends \Magento\Customer\Controller\Account\Logout
{

    protected $_requestHelper;

    protected $_refreshHelper;

    protected $_cookieManager;

    /**
     * @param Context $context
     * @param Session $customerSession
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        Request $requestHelper,
        \Eguana\MiniCartRefresh\Helper\Data $data
    ) {
        $this->_requestHelper = $requestHelper;
        $this->_refreshHelper = $data;
        parent::__construct($context,$customerSession);
    }

    /**
     * Customer logout action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $this->_refreshHelper->setRefresh(true);
        $this->_requestHelper->logoutRequest();


        return parent::execute();
    }
}
