<?php
/**
 * Created by Kenneth.
 * Date: 2018-12-06
 * Time: 오후 2:49
 */

namespace Eguana\SSO\Controller\Account;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Customer\Model\Session;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Customer\Model\Account\Redirect as AccountRedirect;
use Magento\Store\Model\StoreManagerInterface;

class LogoutSuccess extends \Magento\Customer\Controller\Account\LogoutSuccess
{

    protected $_customerSession;

    protected $_cookieManager;

    protected $_storeManager;

    /**
     * @var AccountRedirect
     */
    protected $accountRedirect;

    /**
     * @var \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory
     */
    private $cookieMetadataFactory;

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        Session $customerSession,
        CookieManagerInterface $cookieManager,
        StoreManagerInterface $storeManager,
        \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory
        )
    {
        $this->_cookieManager = $cookieManager;
        $this->_customerSession = $customerSession;
        $this->_storeManager = $storeManager;
        $this->cookieMetadataFactory = $cookieMetadataFactory;
        parent::__construct($context, $resultPageFactory);
    }


    /**
     * Logout success page
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $cookieMetadata = $this->cookieMetadataFactory->createPublicCookieMetadata()
            ->setDomain(".lg.com")
            ->setDuration(86400)
            ->setPath('/')
            ->setHttpOnly(false);
        $this->_cookieManager->setPublicCookie('LG4_LOGIN_PL', "N", $cookieMetadata);

        $redirectUrl = $this->_redirect->getRefererUrl();
        $resultRedirect = $this->resultRedirectFactory->create();
        // URL is checked to be internal in $this->_redirect->success()


        if(strpos($redirectUrl, 'customer/') !== false ||
            strpos($redirectUrl, 'sales/order/history') !== false||
            strpos($redirectUrl, 'wishlist') !== false){
            $redirectUrl = $this->_storeManager->getStore()->getBaseUrl();
        }

        $resultRedirect->setUrl($this->_redirect->success($redirectUrl));

        return $resultRedirect;

    }


}