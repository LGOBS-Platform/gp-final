<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Eguana\SSO\Controller\Account;

use Eguana\SSO\Helper\Data;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Action;

class Edit extends Action
{
    /**
     * @var Session
     */
    protected $session;

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    protected $_ssoDataHelper;

    /**
     * @param Context $context
     * @param Session $customerSession
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        Data $ssoDataHelper,
        PageFactory $resultPageFactory
    ) {
        $this->session = $customerSession;
        $this->resultPageFactory = $resultPageFactory;
        $this->_ssoDataHelper = $ssoDataHelper;
        parent::__construct($context);
    }

    /**
     * Customer login form page
     *
     * @return \Magento\Framework\Controller\Result\Redirect|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        if ($this->session->isLoggedIn()) {

            $ssoDataHelper = $this->_ssoDataHelper;
            $params = [
                'returnType' => $ssoDataHelper->getLinkReturnType(),
                'returnUrl' => $this->_redirect->getRefererUrl(),
            ];

            $isPassword = $this->getRequest()->getParam('changepass');

            if($isPassword){
                $redirectPath = $ssoDataHelper->getChangePasswordLink();
            }else{
                $redirectPath = $ssoDataHelper->getChangeAccountLink();
            }

            return $this->_redirect(
                $ssoDataHelper->getLinkUri().$redirectPath.'?'.http_build_query($params)
            );

        }else{
            $resultRedirect->setPath('*/*/');
        }

        return $resultRedirect;
    }
}
