<?php
/**
 * Created by Kenneth.
 * Date: 2018-10-05
 * Time: 오전 10:49
 */
namespace Eguana\SSO\Controller\Checkout;


class Index extends \Magento\Checkout\Controller\Index\Index
{
    /**
     *
     *
     * @return \Magento\Framework\Controller\Result\Redirect|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        if ($this->_customerSession->isLoggedIn()) {
            return parent::execute();
        }else{
            $ssoDataHelper = $this->_objectManager->get(\Eguana\SSO\Helper\Data::class);

            $params = [
                'returnType' => $ssoDataHelper->getLinkReturnType(),
                'returnUrl' => $this->_redirect->getRefererUrl(),
            ];

            return $resultRedirect->setPath(
                $ssoDataHelper->getLinkUri().$ssoDataHelper->getSignInLink().'?'.http_build_query($params)
            );
        }

    }

}