<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Eguana\SSO\Block\Account;

use Magento\Customer\Model\Context;
use Magento\Customer\Block\Account\SortLinkInterface;
use Magento\Customer\Model\Session;

/**
 * Customer authorization link
 *
 * @api
 * @SuppressWarnings(PHPMD.DepthOfInheritance)
 * @since 100.0.2
 */
class AuthorizationLink extends \Magento\Customer\Block\Account\AuthorizationLink
{
    protected $_customerSession;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\App\Http\Context $httpContext
     * @param \Magento\Customer\Model\Url $customerUrl
     * @param \Magento\Framework\Data\Helper\PostHelper $postDataHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\App\Http\Context $httpContext,
        \Magento\Customer\Model\Url $customerUrl,
        \Magento\Framework\Data\Helper\PostHelper $postDataHelper,
        Session $session,
        array $data = []
    ) {
        $this->_customerSession = $session;
        parent::__construct($context,$httpContext,$customerUrl,$postDataHelper,$data);
    }

    public function getIsLoginUrl(){
        return $this->getUrl('sso/account/islogin');
    }

    public function getLoginUrl(){
        return $this->_customerUrl->getLoginUrl();
    }

    public function getLogoutUrl(){
        return $this->_customerUrl->getLogoutUrl();
    }
    
    /**
     * Is logged in
     *
     * @return bool
     */
    public function isLoggedIn()
    {
        return $this->_customerSession->isLoggedIn();
    }
}
