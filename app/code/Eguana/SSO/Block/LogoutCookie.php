<?php
/**
 * Created by Kenneth.
 * Date: 2018-12-06
 * Time: 오후 3:56
 */

namespace Eguana\SSO\Block;

use Magento\Framework\View\Element\Template;
use Magento\Customer\Model\Session;
use Magento\Framework\Stdlib\CookieManagerInterface;

class LogoutCookie extends \Magento\Framework\View\Element\Template
{

    protected $_customerSession;

    protected $_cookieManager;
    /**
     * @var \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory
     */
    private $cookieMetadataFactory;

    public function __construct(
        Template\Context $context,
        Session $customerSession,
        CookieManagerInterface $cookieManager,
        \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory,
        array $data = [])
    {

        $this->_cookieManager = $cookieManager;
        $this->_customerSession = $customerSession;
        $this->cookieMetadataFactory = $cookieMetadataFactory;
        parent::__construct($context, $data);
    }

    public function logoutCookie(){
        if(!$this->_customerSession->isLoggedIn()){
            $cookieMetadata = $this->cookieMetadataFactory->createPublicCookieMetadata()
                ->setDomain(".lg.com")
                ->setDuration(86400)
                ->setPath('/')
                ->setHttpOnly(false);
            $this->_cookieManager->setPublicCookie('LG4_LOGIN_PL', "N", $cookieMetadata);

        }
    }
}