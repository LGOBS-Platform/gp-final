/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
var config = {
    "map": {
        "*": {
            'swiper': "Eguana_Theme/js/swiper",
            'bxslider': "Eguana_Theme/js/jquery.bxslider",
            'rwdImageMap': "Eguana_Theme/js/jquery.rwdImageMaps",
            'fotorama/fotorama': "Eguana_Theme/fotorama/fotorama",
            'mage/gallery/gallery': "Eguana_Theme/mage/gallery/gallery"
        }

    }
};