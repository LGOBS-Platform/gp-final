<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Eguana\Theme\Block\Html;

/**
 * Html page top menu block
 *
 * @api
 * @since 100.0.2
 */
class Topmenu extends \Magento\Theme\Block\Html\Topmenu
{
    /**
     * Add sub menu HTML code for current menu item
     *
     * @param \Magento\Framework\Data\Tree\Node $child
     * @param string $childLevel
     * @param string $childrenWrapClass
     * @param int $limit
     * @return string HTML code
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _addSubMenu($child, $childLevel, $childrenWrapClass, $limit)
    {
        $html = '';
        if (!$child->hasChildren()) {
            return $html;
        }

        $colStops = null;
        if ($childLevel == 0 && $limit) {
            $colStops = $this->_columnBrake($child->getChildren(), $limit);
        }

        if($childLevel == 0) {
            $html .= '<div class="'. $childrenWrapClass . '"><div class="sub-contents">';
            $html .= '<strong>'.$child->getName().'</strong>';
            if($this->getLayout()->createBlock('Magento\Cms\Block\Block')->setBlockId($child->getId())->toHtml()){
                $html .= $this->getLayout()->createBlock('Magento\Cms\Block\Block')->setBlockId($child->getId())->toHtml();
            }

            $html.= '<div class="menu-div">';
        }
        $html .= '<ul class="level' . $childLevel . ' ' . $childrenWrapClass. '">';
        $html .= $this->_getHtml($child, $childrenWrapClass, $limit, $colStops);
        $html .= '</ul>';

        if($childLevel == 0) {
            $html .= '</div>';
            $html .= '</div>';
            if($this->getLayout()->createBlock('Magento\Cms\Block\Block')->setBlockId('category-bottom-banner')->toHtml()){
                $html .= '<div class="all-banner">';
                $html .= $this->getLayout()->createBlock('Magento\Cms\Block\Block')->setBlockId('category-bottom-banner')->toHtml();
                $html .= '</div>';
            }
            $html .= '</div>';
        }
        return $html;
    }
}
