<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016-10-13
 * Time: 오전 11:11
 */

namespace Eguana\Theme\Block\Html;


class Pager extends \Magento\Theme\Block\Html\Pager
{
    /**
     * The list of available pager limits
     *
     * @var array
     */
    protected $_availableLimit = [5 => 5, 10 => 10, 15 => 15];
}