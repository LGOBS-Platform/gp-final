<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

// @codingStandardsIgnoreFile

namespace Eguana\Theme\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Order item render block
 *
 * @api
 * @since 100.0.2
 */
class Product extends AbstractHelper
{
    protected $_storeManager;

    public function __construct(
        StoreManagerInterface $storeManager,
        \Magento\Framework\App\Helper\Context $context
    )
    {
        $this->_storeManager = $storeManager;
        parent::__construct($context);
    }

    /**
     * @param $product \Magento\Catalog\Model\Product
    */
    public function getProductFlagArray($product){

        $productFlagValue = $product->load($product->getId())->getAttributeText('product_flag');

        if(is_array($productFlagValue))
            return $productFlagValue;

        if(empty($productFlagValue))
            return [];

        return [$productFlagValue];
    }
}
