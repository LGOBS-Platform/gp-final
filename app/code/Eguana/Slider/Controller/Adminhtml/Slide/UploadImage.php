<?php
/**
* Copyright 2016 Eguana. All rights reserved.
* See LICENSE.txt for license details.
*/

namespace Eguana\Slider\Controller\Adminhtml\Slide;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Eguana\Slider\Model\Slide\ImageFileUploader;

/**
 * Class UploadImage
 * @package Eguana\Slider\Controller\Adminhtml\Slide
 */
class UploadImage extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Eguana_Slider::slides';

    /**
     * @var ImageFileUploader
     */
    private $imageFileUploader;

    /**
     * @param Context $context
     * @param ImageFileUploader $imageFileUploader
     */
    public function __construct(
        Context $context,
        ImageFileUploader $imageFileUploader
    ) {
        parent::__construct($context);
        $this->imageFileUploader = $imageFileUploader;
    }

    /**
     * Image upload action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $result = $this->imageFileUploader->saveImageToMediaFolder('img_file');
        return $this->resultFactory->create(ResultFactory::TYPE_JSON)->setData($result);
    }
}
