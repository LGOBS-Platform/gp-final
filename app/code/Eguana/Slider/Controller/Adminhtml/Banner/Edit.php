<?php
/**
* Copyright 2016 Eguana. All rights reserved.
* See LICENSE.txt for license details.
*/

namespace Eguana\Slider\Controller\Adminhtml\Banner;

use Magento\Framework\Exception\NoSuchEntityException;
use Eguana\Slider\Api\BannerRepositoryInterface;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class Edit
 * @package Eguana\Slider\Controller\Adminhtml\Banner
 */
class Edit extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Eguana_Slider::banners';

    /**
     * @var BannerRepositoryInterface
     */
    private $bannerRepository;

    /**
     * @var PageFactory
     */
    private $resultPageFactory;

    protected $_layout;
    /**
     * @param Context $context
     * @param BannerRepositoryInterface $bannerRepository
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        BannerRepositoryInterface $bannerRepository,
        \Magento\Framework\View\LayoutFactory $layoutFactory,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->_layout = $layoutFactory;
        $this->bannerRepository = $bannerRepository;
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Edit Banner
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {

        /**@var $categoryTreeBlock \Magento\Catalog\Block\Adminhtml\Category\Checkboxes\Tree*/
        $categoryTreeBlock = $this->_layout->create()->createBlock(
            \Magento\Catalog\Block\Adminhtml\Category\Checkboxes\Tree::class,
            null,
            ['data' => ['js_form_object' => 'categoryIds']]
        );

        $id = (int)$this->getRequest()->getParam('id');
        if ($id) {
            try {
                $this->bannerRepository->get($id);
            } catch (NoSuchEntityException $exception) {
                $this->messageManager->addExceptionMessage(
                    $exception,
                    __('Something went wrong while editing the banner')
                );
                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath('*/*/index');
                return $resultRedirect;
            }
        }

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage
            ->setActiveMenu('Eguana_Slider::banners')
            ->getConfig()->getTitle()->prepend(
                $id ? __('Edit Banner') : __('New Banner')
            );

        return $resultPage;
    }
}
