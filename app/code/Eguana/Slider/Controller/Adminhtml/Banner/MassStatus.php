<?php
/**
* Copyright 2016 Eguana. All rights reserved.
* See LICENSE.txt for license details.
*/

namespace Eguana\Slider\Controller\Adminhtml\Banner;

/**
 * Class MassStatus
 * @package Eguana\Slider\Controller\Adminhtml\Banner
 */
class MassStatus extends AbstractMassAction
{
    /**
     * {@inheritdoc}
     */
    protected function massAction($collection)
    {
        $status = (int)$this->getRequest()->getParam('status');
        $count = 0;
        foreach ($collection->getItems() as $item) {
            $bannerDataObject = $this->bannerRepository->get($item->getId());
            $bannerDataObject->setStatus($status);
            $this->bannerRepository->save($bannerDataObject);
            $count++;
        }
        $this->messageManager->addSuccessMessage(__('A total of %1 record(s) have been updated', $count));
    }
}
