<?php
/**
 * Copyright 2016 aheadWorks. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Eguana\Slider\Ui\DataProvider;

use Magento\Framework\View\Element\UiComponent\DataProvider\DataProviderInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Eguana\Slider\Model\Slide\ImageFileUploader;
use Eguana\Slider\Model\Source\ImageType;
use Magento\Framework\App\RequestInterface;
use Eguana\Slider\Model\ResourceModel\Slide\Grid\CollectionFactory;

/**
 * Class SlideDataProvider
 * @package Eguana\Slider\Ui\DataProvider
 */
class SlideDataProvider extends AbstractDataProvider implements DataProviderInterface
{
    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var ImageFileUploader
     */
    private $imageFileUploader;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param RequestInterface $request
     * @param DataPersistorInterface $dataPersistor
     * @param ImageFileUploader $imageFileUploader
     * @param array $meta
     * @param array $data
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        RequestInterface $request,
        DataPersistorInterface $dataPersistor,
        ImageFileUploader $imageFileUploader,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
        $this->request = $request;
        $this->dataPersistor = $dataPersistor;
        $this->imageFileUploader = $imageFileUploader;
    }

    /**
     * {@inheritdoc}
     */
    public function getData()
    {
        $data = [];
        $dataFromForm = $this->dataPersistor->get('eguana_slide');
        if (!empty($dataFromForm)) {
            $data[$dataFromForm['id']] = $dataFromForm;
            $this->dataPersistor->clear('eguana_slide');
        } else {
            $id = $this->request->getParam($this->getRequestFieldName());
            /** @var \Eguana\Slider\Model\Slide $slide */
            foreach ($this->getCollection()->getItems() as $slide) {
                if ($id == $slide->getId()) {
                    $data[$id] = $this->prepareFormData($slide->getData());
                }
            }
        }
        return $data;
    }

    /**
     * Prepare form data
     *
     * @param array $itemData
     * @return array
     */
    private function prepareFormData(array $itemData)
    {
        if ($itemData['img_type'] == ImageType::TYPE_FILE) {
            $itemData['img_file'] = [
                0 => [
                    'file' => $itemData['img_file'],
                    'url' => $this->imageFileUploader->getMediaUrl($itemData['img_file'])
                ]
            ];
            if($itemData['mob_img_file']){
                $itemData['mob_img_file'] = [
                    0 => [
                        'file' => $itemData['mob_img_file'],
                        'url' => $this->imageFileUploader->getMediaUrl($itemData['mob_img_file'])
                    ]
                ];
            }
        }
        return $itemData;
    }
}
