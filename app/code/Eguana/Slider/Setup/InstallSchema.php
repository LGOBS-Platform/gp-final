<?php
/**
* Copyright 2016 Eguana. All rights reserved.
* See LICENSE.txt for license details.
*/


namespace Eguana\Slider\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        $this->addBannerTable($installer);
        $this->addSlideTable($installer);
        $this->addStaticTable($installer);
        $this->addSlideBannerTable($installer);
        $this->addStoreRelatedTable($installer);
        $this->addCustomerGroupRelatedTable($installer);

        $installer->endSetup();
    }


    /**
     * Add related table store and customer group for the slide table
     *
     * @param SchemaSetupInterface $setup
     * @return $this
     */
    private function addBannerTable(SchemaSetupInterface $installer){

        /**
         * Create table 'eguana_banner'
         */
        $bannerTable = $installer->getConnection()->newTable($installer->getTable('eguana_banner'))
            ->addColumn(
                'id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )
            ->addColumn(
                'name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'Name'
            )
            ->addColumn(
                'status',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                1,
                ['nullable' => false],
                'Status'
            )
            ->addColumn(
                'page_type',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                ['nullable' => false],
                'Type'
            )
            ->addColumn(
                'action_type',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                ['nullable' => false],
                'Action Type'
            )
            ->addColumn(
                'position',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'Position'
            )
            ->addColumn(
                'product_condition',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'Product Condition'
            )
            ->addColumn(
                'category_ids',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'Category IDs'
            )
            ->addColumn(
                'animation_effect',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                ['nullable' => false],
                'Animation Effect'
            )
            ->addColumn(
                'pause_time_between_transitions',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'Pause Time Between Transitions'
            )
            ->addColumn(
                'slide_transition_speed',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'Slide Transition Speed'
            )
            ->addColumn(
                'slide_delay',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'Slide Delay'
            )
            ->addColumn(
                'is_stop_animation_mouse_on_banner',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'Stop Animation While Mouse is on the Banner'
            )
            ->addColumn(
                'content',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                '2M',
                ['nullable' => true],
                'Description'
            )
            ->addColumn(
                'action_category_ids',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                ['nullable' => true],
                'Action Category IDs'
            );
        $installer->getConnection()->createTable($bannerTable);

    }

    /**
     * Add related table store and customer group for the slide table
     *
     * @param SchemaSetupInterface $installer
     * @return $this
     */
    private function addSlideTable(SchemaSetupInterface $installer){
        /**
         * Create table 'eguana_slide'
         */
        $slideTable = $installer->getConnection()->newTable($installer->getTable('eguana_slide'))
            ->addColumn(
                'id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )
            ->addColumn(
                'name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'Name'
            )
            ->addColumn(
                'description',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                '2M',
                ['nullable' => true],
                'Description'
            )
            ->addColumn(
                'status',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                1,
                ['nullable' => false],
                'Status'
            )
            ->addColumn(
                'display_from',
                \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                null,
                [],
                'Display From'
            )
            ->addColumn(
                'display_to',
                \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                null,
                [],
                'Display To'
            )
            ->addColumn(
                'img_type',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                ['nullable' => false],
                'Image Type'
            )
            ->addColumn(
                'img_file',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'Image File'
            )
            ->addColumn(
                'img_url',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'Image to URL'
            )
            ->addColumn(
                'img_title',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'Image Title'
            )
            ->addColumn(
                'img_alt',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'Image Alt'
            )
            ->addColumn(
                'url',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'URL'
            )
            ->addColumn(
                'is_open_url_in_new_window',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                ['nullable' => false],
                'Open URL in New Window'
            )
            ->addColumn(
                'is_add_nofollow_to_url',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                ['nullable' => false],
                'Add No follow to URL'
            );
        $installer->getConnection()->createTable($slideTable);
    }

    /**
     * Add related table store and customer group for the slide table
     *
     * @param SchemaSetupInterface $installer
     * @return $this
     */
    private function addStaticTable(SchemaSetupInterface $installer){

        /**
         * Create table 'eguana_statistic'
         */
        $statisticTable = $installer->getConnection()->newTable($installer->getTable('eguana_statistic'))
            ->addColumn(
                'id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )
            ->addColumn(
                'slide_banner_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false],
                'Slide Banner ID'
            )
            ->addColumn(
                'view_count',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false, 'default' => 0],
                'View Count'
            )
            ->addColumn(
                'click_count',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false, 'default' => 0],
                'Click Count'
            )->addForeignKey(
                $installer->getFkName('eguana_statistic', 'slide_banner_id', 'eguana_slide_banner', 'id'),
                'slide_banner_id',
                $installer->getTable('eguana_slide_banner'),
                'id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            );
        $installer->getConnection()->createTable($statisticTable);
    }

    /**
     * Add related table store and customer group for the slide table
     *
     * @param SchemaSetupInterface $installer
     * @return $this
     */
    private function addSlideBannerTable(SchemaSetupInterface $installer){

        /**
         * Create table 'eguana_slide_banner'
         */
        $slideBannerTable = $installer->getConnection()->newTable($installer->getTable('eguana_slide_banner'))
            ->addColumn(
                'id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )->addColumn(
                'slide_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false],
                'Slide ID'
            )->addColumn(
                'banner_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false],
                'Banner ID'
            )->addColumn(
                'position',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                ['nullable' => false, 'default' => 0],
                'Slide Position'
            )->addForeignKey(
                $installer->getFkName('eguana_slide_banner', 'slide_id', 'eguana_slide', 'id'),
                'slide_id',
                $installer->getTable('eguana_slide'),
                'id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            )->addForeignKey(
                $installer->getFkName('eguana_slide_banner', 'banner_id', 'eguana_banner', 'id'),
                'banner_id',
                $installer->getTable('eguana_banner'),
                'id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            )->setComment(
                'Slide To Banner Linkage Table'
            );
        $installer->getConnection()->createTable($slideBannerTable);

    }

    /**
     * Add related table store and customer group for the slide table
     *
     * @param SchemaSetupInterface $installer
     * @return $this
     */
    private function addStoreRelatedTable(SchemaSetupInterface $installer){

        /**
         * Create table 'eguana_slide_store'
         */
        $storeTable = $installer->getConnection()->newTable(
            $installer->getTable('eguana_slide_store')
        )->addColumn(
            'slide_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Slide ID'
        )->addColumn(
            'store_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Store ID'
        )->addIndex(
            $installer->getIdxName('eguana_slide_store', ['slide_id']),
            ['slide_id']
        )->addIndex(
            $installer->getIdxName('eguana_slide_store', ['slide_id']),
            ['store_id']
        )->addForeignKey(
            $installer->getFkName('eguana_slide_store', 'slide_id', 'eguana_slide', 'id'),
            'slide_id',
            $installer->getTable('eguana_slide'),
            'id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->addForeignKey(
            $installer->getFkName('eguana_slide_store', 'store_id', 'store', 'store_id'),
            'store_id',
            $installer->getTable('store'),
            'store_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->setComment(
            'Eguana Slide To Store Relation Table'
        );
        $installer->getConnection()->createTable($storeTable);
    }

    /**
     * Add related table store and customer group for the slide table
     *
     * @param SchemaSetupInterface $installer
     * @return $this
     */
    private function addCustomerGroupRelatedTable(SchemaSetupInterface $installer)
    {
        /**
         * Create table 'eguana_slide_customer_group'
         */
        $customerGroupTable = $installer->getConnection()->newTable(
            $installer->getTable('eguana_slide_customer_group')
        )->addColumn(
            'slide_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Rule ID'
        )->addColumn(
            'customer_group_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Customer Group ID'
        )->addIndex(
            $installer->getIdxName('eguana_slide_customer_group', ['slide_id']),
            ['slide_id']
        )->addIndex(
            $installer->getIdxName('eguana_slide_customer_group', ['customer_group_id']),
            ['customer_group_id']
        )->addForeignKey(
            $installer->getFkName('eguana_slide_customer_group', 'slide_id', 'eguana_slide', 'id'),
            'slide_id',
            $installer->getTable('eguana_slide'),
            'id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->setComment(
            'Eguana Slide To Customer Group Relation Table'
        );
        $installer->getConnection()->createTable($customerGroupTable);

        return $this;
    }

}
