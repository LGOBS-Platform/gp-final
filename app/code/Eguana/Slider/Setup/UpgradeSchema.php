<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-08-27
 * Time: 오전 10:33
 */

namespace Eguana\Slider\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();


        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $this->mobileImageColumn($setup);
        }

        $setup->endSetup();
    }

    private function mobileImageColumn(SchemaSetupInterface $setup){

        $connection = $setup->getConnection();

        $connection->addColumn(
            $connection->getTableName('eguana_slide'),
            'mob_img_file',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => false,
                'comment' => 'Mobile Image File'
            ]
        );

        $connection->addColumn(
            $connection->getTableName('eguana_slide'),
            'mob_img_url',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => false,
                'comment' => 'Mobile Image to URL'
            ]
        );
    }
}