<?php
/**
* Copyright 2016 Eguana. All rights reserved.
* See LICENSE.txt for license details.
*/

namespace Eguana\Slider\Api\Data;

/**
 * Block interface
 * @api
 */
interface BlockInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const BANNER = 'banner';
    const SLIDES = 'slides';
    /**#@-*/

    /**
     * Get banner
     *
     * @return \Eguana\Slider\Api\Data\BannerInterface|null
     */
    public function getBanner();

    /**
     * Set banner
     *
     * @param \Eguana\Slider\Api\Data\BannerInterface $banner
     * @return BlockInterface
     */
    public function setBanner($banner);

    /**
     * Get slides
     *
     * @return \Eguana\Slider\Api\Data\SlideInterface[]|null
     */
    public function getSlides();

    /**
     * Set slides
     *
     * @param \Eguana\Slider\Api\Data\SlideInterface[] $slides
     * @return BlockInterface
     */
    public function setSlides($slides);

    /**
     * Retrieve existing extension attributes object or create a new one
     *
     * @return \Eguana\Slider\Api\Data\BlockExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object
     *
     * @param \Eguana\Slider\Api\Data\BlockExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(\Eguana\Slider\Api\Data\BlockExtensionInterface $extensionAttributes);
}
