<?php
/**
* Copyright 2016 Eguana. All rights reserved.
* See LICENSE.txt for license details.
*/

namespace Eguana\Slider\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface for statistic search results.
 * @api
 */
interface StatisticSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get banners list.
     *
     * @return \Eguana\Slider\Api\Data\StatisticInterface[]
     */
    public function getItems();

    /**
     * Set banners list.
     *
     * @param \Eguana\Slider\Api\Data\StatisticInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
