<?php
/**
* Copyright 2016 Eguana. All rights reserved.
* See LICENSE.txt for license details.
*/

namespace Eguana\Slider\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface for Slider block search results
 *
 * @api
 */
interface BlockSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get blocks list
     *
     * @return \Eguana\Slider\Api\Data\BlockInterface[]
     */
    public function getItems();

    /**
     * Set blocks list
     *
     * @param \Eguana\Slider\Api\Data\BlockInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
