<?php
/**
* Copyright 2016 Eguana. All rights reserved.
* See LICENSE.txt for license details.
*/

namespace Eguana\Slider\Api\Data;

/**
 * Banner interface
 * @api
 */
interface BannerInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{
    /**#@+
     * Constants defined for keys of the data array.
     * Identical to the name of the getter in snake case
     */
    const ID = 'id';
    const NAME = 'name';
    const STATUS = 'status';
    const PAGE_TYPE = 'page_type';
    const POSITION = 'position';
    const PRODUCT_CONDITION = 'product_condition';
    const CATEGORY_IDS = 'category_ids';
    const ANIMATION_EFFECT = 'animation_effect';
    const PAUSE_TIME_BETWEEN_TRANSITIONS = 'pause_time_between_transitions';
    const SLIDE_TRANSITION_SPEED = 'slide_transition_speed';
    const IS_STOP_ANIMATION_MOUSE_ON_BANNER = 'is_stop_animation_mouse_on_banner';
    const SLIDE_IDS = 'slide_ids';
    const SLIDE_POSITION = 'slide_position';
    const ACTION_TYPE = 'action_type';
    const CONTENT = 'content';
    const ACTION_CATEGORY_IDS = 'action_category_ids';
    const SLIDE_DELAY = 'slide_delay';
    /**#@-*/

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Set ID
     *
     * @param int $id
     * @return $this
     */
    public function setId($id);

    /**
     * Get name
     *
     * @return string
     */
    public function getName();

    /**
     * Set name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name);

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus();

    /**
     * Set status
     *
     * @param int $status
     * @return $this
     */
    public function setStatus($status);

    /**
     * Get page type
     *
     * @return int
     */
    public function getPageType();

    /**
     * Set page type
     *
     * @param int $pageType
     * @return $this
     */
    public function setPageType($pageType);

    /**
     * Get position
     *
     * @return int
     */
    public function getPosition();

    /**
     * Set position
     *
     * @param int $position
     * @return $this
     */
    public function setPosition($position);

    /**
     * Get product condition
     *
     * @return \Eguana\Slider\Api\Data\ConditionInterface
     */
    public function getProductCondition();

    /**
     * Set product condition
     *
     * @param \Eguana\Slider\Api\Data\ConditionInterface $productCondition
     * @return $this
     */
    public function setProductCondition($productCondition);

    /**
     * Get category ids
     *
     * @return string
     */
    public function getCategoryIds();

    /**
     * Set category ids
     *
     * @param string $categoryIds
     * @return $this
     */
    public function setCategoryIds($categoryIds);

    /**
     * Get animation effect
     *
     * @return int
     */
    public function getAnimationEffect();

    /**
     * Set animation effect
     *
     * @param int $animationEffect
     * @return $this
     */
    public function setAnimationEffect($animationEffect);

    /**
     * Get pause time between transitions
     *
     * @return int
     */
    public function getPauseTimeBetweenTransitions();

    /**
     * Set pause time between transitions
     *
     * @param int $pauseTimeBetweenTransitions
     * @return $this
     */
    public function setPauseTimeBetweenTransitions($pauseTimeBetweenTransitions);

    /**
     * Get slide transition speed
     *
     * @return int
     */
    public function getSlideTransitionSpeed();

    /**
     * Set slide transition speed
     *
     * @param int $slideTransitionSpeed
     * @return $this
     */
    public function setSlideTransitionSpeed($slideTransitionSpeed);

    /**
     * Get is stop animation mouse on banner
     *
     * @return bool
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getIsStopAnimationMouseOnBanner();

    /**
     * Set is stop animation mouse on banner
     *
     * @param bool $isStopAnimationMouseOnBanner
     * @return $this
     */
    public function setIsStopAnimationMouseOnBanner($isStopAnimationMouseOnBanner);


    /**
     * Get slide ids
     *
     * @return int[]
     */
    public function getSlideIds();

    /**
     * Set slide ids
     *
     * @param int[] $slideIds
     * @return $this
     */
    public function setSlideIds($slideIds);

    /**
     * Get slide position
     *
     * @return string
     */
    public function getSlidePosition();

    /**
     * Set slide position
     *
     * @param string $slidePosition
     * @return $this
     */
    public function setSlidePosition($slidePosition);

    /**
     * Get slide action type
     *
     * @return int
     */
    public function getActionType();

    /**
     * Set slide action type
     *
     * @param int $actionType
     * @return $this
     */
    public function setActionType($actionType);

    /**
     * Get slide Content
     *
     * @return String
     */
    public function getContent();

    /**
     * Set slide Content
     *
     * @param String $content
     * @return $this
     */
    public function setContent($content);

    /**
     * Get slide Action Category Ids
     *
     * @return String
     */
    public function getActionCategoryIds();

    /**
     * Set slide Action Category Ids
     *
     * @param String $actionCategoryIds
     * @return $this
     */
    public function setActionCategoryIds($actionCategoryIds);

    /**
     * Get slide delay
     *
     * @return int
     */
    public function getSlideDelay();

    /**
     * Set slide delay
     *
     * @param int $delay
     * @return $this
     */
    public function setSlideDelay($delay);


    /**
     * Retrieve existing extension attributes object or create a new one.
     *
     * @return \Eguana\Slider\Api\Data\BannerExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     *
     * @param \Eguana\Slider\Api\Data\BannerExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(\Eguana\Slider\Api\Data\BannerExtensionInterface $extensionAttributes);
}
