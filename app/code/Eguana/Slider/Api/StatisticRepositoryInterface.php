<?php
/**
* Copyright 2016 Eguana. All rights reserved.
* See LICENSE.txt for license details.
*/

namespace Eguana\Slider\Api;

/**
 * Statistic CRUD interface.
 * @api
 */
interface StatisticRepositoryInterface
{
    /**
     * Save statistic.
     *
     * @param \Eguana\Slider\Api\Data\StatisticInterface $statistic
     * @return \Eguana\Slider\Api\Data\StatisticInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Eguana\Slider\Api\Data\StatisticInterface $statistic);

    /**
     * Retrieve statistic.
     *
     * @param int $statisticId
     * @return \Eguana\Slider\Api\Data\StatisticInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($statisticId);

    /**
     * Retrieve statistics matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Eguana\Slider\Api\Data\StatisticSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete statistic.
     *
     * @param \Eguana\Slider\Api\Data\StatisticInterface $statistic
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Eguana\Slider\Api\Data\StatisticInterface $statistic);

    /**
     * Delete statistic by ID.
     *
     * @param int $statisticId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($statisticId);
}
