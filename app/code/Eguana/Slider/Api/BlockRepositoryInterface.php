<?php
/**
* Copyright 2016 Eguana. All rights reserved.
* See LICENSE.txt for license details.
*/

namespace Eguana\Slider\Api;

use Magento\Framework\Exception\LocalizedException;

/**
 * Slider block repository interface
 *
 * @api
 */
interface BlockRepositoryInterface
{
    /**
     * Retrieve block(s) matching the specified blockType and blockPosition
     * Update views block statistics if necessary
     *
     * @param int $blockType
     * @param int $blockPosition
     * @param bool $allBlocks
     * @param bool $updateViewsStatistic
     * @param int $bannerId
     *
     * @return \Eguana\Slider\Api\Data\BlockSearchResultsInterface
     * @throws LocalizedException
     */
    public function getList($blockType, $blockPosition, $allBlocks = true, $updateViewsStatistic = true, $bannerId = 0);
}
