<?php
/**
* Copyright 2016 Eguana. All rights reserved.
* See LICENSE.txt for license details.
*/

namespace Eguana\Slider\Model;

use Eguana\Slider\Model\ResourceModel\Slide as ResourceSlide;

/**
 * Class Slide
 * @package Eguana\Slider\Model
 */
class Slide extends \Magento\Framework\Model\AbstractModel
{
    /**
     * {@inheritdoc}
     */
    protected function _construct()
    {
        $this->_init(ResourceSlide::class);
    }
}
