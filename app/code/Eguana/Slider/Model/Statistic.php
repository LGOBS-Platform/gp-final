<?php
/**
* Copyright 2016 Eguana. All rights reserved.
* See LICENSE.txt for license details.
*/

namespace Eguana\Slider\Model;

use Eguana\Slider\Model\ResourceModel\Statistic as ResourceStatistic;

/**
 * Class Statistic
 * @package Eguana\Slider\Model
 */
class Statistic extends \Magento\Framework\Model\AbstractModel
{
    /**
     * {@inheritdoc}
     */
    protected function _construct()
    {
        $this->_init(ResourceStatistic::class);
    }
}
