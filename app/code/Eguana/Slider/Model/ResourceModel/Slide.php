<?php
/**
* Copyright 2016 Eguana. All rights reserved.
* See LICENSE.txt for license details.
*/

namespace Eguana\Slider\Model\ResourceModel;

/**
 * Class Slide
 * @package Eguana\Slider\Model\ResourceModel
 */
class Slide extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('eguana_slide', 'id');
    }
}
