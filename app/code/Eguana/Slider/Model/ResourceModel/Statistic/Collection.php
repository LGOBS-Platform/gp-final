<?php
/**
* Copyright 2016 Eguana. All rights reserved.
* See LICENSE.txt for license details.
*/

namespace Eguana\Slider\Model\ResourceModel\Statistic;

use Eguana\Slider\Model\Statistic;
use Eguana\Slider\Model\ResourceModel\Statistic as ResourceStatistic;
use Eguana\Slider\Model\ResourceModel\AbstractCollection;

/**
 * Class Collection
 * @package Eguana\Slider\Model\ResourceModel\Statistic
 */
class Collection extends AbstractCollection
{
    /**
     * Name of object id field
     *
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Resource initialization
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init(Statistic::class, ResourceStatistic::class);
    }

    /**
     * {@inheritdoc}
     */
    protected function _initSelect()
    {
        parent::_initSelect();
        $ctrQuery = $this->getConnection()->select()
            ->from(
                [$this->getTable('eguana_statistic')],
                ['ctr_id' => 'id', 'ctr' => 'ROUND(click_count/IF(view_count > 0, view_count, 1) * 100, 0)']
            );
        $this->getSelect()
            ->joinLeft(
                ['stat' => $ctrQuery],
                'main_table.id = stat.ctr_id',
                ['ctr']
            );
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    protected function _afterLoad()
    {
        $this->attachSlideBanner();
        return parent::_afterLoad();
    }

    /**
     * {@inheritdoc}
     */
    protected function _renderFiltersBefore()
    {
        $this->joinLinkageTable('eguana_slide_banner', 'slide_banner_id', 'id', 'banner_id');
        $this->joinLinkageTable('eguana_slide_banner', 'slide_banner_id', 'id', 'slide_id');
        $this->joinNameField('slide_name', 'eguana_slide', 'slide_id');
        $this->joinNameField('banner_name', 'eguana_slide_banner', 'banner_id');
        parent::_renderFiltersBefore();
    }

    /**
     * Attach slide and banner data to collection items
     *
     * @return void
     */
    private function attachSlideBanner()
    {
        $connection = $this->getConnection();
        $select = $connection->select()
            ->from(
                ['sb' => $this->getTable('eguana_slide_banner')],
                []
            )->joinLeft(
                ['s' => $this->getTable('eguana_slide')],
                'sb.slide_id = s.id',
                [
                    'slide_id' => 'id',
                    'slide_name' => 'name',
                    'img_type',
                    'img_file',
                    'mob_img_file',
                    'img_url',
                    'mob_img_url'
                ]
            )->joinLeft(
                ['b' => $this->getTable('eguana_banner')],
                'sb.banner_id = b.id',
                [
                    'banner_id' => 'id',
                    'banner_name' => 'name'
                ]
            )->where('sb.id = :id');
        /** @var \Magento\Framework\DataObject $item */
        foreach ($this as $item) {
            $data = $connection->fetchRow($select, ['id' => $item->getData('slide_banner_id')]);
            $item->setData('ctr', $item->getData('ctr') . '%');
            $item->addData($data);
        }
    }

    /**
     * Join field if sorting applied
     *
     * @param string $fieldName
     * @param string $tableName
     * @param string $linkageColumnName
     * @return void
     */
    private function joinNameField($fieldName, $tableName, $linkageColumnName)
    {
        if (array_key_exists($fieldName, $this->_orders)) {
            $this->getSelect()
                ->joinLeft(
                    [$fieldName . '_sb' => $this->getTable('eguana_slide_banner')],
                    'main_table.slide_banner_id = ' . $fieldName . '_sb.id',
                    []
                )->joinLeft(
                    [$fieldName . '_s' => $this->getTable($tableName)],
                    $fieldName . '_s.id = ' . $fieldName . '_sb.' . $linkageColumnName,
                    [$fieldName => 'name']
                );
        }
    }
}
