<?php
/**
* Copyright 2016 Eguana. All rights reserved.
* See LICENSE.txt for license details.
*/

namespace Eguana\Slider\Model\Source;

/**
 * Class PageType
 * @package Eguana\Slider\Model\Source
 */
class ActionType implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * Page type values
     */
    const SLIDE = 1;

    const SORT_PRINT = 2;

    const CONTENT = 3;

    /**
     * @return array
     */
    public function getOptionArray()
    {
        $optionArray = [];
        foreach ($this->toOptionArray() as $option) {
            $optionArray[$option['value']] = $option['label'];
        }
        return $optionArray;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => self::SLIDE,  'label' => __('Slide')],
            ['value' => self::SORT_PRINT,  'label' => __('Sort Print')],
            ['value' => self::CONTENT,  'label' => __('Content')],
        ];
    }
}
