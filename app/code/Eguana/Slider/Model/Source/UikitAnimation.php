<?php
/**
* Copyright 2016 Eguana. All rights reserved.
* See LICENSE.txt for license details.
*/

namespace Eguana\Slider\Model\Source;

use Eguana\Slider\Model\Source\AnimationEffect;

/**
 * Class UikitAnimation
 * @package Eguana\Slider\Model\Source
 */
class UikitAnimation
{
    /**
     * @var array
     */
    private $animation = [
        AnimationEffect::SLIDE => 'scroll',
        AnimationEffect::FADE_OUT_IN => 'fade',
    ];

    /**
     * Retrieve animation effect name by key
     *
     * @param int $key
     * @return string
     */
    public function getAnimationEffectByKey($key)
    {
        return $this->animation[$key];
    }
}
