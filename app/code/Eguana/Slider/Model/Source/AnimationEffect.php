<?php
/**
* Copyright 2016 Eguana. All rights reserved.
* See LICENSE.txt for license details.
*/

namespace Eguana\Slider\Model\Source;

/**
 * Class AnimationEffect
 * @package Eguana\Slider\Model\Source
 */
class AnimationEffect implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * Animation effect values
     */
    const SLIDE = 0;
    const FADE_OUT_IN = 1;

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => self::SLIDE,  'label' => __('Slide')],
            ['value' => self::FADE_OUT_IN,  'label' => __('Fade Out / In')],
        ];
    }
}
