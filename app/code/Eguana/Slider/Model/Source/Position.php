<?php
/**
* Copyright 2016 Eguana. All rights reserved.
* See LICENSE.txt for license details.
*/

namespace Eguana\Slider\Model\Source;

/**
 * Class Position
 * @package Eguana\Slider\Model\Source
 */
class Position implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * Position values
     */
    const MENU_TOP = 1;
    const MENU_BOTTOM = 2;
    const CONTENT_TOP = 3;
    const PAGE_BOTTOM = 4;
    const SIDEBAR_BOTTOM = 5;
    const SIDEBAR_TOP = 6;

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => self::MENU_TOP,  'label' => __('Menu Top')],
            ['value' => self::MENU_BOTTOM,  'label' => __('Menu Bottom')],
            ['value' => self::CONTENT_TOP,  'label' => __('Content top')],
            ['value' => self::PAGE_BOTTOM,  'label' => __('Page bottom')],
            ['value' => self::SIDEBAR_BOTTOM,  'label' => __('Sidebar Bottom')],
            ['value' => self::SIDEBAR_TOP,  'label' => __('Sidebar Top')]
        ];
    }
}
