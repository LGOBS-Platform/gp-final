<?php
/**
* Copyright 2016 Eguana. All rights reserved.
* See LICENSE.txt for license details.
*/

namespace Eguana\Slider\Model;

/**
 * class Sample
 * @package Eguana\Slider\Model
 */
class Sample extends \Magento\Framework\Config\Data
{
    /**
     * @param \Eguana\Slider\Model\Sample\Reader\Xml $reader
     * @param \Magento\Framework\Config\CacheInterface $cache
     * @param string $cacheId
     */
    public function __construct(
        \Eguana\Slider\Model\Sample\Reader\Xml $reader,
        \Magento\Framework\Config\CacheInterface $cache,
        $cacheId = 'Eguana_Slider_sample_data_cache'
    ) {
        parent::__construct($reader, $cache, $cacheId);
    }
}
