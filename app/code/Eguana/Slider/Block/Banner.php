<?php
/**
 * Copyright 2016 aheadWorks. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Eguana\Slider\Block;

use Eguana\Slider\Api\Data\SlideInterface;
use Eguana\Slider\Model\Source\ActionType;
use Eguana\Slider\Model\Source\AnimationEffect;
use Eguana\Slider\Model\Source\Position;
use Eguana\Slider\Model\Source\PageType;
use Eguana\Slider\Model\Source\ImageType;
use Eguana\Slider\Model\Slide\ImageFileUploader;
use Eguana\Slider\Api\BlockRepositoryInterface;
use Magento\Framework\View\Element\Template\Context;
use Eguana\Slider\Model\Source\UikitAnimation;
use Eguana\Slider\Api\Data\BlockInterface;

/**
 * Class Banner
 * @package Eguana\Slider\Block
 */
class Banner extends \Magento\Framework\View\Element\Template
{
    /**
     * Path to template file in theme
     * @var string
     */
    protected $_template = 'Eguana_Slider::block.phtml';

    /**
     * @var BlockRepositoryInterface
     */
    protected $blocksRepository;

    /**
     * @var int|null
     */
    private $blockPosition;

    /**
     * @var int|null
     */
    private $blockType;

    /**
     * @var ImageFileUploader
     */
    private $imageFileUploader;

    /**
     * @var UikitAnimation
     */
    private $uikitAnimation;

    /**
     * @param Context $context
     * @param BlockRepositoryInterface $blocksRepository
     * @param ImageFileUploader $imageFileUploader
     * @param UikitAnimation $uikitAnimation
     * @param array $data
     */
    public function __construct(
        Context $context,
        BlockRepositoryInterface $blocksRepository,
        ImageFileUploader $imageFileUploader,
        UikitAnimation $uikitAnimation,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->blocksRepository = $blocksRepository;
        $this->imageFileUploader = $imageFileUploader;
        $this->uikitAnimation = $uikitAnimation;
    }

    /**
     * @param \Eguana\Slider\Api\Data\BlockInterface $block
     * @return string
    */
    public function getBannerActionHtml($block){

        $html = '';

        $bannerBlockName = $this->getActionBlockName($block->getBanner()->getActionType());

        if(!is_null($bannerBlockName)){

            $bannerBlock = $this->getLayout()
                ->createBlock($bannerBlockName);

            $bannerBlock->setBanner($block->getBanner());
            $bannerBlock->setSlides($block->getSlides());

            $html = $bannerBlock->toHtml();
        }

        return $html;
    }

    public function isSlideFadeEffect($banner){

        if($banner->getAnimationEffect() == AnimationEffect::FADE_OUT_IN)
            return true;
        
        return false;
    }

    protected function getActionBlockName($type){

        $backendBlockName = null;

        if($type == ActionType::SLIDE){
            $backendBlockName = 'Eguana\Slider\Block\Banner\Slide';
        }else if($type == ActionType::SORT_PRINT){
            $backendBlockName = 'Eguana\Slider\Block\Banner\SortPrint';
        }else if($type == ActionType::CONTENT){
            $backendBlockName = 'Eguana\Slider\Block\Banner\Content';
        }

        return $backendBlockName;
    }

    /**
     * Retrieve banners for current block position and type
     *
     * @return BlockInterface[]
     */
    public function getBlocks()
    {
        return $this->blocksRepository
            ->getList($this->getBlockType(), $this->getBlockPosition())
            ->getItems();
    }

    /**
     * Retrieve slide image url
     *
     * @param SlideInterface $slide
     * @return string
     */
    public function getSlideImgUrl(SlideInterface $slide)
    {
        return $slide->getImgType() == ImageType::TYPE_FILE
            ? $this->imageFileUploader->getMediaUrl($slide->getImgFile())
            : $slide->getImgUrl();
    }

    /**
     * Get link for redirect on slider click
     *
     * @param int $slideId
     * @param int $bannerId
     * @return string
     */
    public function getLinkUrl($slideId, $bannerId)
    {
        return $this->getUrl(
            'aw_rbslider/countClicks/redirect',
            [
                'slide_id' => $slideId,
                'banner_id' => $bannerId,
            ]
        );
    }

    /**
     * Retrieve animation effect name by key
     *
     * @param int $key
     * @return string
     */
    public function getAnimation($key)
    {
        return $this->uikitAnimation->getAnimationEffectByKey($key);
    }

    /**
     * Retrieve block position
     *
     * @return int|null
     */
    private function getBlockPosition()
    {
        if (!$this->blockPosition) {
            if (false !== strpos($this->getNameInLayout(), 'menu_top')) {
                $this->blockPosition = Position::MENU_TOP;
            }
            if (false !== strpos($this->getNameInLayout(), 'menu_bottom')) {
                $this->blockPosition = Position::MENU_BOTTOM;
            }
            if (false !== strpos($this->getNameInLayout(), 'content_top')) {
                $this->blockPosition = Position::CONTENT_TOP;
            }
            if (false !== strpos($this->getNameInLayout(), 'page_bottom')) {
                $this->blockPosition = Position::PAGE_BOTTOM;
            }
            if (false !== strpos($this->getNameInLayout(), 'sidebar_bottom')) {
                $this->blockPosition = Position::SIDEBAR_BOTTOM;
            }
            if (false !== strpos($this->getNameInLayout(), 'sidebar_top')) {
                $this->blockPosition = Position::SIDEBAR_TOP;
            }
        }
        return $this->blockPosition;
    }

    /**
     * Retrieve block type
     *
     * @return int|null
     */
    private function getBlockType()
    {
        if (!$this->blockType) {
            if (false !== strpos($this->getNameInLayout(), 'banner_product')) {
                $this->blockType = PageType::PRODUCT_PAGE;
            }
            if (false !== strpos($this->getNameInLayout(), 'banner_category')) {
                $this->blockType = PageType::CATEGORY_PAGE;
            }
            if (false !== strpos($this->getNameInLayout(), 'banner_home')) {
                $this->blockType = PageType::HOME_PAGE;
            }
        }
        return $this->blockType;
    }
}
