<?php
/**
* Copyright 2016 Eguana. All rights reserved.
* See LICENSE.txt for license details.
*/

namespace Eguana\Slider\Block\Widget;

use Eguana\Slider\Model\Source\Position;
use Eguana\Slider\Model\Source\PageType;
use Eguana\Slider\Api\Data\BlockInterface;

/**
 * Class Banner
 * @package Magento\Blog\Block\Widget
 */
class Banner extends \Eguana\Slider\Block\Banner implements \Magento\Widget\Block\BlockInterface
{
    /**
     * @var string
     */
    const WIDGET_NAME_PREFIX = 'eguana_slider_widget_';

    /**
     * Retrieve banner for widget
     *
     * @return BlockInterface[]
     */
    public function getBlocks()
    {
        $bannerId = $this->getData('banner_id');
        $blocks = $this->blocksRepository
            ->getList(PageType::CUSTOM_WIDGET, Position::CONTENT_TOP, true, true, $bannerId)
            ->getItems();

        foreach ($blocks as $block) {
            if ($block->getBanner()->getId() == $bannerId) {
                return [$block];
            }
        }
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getNameInLayout()
    {
        return self::WIDGET_NAME_PREFIX . $this->getData('banner_id');
    }
}
