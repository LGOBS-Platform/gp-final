<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-08-23
 * Time: 오전 10:08
 */

namespace Eguana\Slider\Block\Banner;


use Eguana\Slider\Model\Source\AnimationEffect;

class Slide extends AbstractBanner
{
    protected $_template = 'Eguana_Slider::banner/slide.phtml';


    public function isSlideFadeEffect(){

        if($this->getBanner()->getAnimationEffect() == AnimationEffect::FADE_OUT_IN)
            return true;

        return false;
    }
}