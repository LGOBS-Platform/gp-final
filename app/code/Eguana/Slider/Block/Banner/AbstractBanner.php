<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-08-23
 * Time: 오전 10:07
 */

namespace Eguana\Slider\Block\Banner;


use Eguana\Slider\Model\Source\ImageType;
use Eguana\Slider\Model\Slide\ImageFileUploader;

abstract class AbstractBanner extends \Magento\Framework\View\Element\Template
{
    /** @var \Eguana\Slider\Api\Data\BannerInterface */
    protected $_banner;

    /** @var \Eguana\Slider\Api\Data\SlideInterface[] */
    protected $_slides;

    protected $_filterProvider;

    /**
     * @var ImageFileUploader
     */
    private $_imageFileUploader;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Cms\Model\Template\FilterProvider $filterProvider,
        ImageFileUploader $imageFileUploader,
        array $data = []
    )
    {
        $this->_filterProvider = $filterProvider;
        $this->_imageFileUploader = $imageFileUploader;
        parent::__construct($context, $data);
    }

    public function contentFilter($content){
        $storeId = $this->_storeManager->getStore()->getId();
        $html = $this->_filterProvider->getBlockFilter()->setStoreId($storeId)->filter($content);
        return $html;
    }

    /**
     * @param \Eguana\Slider\Api\Data\BannerInterface $banner
     * @return $this
     */
    public function setBanner($banner){
        $this->_banner = $banner;
        return $this;
    }

    /**
     * @return \Eguana\Slider\Api\Data\BannerInterface $banner
     */
    public function getBanner(){
        return $this->_banner;
    }

    /**
     * @param \Eguana\Slider\Api\Data\SlideInterface[] $slides
     * @return $this
     */
    public function setSlides($slides){
        $this->_slides = $slides;
        return $this;
    }

    /**
     * @return \Eguana\Slider\Api\Data\SlideInterface[]
     */
    public function getSlides(){
        return $this->_slides;
    }

    /**
     * Retrieve slide image url
     *
     * @param \Eguana\Slider\Api\Data\SlideInterface $slide
     * @return string
     */
    public function getSlideImgUrl(\Eguana\Slider\Api\Data\SlideInterface $slide)
    {
        return $slide->getImgType() == ImageType::TYPE_FILE
            ? $this->_imageFileUploader->getMediaUrl($slide->getImgFile())
            : $slide->getImgUrl();
    }

    public function getSlideUrl($path){
        return $this->_urlBuilder->getUrl().$path;
    }

    /**
     * Retrieve slide mobile image url
     *
     * @param \Eguana\Slider\Api\Data\SlideInterface $slide
     * @return string
     */
    public function getSlideMobImgUrl(\Eguana\Slider\Api\Data\SlideInterface $slide)
    {
        return $slide->getImgType() == ImageType::TYPE_FILE
            ? $this->_imageFileUploader->getMediaUrl($slide->getMobImgFile())
            : $slide->getMobImgUrl();
    }

    public function getMobileImgWidth(){
        return $this->_scopeConfig->getValue('slider/slide/mobile_width');
    }

    /**
     * @param \Eguana\Slider\Api\Data\SlideInterface $slide
     * @return boolean
     */
    public function mobileImgExist($slide){

        if($slide->getImgType() == ImageType::TYPE_FILE)
        {
            if($slide->getMobImgFile() == ''){
                return false;
            }
        }else{
            if($slide->getMobImgUrl() == ''){
                return false;
            }
        }

        return true;
    }

}