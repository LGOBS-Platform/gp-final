<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Categories tree with checkboxes
 *
 * @author     Magento Core Team <core@magentocommerce.com>
 */
namespace Eguana\Slider\Block\Adminhtml\Category\Checkboxes;

class Tree extends \Magento\Catalog\Block\Adminhtml\Category\Checkboxes\Tree
{
}
