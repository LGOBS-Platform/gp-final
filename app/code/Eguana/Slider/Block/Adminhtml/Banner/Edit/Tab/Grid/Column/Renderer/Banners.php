<?php
/**
* Copyright 2016 Eguana. All rights reserved.
* See LICENSE.txt for license details.
*/

namespace Eguana\Slider\Block\Adminhtml\Banner\Edit\Tab\Grid\Column\Renderer;

use Magento\Framework\DataObject;

/**
 * Class Banners
 * @package Eguana\Slider\Block\Adminhtml\Banner\Edit\Tab\Grid\Column\Renderer
 */
class Banners extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\Text
{
    /**
     * @param \Magento\Framework\DataObject $row
     * @return string
     */
    public function render(DataObject $row)
    {
        $bannerUrlsRow = [];
        if (count($row->getBannerIds())) {
            $columnOptions = $this->getColumn()->getOptions();
            foreach ($row->getBannerIds() as $id) {
                $name = (is_array($columnOptions) && isset($columnOptions[$id]))
                    ? $columnOptions[$id]
                    : $id;
                $url = $this->getUrl(
                    'eguana_slider/banner/edit',
                    ['id' => $id]
                );
                $bannerUrlsRow[] = '<a href="' . $url . '" target="_blank">' . $name . '</a>';
            }
        }
        return implode(', ', $bannerUrlsRow);
    }
}
