<?php
/**
* Copyright 2016 Eguana. All rights reserved.
* See LICENSE.txt for license details.
*/

namespace Eguana\Slider\Block\Adminhtml\Banner\Edit\Tab\Grid\Column\Renderer;

/**
 * Class SlideName
 * @package Eguana\Slider\Block\Adminhtml\Banner\Edit\Tab\Grid\Column\Renderer
 */
class SlideName extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\Text
{
    /**
     * @param \Magento\Framework\DataObject $row
     * @return string
     */
    public function render(\Magento\Framework\DataObject $row)
    {
        $url = $this->getUrl(
            'eguana_slider/slide/edit',
            ['id' => $row->getId()]
        );
        return '<a href="' . $url . '" target="_blank">' . $row->getName() . '</a>';
    }
}
