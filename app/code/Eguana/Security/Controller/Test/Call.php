<?php
/**
 * Created by Eguana.
 * User: jason
 * Date: 2018-08-27
 * Time: 오전 9:47
 */

namespace Eguana\Security\Controller\Test;

use Eguana\Security\Controller\AbstractAction;

class Call extends AbstractAction
{

    public function execute()
    {
        echo base64_encode($this->helper->getAuthKey());
    }

}