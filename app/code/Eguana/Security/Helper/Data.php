<?php

/**
 * Created by Eguana.
 * User: jason
 * Date: 2018-08-27
 * Time: 오전 9:59
 */

namespace Eguana\Security\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{
    const PHP_SESSION_COOKIE_KIY = 'PHPSESSID';

    const AUTH_TIME_INTERVAL = 300;

    const DOT_COM_ENCRYPT_FREFIX_DATA = 'lgepl_prd';



    protected $_securityLogger;

    protected $_securityHelper;

    protected $_logData;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Eguana\Security\Logger\Security $securityLogger,
        Security $securityHelper
    )
    {
        $this->_securityLogger = $securityLogger;
        $this->_securityHelper = $securityHelper;
        parent::__construct($context);
    }


    public function auth(){

        $result = false;

        if($this->getSecurityConfig('enable')){

            $encryptData = $this->_getRequest()->getHeader('authKey');
            $this->log(__METHOD__,$encryptData);

            $decrypt = $this->_securityHelper->decrypt(base64_decode($encryptData),$this->getSecurityPrivateKey());
            $decryptArray = explode(',',$decrypt);
            if(!isset($decryptArray[1]))
            {
                return false;
            }
            $decryptTime = $this->_securityHelper->decryptToTimestamp($decrypt);
            $currentTime = $this->_securityHelper->getCurrentKoreaTimestamp();
            $authPlusTime = $decryptTime + self::AUTH_TIME_INTERVAL;
            $authMinusTime = $decryptTime - self::AUTH_TIME_INTERVAL;

            $this->log(__METHOD__,'DECRYPT : '.$decrypt);
            $this->log(__METHOD__,'DECRYPT TIME : '.$decryptTime);
            $this->log(__METHOD__,'PLUS TIME'.$authPlusTime);
            $this->log(__METHOD__,'MINUS TIME : '.$authMinusTime);
            $this->log(__METHOD__,'CURRENT TIME : '.$currentTime,true);

            if($authMinusTime < $currentTime && $currentTime < $authPlusTime)
                $result = true;

        }else{
            $result = true;
        }

        return $result;
    }

    public function getAuthKey(){

        $encryptData[] = self::DOT_COM_ENCRYPT_FREFIX_DATA;
        $encryptData[] = $this->_securityHelper->getCurrentKoreaDate();

        return $this->_securityHelper->encrypt(implode(',',$encryptData),$this->getDotComPublicKey());
    }

    public function getEguanaConfig($path){
        return $this->scopeConfig->getValue('eguana/'.$path);
    }

    public function getEguanaGeneralConfig($path){
        return $this->getEguanaConfig('general/'.$path);
    }

    protected function getSecurityConfig($path){
        return $this->getEguanaConfig('security/'.$path);
    }

    public function getRequestConfig($path){
        return $this->getEguanaConfig('request/'.$path);
    }

    protected function getSecurityPublicKey(){
        return $this->getKeyFile($this->getSecurityConfig('security_pub'));
    }

    protected function getSecurityPrivateKey(){
        return $this->getKeyFile($this->getSecurityConfig('security_private'));
    }

    protected function getDotComPublicKey(){
        return $this->getKeyFile($this->getSecurityConfig('security_com_pub'));
    }

    protected function getKeyFile($filename){
        return $this->_securityHelper->getKeyFile($filename);
    }

    public function log($key,$message = null ,$write = false){

        if($write && $this->getEguanaGeneralConfig('log_enable')){
            $this->_logData[$key][] = $message;
            $this->_securityLogger->info($key,$this->_logData[$key]);
            unset($this->_logData[$key]);
        }else{
            $this->_logData[$key][] = $message;
        }

    }

}