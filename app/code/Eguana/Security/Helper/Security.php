<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-09-28
 * Time: 오후 1:50
 */

namespace Eguana\Security\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Security extends AbstractHelper
{
    protected $_moduleReader;

    protected $_fileSystem;

    protected $_directoryRead;

    protected $_localeDate;

    const EGUANA_SECURITY_KEY_PATH = 'sso/key/';

    const EGUANA_SECURITY_KEY_DIRECTORY = 'etc';

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\Module\Dir\Reader $moduleReader,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Framework\Filesystem $filesystem
    )
    {
        $this->_moduleReader = $moduleReader;
        $this->_fileSystem = $filesystem;
        $this->_localeDate = $localeDate;
        parent::__construct($context);
    }

    public function decryptToTimestamp($decrypt){
        $decryptArray = explode(',',$decrypt);
        $decryptDate = $decryptArray[1];
        $dateString = $decryptDate[0].$decryptDate[1].$decryptDate[2].$decryptDate[3].'-'
            .$decryptDate[4].$decryptDate[5].'-'
            .$decryptDate[6].$decryptDate[7].' '
            .$decryptDate[8].$decryptDate[9].':'
            .$decryptDate[10].$decryptDate[11].':'
            .$decryptDate[12].$decryptDate[13].' KST';

        return strtotime($dateString);
    }

    public function getCurrentKoreaTimestamp(){

        return strtotime($this->getCurrentKoreaDate().' KST');
    }

    public function getCurrentKoreaDate(){

        $utcDate = $this->_localeDate->date(null,null,false)->format('Y-m-d H:i:s T');
        $koreaDate = new \DateTime($utcDate, new \DateTimeZone('Asia/Seoul'));
        $koreaDate->setTimezone(new \DateTimeZone('KST'));
        $koreaDate->add(new \DateInterval('PT30M'));

        return $koreaDate->format('YmdHis');
    }

    public  function getKeyFile($fileName){

        return $this->getDirectoryRead()
            ->readFile($this->getKeyDirectory().$fileName);

    }

    public function encrypt($message,$key){
        $encrypt = null;
        openssl_public_encrypt($message,$encrypt,$key);
        return $encrypt;
    }

    public function decrypt($message,$key){
        $decrypt = null;
        openssl_private_decrypt($message,$decrypt,$key);
        return $decrypt;
    }

    protected function getDirectoryRead(){

        if(is_null($this->_directoryRead)){
            $this->_directoryRead = $this->_fileSystem->getDirectoryRead(self::EGUANA_SECURITY_KEY_DIRECTORY);
        }

        return $this->_directoryRead;
    }

    public function getKeyDirectory(){

        return $this->_fileSystem
            ->getDirectoryRead(self::EGUANA_SECURITY_KEY_DIRECTORY)
            ->getAbsolutePath(self::EGUANA_SECURITY_KEY_PATH);

    }

}