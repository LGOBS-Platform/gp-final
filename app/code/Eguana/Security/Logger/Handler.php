<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-09-28
 * Time: 오전 11:38
 */

namespace Eguana\Security\Logger;

use Magento\Framework\Filesystem\DriverInterface;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class Handler extends StreamHandler
{
    /**
     * Logging level
     * @var int
     */
    protected $loggerType = Logger::INFO;

    /**
     * File name
     * @var string
     */
    protected $fileName = 'security.log';

    protected $directorySeparator = 'security-';


    /**
     * @var DriverInterface
     */
    protected $filesystem;

    /**
     * @param DriverInterface $filesystem
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     */
    public function __construct(
        DriverInterface $filesystem,
        \Magento\Framework\Stdlib\DateTime\DateTime $date
    ) {
        $this->filesystem = $filesystem;
        parent::__construct(
            BP .'/var/log/'.$this->directorySeparator .$date->date('Y-m-d').'.log',
            $this->loggerType
        );
        $this->setFormatter(new LineFormatter(null, null, true));
    }

    /**
     * @{inheritDoc}
     *
     * @param $record array
     * @return void
     */
    public function write(array $record)
    {
        $logDir = $this->filesystem->getParentDirectory($this->url);
        if (!$this->filesystem->isDirectory($logDir)) {
            $this->filesystem->createDirectory($logDir);
        }

        parent::write($record);
    }
}