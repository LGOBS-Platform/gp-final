<?php
/**
 * Created by Kenneth.
 * Date: 2018-10-19
 * Time: 오전 11:59
 */
namespace Eguana\Terms\Observer;


use Eguana\Terms\Helper\Data;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Eguana\Terms\Model\CheckoutAgreementsRepository;


class CheckoutSuccessAction implements ObserverInterface
{
    protected $_CheckoutAgreementsRepository;

    protected $_termsHelper;



    public function __construct(
        CheckoutAgreementsRepository $CheckoutAgreementsRepository,
        Data $termsHelper
    )
    {
        $this->_CheckoutAgreementsRepository = $CheckoutAgreementsRepository;
        $this->_termsHelper = $termsHelper;
    }

    public function execute(Observer $observer)
    {
        $this->_termsHelper->updateCustomerTerms();
    }
}
