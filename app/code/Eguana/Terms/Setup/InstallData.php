<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2017-12-12
 * Time: 오후 6:48
 */
namespace Eguana\Terms\Setup;

use Eguana\Terms\Helper\Data;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Model\Config;
use Magento\Customer\Setup\CustomerSetup;

class InstallData implements InstallDataInterface
{
    /** @var \Magento\Customer\Setup\CustomerSetupFactory */
    protected $_customerSetupFactory;

    /** @var Config */
    protected $_eavConfig;

    /** @var \Magento\Customer\Model\GroupFactory */
    protected $_customerGroupFactory;

    public function __construct(
        CustomerSetupFactory $customerSetupFactory,
        \Magento\Customer\Model\GroupFactory $customerGroupFactory,
        Config $eavConfig
    )
    {
        $this->_eavConfig = $eavConfig;
        $this->_customerSetupFactory = $customerSetupFactory;
        $this->_customerGroupFactory = $customerGroupFactory;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {

        /** @var \Magento\Customer\Setup\CustomerSetup $customerSetup */
        $customerSetup = $this->_customerSetupFactory->create(['setup' => $setup]);

        $customerSetup->addAttribute(
            \Magento\Customer\Model\Customer::ENTITY,
            Data::TERMS,
            [
                'type' => 'varchar',
                'label' => 'Terms',
                'input' => 'text',
                'required' => false,
                'default' => '',
                'sort_order' => 150,
                'system' => false,
                'position' => 150,
            ]
        );

        $this->customColumnFormAdd($setup);
        $this->defaultColumnChange($setup);

    }

    protected function customColumnFormAdd(ModuleDataSetupInterface $setup){

        $sampleAttribute = $this->_eavConfig->getAttribute(\Magento\Customer\Model\Customer::ENTITY, Data::TERMS);
        $sampleAttribute->setData(
            'used_in_forms',
            ['adminhtml_customer', 'customer_account_create','customer_account_edit']
        );

        $sampleAttribute->save();
    }

    protected function defaultColumnChange(ModuleDataSetupInterface $setup){

        $customerSetup = $this->_customerSetupFactory->create(['setup' => $setup]);

        $entityAttributes = [
            'customer' => [
                Data::TERMS => [
                    'is_visible_in_grid' => false,
                    'is_filterable_in_grid' => false,
                    'is_searchable_in_grid' => false,
                    'is_used_for_customer_segment' => false,
                ],
            ],
        ];

        $this->upgradeAttributes($entityAttributes,$customerSetup);
    }

    /**
     * @param array $entityAttributes
     * @param CustomerSetup $customerSetup
     * @return void
     */
    protected function upgradeAttributes(array $entityAttributes, CustomerSetup $customerSetup)
    {
        foreach ($entityAttributes as $entityType => $attributes) {
            foreach ($attributes as $attributeCode => $attributeData) {
                $attribute = $customerSetup->getEavConfig()->getAttribute($entityType, $attributeCode);
                foreach ($attributeData as $key => $value) {
                    $attribute->setData($key, $value);
                }
                $attribute->save();
            }
        }
    }

}
