<?php
/**
 * Created by Kenneth.
 * Date: 2018-11-12
 * Time: 오전 10:58
 */
namespace Eguana\Terms\Model;


use Eguana\Terms\Model\CheckoutAgreementsRepository;
use Magento\Customer\Model\Session;


class AgreementsValidator extends \Magento\CheckoutAgreements\Model\AgreementsValidator
{

    protected $_CheckoutAgreementsRepository;

    protected $_customerSession;


    /**
     * @param AgreementsProviderInterface[] $list
     * @codeCoverageIgnore
     */
    public function __construct(
        $list = null,
        CheckoutAgreementsRepository $checkoutAgreementsRepository,
        Session $customerSession
    )
    {
        $this->_CheckoutAgreementsRepository = $checkoutAgreementsRepository;
        $this->_customerSession = $customerSession;
        parent::__construct($list);
    }

    /**
     * Validate that all required agreements is signed
     *
     * @param int[] $agreementIds
     * @return bool
     */
    public function isValid($agreementIds = [])
    {

        if($this->_customerSession->isLoggedIn() && $this->_CheckoutAgreementsRepository->getCustomerTerms() != null){
            $agreementIds = $this->_CheckoutAgreementsRepository->getCustomerTerms();
        }

        $agreementIds = $agreementIds === null ? [] : $agreementIds;
        $requiredAgreements = [];
        foreach ($this->agreementsProviders as $agreementsProvider) {
            $requiredAgreements = array_merge($requiredAgreements, $agreementsProvider->getRequiredAgreementIds());
        }
        $agreementsDiff = array_diff($requiredAgreements, $agreementIds);


        return empty($agreementsDiff);
    }

}