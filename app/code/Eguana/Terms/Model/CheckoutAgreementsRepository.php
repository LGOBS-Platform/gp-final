<?php
/**
 * Created by Kenneth.
 * Date: 2018-10-16
 * Time: 오후 12:18
 */
namespace Eguana\Terms\Model;


use Magento\CheckoutAgreements\Model\ResourceModel\Agreement\CollectionFactory as AgreementCollectionFactory;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\CheckoutAgreements\Api\CheckoutAgreementsRepositoryInterface;
use Magento\Customer\Model\Session;


class CheckoutAgreementsRepository extends \Magento\CheckoutAgreements\Model\CheckoutAgreementsRepository implements CheckoutAgreementsRepositoryInterface
{

    /**
     * Collection factory.
     *
     * @var AgreementCollectionFactory
     */
    private $_collectionFactory;

    /**
     * Store manager.
     *
     * @var  StoreManagerInterface
     */
    private $_storeManager;

    /**
     * Scope config.
     *
     * @var ScopeConfigInterface
     */
    private $_scopeConfig;


    /**
     * @var JoinProcessorInterface
     */
    private $_extensionAttributesJoinProcessor;


    protected $_customerSession;

    protected $_agreementsValidator;



    public function __construct(
        AgreementCollectionFactory $collectionFactory,
        StoreManagerInterface $storeManager,
        ScopeConfigInterface $scopeConfig,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        Session $customerSession
    ) {
        $this->_collectionFactory = $collectionFactory;
        $this->_storeManager = $storeManager;
        $this->_scopeConfig = $scopeConfig;
        $this->_extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->_customerSession = $customerSession;
    }



    public function getList($isAll = false, $requereMode = false)
    {
        if (!$this->_scopeConfig->isSetFlag('checkout/options/enable_agreements', ScopeInterface::SCOPE_STORE)) {
            return [];
        }
        $storeId = $this->_storeManager->getStore()->getId();
        /** @var $agreementCollection AgreementCollection */
        $agreementCollection = $this->_collectionFactory->create();
        $this->_extensionAttributesJoinProcessor->process($agreementCollection);
        $agreementCollection->addStoreFilter($storeId);
        $agreementCollection->addFieldToFilter('is_active', 1);

        if(!$isAll){

            $termCheck = $this->getCustomerTerms();
            if(count($termCheck) > 0) {
                $agreementCollection->addFieldToFilter('agreement_id', ['nin' => $termCheck]);
            }
        }

        if($requereMode){
            $agreementCollection->addFieldToFilter('mode', 1);
        }

        $agreementDataObjects = [];
        foreach ($agreementCollection as $agreement) {
            $agreementDataObjects[] = $agreement;
        }

        return $agreementDataObjects;

    }


    public function getCustomerTerms()
    {
        $decodeTerms = [];
        $customerTerms = $this->_customerSession->getCustomer()->getData('terms');
        if($customerTerms){
            $decodeTerms = json_decode($customerTerms,true);
        }

        return $decodeTerms;

    }


}