<?php
/**
 * Created by Kenneth.
 * Date: 2018-10-16
 * Time: 오전 11:40
 */
namespace Eguana\Terms\Helper;

use Eguana\Terms\Model\CheckoutAgreementsRepository;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Customer\Model\Session;
use Magento\Customer\Api\CustomerRepositoryInterface;




class Data extends AbstractHelper
{
    const TERMS = 'terms';

    protected $_checkoutAgreementsRepository;

    protected $_customerSession;

    protected $_customerRepository;




    public function __construct(
        Context $context,
        CheckoutAgreementsRepository $checkoutAgreementsRepository,
        Session $customerSession,
        CustomerRepositoryInterface $customerRepository
    )
    {
        $this->_checkoutAgreementsRepository = $checkoutAgreementsRepository;
        $this->_customerSession = $customerSession;
        $this->_customerRepository = $customerRepository;
        parent::__construct($context);
    }

    public function updateCustomerTerms()
    {
        if($this->_customerSession->isLoggedIn()){
            $requereAgreement = $this->_checkoutAgreementsRepository->getList(true, true);
            $decodeCustomerTerms = $this->_checkoutAgreementsRepository->getCustomerTerms();
            $custmoerId = $this->_customerSession->getId();


            $arrAgreement = [];

            foreach ($requereAgreement as $checklist){
                $arrAgreement[] = $checklist->getAgreementId();
            }


            $arrMergeAgreement = array_merge($decodeCustomerTerms, $arrAgreement);
            $encodeAgreement = json_encode($arrMergeAgreement);

            $customer = $this->_customerRepository->getById($custmoerId);

            $customer->setCustomAttribute('terms', $encodeAgreement);
            $this->_customerRepository->save($customer);
        }
    }


}