<?php
/**
 * Created by PhpStorm.
 * User: sonia
 * Date: 18. 10. 15
 * Time: 오후 4:07
 */
namespace Eguana\ISAC\Plugin\Controller\Adminhtml\System\Account;

use Magento\Backend\Controller\Adminhtml\System\Account\Save;

class SavePlugin
{

    public function afterExecute(Save $subject, $result)
    {
        return $result->setPath("*/auth/logout");
    }
}