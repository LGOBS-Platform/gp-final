<?php
/**
 * Created by PhpStorm.
 * User: sonia
 * Date: 18. 10. 22
 * Time: 오전 6:50
 */

namespace Eguana\ISAC\Helper;


use Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{
    public function getExpirationPeriod()
    {
        return $this->scopeConfig->getValue('admin/security/user_inactivation_interval');
    }

    public function isExpiredUserInactivationEnabled()
    {
        return $this->scopeConfig->getValue('admin/security/enable_expired_user_inactivation');
    }

}