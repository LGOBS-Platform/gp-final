<?php
/**
 * Created by PhpStorm.
 * User: sonia
 * Date: 18. 10. 22
 * Time: 오전 6:39
 */

namespace Eguana\ISAC\Cron;


use Magento\Framework\Stdlib\DateTime\DateTime;
use Psr\Log\LoggerInterface;

class InactivateExpiredUser
{
    /**
     * @var \Eguana\ISAC\Helper\Data
     */
    protected $helper;
    /**
     * @var LoggerInterface
     */
    protected $logger;
    /**
     * @var DateTime
     */
    protected $dateTime;
    /**
     * @var \Magento\User\Model\ResourceModel\User\CollectionFactory
     */
    protected $collectionFactory;

    public function __construct(
        \Magento\User\Model\ResourceModel\User\CollectionFactory $collectionFactory,
        \Eguana\ISAC\Helper\Data $helper,
        LoggerInterface $logger,
        DateTime $dateTime
    )
    {
        $this->helper = $helper;
        $this->logger = $logger;
        $this->dateTime = $dateTime;
        $this->collectionFactory = $collectionFactory;
    }

    public function execute()
    {
        if (!$this->isExpiredUserInactivationEnabled())
        {
            return $this;
        }
        /** @var \Magento\User\Model\ResourceModel\User\Collection $users */
        $users = $this->collectionFactory->create();
        $users->addFieldToSelect(['username', 'logdate', 'is_active'])
            ->addFieldToFilter('is_active', ['eq' => 1])
            ->addFieldToFilter('logdate', ['lt' => $this->dateTime->date('Y-m-d h:i:s', strtotime($this->getExpirationPeriod()))]);
        try {
            $users->setDataToAll('is_active', 0)->save();
        } catch (\Exception $e)
        {
            $this->logger->critical($e);
        }

    }

    protected function getExpirationPeriod()
    {
        return '-' . $this->helper->getExpirationPeriod(). ' months' ;
    }

    protected function isExpiredUserInactivationEnabled()
    {
        return $this->helper->isExpiredUserInactivationEnabled();
    }

}