<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Eguana\CustomCatalog\ViewModel\Product;

use Magento\Catalog\Helper\Data;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\DataObject;
use Magento\Framework\View\Element\Block\ArgumentInterface;

/**
 * Product breadcrumbs view model.
 */
class Breadcrumbs extends \Magento\Catalog\ViewModel\Product\Breadcrumbs
{
    /**
     * Catalog data.
     *
     * @var Data
     */
    private $catalogData;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @param Data $catalogData
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        Data $catalogData,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->catalogData = $catalogData;
        $this->scopeConfig = $scopeConfig;
        parent::__construct($catalogData,$scopeConfig);
    }

    /**
     * Returns product name.
     *
     * @return string
     */
    public function getProductSku()
    {
        return $this->catalogData->getProduct() !== null
            ? $this->catalogData->getProduct()->getModelName()
            : '';
    }
}
