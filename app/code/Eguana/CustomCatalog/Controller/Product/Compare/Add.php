<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Eguana\CustomCatalog\Controller\Product\Compare;


use Magento\Framework\Exception\NoSuchEntityException;

class Add extends \Magento\Catalog\Controller\Product\Compare\Add
{

    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        if (!$this->_formKeyValidator->validate($this->getRequest())) {
            return $resultRedirect->setRefererUrl();
        }

        $productId = (int)$this->getRequest()->getParam('product');
        if ($productId && ($this->_customerVisitor->getId() || $this->_customerSession->isLoggedIn())) {
            $storeId = $this->_storeManager->getStore()->getId();
            try {
                $product = $this->productRepository->getById($productId, false, $storeId);
            } catch (NoSuchEntityException $e) {
                $product = null;
            }

            if ($product) {
                $arrayMerge = [];

                $checkData = $this->_objectManager->get(\Magento\Catalog\Helper\Product\Compare::class)->getItemCollection();

                foreach ($checkData as $data)
                {
                    $childCategoryIds = array_merge($arrayMerge, $this->getCategory($data->getData('category_ids')));
                    $arrayMerge = $childCategoryIds;
                }

                $currentProductCatIds = $this->getCategory($product->getData('category_ids'));

                if($arrayMerge==NULL)
                {
                    $this->_catalogProductCompareList->addProduct($product);
                    $productName = $this->_objectManager->get(
                        \Magento\Framework\Escaper::class
                    )->escapeHtml($product->getName());
                    $this->messageManager->addSuccess(__('You added product %1 to the comparison list.', $productName));
                    $this->_eventManager->dispatch('catalog_product_compare_add_product', ['product' => $product]);
                }else if(count($arrayMerge)>0)
                {
                    if(!array_intersect($arrayMerge,$currentProductCatIds))
                    {
                        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                        $message = $objectManager->create('Magento\Framework\Message\ManagerInterface');
                        $message->addWarningMessage(__('Current product category is not same with your compare list'));
                    }
                    else{
                        $this->_catalogProductCompareList->addProduct($product);
                        $productName = $this->_objectManager->get(
                            \Magento\Framework\Escaper::class
                        )->escapeHtml($product->getName());
                        $this->messageManager->addSuccess(__('You added product %1 to the comparison list.', $productName));
                        $this->_eventManager->dispatch('catalog_product_compare_add_product', ['product' => $product]);
                    }
                }

            }

            $this->_objectManager->get(\Magento\Catalog\Helper\Product\Compare::class)->calculate();
        }
        return $resultRedirect->setRefererOrBaseUrl();
    }


    protected function getCategory($categoryIds)
    {
        $childCategory = [];
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        foreach ($categoryIds as $categoryId)
        {
            $category = $objectManager->create('Magento\Catalog\Model\Category')->load($categoryId);
            $level = $category->getLevel();

            if($level == 3) // level 3 == child category / level 2 == parent category
            {
                $childCategory[] = $categoryId;
            }
        }

        return $childCategory;
    }

}
