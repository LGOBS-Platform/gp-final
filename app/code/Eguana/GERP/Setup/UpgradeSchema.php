<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-08-27
 * Time: 오전 10:33
 */

namespace Eguana\GERP\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.1.1', '<')) {
            $this->_backInterfaceColumnAdd($setup);
        }

        if (version_compare($context->getVersion(), '1.1.2', '<')) {
            $this->_rmatableColumnAdd($setup);
        }

        if (version_compare($context->getVersion(), '1.1.4', '<')) {
            $this->_rmaGridTableColumnAdd($setup);
        }

        if (version_compare($context->getVersion(), '1.1.5', '<')) {
            $this->_priceDataTableAdd($setup);
        }

        if (version_compare($context->getVersion(), '1.1.6', '<')) {
            $this->_priceDataColumnAdd($setup);
        }

        if (version_compare($context->getVersion(), '1.1.7', '<')) {
            $this->_priceDataUpdateColumnAdd($setup);
        }

        if (version_compare($context->getVersion(), '1.1.8', '<')) {
            $this->_priceDataColumnFix($setup);
        }

        if (version_compare($context->getVersion(), '1.1.9', '<')) {
            $this->_stockUpdateTableAdd($setup);
        }

        $setup->endSetup();
    }

    protected function _rmaGridTableColumnAdd(SchemaSetupInterface $setup){

        /**
         * @var $installer \Magento\Framework\Setup\SchemaSetupInterface
         */
        $setup->getConnection()->addColumn(
            $setup->getTable('magento_rma_grid'),'omv_header_id',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => 100,
                'nullable' => true,
                'comment' => 'GERP Omv Header ID'
            ]
        );


        /**
         * @var $installer \Magento\Framework\Setup\SchemaSetupInterface
         */
        $setup->getConnection()
            ->addColumn(
                $setup->getTable('magento_rma_grid'),'gerp_header_id',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 100,
                    'nullable' => true,
                    'comment' => 'GERP Header ID'
                ]
            );
    }

    protected function _rmatableColumnAdd(SchemaSetupInterface $setup){

        /**
         * @var $installer \Magento\Framework\Setup\SchemaSetupInterface
         */
        $setup->getConnection()->addColumn(
            $setup->getTable('magento_rma'),'omv_header_id',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => 100,
                'nullable' => true,
                'comment' => 'GERP Omv Header ID'
            ]
        );


        /**
         * @var $installer \Magento\Framework\Setup\SchemaSetupInterface
         */
        $setup->getConnection()
            ->addColumn(
                $setup->getTable('magento_rma'),'gerp_header_id',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 100,
                    'nullable' => true,
                    'comment' => 'GERP Header ID'
                ]
            );

        /**
         * @var $installer \Magento\Framework\Setup\SchemaSetupInterface
         */
        $setup->getConnection()
            ->addColumn(
                $setup->getTable('magento_rma_item_entity'),'gerp_line_id',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 100,
                    'nullable' => true,
                    'comment' => 'GERP LINE ID'
                ]
            );

        /**
         * @var $installer \Magento\Framework\Setup\SchemaSetupInterface
         */
        $setup->getConnection()
            ->addColumn(
                $setup->getTable('magento_rma_item_entity'),'erp_order_no',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 100,
                    'nullable' => true,
                    'comment' => 'ERP Order No'
                ]
            );
    }

    protected function _backInterfaceColumnAdd(SchemaSetupInterface $setup){

        $setup->getConnection()
            ->addColumn(
                $setup->getTable('sales_order_item'),'back_interface',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 1,
                    'nullable' => true,
                    'comment' => 'Back Interface'
                ]
            );

        $setup->getConnection()
            ->addColumn(
                $setup->getTable('magento_rma_item_entity'),'interface_status',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 2,
                    'nullable' => true,
                    'comment' => 'Interface Interface'
                ]
            );

        $setup->getConnection()
            ->addColumn(
                $setup->getTable('magento_rma_item_entity'),'back_interface',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 1,
                    'nullable' => true,
                    'comment' => 'Back Interface'
                ]
            );
    }

    protected function _priceDataTableAdd(SchemaSetupInterface $setup){
        $table = $setup->getConnection()
            ->newTable($setup->getTable('eguana_gerp_price_date'))
            ->addColumn(
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false, 'primary' => true, 'identity' =>true],
                'Entity ID'
            )
            ->addColumn(
                'apply_date',
                \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                null,
                ['unsigned' => true, 'nullable' => false],
                'Apply Date'
            )
            ->addColumn(
                'expiration_date',
                \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                null,
                ['nullable' => false, 'unsigned' => true],
                'Expiration Date'
            )
            ->addColumn(
                'unit_price',
                \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                null,
                ['nullable' => false, 'default' => '0'],
                'Unit Price'
            )
            ->addColumn(
                'rrp_price',
                \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                null,
                ['nullable' => false, 'default' => '0'],
                'RRP Price'
            )
            ->addColumn(
                'sku',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'SKU'
            )
            ->addColumn(
                'status',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                ['nullable' => true],
                'Status'
            )
            ->setComment('Eguana GERP price update data for Cron');

        $setup->getConnection()->createTable($table);
    }

    protected function _priceDataColumnAdd(SchemaSetupInterface $setup){

        /**
         * @var $installer \Magento\Framework\Setup\SchemaSetupInterface
         */
        $setup->getConnection()->addColumn(
            $setup->getTable('eguana_gerp_price_date'),'locale_code',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => 100,
                'nullable' => true,
                'comment' => 'Locale Code'
            ]
        );

        /**
         * @var $installer \Magento\Framework\Setup\SchemaSetupInterface
         */
        $setup->getConnection()->addColumn(
            $setup->getTable('eguana_gerp_price_date'),'creation_date',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                'nullable' => false,
                'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT,
                'comment' => 'Date Time'
            ]
        );

        /**
         * @var $installer \Magento\Framework\Setup\SchemaSetupInterface
         */
        $setup->getConnection()->addColumn(
            $setup->getTable('eguana_gerp_price_date'),'update_date',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                'nullable' => false,
                'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE,
                'comment' => 'Update Time'
            ]
        );
    }

    protected function _priceDataUpdateColumnAdd(SchemaSetupInterface $setup){
        /**
         * @var $installer \Magento\Framework\Setup\SchemaSetupInterface
         */
        $setup->getConnection()->addColumn(
            $setup->getTable('eguana_gerp_price_date'),'gerp_data',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' =>'2M',
                'nullable' => true,
                'comment' => 'Data'
            ]
        );
    }

    protected function _priceDataColumnFix(SchemaSetupInterface $setup){
        /**
         * @var $installer \Magento\Framework\Setup\SchemaSetupInterface
         */
        $setup->getConnection()->changeColumn(
            $setup->getTable('eguana_gerp_price_date'),'unit_price','unit_price',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_FLOAT,
                'nullable' => false,
                'default' => '0'
            ]
        );

        /**
         * @var $installer \Magento\Framework\Setup\SchemaSetupInterface
         */
        $setup->getConnection()->changeColumn(
            $setup->getTable('eguana_gerp_price_date'),'rrp_price','rrp_price',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_FLOAT,
                'nullable' => false,
                'default' => '0'
            ]
        );
    }

    protected function _stockUpdateTableAdd(SchemaSetupInterface $setup){

        $table = $setup->getConnection()
            ->newTable($setup->getTable('eguana_gerp_stock_update'))
            ->addColumn(
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false, 'primary' => true, 'identity' =>true],
                'Entity ID'
            )
            ->addColumn(
                'sku',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'SKU'
            )
            ->addColumn(
                'stock',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['nullable' => true],
                'Reserved Stock'
            )
            ->addColumn(
                'org_code',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                10,
                ['nullable' => false],
                'ORG Code'
            )
            ->addColumn(
                'end_date',
                \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                null,
                ['unsigned' => true, 'nullable' => true],
                'End Date'
            )
            ->addColumn(
                'status',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                ['nullable' => true],
                'Status'
            )
            ->addColumn(
                'affiliate_code',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                ['nullable' => true],
                'affiliate_code'
            )
            ->addColumn(
                'bill_to_code',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                ['nullable' => true],
                'bill_to_code'
            )
            ->addColumn(
                'store_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'store_id'
            )
            ->addColumn(
                'gerp_creation_date',
                \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                null,
                [
                    'unsigned' => true,
                    'nullable' => true,

                ],
                'GERP Log Creation Date'
            )
            ->addColumn(
                'gerp_last_update_date',
                \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                null,
                [
                    'unsigned' => true,
                    'nullable' => true,

                ],
                'GERP Log Last Update Date'
            )
            ->addColumn(
                'creation_date',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                [
                    'unsigned' => true,
                    'nullable' => false,
                    'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT
                ],
                'Creation Date'
            )
            ->addColumn(
                'update_date',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                [
                    'unsigned' => true,
                    'nullable' => false,
                    'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE
                ],
                'Update Date'
            )
            ->addColumn(
                'gerp_data',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                ['nullable' => true],
                'GERP Data'
            )->addIndex(
                $setup->getIdxName(
                    'eguana_gerp_stock_update',
                    ['sku'],
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
                ),
                ['sku'],
                ['type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE]
            )
            ->setComment('Eguana GERP stock update data for Cron');

        $setup->getConnection()->createTable($table);
    }
}