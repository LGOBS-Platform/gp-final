<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-10-18
 * Time: 오후 2:28
 */

namespace Eguana\GERP\Setup;


use Magento\Eav\Api\Data\AttributeOptionInterface;
use Magento\Eav\Api\Data\AttributeOptionLabelInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Catalog\Setup\CategorySetupFactory;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Eav\Model\Config;


class UpgradeData implements UpgradeDataInterface
{
    /** @var \Magento\Catalog\Setup\CategorySetupFactory */
    protected $_categorySetupFactory;

    /** @var Config */
    protected $_eavConfig;

    /** @var \Magento\Customer\Model\GroupFactory */
    protected $_customerGroupFactory;

    protected $_option;

    protected $_storeLabel;

    public function __construct(
        CategorySetupFactory $categorySetupFactory,
        Config $eavConfig,
        AttributeOptionInterface $optionInterface,
        AttributeOptionLabelInterface $optionLabelInterface
    )
    {
        $this->_categorySetupFactory = $categorySetupFactory;
        $this->_eavConfig = $eavConfig;
        $this->_option = $optionInterface;
        $this->_storeLabel = $optionLabelInterface;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $setup->endSetup();

    }

    public function _rmaStatusUpdate($setup){

    }

}