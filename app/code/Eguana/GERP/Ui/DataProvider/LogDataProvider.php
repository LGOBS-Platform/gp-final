<?php
/**
* Copyright 2016 aheadWorks. All rights reserved.
* See LICENSE.txt for license details.
*/

namespace Eguana\GERP\Ui\DataProvider;

use Magento\Framework\View\Element\UiComponent\DataProvider\DataProviderInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Magento\Framework\App\RequestInterface;
use Eguana\GERP\Model\ResourceModel\Log\Grid\CollectionFactory;

/**
 * Class BannerDataProvider
 * @package Eguana\Slider\Model\Banner
 */
class LogDataProvider extends AbstractDataProvider implements DataProviderInterface
{
    /**
     * @var RequestInterface
     */
    private $request;

    private $_coreRegistry;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param RequestInterface $request
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        RequestInterface $request,
        \Magento\Framework\Registry $coreRegistry,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
        $this->request = $request;
        $this->_coreRegistry = $coreRegistry;
    }

    /**
     * {@inheritdoc}
     */
    public function getData()
    {
        $data = [];
        $gerpLogModel = $this->_coreRegistry->registry('eguana_gerp_log');

        if (!empty($gerpLogModel)) {
            $data[$gerpLogModel->getId()] = $gerpLogModel->getData();
        } else {
            $id = $this->request->getParam($this->getRequestFieldName());
            /** @var \Eguana\Slider\Model\Banner $slide */
            foreach ($this->getCollection()->getItems() as $log) {
                if ($id == $log->getId()) {
                    $data[$id] = $log->getData();
                }
            }
        }
        return $data;
    }
}
