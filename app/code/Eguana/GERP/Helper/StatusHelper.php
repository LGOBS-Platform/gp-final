<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-10-05
 * Time: 오후 5:22
 */

namespace Eguana\GERP\Helper;


use Eguana\GERP\Model\Source\Status;
use Magento\Framework\App\Helper\AbstractHelper;

class StatusHelper extends AbstractHelper
{
    protected $_status;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        Status $status)
    {
        $this->_status = $status;
        parent::__construct($context);
    }

    public function getLabelByValue($value){
        return $this->_status->getOptionText($value);
    }

}