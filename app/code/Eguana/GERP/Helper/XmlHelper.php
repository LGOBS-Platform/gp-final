<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-10-05
 * Time: 오후 5:22
 */

namespace Eguana\GERP\Helper;


use Magento\Framework\View\Design\FileResolution\Fallback\Resolver\Simple;
use Magento\Store\Model\StoreManagerInterface;

class XmlHelper extends Data
{

    public function arrayToXml($array , $xml = null){

        if(is_null($xml)){
            $key = key($array);
            $xml = $this->_createSimpleXml($key);
            $array = $array[$key];
        }

        foreach ($array as $key => $data){

            if(is_array($data)){
                if(is_int($key)){
                    $this->arrayToXml($data,$xml);
                }else{
                    $childXml = $xml->addChild($key);
                    $this->arrayToXml($data,$childXml);
                }
            }else{
                $xml->addChild($key,$data);
            }

        }

        return $xml;
    }

    protected function _createSimpleXml($key){
        return new \SimpleXMLElement("<$key/>");
    }

}