<?php

namespace Eguana\GERP\Cron;
use Eguana\GERP\Helper\Data;
use Magento\Framework\ObjectManagerInterface;

/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-10-15
 * Time: 오후 3:06
 */
class AbstractCron
{
    protected $_dataHelper;

    protected $_objectManager;

    protected $_documentType = '';

    public function __construct(
        Data $dataHelper,
        ObjectManagerInterface $objectManager
    )
    {
        $this->_dataHelper = $dataHelper;
        $this->_objectManager = $objectManager;
    }

    public function execute(){

        $gerpWebsiteConfig = $this->_getDataHelper()->getGerpWebsiteData();
        $gerpInterfaceConfig = $this->_getDataHelper()->getGerpInterfaceConfig();
        $interface = $this->_documentType;

        $object = $this->_getInstance($gerpInterfaceConfig[$interface]['object']);
        $configArea = $gerpInterfaceConfig[$interface]['config_area'];

        foreach ($gerpWebsiteConfig as $websiteConfig){
            $websiteId = $websiteConfig['website_id'];
            $websiteCode = $websiteConfig['website_code'];
            if($this->_precessWebsiteEnabled($interface,$websiteId,$configArea)){
                $object->setWebsiteCode($websiteCode);
                $object->execute();
            }

        }

    }

    protected function _precessWebsiteEnabled($documentType,$websiteId,$configArea){
        if($configArea == 'product'){
            return $this->_getDataHelper()->getGerpProductConfig(strtolower($documentType).'_enabled',$websiteId);
        }elseif ($configArea == 'order'){
            return $this->_getDataHelper()->getGerpOrderConfig(strtolower($documentType).'_enabled',$websiteId);
        }
    }

    protected function _getDocumentType(){
        return $this->_documentType;
    }

    protected function _getDataHelper(){
        return $this->_dataHelper;
    }

    protected function _getInstance($type){
        return $this->_objectManager->get($type);
    }

}