<?php
/**
 * Created by ${Eguana}.
 * User: ${glenn}
 * Date: 2019-02-13
 * Time: 오전 9:17
 */

namespace Eguana\GERP\Cron\Product\OMD;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Exception\NoSuchEntityException;
use Amasty\MultiInventory\Model\WarehouseRepository;

class StockUpdate
{
    protected $_resource;

    protected $_productRepository;

    protected $_warehouseRepository;

    public function __construct(
        ResourceConnection $productResource,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        WarehouseRepository $warehouseRepository
    )
    {
        $this->_resource = $productResource;
        $this->_productRepository = $productRepository;
        $this->_warehouseRepository = $warehouseRepository;
    }

    public function execute()
    {
        $resource = $this->_resource;
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('eguana_gerp_stock_update');
        $productIdArray = [];

        $writer = new \Zend\Log\Writer\Stream(BP . sprintf('/var/log/gerp_stock_update.%s.log', date('y-m-d')));
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);


        $sql = "Select sku,store_id,stock,org_code FROM $tableName";
        $result = $connection->fetchAll($sql);

        $omvProductId = $this->exceptOMV($connection);

        foreach ($result as $key => $data) {
            try {
                $product = $this->_productRepository->get($data['sku'], false, $data['store_id']);
                $this->updateStock($connection, $product->getId(), $data['stock'], $data['sku'], $omvProductId, $data['org_code']);
            } catch (NoSuchEntityException $e) {
                $logger->info($e->getMessage());
                continue;
            }
        }
    }

    protected function exceptOMV($connection)
    {
        $warehouseTableName = 'amasty_multiinventory_warehouse_item';
        $omvProductIds = [];
        $select = "SELECT DISTINCT warehouse.product_id FROM $warehouseTableName as warehouse LEFT JOIN catalog_product_entity as cpe ON warehouse.product_id = cpe.entity_id LEFT JOIN catalog_product_entity_varchar as cpev ON cpev.row_id = cpe.row_id AND cpev.attribute_id=526 WHERE warehouse.product_id in (select product_id from catalog_product_website where website_id = 2) AND (cpev.value='OMV' OR cpev.value is NULL)";

        $exceptOMV = $connection->fetchAll($select);

        foreach ($exceptOMV as $key => $value)
        {
            $omvProductIds[] = $value['product_id'];
        }

        $omvProductId = implode(',',$omvProductIds);

        return $omvProductId;
    }

    protected function updateStock($connection,$productId,$stock,$sku,$omvProductId,$org_code)
    {
        $warehouseTableName = 'amasty_multiinventory_warehouse_item';
        $stockStatusTableName ='eguana_gerp_stock_update';

        $status='';
        $warehouse_id = $this->getWarehouseId($org_code);

        $select = "SELECT status FROM $stockStatusTableName WHERE sku = '$sku'";
        $selectResult = $connection->fetchAll($select);

        foreach ($selectResult as $key => $data)
        {
            $status = $data['status'];
        }

        if($status == 'N')
        {
            $statusN = "UPDATE $warehouseTableName SET qty=$stock,available_qty=$stock,stock_status=1 WHERE product_id =$productId AND product_id not in ($omvProductId) AND warehouse_id = $warehouse_id";
            $stockUpdateStatus = $connection->query($statusN);
        }else{
            $statusY = "UPDATE $warehouseTableName SET qty=0,available_qty=0,stock_status=0 WHERE product_id =$productId AND product_id not in ($omvProductId) AND warehouse_id = $warehouse_id";
            $stockUpdateStatus = $connection->query($statusY);
        }

        if($stockUpdateStatus)
        {
            $updateStatus = "UPDATE $stockStatusTableName SET status='Y' WHERE sku='$sku'";
            $this->updateInventoryTotalStock($connection,$productId,$omvProductId);
        }else{
            $updateStatus = "UPDATE $stockStatusTableName SET status='E' WHERE sku='$sku'";
        }

        $result = $connection->query($updateStatus);

        return $result;
    }

    protected function getWarehouseId($org_code)
    {
        try{
            $warehouse = $this->_warehouseRepository->getByCode($org_code);
            $warehouse_id = $warehouse->getId();

            return $warehouse_id;
        }catch (\Exception $e){
            $e->getMessage();
        }
    }

    protected function updateInventoryTotalStock($connection,$productId,$omvProductId)
    {
        $warehouseTableName = 'amasty_multiinventory_warehouse_item';
        $qty=0;
        $available_qty=0;

        $select = "SELECT qty,available_qty FROM $warehouseTableName WHERE product_id = $productId AND product_id not in ($omvProductId) AND warehouse_id !=1";
        $fetch = $connection->fetchAll($select);

        foreach($fetch as $key => $data)
        {
            $qty += (float)$data['qty'];
            $available_qty += (float)$data['available_qty'];
        }

        $update = "UPDATE $warehouseTableName SET qty = '$qty', available_qty='$available_qty',stock_status=1 WHERE product_id = $productId AND product_id not in ($omvProductId) AND warehouse_id =1";
        $result = $connection->query($update);

        return $result;
    }
}
