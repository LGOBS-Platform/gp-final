<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-10-15
 * Time: 오후 3:09
 */

namespace Eguana\GERP\Cron\Product\OMD;


use Eguana\GERP\Cron\AbstractCron;

class InvPto extends AbstractCron
{
    protected $_documentType = 'CXML_PTO_MDM';
}