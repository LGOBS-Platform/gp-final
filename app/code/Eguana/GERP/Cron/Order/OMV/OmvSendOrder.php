<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-10-15
 * Time: 오후 3:09
 */

namespace Eguana\GERP\Cron\Order\OMV;


class OmvSendOrder extends \Eguana\GERP\Cron\AbstractCron
{
    protected $_documentType = 'CXML_ACC_ORDREQ';
}