<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-10-04
 * Time: 오후 4:05
 */

namespace Eguana\GERP\Controller\Webmethod;


use Eguana\GERP\Helper\Data;
use Eguana\GERP\Model\Log;
use Eguana\GERP\Model\LogFactory;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;

class Call extends Action
{
    protected $_dataHelper;

    protected $_gerpLogModelFactory;

    protected $_queryString = [];

    protected $_gerpInterfaceConfig;

    public function __construct(
        Context $context,
        Data $dataHelper,
        LogFactory $gerpLogModelFactory
    )
    {
        $this->_dataHelper = $dataHelper;
        $this->_gerpLogModelFactory = $gerpLogModelFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $xml = $this->getRequestRawData();
        $requestObject = simplexml_load_string($xml);
        $websiteCode = null;
        $object = null;

        $writer = new \Zend\Log\Writer\Stream(BP . sprintf('/var/log/gerp_webmethod.%s.log',date('y-m-d')));
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info(__METHOD__);
        $logger->info($xml);

        $headerData = $requestObject->MessageHeader;
        $bodyDataList = $requestObject->MessageBody->ContentList;

        if(isset($headerData->DocumentType)){
            $object = $this->_objectManager
                ->get($this->_getGerpInterfaceConfig()["$headerData->DocumentType"]['object']);
        }

        foreach ($bodyDataList as $bodyData){

            $websiteScope = null;

            if(!is_null($object)){
                $websiteScope = $object->getDataWebsiteScope($bodyData);
            }

            $this->_gerpLogSave($headerData,$bodyData,$websiteScope);
        }

        echo true;die;
    }

    protected function _getGerpInterfaceConfig(){

        if(is_null($this->_gerpInterfaceConfig)){
            $this->_gerpInterfaceConfig = $this->_getDataHelper()->getGerpInterfaceConfig();
        }

        return $this->_gerpInterfaceConfig;
    }

    protected function _gerpLogSave($headerData,$bodyData,$websiteCode = null){

        /** @var $gerpLogModel Log */
        $gerpLogModel = $this->_gerpLogModelFactory->create();
        $config = $this->_getGerpInterfaceConfig();
        $interfaceId = $headerData->InterfaceID;
        $senderId = $headerData->SenderID;
        $receiverId = $headerData->ReceiverID;
        $documentType = strval($headerData->DocumentType);

        if(isset($config[$documentType])) {
            $currentConfig = $config[$documentType];
            $gerpLogModel->setGERPFlag($currentConfig['flag']);
            $gerpLogModel->setIdentifierName($currentConfig['identifier_name']);
            $gerpLogModel->setIdentifier($bodyData->{$currentConfig['identifier_name']});
            $gerpLogModel->setSubIdentifierName($currentConfig['sub_identifier_name']);
            $gerpLogModel->setSubIdentifier($bodyData->{$currentConfig['sub_identifier_name']});
            $gerpLogModel->setWebsiteCode($websiteCode);
        }else{
            $gerpLogModel->setIdentifierName('DocumentType');
            $gerpLogModel->setIdentifier($documentType);
        }

        $gerpLogModel->setInterfaceId($interfaceId);
        $gerpLogModel->setSenderId($senderId);
        $gerpLogModel->setReceiverId($receiverId);
        $gerpLogModel->setGERPData(json_encode($bodyData,true));

        $gerpLogModel->save();


    }

    protected function _getDataHelper(){
        return $this->_dataHelper;
    }

    private function getRequestRawData(){
        return file_get_contents("php://input");
    }

}