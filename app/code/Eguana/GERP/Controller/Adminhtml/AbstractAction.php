<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-10-08
 * Time: 오후 5:08
 */

namespace Eguana\GERP\Controller\Adminhtml;


use Magento\Backend\App\Action;

abstract class AbstractAction extends Action
{

    /**
     * Authorization level of a basic admin session
     */
    const ADMIN_RESOURCE = 'Eguana_GERP::log';



    /**
     * Init page
     *
     * @param \Magento\Backend\Model\View\Result\Page $resultPage
     * @return \Magento\Backend\Model\View\Result\Page
     */
    protected function initPage($resultPage)
    {
        $resultPage->setActiveMenu('Eguana_GERP::log')
            ->addBreadcrumb(__('GERP'), __('GERP'))
            ->addBreadcrumb(__('Log'), __('Log'));
        return $resultPage;
    }
}