<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-10-08
 * Time: 오후 5:10
 */

namespace Eguana\GERP\Controller\Adminhtml\Log;


use Eguana\GERP\Controller\Adminhtml\AbstractAction;
use Magento\Framework\Controller\ResultFactory;

class Index extends AbstractAction
{

    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage
            ->setActiveMenu('Eguana_GERP::log')
            ->getConfig()->getTitle()->prepend(__('Log'));

        return $resultPage;
    }

}