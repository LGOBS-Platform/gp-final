<?php
/**
* Copyright 2016 Eguana. All rights reserved.
* See LICENSE.txt for license details.
*/

namespace Eguana\GERP\Controller\Adminhtml\Log;

use Eguana\GERP\Controller\Adminhtml\AbstractAction;
use Eguana\GERP\Model\Log;
use Magento\Backend\App\Action\Context;

/**
 * Class Delete
 * @package Eguana\GERP\Controller\Adminhtml\Log
 */
class Delete extends AbstractAction
{
    protected $_gerpLogModel;

    public function __construct(
        Context $context,
        Log $gerpLogModel
    ) {
        $this->_gerpLogModel = $gerpLogModel;
        parent::__construct($context);
    }

    /**
     * Delete banner action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = (int)$this->getRequest()->getParam('entity_id');
        if ($id) {
            try {
                $this->_gerpLogModel->load($id)->delete();
                $this->messageManager->addSuccessMessage(__('row was successfully deleted'));
                return $resultRedirect->setPath('*/*/index');
            } catch (\Exception $exception) {
                $this->messageManager->addErrorMessage($exception->getMessage());
            }
        }
        $this->messageManager->addErrorMessage(__('row could not be deleted'));
        return $resultRedirect->setPath('*/*/index');
    }
}
