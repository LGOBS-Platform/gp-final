<?php
/**
* Copyright 2016 Eguana. All rights reserved.
* See LICENSE.txt for license details.
*/

namespace Eguana\GERP\Controller\Adminhtml\Log;

use Eguana\GERP\Controller\Adminhtml\AbstractAction;
use Eguana\GERP\Helper\Data;
use Eguana\GERP\Model\Log;
use Magento\Backend\App\Action\Context;

/**
 * Class Delete
 * @package Eguana\GERP\Controller\Adminhtml\Log
 */
class Exec extends AbstractAction
{
    protected $_dataHelper;

    public function __construct(
        Context $context,
        Data $dataHelper
    )
    {
        $this->_dataHelper = $dataHelper;
        parent::__construct($context);
    }

    public function execute(){

        $gerpWebsiteConfig = $this->_getDataHelper()->getGerpWebsiteData();
        $gerpInterfaceConfig = $this->_getDataHelper()->getGerpInterfaceConfig();
        $resultRedirect = $this->resultRedirectFactory->create();
        $type = $this->_getDocumentType();

        if ($type && isset($gerpInterfaceConfig[$type])) {

            try {
                $object = $this->_getInstance($gerpInterfaceConfig[$type]['object']);

                foreach ($gerpWebsiteConfig as $websiteConfig){
                    $websiteId = $websiteConfig['website_id'];
                    $websiteCode = $websiteConfig['website_code'];

                    if($this->_precessWebsiteEnabled($websiteId,$gerpInterfaceConfig[$type]['config_area'])){
                        $object->setWebsiteCode($websiteCode);
                        $object->execute();
                    }

                }
                $this->messageManager->addSuccessMessage(__('%1 Type Success.',$type));
                return $resultRedirect->setPath('*/*/index');
            } catch (\Exception $exception) {
                $this->messageManager->addErrorMessage($exception->getMessage());
            }
        }
        $this->messageManager->addErrorMessage(__('The type is not set.'));
        return $resultRedirect->setPath('*/*/index');

    }

    protected function _precessWebsiteEnabled($websiteId,$configArea){

        if($configArea == 'order'){
            return $this->_getDataHelper()->getGerpOrderConfig(strtolower($this->_getDocumentType()).'_enabled',$websiteId);
        }else{
            return $this->_getDataHelper()->getGerpProductConfig(strtolower($this->_getDocumentType()).'_enabled',$websiteId);
        }
    }

    protected function _getDocumentType(){
        return $this->getRequest()->getParam('type');
    }

    protected function _getDataHelper(){
        return $this->_dataHelper;
    }

    protected function _getInstance($type){
        return $this->_objectManager->get($type);
    }
}
