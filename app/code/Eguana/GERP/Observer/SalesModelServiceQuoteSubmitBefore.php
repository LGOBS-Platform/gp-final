<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-10-22
 * Time: 오후 3:57
 */

namespace Eguana\GERP\Observer;


use Magento\Framework\Event\ObserverInterface;

class SalesModelServiceQuoteSubmitBefore implements ObserverInterface
{

    /**
     * @var \Magento\Framework\DataObject\Copy
     */
    protected $objectCopyService;


    /**
     * @param \Magento\Framework\DataObject\Copy $objectCopyService
     */
    public function __construct(
        \Magento\Framework\DataObject\Copy $objectCopyService
    )
    {
        $this->objectCopyService = $objectCopyService;
    }

    /**
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /* @var \Magento\Sales\Model\Order $order */
        $order = $observer->getEvent()->getData('order');
        /* @var \Magento\Quote\Model\Quote $quote */
        $quote = $observer->getEvent()->getData('quote');

        $quoteItemById = [];
        foreach ($quote->getAllItems() as $quoteItem){
            $quoteItemById[$quoteItem->getId()] = $quoteItem;
        }

        foreach ($order->getAllItems() as $item){
            $item->setData('product_from',$quoteItemById[$item->getQuoteItemId()]->getData('product_from'));
            $item->setData('delivery_type',$quoteItemById[$item->getQuoteItemId()]->getData('delivery_type'));
            $item->setData('product_carrier',$quoteItemById[$item->getQuoteItemId()]->getData('product_carrier'));
        }

        $this->objectCopyService->copyFieldsetToTarget('sales_convert_quote', 'to_order', $quote, $order);

        return $this;
    }
}