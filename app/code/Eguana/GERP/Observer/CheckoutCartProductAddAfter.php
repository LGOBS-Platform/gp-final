<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-10-22
 * Time: 오후 3:57
 */

namespace Eguana\GERP\Observer;


use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

class CheckoutCartProductAddAfter implements ObserverInterface
{
    protected $_productRepository;

    public function __construct(
        ProductRepository $productRepository
    )
    {
        $this->_productRepository = $productRepository;
    }

    /**
     * @param EventObserver $observer
     * @return void
     */
    public function execute(EventObserver $observer)
    {
        /**
         * @var \Magento\Quote\Model\Quote\Item[] $items
         * @var \Magento\Catalog\Model\Product $product
         */
        $items = $observer->getItems();

        foreach ($items as $item){
            $product = $this->_productRepository->get($item->getSku());

            if($product->getCustomAttribute('product_from')){
                $item->setData('product_from',$product->getCustomAttribute('product_from')->getValue());
            }
            if($product->getCustomAttribute('delivery_type')){
                $item->setData('delivery_type',$product->getCustomAttribute('delivery_type')->getValue());
            }
            if($product->getCustomAttribute('product_carrier')){
                $item->setData('product_carrier',$product->getCustomAttribute('product_carrier')->getValue());
            }
        }


    }
}