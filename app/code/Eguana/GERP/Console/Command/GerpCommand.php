<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Eguana\Gerp\Console\Command;

use Eguana\GERP\Helper\Data;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GerpCommand extends \Symfony\Component\Console\Command\Command
{
    /**
     * @var \Magento\Framework\App\State
     */
    protected $appState;


    protected $_gerpHelper;

    protected $_objectManager;

    /**
     * @param \Magento\Framework\App\State $appState
     */
    public function __construct(
        \Magento\Framework\App\State $appState,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        Data $gerpHelper
    ) {
        $this->appState = $appState;
        $this->_objectManager = $objectManager;
        $this->_gerpHelper = $gerpHelper;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $commandDescription = [];
        foreach ($this->_gerpHelper->getGerpInterfaceConfig() as $item){
            $commandDescription[] = $item['document_type'];
        }

        $this->setName('gerp')
            ->setDescription('INTERFACE : '.implode(',',$commandDescription));
        $this->addArgument('interface');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->appState->setAreaCode(\Magento\Framework\App\Area::AREA_GLOBAL);

        $interface = $input->getArgument('interface');

        $gerpWebsiteConfig = $this->_getDataHelper()->getGerpWebsiteData();
        $gerpInterfaceConfig = $this->_getDataHelper()->getGerpInterfaceConfig();

        if(!$interface){
            $output->writeln('Interface Argument Require Field.');
            return;
        }


        $object = $this->_getInstance($gerpInterfaceConfig[$interface]['object']);
        $configArea = $gerpInterfaceConfig[$interface]['config_area'];

        foreach ($gerpWebsiteConfig as $websiteConfig){
            $websiteId = $websiteConfig['website_id'];
            $websiteCode = $websiteConfig['website_code'];
            if($this->_precessWebsiteEnabled($interface,$websiteId,$configArea)){
                $object->setWebsiteCode($websiteCode);
                $object->execute();
            }

        }

    }

    protected function _precessWebsiteEnabled($documentType,$websiteId,$configArea){
        if($configArea == 'product'){
            return $this->_getDataHelper()->getGerpProductConfig(strtolower($documentType).'_enabled',$websiteId);
        }elseif ($configArea == 'order'){
            return $this->_getDataHelper()->getGerpOrderConfig(strtolower($documentType).'_enabled',$websiteId);
        }
    }

    protected function _getDataHelper(){
        return $this->_gerpHelper;
    }

    protected function _getInstance($type){
        return $this->_objectManager->get($type);
    }
}
