<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Eguana\GERP\Block\Adminhtml\Rma\Edit\Tab\Items;
use Eguana\GERP\Model\Source\Status;

/**
 * Admin RMA create order grid block
 *
 * @api
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @since 100.0.2
 */
class Grid extends \Magento\Rma\Block\Adminhtml\Rma\Edit\Tab\Items\Grid
{
    protected $_interfaceStatus;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Magento\Rma\Model\Item\Status $itemStatus
     * @param \Magento\Rma\Model\ResourceModel\Item\Attribute\Collection $attributeCollection,
     * @param \Magento\Rma\Helper\Eav $rmaEav
     * @param \Magento\Framework\Registry $coreRegistry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Rma\Model\Item\Status $itemStatus,
        \Magento\Rma\Model\ResourceModel\Item\Attribute\Collection $attributeCollection,
        \Magento\Rma\Helper\Eav $rmaEav,
        \Magento\Framework\Registry $coreRegistry,
        Status $interfaceStatus,
        array $data = []
    ) {
        $this->_interfaceStatus = $interfaceStatus;
        parent::__construct($context, $backendHelper, $itemStatus,$attributeCollection,$rmaEav,$coreRegistry,$data);
    }

    /**
     * Prepare columns
     *
     * @return \Magento\Backend\Block\Widget\Grid\Extended
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns()
    {
        $rma = $this->_coreRegistry->registry('current_rma');
        if ($rma && ($rma->getStatus() === \Magento\Rma\Model\Rma\Source\Status::STATE_CLOSED ||
                $rma->getStatus() === \Magento\Rma\Model\Rma\Source\Status::STATE_PROCESSED_CLOSED)
        ) {
            $this->_itemStatus->setOrderIsClosed();
        }

        $this->addColumn(
            'product_admin_name',
            [
                'header' => __('Product'),
                'type' => 'text',
                'index' => 'product_admin_name',
                'escape' => true,
                'header_css_class' => 'col-product',
                'column_css_class' => 'col-product'
            ]
        );

        $this->addColumn(
            'product_admin_sku',
            [
                'header' => __('SKU'),
                'type' => 'text',
                'index' => 'product_admin_sku',
                'header_css_class' => 'col-sku',
                'column_css_class' => 'col-sku'
            ]
        );

        $this->addColumn(
            'back_interface',
            [
                'header' => __('BACK INTERFACE'),
                'type' => 'text',
                'index' => 'back_interface',
                'header_css_class' => 'col-sku',
                'column_css_class' => 'col-sku'
            ]
        );

        $this->addColumn(
            'interface_status',
            [
                'header' => __('INTERFACE STATUS'),
                'type' => 'options',
                'index' => 'interface_status',
                'options' => $this->_interfaceStatus->getValues(),
                'header_css_class' => 'col-sku',
                'column_css_class' => 'col-sku'
            ]
        );

        $this->addColumn(
            'erp_order_no',
            [
                'header' => __('ERP ORDER NO'),
                'type' => 'text',
                'index' => 'erp_order_no',
                'header_css_class' => 'col-sku',
                'column_css_class' => 'col-sku'
            ]
        );

        //Renderer puts available quantity instead of order_item_id
        $this->addColumn(
            'qty_ordered',
            [
                'header' => __('Remaining'),
                'getter' => [$this, 'getRemainingQty'],
                'renderer' => \Magento\Rma\Block\Adminhtml\Rma\Edit\Tab\Items\Grid\Column\Renderer\Quantity::class,
                'index' => 'qty_ordered',
                'order_data' => $this->getOrderItemsData(),
                'header_css_class' => 'col-qty',
                'column_css_class' => 'col-qty'
            ]
        );

        $this->addColumn(
            'qty_requested',
            [
                'header' => __('Requested'),
                'index' => 'qty_requested',
                'renderer' => \Magento\Rma\Block\Adminhtml\Rma\Edit\Tab\Items\Grid\Column\Renderer\Textinput::class,
                'validate_class' => 'validate-greater-than-zero',
                'header_css_class' => 'col-qty',
                'column_css_class' => 'col-qty'
            ]
        );

        $this->addColumn(
            'qty_authorized',
            [
                'header' => __('Authorized'),
                'index' => 'qty_authorized',
                'renderer' => \Magento\Rma\Block\Adminhtml\Rma\Edit\Tab\Items\Grid\Column\Renderer\Textinput::class,
                'validate_class' => 'validate-greater-than-zero',
                'header_css_class' => 'col-qty',
                'column_css_class' => 'col-qty'
            ]
        );

        $this->addColumn(
            'qty_returned',
            [
                'header' => __('Returned'),
                'index' => 'qty_returned',
                'renderer' => \Magento\Rma\Block\Adminhtml\Rma\Edit\Tab\Items\Grid\Column\Renderer\Textinput::class,
                'validate_class' => 'validate-greater-than-zero',
                'header_css_class' => 'col-qty',
                'column_css_class' => 'col-qty'
            ]
        );

        $this->addColumn(
            'qty_approved',
            [
                'header' => __('Approved'),
                'index' => 'qty_approved',
                'renderer' => \Magento\Rma\Block\Adminhtml\Rma\Edit\Tab\Items\Grid\Column\Renderer\Textinput::class,
                'validate_class' => 'validate-greater-than-zero',
                'header_css_class' => 'col-qty',
                'column_css_class' => 'col-qty'
            ]
        );

        $this->addColumn(
            'reason',
            [
                'header' => __('OMD REASON'),
                'getter' => [$this, 'getReasonOptionStringValue'],
                'renderer' => \Magento\Rma\Block\Adminhtml\Rma\Edit\Tab\Items\Grid\Column\Renderer\Reasonselect::class,
                'options' => $this->_rmaEav->getAttributeOptionValues('reason'),
                'index' => 'reason',
                'header_css_class' => 'col-reason',
                'column_css_class' => 'col-reason'
            ]
        );

        $this->addColumn(
            'condition',
            [
                'header' => __('OMV REASON'),
                'getter' => [$this, 'getConditionOptionStringValue'],
                'renderer' => \Magento\Rma\Block\Adminhtml\Rma\Edit\Tab\Items\Grid\Column\Renderer\Textselect::class,
                'options' => $this->_rmaEav->getAttributeOptionValues('condition'),
                'index' => 'condition',
                'header_css_class' => 'col-condition',
                'column_css_class' => 'col-condition'
            ]
        );

        $this->addColumn(
            'resolution',
            [
                'header' => __('Resolution'),
                'index' => 'resolution',
                'getter' => [$this, 'getResolutionOptionStringValue'],
                'renderer' => \Magento\Rma\Block\Adminhtml\Rma\Edit\Tab\Items\Grid\Column\Renderer\Textselect::class,
                'options' => $this->_rmaEav->getAttributeOptionValues('resolution'),
                'header_css_class' => 'col-resolution',
                'column_css_class' => 'col-resolution'
            ]
        );

        $this->addColumn(
            'status',
            [
                'header' => __('Status'),
                'index' => 'status',
                'getter' => [$this, 'getStatusOptionStringValue'],
                'renderer' => \Magento\Rma\Block\Adminhtml\Rma\Edit\Tab\Items\Grid\Column\Renderer\Status::class,
                'header_css_class' => 'col-status',
                'column_css_class' => 'col-status'
            ]
        );

        if (!$this->hasUserDefinedAttributes()) {
            $actionsArray = [
                [
                    'caption' => __('Details'),
                    'class' => 'action action-item-details disabled _disabled',
                    'disabled' => 'disabled',
                ],
            ];
        } else {
            $actionsArray = [['caption' => __('Details'), 'class' => 'action action-item-details']];
        }
        if (!($rma && ($rma->getStatus() === \Magento\Rma\Model\Rma\Source\Status::STATE_CLOSED ||
                $rma->getStatus() === \Magento\Rma\Model\Rma\Source\Status::STATE_PROCESSED_CLOSED))
        ) {
            $actionsArray[] = [
                'caption' => __('Split'),
                'class' => 'action action-item-split-line',
                'status_depended' => '1'
            ];
        }

        $this->addColumn(
            'action',
            [
                'header' => __('Action'),
                'renderer' => \Magento\Rma\Block\Adminhtml\Rma\Edit\Tab\Items\Grid\Column\Renderer\Action::class,
                'actions' => $actionsArray,
                'is_system' => true,
                'header_css_class' => 'col-actions',
                'column_css_class' => 'col-actions'
            ]
        );

        $this->sortColumnsByOrder();

        return $this;
    }
}
