<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Eguana\GERP\Block\Adminhtml\Rma\NewRma\Tab\Items\Renderer;
use Magento\Framework\Data\Form\Element\AbstractElement;

/**
 * Reason field renderer
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Reason extends \Magento\Rma\Block\Adminhtml\Rma\NewRma\Tab\Items\Renderer\Reason
{
    /**
     * Rma reason template name
     *
     * @var string
     */
    protected $_template = 'Magento_Rma::new/items/renderer/reason.phtml';
}
