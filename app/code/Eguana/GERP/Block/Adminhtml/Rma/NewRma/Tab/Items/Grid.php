<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Eguana\GERP\Block\Adminhtml\Rma\NewRma\Tab\Items;

use Magento\Catalog\Model\Product;

/**
 * Admin RMA create order grid block
 *
 * @api
 * @since 100.0.2
 */
class Grid extends \Magento\Rma\Block\Adminhtml\Rma\NewRma\Tab\Items\Grid
{

    /**
     * Prepare grid collection object
     *
     * @return \Magento\Rma\Block\Adminhtml\Rma\Edit\Tab\Items\Grid
     */
    protected function _prepareCollection()
    {
        /** @var $collection \Magento\Rma\Model\ResourceModel\Item\Collection */
        $collection = $this->_collectionFactory->create();
        $collection->addAttributeToSelect('*');
        $collection->addAttributeToFilter('entity_id', null);

        $collection->getSelect()->join('sales_order_item','order_item_id = item_id','product_from');

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * Prepare columns
     *
     * @return \Magento\Backend\Block\Widget\Grid\Extended
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'product_name',
            [
                'header' => __('Product'),
                'type' => 'text',
                'index' => 'product_name',
                'sortable' => false,
                'escape' => true,
                'header_css_class' => 'col-product required',
                'column_css_class' => 'col-product'
            ]
        );

        $this->addColumn(
            'product_sku',
            [
                'header' => __('SKU'),
                'type' => 'text',
                'index' => 'product_sku',
                'sortable' => false,
                'escape' => true,
                'header_css_class' => 'col-sku',
                'column_css_class' => 'col-sku'
            ]
        );

        //Renderer puts available quantity instead of order_item_id
        $this->addColumn(
            'qty_ordered',
            [
                'header' => __('Remaining'),
                'getter' => [$this, 'getRemainingQty'],
                'type' => 'text',
                'index' => 'qty_ordered',
                'sortable' => false,
                'order_data' => $this->getOrderItemsData(),
                'renderer' => \Magento\Rma\Block\Adminhtml\Rma\Edit\Tab\Items\Grid\Column\Renderer\Quantity::class,
                'header_css_class' => 'col-qty',
                'column_css_class' => 'col-qty'
            ]
        );

        $this->addColumn(
            'qty_requested',
            [
                'header' => __('Requested'),
                'index' => 'qty_requested',
                'type' => 'input',
                'sortable' => false,
                'header_css_class' => 'col-qty required',
                'column_css_class' => 'col-qty'
            ]
        );

        $eavHelper = $this->_rmaEav;
        $this->addColumn(
            'reason',
            [
                'header' => __('OMD REASON'),
                'getter' => [$this, 'getReasonOptionStringValue'],
                'type' => 'select',
                'options' => ['' => ''] + $eavHelper->getAttributeOptionValues('reason'),
                'index' => 'reason',
                'sortable' => false,
                'header_css_class' => 'col-reason required',
                'column_css_class' => 'col-reason'
            ]
        );

        $this->addColumn(
            'condition',
            [
                'header' => __('OMV REASON'),
                'type' => 'select',
                'options' => ['' => ''] + $eavHelper->getAttributeOptionValues('condition'),
                'index' => 'condition',
                'sortable' => false,
                'header_css_class' => 'col-condition required',
                'column_css_class' => 'col-condition'
            ]
        );

        $this->addColumn(
            'resolution',
            [
                'header' => __('Resolution'),
                'index' => 'resolution',
                'type' => 'select',
                'options' => ['' => ''] + $eavHelper->getAttributeOptionValues('resolution'),
                'sortable' => false,
                'header_css_class' => 'col-resolution required',
                'column_css_class' => 'col-resolution'
            ]
        );

        $actionsArray = [
            [
                'caption' => __('Delete'),
                'url' => ['base' => '*/*/delete'],
                'field' => 'id',
                'onclick' => 'alert(\'Delete\');return false;',
            ],
            [
                'caption' => __('Add Details'),
                'url' => ['base' => '*/*/edit'],
                'field' => 'id',
                'onclick' => 'alert(\'Details\');return false;'
            ],
        ];

        $this->addColumn(
            'action',
            [
                'header' => __('Action'),
                'renderer' => \Magento\Rma\Block\Adminhtml\Rma\Edit\Tab\Items\Grid\Column\Renderer\Action::class,
                'actions' => $actionsArray,
                'sortable' => false,
                'is_system' => true,
                'header_css_class' => 'col-actions',
                'column_css_class' => 'col-actions'
            ]
        );

        $this->sortColumnsByOrder();
        return $this;
    }
}
