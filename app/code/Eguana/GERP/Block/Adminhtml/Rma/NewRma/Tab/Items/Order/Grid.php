<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Eguana\GERP\Block\Adminhtml\Rma\NewRma\Tab\Items\Order;

/**
 * Admin RMA create order grid block
 *
 * @api
 * @since 100.0.2
 */
class Grid extends \Magento\Rma\Block\Adminhtml\Rma\NewRma\Tab\Items\Order\Grid
{
    /**
     * Prepare columns
     *
     * @return $this
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'select',
            [
                'header' => __('Select'),
                'type' => 'checkbox',
                'align' => 'center',
                'sortable' => false,
                'index' => 'item_id',
                'values' => $this->_getSelectedProducts(),
                'name' => 'in_products',
                'header_css_class' => 'col-select',
                'column_css_class' => 'col-select'
            ]
        );

        $this->addColumn(
            'product_name',
            [
                'header' => __('Product'),
                'renderer' => \Magento\Rma\Block\Adminhtml\Product\Bundle\Product::class,
                'index' => 'name',
                'escape' => true,
                'header_css_class' => 'col-product col-rma-product',
                'column_css_class' => 'col-product col-rma-product'
            ]
        );

        $this->addColumn(
            'product_from',
            [
                'header' => __('PRODUCT FROM'),
                'type' => 'text',
                'index' => 'product_from',
                'header_css_class' => 'col-product col-rma-product',
                'column_css_class' => 'col-product col-rma-product',
            ]
        );

        $this->addColumn(
            'sku',
            [
                'header' => __('SKU'),
                'type' => 'text',
                'index' => 'sku',
                'escape' => true,
                'header_css_class' => 'col-sku',
                'column_css_class' => 'col-sku'
            ]
        );

        $this->addColumn(
            'price',
            [
                'header' => __('Price'),
                'type' => 'currency',
                'index' => 'price',
                'header_css_class' => 'col-price',
                'column_css_class' => 'col-price'
            ]
        );

        $this->addColumn(
            'available_qty',
            [
                'header' => __('Remaining'),
                'type' => 'text',
                'getter' => [$this, 'getRemainingQty'],
                'renderer' => \Magento\Rma\Block\Adminhtml\Rma\Edit\Tab\Items\Grid\Column\Renderer\Quantity::class,
                'index' => 'available_qty',
                'header_css_class' => 'col-qty',
                'column_css_class' => 'col-qty',
                'filter' => false,
                'sortable' => false,
            ]
        );

        return parent::_prepareColumns();
    }
}
