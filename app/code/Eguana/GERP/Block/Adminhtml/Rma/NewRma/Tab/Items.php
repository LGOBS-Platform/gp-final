<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Eguana\GERP\Block\Adminhtml\Rma\NewRma\Tab;

/**
 * Items Tab in Edit RMA form
 *
 * @api
 * @author     Magento Core Team <core@magentocommerce.com>
 * @since 100.0.2
 */
class Items extends \Magento\Rma\Block\Adminhtml\Rma\NewRma\Tab\Items
{
    /**
     * Prepare form before rendering HTML
     *
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    protected function _prepareForm()
    {
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();
        $htmlIdPrefix = 'rma_properties_';
        $form->setHtmlIdPrefix($htmlIdPrefix);

        $model = $this->_coreRegistry->registry('current_rma');

        $fieldset = $form->addFieldset('rma_item_fields', []);

        $fieldset->addField(
            'product_name',
            'text',
            ['label' => __('Product Name'), 'name' => 'product_name', 'required' => false]
        );

        $fieldset->addField(
            'product_sku',
            'text',
            ['label' => __('SKU'), 'name' => 'product_sku', 'required' => false]
        );

        //Renderer puts available quantity instead of order_item_id
        $fieldset->addField(
            'qty_ordered',
            'text',
            ['label' => __('Remaining Qty'), 'name' => 'qty_ordered', 'required' => false]
        );

        $fieldset->addField(
            'qty_requested',
            'text',
            [
                'label' => __('Requested Qty'),
                'name' => 'qty_requested',
                'required' => false,
                'class' => 'validate-greater-than-zero'
            ]
        );

        /** @var $itemForm \Magento\Rma\Model\Item\Form */
        $itemForm = $this->_itemFormFactory->create();
        $reasonOtherAttribute = $itemForm->setFormCode('default')->getAttribute('reason_other');

        $fieldset->addField(
            'reason_other',
            'text',
            [
                'label' => $reasonOtherAttribute->getStoreLabel(),
                'name' => 'reason_other',
                'maxlength' => 255,
                'required' => false
            ]
        );

        $eavHelper = $this->_rmaEav;
        $fieldset->addField(
            'reason',
            'select',
            [
                'label' => __('OMD REASON'),
                'options' => [
                        '' => '',
                    ] + $eavHelper->getAttributeOptionValues(
                        'reason'
                    ) + [
                        'other' => $reasonOtherAttribute->getStoreLabel(),
                    ],
                'name' => 'reason',
                'required' => false
            ]
        )->setRenderer(
            $this->getLayout()->createBlock(\Eguana\GERP\Block\Adminhtml\Rma\NewRma\Tab\Items\Renderer\Reason::class)
        );

        $fieldset->addField(
            'condition',
            'select',
            [
                'label' => __('OMV REASON'),
                'options' => ['' => ''] + $eavHelper->getAttributeOptionValues('condition'),
                'name' => 'condition',
                'required' => false,
                'class' => 'admin__control-select'
            ]
        );

        $fieldset->addField(
            'resolution',
            'select',
            [
                'label' => __('Resolution'),
                'options' => ['' => ''] + $eavHelper->getAttributeOptionValues('resolution'),
                'name' => 'resolution',
                'required' => false,
                'class' => 'admin__control-select'
            ]
        );

        $fieldset->addField(
            'delete_link',
            'label',
            ['label' => __('Delete'), 'name' => 'delete_link', 'required' => false]
        );

        $fieldset->addField(
            'add_details_link',
            'label',
            ['label' => __('Add Details'), 'name' => 'add_details_link', 'required' => false]
        );

        $this->setForm($form);

        return $this;
    }
}
