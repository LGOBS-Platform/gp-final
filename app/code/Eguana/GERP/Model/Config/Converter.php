<?php

namespace Eguana\GERP\Model\Config;

use Eguana\GERP\Helper\Data;

class Converter implements \Magento\Framework\Config\ConverterInterface
{
    public function convert($source)
    {
        $gerpInterfaceList = $source->getElementsByTagName(Data::GERP_INTERFACE_CONFIG_KEY);
        $gerpInterfaceData = [];
        $bindingData = [];
        $iterator = 0;

        foreach ($gerpInterfaceList as $gerpInterface) {

            foreach ($gerpInterface->childNodes as $gerpInterfaceInfo) {

                if(!isset($gerpInterfaceInfo->childNodes))
                    continue;

                foreach ($gerpInterfaceInfo->childNodes as $data){

                    if($data->localName == '')
                        continue;

                    if($data->localName == 'interface_id'){

                        foreach ($data->childNodes as $interfaceIdData) {
                            $gerpInterfaceData[$iterator][$data->localName][$interfaceIdData->localName] = $interfaceIdData->textContent;
                        }

                    }else{
                        $gerpInterfaceData[$iterator][$data->localName] = $data->textContent;
                    }
                }
                $iterator++;
            }
        }

        foreach ($gerpInterfaceData as $data){
            $bindingData[$data['document_type']] = $data;
        }

        return [Data::GERP_INTERFACE_CONFIG_KEY => $bindingData];
    }
}