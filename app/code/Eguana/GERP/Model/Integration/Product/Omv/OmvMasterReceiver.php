<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-10-11
 * Time: 오후 2:43
 */

namespace Eguana\GERP\Model\Integration\Product\Omv;


use Eguana\GERP\Model\Integration\Product\AbstractProduct;
use Eguana\GERP\Model\Source\Status;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\ProductRepository;

class OmvMasterReceiver extends AbstractProduct
{
    protected $_flagKey = '02';

    public function process($bindData)
    {

        $websiteConfig = $this->getDataHelper()->getGerpWebsiteData();

        foreach ($bindData as $key => $item){

            $data = $item[self::GERP_DATA_COLUMN];
            $itemConfigData = $websiteConfig[$data['AFFILIATE_CODE']];

            if(!(isset($data['ITEM_NO'])))
            {
                $bindData[$key]['result'] = Status::GERP_DATA_SKIP;
                $bindData[$key]['message'] = __('Item NO not exist');
                continue;
            }

            $sku = $itemConfigData['sku_prefix'] . $itemConfigData['sku_split'] . trim($data['ITEM_NO']);

            $product = $this->_getProduct($sku);

            if(!$product){

                if($data['ITEM_STATUS']  != 'Active'){
                    $bindData[$key]['result'] = Status::GERP_DATA_SKIP;
                    $bindData[$key]['message'] = __('not selling product.');
                    continue;
                }

                $product = $this->_getNewProduct($sku,self::OMV_PRODUCT_FROM);
                $product->setWebsiteIds([$itemConfigData['website_id']]);
            }


            $product->setStoreId($itemConfigData['store_id']);
            $product = $this->_updateProductData(
                $product,$data
            );

            if($result =  $this->_saveProduct($product) === true)
                $bindData[$key]['result'] = Status::GERP_DATA_PROCESS;
            else
                $bindData[$key]['message'] = $result;

            try{
                $this->_action->updateAttributes([$product->getId()],['price'=>$data['RRP_PRICE']],0);
                // update only all store view price
            }catch (\Exception $e){
                $bindData[$key]['result'] = Status::GERP_DATA_ERRPR;
                $bindData[$key]['message'] = $e->getMessage();
            }
        }

        return $bindData;
    }

    /**
     * @param $product Product
     * @param $data array
     * @return Product
     */
    protected function _updateProductData($product,$data){

        $websiteConfig = $this->getDataHelper()->getGerpWebsiteData();
        $itemConfigData = $websiteConfig[$data['AFFILIATE_CODE']];
        $salesModelSplit= explode('.',$data['ITEM_NO']);

        $product->setName($data['ITEM_NO']);
        $product->setData('description',$data['ITEM_DESC']);
        $product->setData('primary_uom_code',$data['PRIMARY_UOM_CODE']);
        $product->setData('product_from','OMV');
        $product->setData('visibility',4);
        $product->setData('sales_model_code',$salesModelSplit);
        $product->setData('model_id',$data['ITEM_NO']);
        $product->setData('url_key',$itemConfigData['sku_prefix'].'-'.$data['ITEM_NO']);
        $product->setData('transferred',0);
//        if($data['ITEM_STATUS'] == 'Active')
//            $product->setData('status',1);
//        else
//            $product->setData('status',0);

        $product->setPrice($data['ENDUSER_PRICE']);
        $product->setData('special_price',$data['ENDUSER_PRICE']);

        return $product;
    }

}