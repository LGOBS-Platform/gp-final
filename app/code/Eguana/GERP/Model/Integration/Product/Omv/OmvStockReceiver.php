<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-10-11
 * Time: 오후 2:43
 */

namespace Eguana\GERP\Model\Integration\Product\Omv;


use Amasty\MultiInventory\Model\Warehouse;
use Amasty\MultiInventory\Model\WarehouseFactory;
use Eguana\GERP\Model\Integration\Product\AbstractStock;
use Eguana\GERP\Model\Source\Status;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\Exception\NoSuchEntityException;

class OmvStockReceiver extends AbstractStock
{
    protected $_flagKey = '04';

    public function process($bindData)
    {
        $websiteConfig = $this->getDataHelper()->getGerpWebsiteData();

        foreach ($bindData as $key => $item){

            $data = $item[self::GERP_DATA_COLUMN];
            $itemConfigData = $websiteConfig[$data['AFFILIATE_CODE']];
            $sku = $itemConfigData['sku_prefix'] . $itemConfigData['sku_split'] . trim($data['ITEM_NO']);

            $product = $this->_getProduct($sku);

            if(!$product){
                $bindData[$key]['result'] = Status::GERP_DATA_PENDING;
                $bindData[$key]['message'] = __('Product Not Found.');
                continue;
            }

            $warehouseCode = $data['ORGANIZATION_CODE'];

            try{
                $warehouse = $this->_warehouseRepository->getByCode($warehouseCode);
            }catch (NoSuchEntityException $e){

                $warehouse = $this->_newWarehouse($warehouseCode,$warehouseCode,sprintf("AFFILIATE_CODE : %s , ORGANIZATION_CODE : %s CREATE.",
                    $data['AFFILIATE_CODE'],
                    $data['ORGANIZATION_CODE']));
            }

            $warehouseItem = $this->_getWarehouseItem($product->getId(),$warehouse->getId());
            $warehouseItem->setQty($data['AVAIL_QTY']);
            $warehouseItem->setAvailableQty($data['AVAIL_QTY']);
            $product->setData('transferred',0);
            $product->save();

            $total_stock = $this->_getWarehouseItem($product->getId(),1);

            if((int)$data['AVAIL_QTY'] > 0)
            {
                $warehouseItem->setStockStatus(1);

                $total_stock->setStockStatus(1);
            }else if((int)$data['AVAIL_QTY'] == 0){
                $warehouseItem->setStockStatus(0);

                $total_stock->setStockStatus(0);
            }

            try{
                $total_stock->save();
            }catch (\Exception $e){
                $bindData[$key]['message'] = 'error while updating warehouse total stock';
            }

            $warehouseItem->setProductId($product->getId());
            $warehouseItem->setWarehouseId($warehouse->getId());

            if($result = $this->_saveWarehouseItem($warehouseItem) === true){
                $bindData[$key]['result'] = Status::GERP_DATA_PROCESS;
            }else{
                $bindData[$key]['message'] = $result;
            }
        }

        return $bindData;
    }
}