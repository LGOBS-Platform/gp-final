<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-10-11
 * Time: 오후 2:43
 */

namespace Eguana\GERP\Model\Integration\Product;


use Eguana\GERP\Model\Integration\AbstractReceiver;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\Exception\NoSuchEntityException;

abstract class AbstractProduct extends AbstractReceiver
{
    const OMD_PRODUCT_FROM = 'OMD';

    const OMV_PRODUCT_FROM = 'OMV';

    protected $_flagKey = '01';

    protected $_productRepository;

    protected $_productModelFactory;

    protected $_action;

    public function __construct(
        \Eguana\GERP\Model\ResourceModel\Log\Collection $logCollection,
        \Eguana\GERP\Helper\Data $dataHelper,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Catalog\Model\ProductFactory $productModelFactory,
        \Magento\Catalog\Model\ResourceModel\Product\Action $action
    )
    {
        $this->_productRepository = $productRepository;
        $this->_productModelFactory = $productModelFactory;
        $this->_action = $action;
        parent::__construct($logCollection,$dataHelper);
    }

    /**
     * @param $product Product
     * @return boolean | string
     */
    protected function _saveProduct($product){

        $result = true;

        try{
            $product->save();
        }catch (\Exception $e){
            return $e->getMessage();
        }

        return $result;
    }

    /**
     * @param $sku string
     * @return Product | false
     */
    protected function _getProduct($sku){

        try{
            $product = $this->_productRepository->get($sku, false, 2);
        }catch (NoSuchEntityException $e){
            return false;
        }

        return $product;
    }

    /**
     * @param $sku string
     * @return Product
     */
    protected function _getNewProduct($sku, $from = self::OMD_PRODUCT_FROM,$itemType='Q'){
        /** @var $product Product */
        $product = $this->_productModelFactory->create();
        $product->setSku($sku);

        if($itemType == 'C'){
            $product->setTypeId('bundle');
            $product->setPriceType(\Magento\Bundle\Model\Product\Price::PRICE_TYPE_DYNAMIC);
        }else{
            $product->setTypeId('simple');
        }

        $product->setPrice(0);
        $product->setAttributeSetId(16);
        $product->setData('gerp_item_type',$itemType);
        $product->setData('product_from',$from);
        $product->setStatus(2);
        $product->setStockData([
            'manage_stock' => false
        ]);

        return $product;
    }

    public function result($resultData)
    {

    }

}