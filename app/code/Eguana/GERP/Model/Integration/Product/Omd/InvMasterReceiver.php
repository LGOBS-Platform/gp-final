<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-10-11
 * Time: 오후 2:43
 */

namespace Eguana\GERP\Model\Integration\Product\Omd;


use Eguana\GERP\Model\Integration\Product\AbstractProduct;
use Eguana\GERP\Model\Source\Status;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\ProductRepository;

class InvMasterReceiver extends AbstractProduct
{
    protected $_flagKey = '01';

    public function process($bindData)
    {
        $websiteConfig = $this->getDataHelper()->getGerpWebsiteData();
        
        foreach ($bindData as $key => $item){
            $data = $item[self::GERP_DATA_COLUMN];
            $itemConfigData = $websiteConfig[$data['AFFILIATE_CODE']];
            $sku = $itemConfigData['sku_prefix'] . $itemConfigData['sku_split'] . trim($data['SALES_MODEL_CODE']);

            $product = $this->_getProduct($sku);
            
            if(!$product){
                if($data['CUSTOMER_ORDER_ENABLED_FLAG']  != 'Y'){
                    $bindData[$key]['result'] = Status::GERP_DATA_SKIP;
                    $bindData[$key]['message'] = __('not selling product.');
                    continue;
                }
                $product = $this->_getNewProduct($sku,self::OMD_PRODUCT_FROM,$data['ITEM_TYPE']);
                $product->setWebsiteIds([0,$itemConfigData['website_id']]);
            }

            $product->setWebsiteIds([0,$itemConfigData['website_id']]);
            $product = $this->_updateProductData(
                $product,$data
            );

            if($result =  $this->_saveProduct($product) === true)
                $bindData[$key]['result'] = Status::GERP_DATA_PROCESS;
            else
                $bindData[$key]['message'] = $result;
        }

        return $bindData;
    }

    /**
     * @param $product Product
     * @param $data array
     * @return Product
     */
    protected function _updateProductData($product,$data){

        $websiteConfig = $this->getDataHelper()->getGerpWebsiteData();
        $itemConfigData = $websiteConfig[$data['AFFILIATE_CODE']];

        $salesModelSplit= explode('.',$data['SALES_MODEL_CODE']);
        $product->setName($data['MODEL_DESC']);
        $product->setData('description',$data['MODEL_DESC']);
        $product->setData('sales_model_code',$salesModelSplit[0]);
        $product->setData('sales_suffix_code',$salesModelSplit[1]);
        $product->setData('model_name',$data['MODEL_DESC']);
        $product->setData('model_id',$data['SALES_MODEL_CODE']);
        $product->setData('product_from','OMD');
        $product->setData('product_level1_code',$data['PRODUCT_LEVEL1_CODE']);
        $product->setData('product_level2_code',$data['PRODUCT_LEVEL2_CODE']);
        $product->setData('product_level3_code',$data['PRODUCT_LEVEL3_CODE']);
        $product->setData('product_level4_code',$data['PRODUCT_LEVEL4_CODE']);
        $product->setData('product_group1_name',$data['PRODUCT_GROUP1_NAME']);
        $product->setData('product_group2_name',$data['PRODUCT_GROUP2_NAME']);
        $product->setData('product_group3_name',$data['PRODUCT_GROUP3_NAME']);
        $product->setData('product_group4_name',$data['PRODUCT_GROUP4_NAME']);
        $product->setData('product_group4_name',$data['PRODUCT_GROUP4_NAME']);
        $product->setData('primary_uom_code',$data['PRIMARY_UOM_CODE']);
        $product->setData('visibility',4);
        $product->setData('url_key',$itemConfigData['sku_prefix'].'-'.$salesModelSplit[0].'-'.$salesModelSplit[1]);
        $product->setData('price',0);
        $product->setData('special_price',0);
//        if($data['CUSTOMER_ORDER_ENABLED_FLAG'] == 'Y')
//            $product->setData('status',1);
//        else
//            $product->setData('status',0);

        return $product;
    }

}