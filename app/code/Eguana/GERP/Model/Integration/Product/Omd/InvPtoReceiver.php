<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-10-12
 * Time: 오후 4:58
 */

namespace Eguana\GERP\Model\Integration\Product\Omd;


use Eguana\GERP\Model\Integration\AbstractReceiver;
use Eguana\GERP\Model\Source\Status;
use Magento\Bundle\Api\Data\LinkInterface;
use Magento\Bundle\Api\Data\OptionInterface;
use Magento\Bundle\Model\OptionRepository;
use Magento\Catalog\Model\Product;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

class InvPtoReceiver extends AbstractReceiver
{
    protected $_flagKey = '05';

    protected $_productRepository;

    protected $_bundleOptionFactory;

    protected $_bundleLinkFactory;

    protected $_optionRepository;

    protected $_optionCollectionFactory;

    public function __construct(
        \Eguana\GERP\Model\ResourceModel\Log\Collection $logCollection,
        \Eguana\GERP\Helper\Data $dataHelper,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Bundle\Model\OptionFactory $bundleOptionFactory,
        \Magento\Bundle\Model\LinkFactory $bundleLinkFactory,
        OptionRepository $optionRepository,
        \Magento\Bundle\Model\ResourceModel\Option\CollectionFactory $optionCollectionFactory
    )
    {
        $this->_productRepository = $productRepository;
        $this->_bundleOptionFactory = $bundleOptionFactory;
        $this->_bundleLinkFactory = $bundleLinkFactory;
        $this->_optionRepository = $optionRepository;
        $this->_optionCollectionFactory = $optionCollectionFactory;
        parent::__construct($logCollection,$dataHelper);
    }

    public function process($bindData)
    {
        $websiteConfig = $this->getDataHelper()->getGerpWebsiteData();

        foreach ($bindData as $key => $item){

            $data = $item[self::GERP_DATA_COLUMN];
            $itemConfigData = $websiteConfig[$data['AFFILIATE_CODE']];
            $parentSku = $itemConfigData['sku_prefix'] . $itemConfigData['sku_split'] . trim($data['PACKAGE_MODEL_CODE']);
            $childSku = $itemConfigData['sku_prefix'] . $itemConfigData['sku_split'] . trim($data['CHILD_MODEL_CODE']);
            $optionTitle = $data['PACKAGE_MODEL_CODE'].'-'.$data['CHILD_MODEL_CODE'];
            $packageQty = $data['PACKING_UNIT_QTY'];

            $parentProduct = $this->_getProduct($parentSku);
            $childProduct = $this->_getProduct($childSku);

            if(!$parentProduct){
                $bindData[$key]['result'] = Status::GERP_DATA_PENDING;
                $bindData[$key]['message'] = __('Parent Product Sku Not Found.');
                continue;
            }

            if(!$childProduct){
                $bindData[$key]['result'] = Status::GERP_DATA_PENDING;
                $bindData[$key]['message'] = __('Child Product Sku Not Found.');
                continue;
            }

            $this->_removeOption($optionTitle);

            try{
                $this->_createOption($optionTitle,$parentProduct,$childProduct,$packageQty);
                $bindData[$key]['result'] = Status::GERP_DATA_PROCESS;
            }catch (CouldNotSaveException $e){
                $bindData[$key]['result'] = Status::GERP_DATA_ERRPR;
                $bindData[$key]['message'] = $e->getMessage();
            }

        }


        return $bindData;
    }

    /**
     * @param $optionTitle string
     * @param $parentProduct Product
     * @param $chieldProduct Product
     * @param $packageQty double
     * @throws CouldNotSaveException
     */
    protected function _createOption($optionTitle,$parentProduct,$chieldProduct,$packageQty){

        /**
         * @var $bundleOption OptionInterface
         * @var $bundleLink LinkInterface
         */
        $bundleOption = $this->_bundleOptionFactory->create();

        $bundleOption->setTitle($optionTitle);
        $bundleOption->setPosition(0);
        $bundleOption->setType('radio');
        $bundleOption->setRequired(true);

        $bundleLink = $this->_bundleLinkFactory->create();
        $bundleLink->setQty($packageQty);
        $bundleLink->setSku($chieldProduct->getSku());
        $bundleLink->setIsDefault(true);
        $bundleOption->setProductLinks([$bundleLink]);

        $this->_optionRepository->save($parentProduct,$bundleOption);
    }

    /**
     * @param $optionTitle string
     * @return void
     */
    protected function _removeOption($optionTitle){

        /**@var $optionList \Magento\Bundle\Model\ResourceModel\Option\Collection */
        $optionList = $this->_optionCollectionFactory->create();
        $option  = $optionList->joinValues(0)
            ->addFieldToFilter('option_value.title',['eq' => $optionTitle]);

        foreach ($option as $optionItem){
            if($optionItem->getTitle() == $optionTitle){
                $this->_optionRepository->delete($optionItem);
            }
        }
    }

    /**
     * @param $sku string
     * @return Product | false
     */
    protected function _getProduct($sku){

        try{
            $product = $this->_productRepository->get($sku, true, 0);
        }catch (NoSuchEntityException $e){
            return false;
        }

        return $product;
    }

    public function result($resultData)
    {
        // TODO: Implement result() method.
    }

}