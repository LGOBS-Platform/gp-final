<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-10-11
 * Time: 오후 2:43
 */

namespace Eguana\GERP\Model\Integration\Product\Omd;


use Amasty\MultiInventory\Model\Warehouse;
use Amasty\MultiInventory\Model\WarehouseFactory;
use Eguana\GERP\Model\Integration\Product\AbstractStock;
use Eguana\GERP\Model\Source\Status;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\Exception\NoSuchEntityException;

class InvStockReceiver extends AbstractStock
{
    protected $_flagKey = '03';

    public function process($bindData)
    {

        $websiteConfig = $this->getDataHelper()->getGerpWebsiteData();

        foreach ($bindData as $key => $item){

            $data = $item[self::GERP_DATA_COLUMN];
            $itemConfigData = $websiteConfig[$data['AFFILIATE_CODE']];

            if(!(isset($data['CUST_CODE'])))
            {
                $bindData[$key]['result'] = Status::GERP_DATA_SKIP;
                $bindData[$key]['message'] = __('Cust code not exist');
                continue;
            }

            if(!($data['CUST_CODE'] == $itemConfigData['bill_to_code']))
            {
                $bindData[$key]['result'] = Status::GERP_DATA_SKIP;
                $bindData[$key]['message'] = __('Cust code validation fail');
                continue;
            }

            $sku = $itemConfigData['sku_prefix'] . $itemConfigData['sku_split'] . trim($data['MODEL_CODE']);

            $product = $this->_getProduct($sku);


            if(!$product){
                $bindData[$key]['result'] = Status::GERP_DATA_PENDING;
                $bindData[$key]['message'] = __('Product Not Found.');
                continue;
            }

//            $warehouseCode = trim($data['ORG_CODE']);
//
//            try{
//                $warehouse = $this->_warehouseRepository->getByCode($warehouseCode);
//            }catch (\Exception $e){
//
//                $warehouse = $this->_newWarehouse($data['ORG_NAME'],$warehouseCode,sprintf("CUST_CODE : %s , CUST_NAME : %s , ORG_CODE : %s , ORG_NAME : %s CREATE.",
//                    $data['CUST_CODE'],
//                    $data['CUST_NAME'],
//                    $data['ORG_CODE'],
//                    $data['ORG_NAME']));
//            }


            $resource = $this->_resourceConnection;
            $connection = $resource->getConnection();
            $tableName = $resource->getTableName('eguana_gerp_stock_update');
            $reserved_qty = $data['RESERVED_QTY'];
            $warehouse_code = trim($data['ORG_CODE']);
            $end_date = isset($data['EFFECTIVE_END_DATE']) ? $data['EFFECTIVE_END_DATE'] : null;
            $affiliate_code = isset($data['AFFILIATE_CODE']) ? $data['AFFILIATE_CODE'] : null ;
            $bill_to_code = $data['CUST_CODE'];
            $store_id = $itemConfigData['store_id'];
            $gerp_creation_date = isset($data['CREATION_DATE']) ? $data['CREATION_DATE'] : null;
            $gerp_last_update_date = isset($data['LAST_UPDATE_DATE']) ? $data['LAST_UPDATE_DATE'] : null;

            $sql = "INSERT INTO ". $tableName . "(sku,stock,org_code,end_date,status,affiliate_code,bill_to_code,store_id,gerp_creation_date,gerp_last_update_date,gerp_data) VALUES ('".$sku."' , '".$reserved_qty."' , '".$warehouse_code."' , '".$end_date."' , 'N' , '".$affiliate_code."', '".$bill_to_code."', '".$store_id."', '".$gerp_creation_date."', '".$gerp_last_update_date."', '".json_encode($data)."') ON DUPLICATE KEY UPDATE sku='".$sku."',stock='".$reserved_qty."',org_code='".$warehouse_code."',end_date='".$end_date."',status='N',affiliate_code='".$affiliate_code."',bill_to_code='".$bill_to_code."',store_id='".$store_id."',gerp_creation_date='".$gerp_creation_date."',gerp_last_update_date='".$gerp_last_update_date."',gerp_data='".json_encode($data)."',creation_date='".date('Y-m-d H:i:s')."'";

            $result = $connection->query($sql);

            if($result == true)
            {
                $bindData[$key]['result'] = Status::GERP_DATA_PROCESS;
            }else{
                $bindData[$key]['result'] = Status::GERP_DATA_SKIP;
                $bindData[$key]['message'] = $result;
            }

//            $warehouseItem = $this->_getWarehouseItem($product->getId(),$warehouse->getId());
//            $warehouseItem->setQty($data['RESERVED_QTY']);
//            $warehouseItem->setAvailableQty($data['RESERVED_QTY']);
//            $product->setData('transferred',0);
//            $product->save();

//            $total_stock = $this->_getWarehouseItem($product->getId(),1);
//
//            if((int)$data['RESERVED_QTY'] > 0)
//            {
//                $warehouseItem->setStockStatus(1);
//
//                $total_stock->setStockStatus(1);
//            }else if((int)$data['RESERVED_QTY'] == 0){
//                $warehouseItem->setStockStatus(0);
//
//                $total_stock->setStockStatus(0);
//            }


//            try{
//                $total_stock->save();
//            }catch (\Exception $e){
//                $bindData[$key]['message'] = 'error while updating warehouse total stock';
//            }
//            $warehouseItem->setProductId($product->getId());
//            $warehouseItem->setWarehouseId($warehouse->getId());

//            if($result = $this->_saveWarehouseItem($warehouseItem) === true){
//                $bindData[$key]['result'] = Status::GERP_DATA_PROCESS;
//            }else{
//                $bindData[$key]['message'] = $result;
//            }
        }

        return $bindData;
    }
}