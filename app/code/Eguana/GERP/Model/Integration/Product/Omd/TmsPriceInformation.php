<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-10-11
 * Time: 오후 2:43
 */

namespace Eguana\GERP\Model\Integration\Product\Omd;


use Eguana\GERP\Model\Integration\Product\AbstractProduct;
use Eguana\GERP\Model\Source\Status;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\App\Area;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\ValidatorException;
use Magento\Framework\Registry;
use Magento\Store\Model\StoreManagerInterface;

class TmsPriceInformation extends AbstractProduct
{
    protected $_flagKey = '06';

    protected $_gerpWebsiteData;

    protected $_updateRepository;

    protected $_updateFactory;

    protected $_localeDate;

    protected $_productStaging;

    protected $_versionManagerFactory;

    protected $_storeManager;

    protected $_productResource;

    protected $_stagingDelete;

    protected $_registry;

    public function __construct(
        \Eguana\GERP\Model\ResourceModel\Log\Collection $logCollection,
        \Eguana\GERP\Helper\Data $dataHelper,
        ProductRepository $productRepository,
        ProductFactory $productModelFactory,
        \Magento\Staging\Api\UpdateRepositoryInterface $updateRepositoryInterface,
        \Magento\Staging\Api\Data\UpdateInterfaceFactory $updateFactory,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\CatalogStaging\Api\ProductStagingInterface $productStagingInterface,
        \Magento\Staging\Model\VersionManagerFactory $versionManagerFactory,
        StoreManagerInterface $storeManager,
        ResourceConnection $productResource,
        \Magento\CatalogStaging\Model\Product\Retriever $stagingDelete,
        \Magento\Catalog\Model\ResourceModel\Product\Action $action,
        Registry $registry
    )
    {
        $this->_updateRepository = $updateRepositoryInterface;
        $this->_updateFactory = $updateFactory;
        $this->_localeDate = $localeDate;
        $this->_productStaging = $productStagingInterface;
        $this->_versionManagerFactory = $versionManagerFactory;
        $this->_storeManager = $storeManager;
        $this->_productResource = $productResource;
        $this->_stagingDelete = $stagingDelete;
        $this->_registry = $registry;
        parent::__construct($logCollection, $dataHelper, $productRepository, $productModelFactory,$action);
    }

    public function process($bindData)
    {
        $this->_registry->register('isSecureArea', true);
        $websiteConfig = $this->getGerpWebsiteData();

        $resource = $this->_productResource;
        $connection = $resource->getConnection();

        foreach ($bindData as $key => $item){
            $data = $item[self::GERP_DATA_COLUMN];

            $startDate = isset($data['APPLY_DATE']) && $data['APPLY_DATE'] != '9999-12-31 00:00:00'? $data['APPLY_DATE']:null;
            $endDate = isset($data['EXPIRATION_DATE']) && $data['EXPIRATION_DATE'] != '9999-12-31 00:00:00'? $data['EXPIRATION_DATE']:null;

            if(!(isset($data['SUBSIDIARY_CODE'])))
            {
                $bindData[$key]['result'] = Status::GERP_DATA_SKIP;
                $bindData[$key]['message'] = __('Subsidiary code not exist');
                continue;
            }

            $itemConfigData = $websiteConfig[$data['SUBSIDIARY_CODE']];

            if(!(isset($data['CUSTOMER_CODE'])))
            {
                $bindData[$key]['result'] = Status::GERP_DATA_SKIP;
                $bindData[$key]['message'] = __('Customer code not exist');
                continue;
            }

            if(!($data['CUSTOMER_CODE'] == $itemConfigData['ship_to_code']))
            {
                $bindData[$key]['result'] = Status::GERP_DATA_SKIP;
                $bindData[$key]['message'] = __('Customer code validation fail');
                continue;
            }

            if(!($data['STATUS_CODE'] == 'ACTIVE'))
            {
                $bindData[$key]['result'] = Status::GERP_DATA_SKIP;
                $bindData[$key]['message'] = __('Status code is not active');
                continue;
            }

            $sku = $itemConfigData['sku_prefix'] . $itemConfigData['sku_split'] . trim($data['MODEL_CODE']);

            $product = $this->_getProduct($sku);

            if(!$product){
                $bindData[$key]['result'] = Status::GERP_DATA_PENDING;
                $bindData[$key]['message'] = __('Product Sku Not Found.');
                continue;
            }

            $rrp_price = $data['RRP_PRICE'];
            $unit_price = $data['UNIT_PRICE'];
            $locale_code = $itemConfigData['website_code'];

            $tableName = $resource->getTableName('eguana_gerp_price_date');
            $sql = "INSERT INTO " . $tableName . "(apply_date, expiration_date, unit_price , rrp_price, sku, status, locale_code, gerp_data) VALUES ('".$startDate."','".$endDate."','".$unit_price."','".$rrp_price."','".$sku."','N','".$locale_code."','".json_encode($data)."')";

            $updateResult = $connection->query($sql);


            if($updateResult)
            {
                $bindData[$key]['result'] = Status::GERP_DATA_PROCESS;
            }else{
                $bindData[$key]['message'] = Status::GERP_DATA_ERRPR;
                $bindData[$key]['message'] = __('An Error occurred while saving data');
            }
        }
        return $bindData;
    }

    protected function _getUpdateRepository($schedule){

        /**  @var $stagingRepo  \Magento\Staging\Api\Data\UpdateInterface */
        $stagingRepo = $this->_updateRepository->save($schedule);

        return $stagingRepo;
    }

    protected function _getSchedule($startDate,$endDate,$name = 'PTO PRICE UPDATE'){

        /** @var \Magento\Staging\Api\Data\UpdateInterface $schedule */
        $schedule = $this->_updateFactory->create();
        $schedule->setName($name);

        $currentDate = date('Y-m-d H:i:s T',time());

        if($currentDate > $startDate)
            $schedule->setStartTime($currentDate);
        else
            $schedule->setStartTime($startDate);

        $schedule->setEndTime($endDate);

        return $schedule;
    }

    protected function getGerpWebsiteData(){

        if(!$this->_gerpWebsiteData){

            $websiteData = $this->getDataHelper()->getGerpWebsiteData();
            $replaceData = [];

            foreach ($websiteData as $item){
                $replaceData[$item['legal_entity_name']] = $item;
            }

            $this->_gerpWebsiteData = $replaceData;
        }

        return $this->_gerpWebsiteData;
    }

    public function getDataWebsiteScope($data){
        $websiteData = $this->getGerpWebsiteData();
        return $websiteData["$data->SUBSIDIARY_CODE"]['website_code'];
    }

}