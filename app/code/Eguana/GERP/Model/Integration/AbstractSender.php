<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-10-18
 * Time: 오전 11:32
 */

namespace Eguana\GERP\Model\Integration;


use Eguana\GERP\Helper\GerpLogHelper;
use Eguana\GERP\Model\Source\Status;
use Magento\Catalog\Model\ProductRepository;

abstract class AbstractSender implements SenderInterface
{

    protected $_xmlHelper;

    protected $_processingWebsite;

    protected $_gerpLogHelper;

    protected $_documentType = '';

    protected $_currentGerpWebsiteData;

    public function __construct(
        \Eguana\GERP\Helper\XmlHelper $xmlHelper,
        GerpLogHelper $gerpLogHelper
    )
    {
        $this->_xmlHelper = $xmlHelper;
        $this->_gerpLogHelper = $gerpLogHelper;
    }


    public function execute(){

        $this->setWebsiteCode('pl');
        if(!$this->getWebsiteCode()){
            return;
        }

        $bindData = $this->bindData();

        if(count($bindData['CustomXML']['MessageBody']) == 0)
            return;

        $result = $this->process($bindData);

        $this->_result($result);

    }

    public function process($data)
    {
        $websiteConfig = $this->getGerpWebsiteData();
        $xml = $this->_xmlHelper->arrayToXml($data);

        $result = $this->transectionCall($websiteConfig['uri'],$xml->asXML());

        return [
            'result' => $result['result'],
            'message' => $result['message'],
            'data' => $data
        ];
    }

    protected function transectionCall($url,$data){

        $result = [
            'result' => false,
            'message' => __('Data transfer failed.')
        ];

        $ch=curl_init();
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        // post_data
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout in seconds
        curl_setopt($ch, CURLOPT_SSLVERSION,CURL_SSLVERSION_SSLv3);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Accept: application/xml', 'Content-Type: text/xml']);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        $response = curl_exec($ch);
        $info = curl_getinfo($ch);

        $writer = new \Zend\Log\Writer\Stream(BP . sprintf('/var/log/gerp_webmethod.%s.log',date('y-m-d')));
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info(__METHOD__);
        $logger->info('__HEADER__');
//        $logger->info($info['request_header']);
        $logger->info('__BODY__');
        $logger->info($data);

        
        // error
        if (!$response) {
            $result['message'] = curl_error($ch);
        } else {
            $responseStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            if(curl_getinfo($ch, CURLINFO_HTTP_CODE) == 200){
                $result['result'] = true;
            }

            $result['message'] = sprintf("RESPONSE CODE : %s. SUCCESS.",$responseStatus);
        }
        curl_close($ch);

        return $result;
    }

    public function setWebsiteCode($websiteCode){
        $this->_processingWebsite = $websiteCode;
        return $this;
    }

    public function getWebsiteCode(){
        return $this->_processingWebsite;
    }

    protected function _result($data){

        $status = Status::GERP_DATA_PROCESS;
        $message = $data['message'];


        if(!$data['result']) {
            $status = Status::GERP_DATA_ERRPR;
        }

        $logData = [];
        $interfaceData = $this->_xmlHelper->getGerpInterfaceConfig();
        $curInterface = $interfaceData[$this->_documentType];
        $headerData = $data['data']['CustomXML']['MessageHeader'];


        foreach ($data['data']['CustomXML']['MessageBody'] as $item){
            $logData[] = [
                'gerp_flag' => $curInterface['flag'],
                'status' => $status,
                'identifier_name' => $curInterface['identifier_name'],
                'identifier' => $item['ContentList'][$curInterface['identifier_name']],
                'sub_identifier_name' => $curInterface['sub_identifier_name'],
                'sub_identifier' => $item['ContentList'][$curInterface['sub_identifier_name']],
                'website_code' => $this->getWebsiteCode(),
                'interface_id' => $headerData['InterfaceID'],
                'sender_id' => $headerData['SenderID'],
                'receiver_id' => $headerData['ReceiverID'],
                'gerp_data' => json_encode($item,true),
                'message' => $message
            ];
        }

        $this->_gerpLogHelper->multipleInsert($logData);

        $this->result($data);
    }

    public function getGerpWebsiteData(){

        if(!$this->_currentGerpWebsiteData){
            $websiteConfig = $this->_xmlHelper->getGerpWebsiteData();
            $websiteCode = $this->getWebsiteCode();
            $currentWebsiteConfig = null;

            foreach ($websiteConfig as $data){
                if($data['website_code']  == $websiteCode){
                    $currentWebsiteConfig = $data;
                    $currentWebsiteConfig['interface_id'] = $this->getInterfaceId();
                    break;
                }
            }
            $this->_currentGerpWebsiteData = $currentWebsiteConfig;
        }

        return $this->_currentGerpWebsiteData;
    }

}