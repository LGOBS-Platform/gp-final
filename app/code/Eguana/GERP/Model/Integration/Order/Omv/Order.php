<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-10-18
 * Time: 오후 12:29
 */

namespace Eguana\GERP\Model\Integration\Order\Omv;


use Eguana\GERP\Model\Integration\Product\AbstractProduct;
use Eguana\GERP\Model\Source\Status;

class Order extends \Eguana\GERP\Model\Integration\Order\Omd\Order
{
    protected $_documentType = 'CXML_ACC_ORDREQ';

    protected function getOrderItemCollection(){
        if(!$this->_orderItemCollection){
            $orderItemCollection = $this->_createOrderItemCollection();

            $orderItemCollection->addFieldToFilter('product_from',['eq' => AbstractProduct::OMV_PRODUCT_FROM])
                ->addFieldToFilter('main_table.interface_status',['eq' => Status::GERP_DATA_PENDING]);

            $orderItemCollection->getSelect()->joinLeft(['so' => 'sales_order'],'order_id = so.entity_id',['status','state']);
            $orderItemCollection->addFieldToFilter('product_type', ['in' => ['simple','bundle']]);
            $orderItemCollection->addFieldToFilter('state',['in' => ['processing']]);
            $orderItemCollection->addFieldToFilter('so.created_at',['gt' => '2018-12-17 12:00:00']);
            $this->_orderItemCollection = $orderItemCollection;
        }

        return $this->_orderItemCollection;
    }


    protected function _dataToArray($gerpWebsiteData,$order,$item,$shippingAddress,$modelCode,$wareHouseCode,$isPtoMaster,$parentModelCode,$isPtoChild){

        $externalIncrementId = $this->convertExternalIncrementId($order->getIncrementId());
        $externalItemId = $this->convertExternalIncrementId($item->getItemId());
        $product = $this->_productRepository->get($item->getSku());
        $uomAttribute = $product->getCustomAttribute('primary_uom_code');
        $uomValue = ($uomAttribute)?$uomAttribute->getValue():'';


        return [
            'AFFILIATE_CODE' => $gerpWebsiteData['affiliate_code'],
            'ORDER_TYPE' => 'PO',
            'ORIG_SYS_DOCUMENT_REF' => $externalIncrementId,
            'CUSTOMER_ORDER_DATE' => $this->defaultTimeToStoreTime($order->getCreatedAt(),$gerpWebsiteData['timezone']),
            'CURRENCY_CODE' => $order->getOrderCurrencyCode(),
            'TOT_ORDER_AMT' => $order->getGrandTotal(),
            'SHIPPING_METHOD_CODE' => $gerpWebsiteData['omv_shipping_method_code'],
            'SHIP_TO_CODE' => $gerpWebsiteData['ship_to_code'],
            'BILL_TO_CODE' => $gerpWebsiteData['bill_to_code'],
            'SHIP_TO_ADDRESS1_INFO' => $shippingAddress->getStreetLine(1),
            'SHIP_TO_ADDRESS2_INFO' => $shippingAddress->getStreetLine(2).' '.$shippingAddress->getRegion(),
            'SHIP_TO_POSTAL_CODE' => $shippingAddress->getPostcode(),
            'SHIP_TO_COUNTRY_CODE' => strtoupper($shippingAddress->getCountryId()),
            'SHIP_TO_CUSTOMER_NAME' => $shippingAddress->getName(),
            'SHIP_TO_CUSTOMER_EMAIL' => $shippingAddress->getEmail(),
            'SHIP_TO_CUSTOMER_PHONE1' => $shippingAddress->getTelephone(),
            'ORIG_SYS_LINE_REF' => $externalItemId,
            'ORDER_QTY' => round($item->getQtyOrdered(),2),
            'PRIMARY_UOM_CODE' => $uomValue,
            'CUSTOMER_UNIT_PRICE' => $item->getPrice(),
            'ITEM_NO' => $modelCode,
            'SHIP_FROM_ORG' => $wareHouseCode,
            'NET_AMT' => $item->getPrice(),
            'EDI_SENDER_ID' => $gerpWebsiteData['obs_id'],
            'EDI_RECEIVER_ID' => $gerpWebsiteData['gerp_id'],
        ];
    }

}