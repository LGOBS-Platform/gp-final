<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-10-22
 * Time: 오후 8:15
 */

namespace Eguana\GERP\Model\Integration\Order\Omv;

use Eguana\GERP\Helper\GerpLogHelper;
use Eguana\GERP\Model\Integration\Order\AbstractReceiveOrder;
use Eguana\GERP\Model\Source\Status;

class OrderReceiver extends \Eguana\GERP\Model\Integration\Order\Omd\OrderReceiver
{
    protected $_flagKey = '14';

    protected $_statusCodeKey = 'LINE_STATUS_CODE';

    protected function returnProcess($status,$statusCode,$data,$rmaItemId){

        $result = [
            'result' => Status::GERP_DATA_SKIP,
            'message' => 'Invalid status code.',
        ];

        $item = $this->getRmaItem($rmaItemId);
        $rma = $this->getRma($item->getRmaEntityId());
        $erpOrderNo = null;

        if(isset($data['OE_HEADER_ID'])) {
            $rma->setData('omv_header_id', $data['OE_HEADER_ID']);
        }

        if(isset($data['ERP_ORDER_NO']))
        {
            $erpOrderNo = $data['ERP_ORDER_NO'];
        }

        if(isset($data['RETURN_APPROVAL_FLAG']) && $data['RETURN_APPROVAL_FLAG'] != 'Y'){
            $result = $this->_updateRma($rmaItemId,'gerp_reject',Status::GERP_RETURN_REJECT,$data['OE_LINE_ID'],$erpOrderNo);
        }else{
            switch ($statusCode){
                case $status[Status::GERP_RETURN_BOOKED] :
                    $result = $this->_updateRma($rmaItemId,'return_processing',Status::GERP_RETURN_BOOKED,$data['OE_LINE_ID'],$erpOrderNo);
                    break;
                case $status[Status::GERP_RETURN_AWAITING_RETURN] :
                    $result = $this->_updateRma($rmaItemId,'return_processing',Status::GERP_RETURN_AWAITING_RETURN,$data['OE_LINE_ID'],$erpOrderNo);
                    break;
                case $status[Status::GERP_RETURN_CLOSED] :
                    $result = $this->_updateRma($rmaItemId,'received',Status::GERP_RETURN_CLOSED,$data['OE_LINE_ID'],$erpOrderNo);
                    break;
                case 'E' :
                    $this->_updateErrorOrder($rmaItemId);
                    $result = [
                        'result' => Status::GERP_DATA_SKIP,
                        'message' => 'Return Error',
                    ];
                    break;
            }
        }

        return $result;
    }

    protected function orderProcess($status,$statusCode,$data,$orderItemId){

        $result = [
            'result' => Status::GERP_DATA_SKIP,
            'message' => 'Invalid status code.',
        ];

        $item = $this->getOrderItem($orderItemId);
        $order = $this->getOrder($item->getOrderId());
        $erpOrderNo = null;

        if(isset($data['OE_HEADER_ID']))
        {
            $order->setData('omv_header_id',$data['OE_HEADER_ID']);
        }

        if(isset($data['ERP_ORDER_NO']))
        {
            $erpOrderNo = $data['ERP_ORDER_NO'];
        }

        switch ($statusCode){
            case $status[Status::GERP_ORDER_BOOCKED] :
                $result = $this->_updateOrder($orderItemId,'processing','order_processing',Status::GERP_ORDER_BOOCKED,$data['OE_LINE_ID'],$erpOrderNo);
                break;
            case $status[Status::GERP_ORDER_BACKORERED] :
                $result = $this->_updateOrder($orderItemId,'holded','holded',Status::GERP_ORDER_BACKORERED,$data['OE_LINE_ID'],$erpOrderNo);
                break;
            case $status[Status::GERP_ORDER_AWAITING_SHIPPING] :
                $result = $this->_updateOrder($orderItemId,'processing','preparing_for_delivery',Status::GERP_ORDER_AWAITING_SHIPPING,$data['OE_LINE_ID'],$erpOrderNo);
                break;
            case $status[Status::GERP_RETURN_CLOSED] :
                $result = $this->_updateOrder($orderItemId,'processing','on_delivery',Status::GERP_RETURN_CLOSED,$data['OE_LINE_ID'],$erpOrderNo);
                break;
            case $status[Status::GERP_ORDER_CANCELED] :
                $result = $this->_updateOrder($orderItemId,'canceled','canceled',Status::GERP_ORDER_CANCELED,$data['OE_LINE_ID']);
                break;
            case 'E' :
                $this->_updateErrorOrder($orderItemId);
                $result = [
                    'result' => Status::GERP_DATA_SKIP,
                    'message' => $data['DETAIL_STATUS_DESC'],
                ];
                break;
        }

        return $result;
    }

    public function getDataWebsiteScope($data){
        $websiteData = $this->getDataHelper()->getGerpWebsiteData();
        return $websiteData["$data->AFFILIATE_CODE"]['website_code'];
    }



}