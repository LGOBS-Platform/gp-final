<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-10-18
 * Time: 오후 12:11
 */

namespace Eguana\GERP\Model\Integration\Order;


use Eguana\GERP\Helper\GerpLogHelper;
use Eguana\GERP\Model\Integration\AbstractReceiver;
use Eguana\GERP\Model\Source\Status;
use Magento\Rma\Api\Data\RmaInterface;
use Magento\Rma\Model\ItemFactory;
use Magento\Rma\Model\Item;
use Magento\Rma\Model\RmaRepository;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Sales\Model\Order\ItemRepository;
use Magento\Sales\Model\OrderRepository;

abstract class AbstractReceiveOrder extends AbstractReceiver
{
    const EXTERNAL_ORDER_INCREMENT_PREFIX = 'ORDER';

    protected $_orderResource;
    
    protected $_gerpLogHelper;

    protected $_incrementPrefix = self::EXTERNAL_ORDER_INCREMENT_PREFIX;

    protected $_incrementSplit = '_';
    
    protected $_status;

    protected $_connection;

    protected $_resource;

    protected $_orderData;

    protected $_orderItemData;

    protected $_gerpWebsiteData;

    protected $_orderRepository;

    protected $_orderItemRepository;

    protected $_rmaData;

    protected $_rmaItemData;

    protected $_rmaRepository;

    protected $_rmaItemFactory;

    public function __construct(
        \Eguana\GERP\Model\ResourceModel\Log\Collection $logCollection,
        \Eguana\GERP\Helper\Data $dataHelper,
        \Magento\Sales\Model\ResourceModel\Order $orderResource,
        GerpLogHelper $gerpLogHelper,
        Status $status,
        OrderRepository $orderRepository,
        ItemRepository $orderItemRepository,
        RmaRepository $rmaRepository,
        ItemFactory $rmaItemFactory
    )
    {
        parent::__construct($logCollection, $dataHelper);
        $this->_orderResource = $orderResource;
        $this->_gerpLogHelper = $gerpLogHelper;
        $this->_status = $status;
        $this->_orderRepository = $orderRepository;
        $this->_orderItemRepository = $orderItemRepository;
        $this->_rmaRepository = $rmaRepository;
        $this->_rmaItemFactory = $rmaItemFactory;

    }

    protected function _updateItemById($itemId,$data){
        $item = $this->getOrderItem($itemId);
        foreach ($data as $key => $value){
            $item->setData($key,$value);
        }

    }

    public function getOrderType($externalIncrementId){

        $splitIncementId = explode($this->_incrementSplit,$externalIncrementId);

        if(isset($splitIncementId[1])){
            return $splitIncementId[0];
        }

        return $this->_incrementPrefix;
    }

    public function convertExternalIncrementId($internalIncrementId){
        return $this->_incrementPrefix.$this->_incrementSplit.$internalIncrementId;
    }

    public function convertInternalIncrementId($externalIncrementId){

        $splitIncementId = explode($this->_incrementSplit,$externalIncrementId);

        if(isset($splitIncementId[1])){
            return $splitIncementId[1];
        }

        return $externalIncrementId;
    }

    /**
     * @param $rmaId string
     * @return RmaInterface
     */
    protected function getRma($rmaId){
        if(!isset($this->_rmaData[$rmaId])){
            $this->_rmaData[$rmaId] = $this->_rmaRepository->get($rmaId);
        }
        return $this->_rmaData[$rmaId];
    }

    /**
     * @param $itemId string
     * @return Item
     */
    protected function getRmaItem($itemId){
        if(!isset($this->_rmaItemData[$itemId])){
            $this->_rmaItemData[$itemId] = $this->_rmaItemFactory->create()->load($itemId);
        }
        return $this->_rmaItemData[$itemId];
    }



    /**
     * @param $rmaItemId string
     * @param $updateStatus string
     * @param $interfaceStatus string
     * @param $gerpLineId string
     * @return array
     */
    protected function _updateRma($rmaItemId,$updateStatus,$interfaceStatus,$gerpLineId,$erpOrderNo = null){

        $item = $this->getRmaItem($rmaItemId);
        $rma = $this->getRma($item->getRmaEntityId());

        $rmaStatus = $rma->getStatus();

        $result = [
            'result' => Status::GERP_DATA_SKIP,
            'message' => '',
        ];

        $message = null;

        $item->setData('interface_status',$interfaceStatus);
        $item->setData('gerp_line_id',$gerpLineId);
        $item->setStatus($updateStatus);

        if($erpOrderNo)
            $item->setData('erp_order_no',$erpOrderNo);

        $isStatusUpdate = $this->rmaStatusUpdateCheck($rma,$interfaceStatus,$rmaItemId);

        if($isStatusUpdate){
            $result['result'] = Status::GERP_DATA_PROCESS;
            $rma->setStatus($updateStatus);
        }else{
            $result['message'] = $message ? $message : sprintf('Current Order Status : %s , Order Status Update Skip. Item Update.',$rmaStatus);
        }

        return $result;
    }

    /**
     * @param $orderItemId string
     * @param $updateState string
     * @param $updateStatus string
     * @param $interfaceStatus string
     * @param $gerpLineId string
     * @return array
     */
    protected function _updateOrder($orderItemId,$updateState,$updateStatus,$interfaceStatus,$gerpLineId,$erpOrderNo = null){

        $item = $this->getOrderItem($orderItemId);
        $order = $this->getOrder($item->getOrderId());

        $orderState = $order->getState();
        $result = [
            'result' => Status::GERP_DATA_SKIP,
            'message' => '',
        ];
        $message = null;

        if(!in_array($item->getData('interface_status'),[
                Status::GERP_ORDER_HOLD,
                Status::GERP_ORDER_BACK,
            ]) && $item->getData('interface_status') > $interfaceStatus){
            $result['message'] = __('Already in progress');
            return $result;
        }

        $updateItemData = [
            'interface_status' => $interfaceStatus,
            'gerp_line_id' => $gerpLineId,
            'back_interface' => 'S'
        ];

        if($erpOrderNo)
            $updateItemData['erp_order_no'] = $erpOrderNo;

        $this->_updateItemById($orderItemId,$updateItemData);

        $isStatusUpdate = true;

        if($updateState == 'holded'){
        }else if($orderState == 'holded'){
            $isStatusUpdate = $this->orderStatusUpdateCheck($order,$interfaceStatus,$orderItemId,[Status::GERP_ORDER_BACK,Status::GERP_ORDER_HOLD]);
        }else if($orderState == 'processing'){
            $isStatusUpdate = $this->orderStatusUpdateCheck($order,$interfaceStatus,$orderItemId);
        }
        

        if($isStatusUpdate){
            $result['result'] = Status::GERP_DATA_PROCESS;
            $order->setState($updateState);
            $order->setStatus($updateStatus);
        }else{
            $result['message'] = $message ? $message : sprintf('Current Order Status : %s , Order Status Update Skip. Item Update.',$order->getStatus());
        }

        return $result;
    }

    /**
     * @param  $order OrderInterface
     * @param $interfaceStatus string
     * @param $skipItemId int
     * @param $checkInterface array
     * @return boolean
    */
    public function orderStatusUpdateCheck($order,$interfaceStatus,$skipItemId = null,$checkInterface = []){

        $isStatusUpdate = true;

        $arrayCheck = count($checkInterface) > 0 ? true : false;

        foreach ($order->getItems() as $orderItem){

            $orderItem = $this->getOrderItem($orderItem->getItemId());

            if($orderItem->getProductType() == 'configurable')
                continue;

            if($arrayCheck){

                if(in_array($orderItem->getData('interface_status'),$checkInterface)){
                    if($skipItemId != $orderItem->getItemId()){
                        $isStatusUpdate = false;
                        break;
                    }
                }

            }

            if($orderItem->getData('interface_status') < $interfaceStatus){

                if($orderItem->getParentItemId())
                {
                    if($skipItemId && $skipItemId == $orderItem->getParentItemId())
                        continue;
                }

                if($skipItemId && $skipItemId == $orderItem->getItemId())
                    continue;

                $isStatusUpdate = false;
                break;
            }
        }

        return $isStatusUpdate;
    }

    /**
     * @param  $rma RmaInterface
     * @param $interfaceStatus string
     * @param $skipItemId int
     * @param $checkInterface array
     * @return boolean
     */
    public function rmaStatusUpdateCheck($rma,$interfaceStatus,$skipItemId = null){

        $isStatusUpdate = true;

        foreach ($rma->getItemsForDisplay() as $rmaItem){

            $rmaItem = $this->getRmaItem($rmaItem->getEntityId());

            if($rmaItem->getData('interface_status') < $interfaceStatus){

                if($skipItemId && $skipItemId == $rmaItem->getItemId())
                    continue;

                $isStatusUpdate = false;
                break;
            }
        }

        return $isStatusUpdate;
    }

    /**
     * @param $orderId string
     * @return OrderInterface
     */
    protected function getOrder($orderId){
        if(!isset($this->_orderData[$orderId])){
            $this->_orderData[$orderId] = $this->_orderRepository->get($orderId);
        }
        return $this->_orderData[$orderId];
    }

    /**
     * @param $itemId string
     * @return OrderItemInterface
     */
    protected function getOrderItem($itemId){
        if(!isset($this->_orderItemData[$itemId])){
            $this->_orderItemData[$itemId] = $this->_orderItemRepository->get($itemId);
        }
        return $this->_orderItemData[$itemId];
    }

    protected function getGerpWebsiteData(){

        if(!$this->_gerpWebsiteData){

            $websiteData = $this->getDataHelper()->getGerpWebsiteData();
            $replaceData = [];

            foreach ($websiteData as $item){
                $replaceData[$item['legal_entity_name']] = $item;
            }

            $this->_gerpWebsiteData = $replaceData;
        }

        return $this->_gerpWebsiteData;
    }

    public function getDataWebsiteScope($data){
        $websiteData = $this->getGerpWebsiteData();
        return $websiteData["$data->CORPORATION_CODE"]['website_code'];
    }

    protected function _getResource(){
        return $this->_orderResource;
    }

    protected function _getConnection(){
        if(!$this->_connection){
            $this->_connection = $this->_getResource()->getConnection();
        }
        return $this->_connection;
    }

    protected function _updateErrorOrder($orderItemId){

        $updateItemData = [
            'back_interface' => 'E'
        ];

        $this->_updateItemById($orderItemId,$updateItemData);
    }

    public function result($resultData)
    {
        // TODO: Implement result() method.
    }

}