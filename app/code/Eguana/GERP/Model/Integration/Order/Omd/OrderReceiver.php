<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-10-22
 * Time: 오후 8:15
 */

namespace Eguana\GERP\Model\Integration\Order\Omd;

use Eguana\GERP\Helper\GerpLogHelper;
use Eguana\GERP\Model\Integration\Order\AbstractReceiveOrder;
use Eguana\GERP\Model\Source\Status;

class OrderReceiver extends AbstractReceiveOrder
{
    protected $_flagKey = '09';

    protected $_statusCodeKey = 'LINE_STATUS_CODE2';

    public function process($bindData)
    {
        $status = $this->_status->getValues();

        foreach ($bindData as $key => $item){

            $data = $item[self::GERP_DATA_COLUMN];

            try {
                $statusCode = (isset($data[$this->_statusCodeKey])) ? $data[$this->_statusCodeKey] : 'E';
                $type = (isset($data['ORIG_SYS_DOCUMENT_REF'])) ? $this->getOrderType($data['ORIG_SYS_DOCUMENT_REF']) : $this->getOrderType($data['DocumentID']);
//            $type = $this->getOrderType($data['ORIG_SYS_DOCUMENT_REF']);
                $orderItemId = (isset($data['ORIG_SYS_LINE_REF'])) ? $this->convertInternalIncrementId($data['ORIG_SYS_LINE_REF']) : $this->convertInternalIncrementId($data['BusinessID']);
//            $orderItemId = $this->convertInternalIncrementId($data['ORIG_SYS_LINE_REF']);
            }catch (\Exception $e){
                continue;
            }
            if($type == self::EXTERNAL_ORDER_INCREMENT_PREFIX){
                $result = $this->orderProcess($status,$statusCode,$data,$orderItemId);
            }else{
                $result = $this->returnProcess($status,$statusCode,$data,$orderItemId);
            }

            $bindData[$key]['result'] = $result['result'];
            $bindData[$key]['message'] = $result['message'];

        }

        if(count($this->_rmaItemData) > 0) {

            $itemData = $this->_rmaItemData;
            $itemIds = array_keys($itemData);
            $rmaItemResource = $itemData[$itemIds[0]]->getResource();
            $rmaItemConnection = $rmaItemResource->getConnection();

            foreach ($itemData as $rmaItem){
                $rmaItemConnection->update($rmaItemResource->getTable('magento_rma_item_entity'),
                    [
                        'status' => $rmaItem->getStatus(),
                        'interface_status' => $rmaItem->getData('interface_status'),
                    ],
                    ['entity_id in (?)' => $rmaItem->getEntityId()]);
//                $rmaItem->save();
            }
        }

        if(count($this->_rmaData) > 0) {
            foreach ($this->_rmaData as $rma) {
                $this->_rmaRepository->save($rma);
            }
        }

        if(count($this->_orderData) > 0){
            foreach ($this->_orderData as $order){
                $this->_orderRepository->save($order);
            }
        }


        if(count($this->_orderItemData) > 0) {
            foreach ($this->_orderItemData as $item) {
                $this->_orderItemRepository->save($item);
            }
        }

        return $bindData;
    }

    protected function returnProcess($status,$statusCode,$data,$rmaItemId){

        $result = [
            'result' => Status::GERP_DATA_SKIP,
            'message' => 'Invalid status code.',
        ];

        $item = $this->getRmaItem($rmaItemId);
        $rma = $this->getRma($item->getRmaEntityId());
        $rma->setData('gerp_header_id',$data['ORDER_HEADER_ID']);

        switch ($statusCode){
            case $status[Status::GERP_RETURN_AWAITING_RETURN] :
                $result = $this->_updateRma($rmaItemId,'return_processing',Status::GERP_RETURN_AWAITING_RETURN,$data['ORDER_LINE_ID']);
                break;
            case $status[Status::GERP_RETURN_INVO] :
                $result = $this->_updateRma($rmaItemId,'received',Status::GERP_RETURN_INVO,$data['ORDER_LINE_ID']);
                break;
        }

        return $result;
    }

    protected function orderProcess($status,$statusCode,$data,$orderItemId){

        $result = [
            'result' => Status::GERP_DATA_SKIP,
            'message' => 'Invalid status code.',
        ];

        $item = $this->getOrderItem($orderItemId);
        $order = $this->getOrder($item->getOrderId());
        $order->setData('gerp_header_id',$data['ORDER_HEADER_ID']);

        switch ($statusCode){
            case $status[Status::GERP_ORDER_ENTERED] :
                $result = $this->_updateOrder($orderItemId,'processing','order_processing',Status::GERP_ORDER_ENTERED,$data['ORDER_LINE_ID']);
                break;
            case $status[Status::GERP_ORDER_BOOCKED] :
                $result = $this->_updateOrder($orderItemId,'processing','order_processing',Status::GERP_ORDER_BOOCKED,$data['ORDER_LINE_ID']);
                break;
            case $status[Status::GERP_ORDER_HOLD] :
                $result = $this->_updateOrder($orderItemId,'holded','holded',Status::GERP_ORDER_HOLD,$data['ORDER_LINE_ID']);
                break;
            case $status[Status::GERP_ORDER_BACK] :
                $result = $this->_updateOrder($orderItemId,'holded','holded',Status::GERP_ORDER_BACK,$data['ORDER_LINE_ID']);
                break;
            case $status[Status::GERP_ORDER_OPEN] :
                $result = $this->_updateOrder($orderItemId,'processing','preparing_for_delivery',Status::GERP_ORDER_OPEN,$data['ORDER_LINE_ID']);
                break;
            case $status[Status::GERP_ORDER_PICK] :
                $result = $this->_updateOrder($orderItemId,'processing','picking_for_delivery',Status::GERP_ORDER_PICK,$data['ORDER_LINE_ID']);
                break;
            case $status[Status::GERP_ORDER_DELY] :
                $result = $this->_updateOrder($orderItemId,'processing','on_delivery',Status::GERP_ORDER_DELY,$data['ORDER_LINE_ID']);
                break;
            case $status[Status::GERP_ORDER_INVO] :
                $result = $this->_updateOrder($orderItemId,'processing','delivery_completed',Status::GERP_ORDER_INVO,$data['ORDER_LINE_ID']);
                break;
            case $status[Status::GERP_ORDER_CANCELLED] :
                $result = $this->_updateOrder($orderItemId,'canceled','canceled',Status::GERP_ORDER_CANCELLED,$data['ORDER_LINE_ID']);
                break;
        }

        return $result;
    }

}