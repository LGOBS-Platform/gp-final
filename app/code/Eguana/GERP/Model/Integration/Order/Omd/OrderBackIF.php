<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-10-22
 * Time: 오후 8:15
 */

namespace Eguana\GERP\Model\Integration\Order\Omd;


use Eguana\GERP\Model\Integration\Order\AbstractReceiveOrder;
use Eguana\GERP\Model\Source\Status;

class OrderBackIF extends AbstractReceiveOrder
{
    protected $_flagKey = '10';

    protected $_gerpWebsiteData;

    public function process($bindData)
    {
        $resource = $this->_orderResource;
        $connection = $resource->getConnection();

        foreach ($bindData as $key => $item){

            $data = $item[self::GERP_DATA_COLUMN];
            $type = $this->getOrderType($data['ORIG_SYS_DOCUMENT_REF']);

            $result = $this->typeProcess($data,$resource,$connection,$type);

            $bindData[$key]['result'] = $result['result'];
            $bindData[$key]['message'] = $result['message'];
        }

        return $bindData;
    }

    protected function typeProcess($data,$resource,$connection,$type){

        $internalItemId = $this->convertInternalIncrementId($data['ORIG_SYS_LINE_REF']);

        $statusCode = $data['PROCESS_STATUS_CODE'];
        $result = Status::GERP_DATA_ERRPR;
        $message = isset($data['ERROR_TEXT'])?$data['ERROR_TEXT']:null;

        $updateTableNm = ($type == self::EXTERNAL_ORDER_INCREMENT_PREFIX)?'sales_order_item':'magento_rma_item_entity';
        $whereColumn = ($type == self::EXTERNAL_ORDER_INCREMENT_PREFIX)?'item_id':'entity_id';

        $erp_order_no = (isset($data['ERP_ORDER_NO'])) ? $data['ERP_ORDER_NO'] : null;

        switch ($statusCode){
            case 'S':
                $result = Status::GERP_DATA_PROCESS;

                try{
                    $this->updateItemData($resource,$connection,$updateTableNm,[
                        'back_interface' => $statusCode,
                        'erp_order_no' => $erp_order_no
                    ],$internalItemId,$whereColumn);
                }catch (\Exception $e){
                    $result = Status::GERP_DATA_ERRPR;
                    $message = $e->getMessage();
                }
                break;

            case 'E':
                try{
                    $this->updateItemData($resource,$connection,$updateTableNm,[
                        'back_interface' => $statusCode,
                    ],$internalItemId,$whereColumn);
                }catch (\Exception $e){
                    $result = Status::GERP_DATA_ERRPR;
                    $message = $e->getMessage();
                }
                break;
            default :
                $message = 'Invalid status code.';
        }

        return [
            'result' => $result,
            'message' => $message
        ];
    }

    protected function updateItemData($resource,$connection,$updateTableNm,$data,$itemId,$whereColumn){

        $connection->update(
            $resource->getTable($updateTableNm),
            $data,
            [
                $whereColumn.'=?' => $itemId
            ]
        );
    }

    protected function getGerpWebsiteData(){

        if(!$this->_gerpWebsiteData){

            $websiteData = $this->getDataHelper()->getGerpWebsiteData();
            $replaceData = [];

            foreach ($websiteData as $item){
                $replaceData[$item['legal_entity_name']] = $item;
            }

            $this->_gerpWebsiteData = $replaceData;
        }

        return $this->_gerpWebsiteData;
    }

    public function getDataWebsiteScope($data){
        $websiteData = $this->getGerpWebsiteData();
        return $websiteData["$data->CORPORATION_CODE"]['website_code'];
    }

}