<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-10-18
 * Time: 오후 12:29
 */

namespace Eguana\GERP\Model\Integration\Order\Omd;


use Eguana\GERP\Model\Integration\Order\AbstractSendOrder;
use Eguana\GERP\Model\Integration\Product\AbstractProduct;
use Eguana\GERP\Model\Source\Status;

class Cancel extends AbstractSendOrder
{
    protected $_documentType = 'CXML_ORDERS';

    protected $_successOrderStatus = 'canceled';

    protected $_successInterfaceStatus = Status::GERP_DATA_CANCEL;

    protected function getOrderItemCollection(){

        $orderItemCollection = $this->_createOrderItemCollection();

        $orderItemCollection->addFieldToFilter('product_from',['eq' => AbstractProduct::OMD_PRODUCT_FROM])
            ->addFieldToFilter('main_table.interface_status',[
                'nin' => [
                    Status::GERP_DATA_PENDING,
                    Status::GERP_DATA_CANCEL,
                    Status::GERP_ORDER_PICK,
                    Status::GERP_ORDER_DELY,
                    Status::GERP_ORDER_INVO
                ]
            ]);

        $orderItemCollection->getSelect()->joinLeft(['so' => 'sales_order'],'order_id = so.entity_id','status');
        $orderItemCollection->addFieldToFilter('product_type', ['in' => ['simple','bundle']]);
        $orderItemCollection->addFieldToFilter('status',['eq' => 'canceled']);
        $orderItemCollection->addFieldToFilter('so.created_at',['gt' => '2018-12-17 12:00:00']);
        return $orderItemCollection;
    }

    protected function _dataToArray($gerpWebsiteData,$order,$item,$shippingAddress,$modelCode,$wareHouseCode,$isPtoMaster,$parentModelCode,$isPtoChild){

        $externalIncrementId = $this->convertExternalIncrementId($order->getIncrementId());
        $externalItemId = $this->convertExternalIncrementId($item->getItemId());

        return [
            'CORPORATION_CODE' => $gerpWebsiteData['legal_entity_name'],
            'ORIG_SYS_DOCUMENT_REF' => $externalIncrementId,
            'ORIG_SYS_LINE_REF' => $externalItemId,
            'ORDER_TYPE' => $gerpWebsiteData['order_type'],
            'SHIP_TO_CODE' => $gerpWebsiteData['ship_to_code'],
            'CUSTOMER_NAME' => $shippingAddress->getName(),
            'POSTAL_CODE' => $shippingAddress->getPostcode(),
//            'STATE_CODE' => $shippingAddress->getRegion(),
            'CITY_NAME' => $shippingAddress->getCity(),
            'ADDRESS_LINE1_INFO' => $shippingAddress->getStreetLine(1),
            'ADDRESS_LINE2_INFO' => $shippingAddress->getStreetLine(2).' '.$shippingAddress->getRegion(),
            'ADDRESS_LINE3_INFO' => $shippingAddress->getEmail(),
            'PHONE_NO' => $shippingAddress->getTelephone(),
            'EMAIL_ADDR' => $shippingAddress->getEmail(),
            'MOBILE_PHONE_NO' => $shippingAddress->getTelephone(),
            'ONETIME_SHIP_TO_FLAG' => $gerpWebsiteData['ontime_shop_to_flag'],
            'ITEM_NO' => $modelCode,
            'ORDER_QTY' => 0, // Loviit 결제모듈 Cancel Qty 체크 안함 고정값 이용
            'CREATION_DATE' => $this->getCurrentDateString(),
            'LAST_UPDATE_DATE' => $this->getCurrentDateString(),
            'CUST_PO_NO' => $externalIncrementId,
            'COUNTRY_CODE' => strtoupper($shippingAddress->getCountryId()),
            'PROCESS_STATUS_CODE' => 'I',
            'ORDER_SOURCE_NAME' => $gerpWebsiteData['order_source_name'],
            'ATTRIBUTE4' => $order->getShippingAmount(),
            'ORIGINAL_HEADER_ID' => $order->getData('gerp_header_id'),
            'ORIGINAL_LINE_ID' => $item->getData('gerp_line_id'),
            'WAREHOUSE_CODE' => $wareHouseCode,
            'REQUEST_DATE' => $this->getRequestDateString(),
            'UNIT_SELLING_PRICE' => ($isPtoMaster) ? 0: $item->getPrice(),  //unit_price, Set모델의 대표모델은 0 BUNDLE PRODUCT 0
            'CURRENCY_CODE' => $order->getOrderCurrencyCode(),
            'SET_MODEL_CODE' => $parentModelCode,
            'ITEM_TYPE_CODE' => ($isPtoMaster) ? 'MODEL': (($isPtoChild) ? 'OPTION': 'STANDARD'),
            'PARTIAL_FLAG' => $gerpWebsiteData['partial_flag'],
            'OPERATION_CODE' => 'UPDATE',
            'CALCULATION_PRICE_FLAG' => $gerpWebsiteData['calculation_price_flag'],
            'PRICING_DATE' => $this->defaultTimeToStoreTime($order->getCreatedAt(),$gerpWebsiteData['timezone']),
            'DELIVERY_TYPE_CODE' => $item->getData('delivery_type'),
            'RECEIVER_NAME' => $shippingAddress->getName(),
            'RECEIVER_PHONE_NO' => $shippingAddress->getTelephone(),
            'RECEIVER_MOBILE_NO' => $shippingAddress->getTelephone(),
            'ATTRIBUTE30' => $item->getData('product_carrier'),
        ];
    }

    public function result($data)
    {
        $itemIds = [];
        $orderIncrementIds = [];

        $items = $data['data']['CustomXML']['MessageBody'];

        if($data['result'] == 200){

            foreach ($items as $item){
                $itemIds[] = $this->convertInternalIncrementId($item['ContentList']['ORIG_SYS_LINE_REF']);
                $orderIncrementIds[] = $this->convertInternalIncrementId($item['ContentList']['ORIG_SYS_DOCUMENT_REF']);
            }

            $orderItemResource = $this->getOrderItemCollection()->getResource();


            $orderItemResource->getConnection()->update($orderItemResource->getMainTable(),
                ['interface_status' => $this->_successInterfaceStatus],
                ['item_id in (?)' => $itemIds]);

        }
    }

}