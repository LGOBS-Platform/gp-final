<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-10-18
 * Time: 오후 12:29
 */

namespace Eguana\GERP\Model\Integration\Order\Omd;

use Eguana\GERP\Model\Integration\Order\AbstractSendOrder;
use Eguana\GERP\Model\Integration\Product\AbstractProduct;
use Eguana\GERP\Model\Source\Status;
use Amasty\MultiInventory\Model\Warehouse\OrderItemRepository;
use Eguana\GERP\Helper\GerpLogHelper;
use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Sales\Model\Order\Address;
use Magento\Sales\Model\Order\ItemRepository;


class Rma extends AbstractSendOrder
{
    protected $_documentType = 'CXML_ORDERS';

    protected $_incrementPrefix = 'RETURN';

    protected $_rmaItemCollection;

    protected $_bundleItemWareHouseInfo = [];

    protected $_successInterfaceStatus = Status::GERP_DATA_PROCESS;

    protected $_reasonData;

    protected $_rmaItemCollectionFactory;

    protected $_productRepository;

    protected $_timezone;

    protected $_currentDate;

    protected $_currentDateString;

    protected $_requestDateString;

    protected $_warehouseOrderItemRepository;

    protected $_orderItemRepository;

    protected $_eavConfig;

    protected $_orderItemData;

    public function __construct(
        \Eguana\GERP\Helper\XmlHelper $xmlHelper,
        GerpLogHelper $gerpLogHelper,
        ProductRepository $productRepository,
        TimezoneInterface $timezoneInterface,
        \Magento\Sales\Model\ResourceModel\Order\Item\CollectionFactory $orderItemCollectionFactory,
        OrderItemRepository $warehouseOrderItemRepository,
        \Magento\Rma\Model\ResourceModel\Item\CollectionFactory $rmaItemCollectionFactory,
        ItemRepository $orderItemRepository,
        \Magento\Eav\Model\Config $eavConfig
    )
    {
        $this->_rmaItemCollectionFactory = $rmaItemCollectionFactory;
        $this->_orderItemRepository = $orderItemRepository;
        $this->_orderItemCollectionFactory = $orderItemCollectionFactory;
        $this->_productRepository = $productRepository;
        $this->_timezone = $timezoneInterface;
        $this->_warehouseOrderItemRepository = $warehouseOrderItemRepository;
        $this->_eavConfig = $eavConfig;
        parent::__construct( $xmlHelper,$gerpLogHelper,$productRepository,$timezoneInterface,$orderItemCollectionFactory,$warehouseOrderItemRepository,$orderItemRepository);
    }

    public function getAllReturnResonCode(){


        if(is_null($this->_reasonData)){

            $reasonData = [];

            $reasonAttributes = [
                'condition',
                'reason'
            ];

            foreach ($reasonAttributes as $reasonAttribute){

                $attribute = $this->_eavConfig->getAttribute('rma_item', $reasonAttribute);
                $labelOptions = $attribute->getSource()->getAllOptions();
                $attribute->setStoreId(0);
                $valueOptions = $attribute->getSource()->getAllOptions();


                foreach ($labelOptions as $option){

                    if($option['value'] == '')
                        continue;

                    $reasonData[$option['value']]['label'] = $option['label'];
                }


                foreach ($valueOptions as $option){

                    if($option['value'] == '')
                        continue;

                    $reasonData[$option['value']]['value'] = $option['label'];
                }

            }

            $this->_reasonData = $reasonData;
        }

        return $this->_reasonData;
    }

    public function bindData()
    {
        $gerpWebsiteData = $this->getGerpWebsiteData();
        $bindData = $this->getFrameData($gerpWebsiteData);

        $rmaItemCollection  = $this->getRmaItemCollection();

        /**
         * @var $item \Magento\Rma\Model\Item
         */
        foreach ($rmaItemCollection as $item){

            $order = $item->getRma()->getOrder();
            $shippingAddress = $order->getShippingAddress();
            $orderItem = $this->_orderItemRepository->get($item->getOrderItemId());
            $rma = $item->getRma();

            $bindData['CustomXML']['MessageBody'][]['ContentList'] = $this->_rmaItemBinding($rma,$item,$order,$orderItem,$shippingAddress,$gerpWebsiteData);
        }

        return $bindData;
    }

    /**
     * @param $rma \Magento\Rma\Model\Rma
     * @param $rmaItem \Magento\Rma\Model\Item
     * @param $order \Magento\Sales\Model\Order
     * @param $item OrderItemInterface
     * @param $shippingAddress \Magento\Sales\Model\Order\Address
     * @param $gerpWebsiteData []
     * @return array
     */
    protected function _rmaItemBinding($rma,$rmaItem,$order,$item,$shippingAddress,$gerpWebsiteData){

        $skuSplit = explode('.',$item->getSku());
        $isPtoMaster = false;
        $isPtoChild = false;
        $parentModelCode = '';

        if(isset($skuSplit[2]))
            $modelCode = $skuSplit[1].'.'.$skuSplit[2];
        else
            $modelCode = $skuSplit[1];

        $wareHouseCode = null;

        if($item->getParentItemId()){
            $parentItem = $this->getOrderItem($item->getParentItemId());
            if($parentItem->getProductType() == 'bundle'){
                $isPtoChild = true;

                $parentModelCodeSplit = explode('.',$item->getParentItem()->getSku());
                $parentModelCode = $parentModelCodeSplit[1].'.'.$parentModelCodeSplit[2];
            }elseif ($parentItem->getProductType() == 'configurable'){
                $item = $parentItem;
            }
            $wareHouseCode = $this->_warehouseOrderItemRepository->getByOrderItemId($item->getItemId())->getWarehouse()->getCode();
        }else{
            if($item->getProductType() == 'bundle'){
                $isPtoMaster = true;
                $order = $item->getOrder();
                foreach ($order->getAllItems() as $orderItem){
                    if($orderItem->getParentItemId() == $item->getItemId()){
                        $wareHouseCode = $this->_warehouseOrderItemRepository->getByOrderItemId($orderItem->getItemId())->getWarehouse()->getCode();
                        break;
                    }
                }
            }else{
                $wareHouseCode = $this->_warehouseOrderItemRepository->getByOrderItemId($item->getItemId())->getWarehouse()->getCode();
            }
        }

        return $this->_rmaDataToArray($gerpWebsiteData,$rma,$rmaItem,$order,$item,$shippingAddress,$modelCode,$wareHouseCode,$isPtoMaster,$parentModelCode,$isPtoChild);
    }

    public function result($data)
    {
        $itemIds = [];
        $orderIncrementIds = [];

        $items = $data['data']['CustomXML']['MessageBody'];

        if($data['result'] == 200){

            $rmaItemResource = $this->getRmaItemCollection()->getResource();

            foreach ($items as $item){

                $itemId = $this->convertInternalIncrementId($item['ContentList']['ORIG_SYS_LINE_REF']);
                $itemIds[] = $itemId;
                $orderIncrementIds[] = $this->convertInternalIncrementId($item['ContentList']['ORIG_SYS_DOCUMENT_REF']);
                $rmaItemResource->getConnection()->update($rmaItemResource->getTable('magento_rma_item_entity'),
                    [
                        'qty_authorized' => $item['ContentList']['ORDER_QTY'],
                        'status' => 'authorized'
                    ],
                    ['entity_id = ?' => $itemId]);
            }


            $rmaItemResource->getConnection()->update($rmaItemResource->getTable('magento_rma_item_entity'),
                ['interface_status' => $this->_successInterfaceStatus],
                ['entity_id in (?)' => $itemIds]);

            $rmaItemResource->getConnection()->update($rmaItemResource->getTable('magento_rma'),
                ['status' => 'authorized'],
                ['increment_id in (?)' => $orderIncrementIds]);

            $rmaItemResource->getConnection()->update($rmaItemResource->getTable('magento_rma_grid'),
                ['status' => 'authorized'],
                ['increment_id in (?)' => $orderIncrementIds]);

        }
    }

    protected function getRmaItemCollection(){

        if(!$this->_rmaItemCollection){
            $rmaItemCollection = $this->_createRmaItemCollection();
            $rmaItemCollection->addAttributeToSelect('*');
            $rmaItemCollection->getSelect()->joinLeft(['soi' => 'sales_order_item'],'soi.item_id = order_item_id',['product_from','product_type']);
            $rmaItemCollection->getSelect()->where('product_from = ?',AbstractProduct::OMD_PRODUCT_FROM);
            $rmaItemCollection->getSelect()->where('product_type in (?)', ['simple','bundle']);
            $rmaItemCollection->getSelect()->where('soi.created_at > ?', '2018-12-17 12:00:00');
            $rmaItemCollection->getSelect()->where('e.interface_status is null');
            $rmaItemCollection->getSelect()->where('e.status = ?','authorized');
            $this->_rmaItemCollection = $rmaItemCollection;
        }

        return $this->_rmaItemCollection;
    }

    /** @return \Magento\Rma\Model\ResourceModel\Item\Collection */
    protected function _createRmaItemCollection(){
        return $this->_rmaItemCollectionFactory->create();
    }

    /**
     * @param $gerpWebsiteData []
     * @param $rma \Magento\Rma\Model\Rma
     * @param $rmaItem \Magento\Rma\Model\Item
     * @param $order \Magento\Sales\Model\Order
     * @param $item OrderItemInterface
     * @param $shippingAddress \Magento\Sales\Model\Order\Address
     * @param $modelCode string
     * @param $wareHouseCode string
     * @param $isPtoMaster boolean
     * @param $parentModelCode string
     * @param $isPtoChild int
     * @return array
     */
    protected function _rmaDataToArray(
        $gerpWebsiteData,
        $rma,
        $rmaItem,
        $order,
        $item,
        $shippingAddress,
        $modelCode,
        $wareHouseCode,
        $isPtoMaster,
        $parentModelCode,
        $isPtoChild
    ){
        $reasonCode = $this->getAllReturnResonCode();
        $externalIncrementId = $this->convertExternalIncrementId($rma->getIncrementId());
        $externalItemId = $this->convertExternalIncrementId($rmaItem->getId());

        return [
            'CORPORATION_CODE' => $gerpWebsiteData['legal_entity_name'],
            'ORIG_SYS_DOCUMENT_REF' => $externalIncrementId,
            'ORIG_SYS_LINE_REF' => $externalItemId,
            'ORDER_TYPE' => $gerpWebsiteData['return_type'],
            'SHIP_TO_CODE' => $gerpWebsiteData['ship_to_code'],
            'CUSTOMER_NAME' => $shippingAddress->getName(),
            'POSTAL_CODE' => $shippingAddress->getPostcode(),
//            'STATE_CODE' => $shippingAddress->getRegion(),
            'CITY_NAME' => $shippingAddress->getCity(),
            'ADDRESS_LINE1_INFO' => $shippingAddress->getStreetLine(1),
            'ADDRESS_LINE2_INFO' => $shippingAddress->getStreetLine(2).' '.$shippingAddress->getRegion(),
            'ADDRESS_LINE3_INFO' => $shippingAddress->getEmail(),
            'PHONE_NO' => $shippingAddress->getTelephone(),
            'EMAIL_ADDR' => $shippingAddress->getEmail(),
            'MOBILE_PHONE_NO' => $shippingAddress->getTelephone(),
            'ONETIME_SHIP_TO_FLAG' => $gerpWebsiteData['ontime_shop_to_flag'],
            'ITEM_NO' => $modelCode,
            'ORDER_QTY' => round($rmaItem->getQtyAuthorized(),2),
            'CREATION_DATE' => $this->getCurrentDateString(),
            'LAST_UPDATE_DATE' => $this->getCurrentDateString(),
            'CUST_PO_NO' => $externalIncrementId,
            'RETURN_REASON_CODE' => (isset($reasonCode[$rmaItem->getReason()]))?$reasonCode[$rmaItem->getReason()]['value']:$rmaItem->getReason(),
            'COUNTRY_CODE' => strtoupper($shippingAddress->getCountryId()),
            'ORIGINAL_HEADER_ID' => $order->getData('gerp_header_id'),
            'ORIGINAL_LINE_ID' => $item->getData('gerp_line_id'),
            'PROCESS_STATUS_CODE' => 'I',
            'ORDER_SOURCE_NAME' => $gerpWebsiteData['order_source_name'],
            'ATTRIBUTE4' => $order->getShippingAmount(),
            'WAREHOUSE_CODE' => $wareHouseCode,
            'REQUEST_DATE' => $this->getRequestDateString(),
            'UNIT_SELLING_PRICE' => ($isPtoMaster) ? 0: $item->getPrice(),  //unit_price, Set모델의 대표모델은 0 BUNDLE PRODUCT 0
            'CURRENCY_CODE' => $order->getOrderCurrencyCode(),
            'SET_MODEL_CODE' => $parentModelCode,
            'ITEM_TYPE_CODE' => ($isPtoMaster) ? 'MODEL': (($isPtoChild) ? 'OPTION': 'STANDARD'),
            'PARTIAL_FLAG' => $gerpWebsiteData['partial_flag'],
            'OPERATION_CODE' => 'INSERT',
            'CALCULATION_PRICE_FLAG' => $gerpWebsiteData['calculation_price_flag'],
            'PRICING_DATE' => $this->defaultTimeToStoreTime($order->getCreatedAt(),$gerpWebsiteData['timezone']),
            'DELIVERY_TYPE_CODE' => $item->getData('delivery_type'),
            'RECEIVER_NAME' => $shippingAddress->getName(),
            'RECEIVER_PHONE_NO' => $shippingAddress->getTelephone(),
            'RECEIVER_MOBILE_NO' => $shippingAddress->getTelephone(),
            'ATTRIBUTE30' => $item->getData('product_carrier'), 
        ];
    }

    /**
     * @param $itemId string
     * @return OrderItemInterface
     */
    protected function getOrderItem($itemId){
        if(!isset($this->_orderItemData[$itemId])){
            $this->_orderItemData[$itemId] = $this->_orderItemRepository->get($itemId);
        }
        return $this->_orderItemData[$itemId];
    }

}