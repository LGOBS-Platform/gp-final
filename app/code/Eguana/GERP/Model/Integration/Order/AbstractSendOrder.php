<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-10-18
 * Time: 오후 12:11
 */

namespace Eguana\GERP\Model\Integration\Order;


use Amasty\MultiInventory\Model\Warehouse\OrderItemRepository;
use Eguana\GERP\Helper\GerpLogHelper;
use Eguana\GERP\Model\Integration\AbstractSender;
use Eguana\GERP\Model\Integration\Product\AbstractProduct;
use Eguana\GERP\Model\Source\Status;
use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Model\Order\Address;
use Magento\Sales\Model\Order\Item;
use Magento\Sales\Model\Order\ItemRepository;

abstract class AbstractSendOrder extends AbstractSender
{
    const EXTERNAL_ORDER_INCREMENT_PREFIX = 'ORDER';

    protected $_successOrderStatus = 'order_processing';

    protected $_incrementPrefix = self::EXTERNAL_ORDER_INCREMENT_PREFIX;

    protected $_incrementSplit = '_';

    protected $_successInterfaceStatus = Status::GERP_DATA_PROCESS;

    protected $_orderItemCollectionFactory;

    protected $_productRepository;

    protected $_timezone;

    protected $_currentDate;

    protected $_currentDateString;

    protected $_requestDateString;

    protected $_warehouseOrderItemRepository;

    protected $_orderItemData;

    protected $_orderItemRepository;

    public function __construct(
        \Eguana\GERP\Helper\XmlHelper $xmlHelper,
        GerpLogHelper $gerpLogHelper,
        ProductRepository $productRepository,
        TimezoneInterface $timezoneInterface,
        \Magento\Sales\Model\ResourceModel\Order\Item\CollectionFactory $orderItemCollectionFactory,
        OrderItemRepository $warehouseOrderItemRepository,
        ItemRepository $orderItemRepository
    )
    {
        $this->_orderItemCollectionFactory = $orderItemCollectionFactory;
        $this->_productRepository = $productRepository;
        $this->_timezone = $timezoneInterface;
        $this->_warehouseOrderItemRepository = $warehouseOrderItemRepository;
        $this->_orderItemRepository = $orderItemRepository;
        parent::__construct( $xmlHelper,$gerpLogHelper);
    }

    public function getOrderType($externalIncrementId){

        $splitIncementId = explode($this->_incrementSplit,$externalIncrementId);

        if(isset($splitIncementId[1])){
            return $splitIncementId[0];
        }

        return $this->_incrementPrefix;
    }

    public function convertExternalIncrementId($internalIncrementId){
        return $this->_incrementPrefix.$this->_incrementSplit.$internalIncrementId;
    }

    public function convertInternalIncrementId($externalIncrementId){

        $splitIncementId = explode($this->_incrementSplit,$externalIncrementId);

        if(isset($splitIncementId[1])){
            return $splitIncementId[1];
        }

        return $externalIncrementId;
    }

    protected function getOrderItemCollection(){

        $orderCollection = $this->_createOrderItemCollection();

        $orderCollection->addFieldToFilter('product_from',['eq' => AbstractProduct::OMD_PRODUCT_FROM])
            ->addFieldToFilter('interface_status',['eq' => Status::GERP_DATA_PENDING]);

        return $orderCollection;
    }

    public function bindData()
    {
        $gerpWebsiteData = $this->getGerpWebsiteData();
        $bindData = $this->getFrameData($gerpWebsiteData);

        $orderItemCollection  = $this->getOrderItemCollection();

        /**
         * @var $order \Magento\Sales\Model\Order
         * @var $item \Magento\Sales\Model\Order\Item
         */
        foreach ($orderItemCollection as $item){

            $order = $item->getOrder();
            $shippingAddress = $order->getShippingAddress();

            $bindData['CustomXML']['MessageBody'][]['ContentList'] = $this->_ItemBinding($order,$item,$shippingAddress,$gerpWebsiteData);
        }

        return $bindData;
    }

    /**
     * @param $order \Magento\Sales\Model\Order
     * @param $item \Magento\Sales\Model\Order\Item
     * @param $shippingAddress \Magento\Sales\Model\Order\Address
     * @param $gerpWebsiteData []
     * @return array
     */
    protected function _ItemBinding($order,$item,$shippingAddress,$gerpWebsiteData){

        $skuSplit = explode('.',$item->getSku());
        $isPtoMaster = false;
        $isPtoChild = false;
        $parentModelCode = '';

        if(isset($skuSplit[2]))
            $modelCode = $skuSplit[1].'.'.$skuSplit[2];
        else
            $modelCode = $skuSplit[1];

        $wareHouseCode = null;

        if($item->getParentItemId()){
            $wareHouseCode = $this->_warehouseOrderItemRepository->getByOrderItemId($item->getItemId())->getWarehouse()->getCode();
            $parentItem = $this->getOrderItem($item->getParentItemId());
            if($parentItem->getProductType() == 'bundle'){
                $isPtoChild = true;

                $parentModelCodeSplit = explode('.',$item->getParentItem()->getSku());
                $parentModelCode = $parentModelCodeSplit[1].'.'.$parentModelCodeSplit[2];
            }elseif ($parentItem->getProductType() == 'configurable'){
                $item = $parentItem;
            }
        }else{
            if($item->getProductType() == 'bundle'){
                $isPtoMaster = true;
                $order = $item->getOrder();
                foreach ($order->getAllItems() as $orderItem){
                    if($orderItem->getParentItemId() == $item->getItemId()){
                        $wareHouseCode = $this->_warehouseOrderItemRepository->getByOrderItemId($orderItem->getItemId())->getWarehouse()->getCode();
                        break;
                    }
                }
            }else{
                $wareHouseCode = $this->_warehouseOrderItemRepository->getByOrderItemId($item->getItemId())->getWarehouse()->getCode();
            }
        }

        return $this->_dataToArray($gerpWebsiteData,$order,$item,$shippingAddress,$modelCode,$wareHouseCode,$isPtoMaster,$parentModelCode,$isPtoChild);
    }

    /**
     * @param $gerpWebsiteData array
     * @param $order OrderInterface
     * @param $item Item
     * @param $shippingAddress Address
     * @param $modelCode string
     * @param $wareHouseCode string
     * @param  $isPtoMaster boolean
     * @param $parentModelCode string
     * @param $isPtoChild boolean
     * @return array
     */
    protected function _dataToArray($gerpWebsiteData,$order,$item,$shippingAddress,$modelCode,$wareHouseCode,$isPtoMaster,$parentModelCode,$isPtoChild){
        return [];
    }

    public function getInterfaceId(){
        $interfaceData = $this->_xmlHelper->getGerpInterfaceConfig();
        $curInterface = $interfaceData[$this->_documentType];
        return implode(',',[
            $curInterface['interface_id']['first'],
            $curInterface['interface_id']['second'],
            $curInterface['interface_id']['third'],
            date('Ymd'),
            $curInterface['interface_id']['fifth'],
        ]);
    }

    protected function getFrameData($gerpWebsiteData){
        return [
            'CustomXML' =>[
                'MessageHeader' => [
                    'InterfaceID' => $gerpWebsiteData['interface_id'],
                    'SenderID' => $gerpWebsiteData['obs_id'],
                    'ReceiverID' => $gerpWebsiteData['gerp_id'],
                    'DocumentType' => $this->_documentType,
                ],
                'MessageBody' => []
            ]
        ];
    }

    /**
     * @return \Magento\Sales\Model\ResourceModel\Order\Collection
     */
    protected function _createOrderItemCollection(){
        return $this->_orderItemCollectionFactory->create();
    }

    public function getCurrentDate(){

        if(!$this->_currentDate){
            $gerpWebsiteData = $this->getGerpWebsiteData();
            $this->_currentDate = $this->_timezone->date(null);
            $this->_currentDate->setTimezone(new \DateTimeZone($gerpWebsiteData['timezone']));
        }

        return $this->_currentDate;
    }

    public function getCurrentDateString(){

        if(!$this->_currentDateString){
            $this->_currentDateString = $this->getCurrentDate()->format('Y-m-d H:i:s');
        }

        return $this->_currentDateString;
    }

    public function getRequestDateString(){

        if(!$this->_requestDateString){
            $this->_requestDateString = $this->getCurrentDate()->add(new \DateInterval('P1D'))->format('Y-m-d H:i:s');
        }

        return $this->_requestDateString;

    }

    public function defaultTimeToStoreTime($date,$timezone){
        $storeTime = new \DateTime($date, new \DateTimeZone($this->_timezone->getDefaultTimezone()));
        $storeTime->setTimezone(new \DateTimeZone($timezone));
        return $storeTime->format('Y-m-d H:i:s');
    }

    public function getGerpWebsiteData(){

        $websiteConfig = $this->_xmlHelper->getGerpWebsiteData();
        $websiteCode = $this->getWebsiteCode();
        $currentWebsiteConfig = null;

        foreach ($websiteConfig as $data){
            if($data['website_code']  == $websiteCode){
                $currentWebsiteConfig = $data;
                $currentWebsiteConfig['interface_id'] = $this->getInterfaceId();
                break;
            }
        }

        return $currentWebsiteConfig;
    }


    public function result($data)
    {
        $itemIds = [];
        $orderIncrementIds = [];

        $items = $data['data']['CustomXML']['MessageBody'];

        if($data['result'] == 200){

            foreach ($items as $item){
                $itemIds[] = $this->convertInternalIncrementId($item['ContentList']['ORIG_SYS_LINE_REF']);
                $orderIncrementIds[] = $this->convertInternalIncrementId($item['ContentList']['ORIG_SYS_DOCUMENT_REF']);
            }

            $orderItemResource = $this->getOrderItemCollection()->getResource();


            $orderItemResource->getConnection()->update($orderItemResource->getMainTable(),
                ['interface_status' => $this->_successInterfaceStatus],
                ['item_id in (?)' => $itemIds]);

//            $orderItemResource->getConnection()->update($orderItemResource->getTable('sales_order'),
//                ['status' => $this->_successOrderStatus],
//                ['increment_id in (?)' => $orderIncrementIds]);
//
//            $orderItemResource->getConnection()->update($orderItemResource->getTable('sales_order_grid'),
//                ['status' => $this->_successOrderStatus],
//                ['increment_id in (?)' => $orderIncrementIds]);

        }
    }

    /**
     * @param $itemId string
     * @return OrderItemInterface
     */
    protected function getOrderItem($itemId){
        if(!isset($this->_orderItemData[$itemId])){
            $this->_orderItemData[$itemId] = $this->_orderItemRepository->get($itemId);
        }
        return $this->_orderItemData[$itemId];
    }

}