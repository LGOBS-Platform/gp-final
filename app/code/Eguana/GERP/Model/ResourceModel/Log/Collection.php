<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-10-08
 * Time: 오후 5:44
 */

namespace Eguana\GERP\Model\ResourceModel\Log;


use Eguana\GERP\Model\Log;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Resource initialization
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init(Log::class,\Eguana\GERP\Model\ResourceModel\Log::class);
    }

}