<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-10-08
 * Time: 오후 5:33
 */

namespace Eguana\GERP\Model;


use Eguana\GERP\Api\Data\LogInterface;
use Magento\Framework\Model\AbstractModel;

class Log extends AbstractModel implements LogInterface
{

    /**
     * {@inheritdoc}
     */
    protected function _construct()
    {
        $this->_init(\Eguana\GERP\Model\ResourceModel\Log::class);
    }

    public function setGERPFlag($flag)
    {
        return $this->setData(self::GERP_FLAG,$flag);
    }

    public function getGERPFlag()
    {
        return $this->getData(self::GERP_FLAG);
    }

    public function setStatus($status)
    {
        return $this->setData(self::STATUS,$status);
    }

    public function getStatus()
    {
        return $this->getData(self::STATUS);
    }

    public function setIdentifierName($identifierName)
    {
        return $this->setData(self::IDENTIFIER_NAME,$identifierName);
    }

    public function getIdentifierName()
    {
        return $this->getData(self::IDENTIFIER_NAME);
    }

    public function setIdentifier($identifier)
    {
        return $this->setData(self::IDENTIFIER,$identifier);
    }

    public function getIdentifier()
    {
        return $this->getData(self::IDENTIFIER);
    }

    public function setSubIdentifierName($subIdentifierName)
    {
        return $this->setData(self::SUB_IDENTIFIER_NAME,$subIdentifierName);
    }

    public function getSubIdentifierName()
    {
        return $this->getData(self::SUB_IDENTIFIER_NAME);
    }

    public function setSubIdentifier($subIdentifier)
    {
        return $this->setData(self::SUB_IDENTIFIER,$subIdentifier);
    }

    public function getSubIdentifier()
    {
        return $this->getData(self::SUB_IDENTIFIER);
    }

    public function setGERPData($data)
    {
        return $this->setData(self::GERP_DATA,$data);
    }

    public function getGERPData()
    {
        return $this->getData(self::GERP_DATA);
    }

    public function setMessage($message)
    {
        return $this->setData(self::GERP_MESSAGE,$message);
    }

    public function getMessage()
    {
        return $this->getData(self::GERP_MESSAGE);
    }

    /**
     * @param $websiteCode string
     * @return $this
     */
    public function setWebsiteCode($websiteCode){
        return $this->setData(self::WEBSITE_CODE,$websiteCode);
    }

    /** @return string*/
    public function getWebsiteCode(){
        return $this->getData(self::WEBSITE_CODE);
    }

    /**
     * @param $interfaceId string
     * @return $this
     */
    public function setInterfaceId($interfaceId){
        return $this->setData(self::INTERFACE_ID,$interfaceId);
    }

    /** @return string*/
    public function getInterfaceId(){
        return $this->getData(self::INTERFACE_ID);
    }

    /**
     * @param $senderId string
     * @return $this
     */
    public function setSenderId($senderId){
        return $this->setData(self::SENDER_ID,$senderId);
    }

    /** @return string*/
    public function getSenderId(){
        return $this->getData(self::SENDER_ID);
    }

    /**
     * @param $receiverId string
     * @return $this
     */
    public function setReceiverId($receiverId){
        return $this->setData(self::RECEIVER_ID,$receiverId);
    }

    /** @return string*/
    public function getReceiverId(){
        return $this->getData(self::RECEIVER_ID);
    }

}