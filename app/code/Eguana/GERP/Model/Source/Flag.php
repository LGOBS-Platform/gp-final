<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Eguana\GERP\Model\Source;

class Flag extends AbstractGerp
{
    /**
     * Get values
     *
     * @return array
     */
    protected function _getValues()
    {
        $array = [];

        foreach ($this->_dataHelper->getGerpInterfaceConfig() as $list){
            $array[$list['flag']] = $list['document_type'];
        }

        return $array;
    }
}
