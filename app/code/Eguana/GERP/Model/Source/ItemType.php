<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Eguana\GERP\Model\Source;

class ItemType extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    const GERP_SINGLE_PRODUCT = 'Q';

    const GERP_COMPONENTS_PRODUCT = 'C';


    /**
     * Get all options
     *
     * @return array
     */
    public function getAllOptions()
    {
        $result = [];
        foreach ($this->_getValues() as $k => $v) {
            $result[] = ['value' => $k, 'label' => $v];
        }

        return $result;
    }

    /**
     * Get option text
     *
     * @param int|string $value
     * @return null|string
     */
    public function getOptionText($value)
    {
        $options = $this->_getValues();
        if (isset($options[$value])) {
            return $options[$value];
        }
        return null;
    }

    /**
     * Get values
     *
     * @return array
     */
    protected function _getValues()
    {
        return [
            self::GERP_SINGLE_PRODUCT => __('Single'),
            self::GERP_COMPONENTS_PRODUCT => __('Components')
        ];
    }
}
