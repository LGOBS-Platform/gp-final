<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Eguana\GERP\Model\Source;

class ProductCarrier extends AbstractGerp
{
    const CARRIER_CODE_ROHLING = 'Rohlig';

    const CARRIER_CODE_GLS = 'GLS';

    /**
     * Get values
     *
     * @return array
     */
    protected function _getValues()
    {
        return [
            self::CARRIER_CODE_ROHLING => self::CARRIER_CODE_ROHLING,
            self::CARRIER_CODE_GLS => self::CARRIER_CODE_GLS,
        ];
    }
}
