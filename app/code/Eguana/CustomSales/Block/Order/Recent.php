<?php
/*
 * @author Eguana Commerce
 * @copyright Copyright (c) 2018 Eguana Christine
 * @package Eguana_CustomSales
 */
namespace Eguana\CustomSales\Block\Order;

/**
 * Sales order history block
 *
 * @api
 * @since 100.0.2
 */
class Recent extends \Magento\Sales\Block\Order\Recent
{
    /**
     * @return string
     * @throws \Magento\Framework\Exception\ValidatorException
     */
    protected function _toHtml()
    {
        if (!$this->getTemplate()) {
            return '';
        }
        return $this->fetchView($this->getTemplateFile());
    }
}
