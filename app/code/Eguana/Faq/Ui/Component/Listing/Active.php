<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016-07-25
 * Time: 오후 3:36
 */

namespace Eguana\Faq\Ui\Component\Listing;


use Magento\Framework\Option\ArrayInterface;

class Active implements ArrayInterface
{
    private $activeOptions;

    /**
     * Is Active Option
     * @return array
     */
    public function toOptionArray()
    {
        $this->activeOptions = array(
            array(
                'label' => __('Enabled'),
                'value' => 1
            ),
            array(
                'label' => __('Disabled'),
                'value' => 0
            )
        );
        return $this->activeOptions;
    }
}