<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016-07-25
 * Time: 오후 3:36
 */

namespace Eguana\Faq\Ui\Component\Listing;

use Magento\Framework\Option\ArrayInterface;

class Types implements ArrayInterface
{
    private $typeOptions;

    /**
     * Is Type Option
     * @return array
     */
    public function toOptionArray()
    {
        $this->typeOptions = array(
            array(
                'label' => __('General'),
                'value' => 1
            )
        );
        return $this->typeOptions;
    }
}