<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016-09-02
 * Time: 오전 9:19
 */

namespace Eguana\Faq\Block;


use Eguana\Faq\Api\Data\FaqInterface;
use Eguana\Faq\Model\ResourceModel\Faq\CollectionFactory;
use Eguana\Faq\Model\Source\Type;
use Magento\Framework\View\Element\Template;

class Faq extends Template implements \Magento\Framework\DataObject\IdentityInterface
{

    protected $_faqCollectionFactory;

    protected $_faqData;

    protected $_faqType;

    public function __construct(
        Template\Context $context,
        CollectionFactory $collectionFactory,
        Type $faqType,
        array $data = []
    )
    {
        $this->_faqCollectionFactory = $collectionFactory;
        $this->_faqType = $faqType;
        parent::__construct($context, $data);
    }

    public function getFaqBindData(){

        if(is_null($this->_faqData)){
            $this->_faqDataBind();
        }

        return $this->_faqData;
    }

    public function getTypeOptionArray(){
        return $this->_faqType->getOptionArray();
    }

    protected function _faqDataBind(){

        $bindData = [];

        /**
         * @var \Eguana\Faq\Model\ResourceModel\Faq\Collection $faqCollection
         * @var \Eguana\Faq\Model\Faq $faq
         */
        $faqCollection = $this->_faqCollectionFactory->create();
        $currentStoreId = $this->_storeManager->getStore()->getId();
        $faqCollection->addFieldToFilter(FaqInterface::IS_ACTIVE,['eq' => true])
            ->addStoreFilter($currentStoreId);


        foreach ($faqCollection as $faq){
            $bindData[$faq->getType()][] = $faq;
            if($this->_storeManager->getStore()->getCode() == 'br'){
                if($faq->getType() != \Eguana\Faq\Model\Source\Type::TYPE_PRICING){
                    $bindData[7][] = $faq;
                }
            }
        }

        $this->_faqData = $bindData;
    }

    public function getIdentities()
    {
        return [\Eguana\Faq\Model\Faq::CACHE_TAG];
    }

}