<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016-09-02
 * Time: 오전 9:14
 */

namespace Eguana\Faq\Controller\Index;

use Eguana\Faq\Helper\Data;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\Action;

class Index extends Action
{

    /**
     * @var Data
     */
    protected $helper;

    /**
     * @param Context $context
     * @param Data $helper
     */
    public function __construct(
        Context $context,
        Data $helper
    ){
        parent::__construct($context);
        $this->helper = $helper;
    }

    public function execute()
    {
//        if($this->helper->getFaqEnabled() != true){
//            $this->_redirect('');
//        }
        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }
}