<?php
/**
 * Created by Glenn.
 * User: USER
 * Date: 2018-08-22
 * Time: 오후 5:23
 */

namespace Eguana\Faq\Controller\Adminhtml\Faq;

use Eguana\Faq\Controller\Adminhtml\AbstractController;

class Grid extends AbstractController
{

    public function execute()
    {
        $resultPage = $this->_init($this->_resultPageFactory->create());
        return $resultPage;
    }

}