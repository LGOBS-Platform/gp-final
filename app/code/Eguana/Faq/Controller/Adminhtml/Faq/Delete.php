<?php
/**
* Copyright 2016 Eguana. All rights reserved.
* See LICENSE.txt for license details.
*/

namespace Eguana\Faq\Controller\Adminhtml\Faq;

use Eguana\Faq\Controller\Adminhtml\AbstractController;
use Eguana\Faq\Model\FaqFactory;
use Eguana\Faq\Api\FaqRepositoryInterface;
use Magento\Framework\Registry;
use Magento\Backend\App\Action\Context;


/**
 * Class Delete
 * @package Eguana\GERP\Controller\Adminhtml\Log
 */
class Delete extends AbstractController
{
    /**
     * @var FaqFactory
     */
    private $faqFactory;

    /**
     * @var FaqRepositoryInterface
     */
    private $faqRepository;

    public function __construct(
        Context $context,
        Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        FaqFactory $faqFactory = null,
        FaqRepositoryInterface $faqRepository = null
    ) {
        $this->faqFactory = $faqFactory
            ?: \Magento\Framework\App\ObjectManager::getInstance()->get(FaqFactory::class);
        $this->faqRepository = $faqRepository
            ?: \Magento\Framework\App\ObjectManager::getInstance()->get(FaqRepositoryInterface::class);
        parent::__construct($context, $coreRegistry,$resultPageFactory);
    }

    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = (int)$this->getRequest()->getParam('entity_id');
        $model = $this->faqFactory->create();

        if ($id) {
            try {

                /** @var Faq $model */

                $model = $this->faqRepository->getById($id);
                $model->delete();
                $this->messageManager->addSuccessMessage(__('row was successfully deleted'));
                return $resultRedirect->setPath('*/*/index');
            } catch (\Exception $exception) {
                $this->messageManager->addErrorMessage($exception->getMessage());
            }
        }
        $this->messageManager->addErrorMessage(__('row could not be deleted'));
        return $resultRedirect->setPath('*/*/index');
    }
}
