<?php
/**
 * Created by Glenn.
 * User: USER
 * Date: 2018-08-22
 * Time: 오후 5:23
 */

namespace Eguana\Faq\Controller\Adminhtml\Faq;

use Magento\Backend\App\Action;

class Index extends Action
{
    public function execute()
    {
        $this->_forward('grid');
    }
}