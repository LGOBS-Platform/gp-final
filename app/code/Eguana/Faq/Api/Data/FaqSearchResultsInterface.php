<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Eguana\Faq\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;


interface FaqSearchResultsInterface extends SearchResultsInterface
{
    /**
     * @return FaqInterface[]
     */
    public function getItems();

    /**
     * @param FaqInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
