<?php
/**
 * Created by PhpStorm.
 * User: sonia
 * Date: 18. 10. 21
 * Time: 오후 10:45
 */

namespace Eguana\Faq\ViewModel;


use Magento\Framework\View\Element\Block\ArgumentInterface;

class WhatsNewPopup implements ArgumentInterface
{
    /**
     * @var \Eguana\Faq\Api\FaqRepositoryInterface
     */
    protected $faqRepository;
    /**
     * @var \Eguana\Faq\Helper\Data
     */
    protected $helper;

    /**
     * WhatsNewPopup constructor.
     * @param \Eguana\Faq\Api\FaqRepositoryInterface $faqRepository
     * @param \Eguana\Faq\Helper\Data $helper
     */
    public function __construct(
        \Eguana\Faq\Api\FaqRepositoryInterface $faqRepository,
        \Eguana\Faq\Helper\Data $helper

    )
    {
        $this->faqRepository = $faqRepository;
        $this->helper = $helper;
    }

    /**
     * @return array
     */
    public function getPopupContents()
    {
        $whatsnew = $this->faqRepository->getById($this->getWhatsNewId());
        $contents = [];
        $contents['description'] = $whatsnew->getDescription();
        $contents['title'] = $whatsnew->getTitle();

        return $contents;
    }

    /**
     * @return mixed
     */
    public function isPopupEnabled()
    {
        return $this->helper->getPopupEnabled();
    }

    /**
     * @return mixed
     */
    protected function getWhatsNewId()
    {
        return $this->helper->getWhatsNewId();
    }

}