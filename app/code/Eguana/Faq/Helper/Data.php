<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2016-09-22
 * Time: 오후 5:11
 */

namespace Eguana\Faq\Helper;


use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{

    /**
     * Data constructor.
     * @param Context $context
     */
    public function __construct(
        Context $context
    )
    {
        parent::__construct($context);
    }

    /**
     * @return mixed
     */
    public function getFaqEnabled(){
        return $this->scopeConfig->getValue('faq/general/enabled', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getPopupEnabled(){
        return $this->scopeConfig->getValue('faq/popup/enabled', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getWhatsNewId()
    {
        return $this->scopeConfig->getValue('faq/popup/faq_id', ScopeInterface::SCOPE_STORE);
    }
}