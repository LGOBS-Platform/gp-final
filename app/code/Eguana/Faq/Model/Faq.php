<?php
/**
 * Created by Glenn.
 * User: USER
 * Date: 2018-08-23
 * Time: 오전 9:28
 */

namespace Eguana\Faq\Model;

use Eguana\Faq\Api\Data\FaqInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

class Faq extends AbstractModel implements FaqInterface,IdentityInterface
{
    const CACHE_TAG = 'eguana_faq';

    const STATUS_ENABLED = 1;

    const STATUS_DISABLED = 0;

    protected $_cacheTag = self::CACHE_TAG;

    protected function _construct(){
        $this->_init(\Eguana\Faq\Model\ResourceModel\Faq::class);
    }

    /**
     * Return unique ID(s) for each object in system
     *
     * @return string[]
     */
    public function getIdentities(){
        return [self::CACHE_TAG . '_' . $this->getId(), self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @param int $entity_id
     * @return $this
     */
    public function setEntityId($entity_id){
        return $this->setData(self::ENTITY_ID,$entity_id);
    }

    /**
     * @return int
     */
    public function getEntityId(){
        return $this->getData(self::ENTITY_ID);
    }

    public function getType()
    {
        return $this->getData(self::TYPE);
    }

    public function setType($type){
        return $this->setData(self::TYPE,$type);
    }

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle($title){
        return $this->setData(self::TITLE,$title);
    }

    /**
     * @return string
     */
    public function getTitle(){
        return $this->getData(self::TITLE);
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description){
        return $this->setData(self::DESCRIPTION,$description);
    }

    /**
     * @return string
     */
    public function getDescription(){
        return $this->getData(self::DESCRIPTION);
    }

    /**
     * @param string $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt){
        return $this->setData(self::CREATED_AT,$createdAt);
    }

    /**
     * @return string
     */
    public function getCreatedAt(){
        return $this->getData(self::CREATED_AT);
    }

    /**
     * @param string $updatedAt
     * @return $this
     */
    public function setUpdatedAt($updatedAt){
        return $this->setData(self::UPDATED_AT);
    }

    /**
     * @return string
     */
    public function getUpdatedAt(){
        return $this->getData(self::UPDATED_AT);
    }

    /**
     * @param string $isActive
     * @return $this
     */
    public function setIsActive($isActive){
        return $this->setData(self::IS_ACTIVE,$isActive);
    }

    /**
     * @return string
     */
    public function getIsActive(){
        return $this->getData(self::IS_ACTIVE);
    }

    /**
     * Receive page store ids
     *
     * @return int[]
     */
    public function getStores()
    {
        return $this->hasData('stores') ? $this->getData('stores') : $this->getData('store_id');
    }

    /**
     * Prepare block's statuses.
     *
     * @return array
     */
    public function getAvailableStatuses()
    {
        return [self::STATUS_ENABLED => __('Enabled'), self::STATUS_DISABLED => __('Disabled')];
    }

}