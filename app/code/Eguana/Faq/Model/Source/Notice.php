<?php
/**
 * Created by PhpStorm.
 * User: sonia
 * Date: 18. 10. 21
 * Time: 오후 9:30
 */

namespace Eguana\Faq\Model\Source;


class Notice implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @var \Eguana\Faq\Model\ResourceModel\Faq\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var array
     */
    protected $options;

    public function __construct(
        \Eguana\Faq\Model\ResourceModel\Faq\CollectionFactory $collectionFactory
    ) {
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * Return array of options as value-label pairs
     *
     * @return array Format: array(array('value' => '<value>', 'label' => '<label>'), ...)
     */
    public function toOptionArray()
    {
        if (!$this->options) {
            $this->options = $this->collectionFactory->create()
                ->addFieldToSelect(['entity_id', 'title', 'type'])
                ->addFieldToFilter('type', ['eq' => \Eguana\Faq\Model\Source\Type::TYPE_MEMBER])
                ->toOptionArray();
        }
        return $this->options;
    }
}