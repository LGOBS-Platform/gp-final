<?php
/**
* Copyright 2016 Eguana. All rights reserved.
* See LICENSE.txt for license details.
*/

namespace Eguana\Faq\Model\Source;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class Type
 * @package Eguana\Faq\Model\Source
 */
class Type implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * Type values
     */
    const TYPE_MEMBER = 1;

    const TYPE_ORDER = 2;

    const TYPE_PAYMENT = 3;

    const TYPE_SHIPPING = 4;

    const TYPE_CANCEL = 5;

    const TYPE_PRICING= 6;

    const TYPE_ALL = 7;

    const TYPE_WARRANTY = 8;

    protected $_storeManager;

    public function __construct(StoreManagerInterface $storeManager)
    {
        $this->_storeManager = $storeManager;
    }

    /**
     * @return array
     */
    public function getOptionArray()
    {
        $optionArray = [];

        $storeCode = $this->_storeManager->getStore()->getCode();

        if ($storeCode != 'pl' && $storeCode != 'it'){

            foreach ($this->toBrOptionArray() as $brOption){
                $optionArray[$brOption['value']] = $brOption['label'];
            }
            return $optionArray;

        }elseif($storeCode == 'pl'){
            foreach ($this->toPlOptionArray() as $option) {
                $optionArray[$option['value']] = $option['label'];
            }

            return $optionArray;

        }elseif ($storeCode == 'it'){
            foreach ($this->toItOptionArray() as $option){
                $optionArray[$option['value']] = $option['label'];
            }

            return $optionArray;
        }
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => self::TYPE_MEMBER,  'label' => __("What's New")],
            ['value' => self::TYPE_ORDER,  'label' => __('Order')],
            ['value' => self::TYPE_PAYMENT,  'label' => __('Payment & Financing')],
            ['value' => self::TYPE_SHIPPING,  'label' => __('Shipping')],
            ['value' => self::TYPE_CANCEL,  'label' => __('Cancellation & Return')],
            ['value' => self::TYPE_PRICING,  'label' => __('Product & Service')],
            ['value' => self::TYPE_WARRANTY,  'label' => __('LG Account(PL)/Warranty(BR)')],
        ];
    }


    public function toBrOptionArray()
    {
        return [
            ['value' => self::TYPE_ALL,  'label' => __('All')],
            ['value' => self::TYPE_ORDER,  'label' => __('Order')],
            ['value' => self::TYPE_PAYMENT,  'label' => __('Payment')],
            ['value' => self::TYPE_SHIPPING,  'label' => __('Delivery')],
            ['value' => self::TYPE_CANCEL,  'label' => __('Cancellation & Return')],
            ['value' => self::TYPE_WARRANTY,  'label' => __('Warranty(BR)')],
        ];
    }

    public function toItOptionArray()
    {
        return [
            ['value' => self::TYPE_MEMBER,  'label' => __("What's New")],
            ['value' => self::TYPE_ORDER,  'label' => __('Order')],
            ['value' => self::TYPE_PAYMENT,  'label' => __('Payment & Financing')],
            ['value' => self::TYPE_SHIPPING,  'label' => __('Shipping')],
            ['value' => self::TYPE_CANCEL,  'label' => __('Cancellation & Return')],
            ['value' => self::TYPE_PRICING,  'label' => __('Product & Service')],
        ];
    }

    public function toPlOptionArray()
    {
        return [
            ['value' => self::TYPE_ORDER,  'label' => __('Order')],
            ['value' => self::TYPE_PAYMENT,  'label' => __('Payment & Financing')],
            ['value' => self::TYPE_SHIPPING,  'label' => __('Shipping')],
            ['value' => self::TYPE_CANCEL,  'label' => __('Cancellation & Return')],
            ['value' => self::TYPE_PRICING,  'label' => __('Product & Service')],
            ['value' => self::TYPE_WARRANTY,  'label' => __('LG Account(PL)')],
        ];
    }


}
