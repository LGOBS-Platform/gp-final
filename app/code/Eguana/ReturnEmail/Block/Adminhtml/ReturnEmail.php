<?php
/**
 * Created by Kenneth.
 * Date: 2018-11-13
 * Time: 오후 5:02
 */
namespace Eguana\ReturnEmail\Block\Adminhtml;

use Eguana\ReturnEmail\Helper\Data;
use Magento\Rma\Block\Adminhtml\Rma\Edit\Tab\General\Details;


class ReturnEmail extends Details
{
    protected $_returnEmailHelper;

    public function __construct(\Magento\Backend\Block\Template\Context $context,
                                \Magento\Framework\Registry $registry,
                                Data $returnEmailHelper,
                                array $data = [])
    {
        $this->_returnEmailHelper = $returnEmailHelper;
        parent::__construct($context, $registry, $data);
    }


    public function returnEmailHelper()
    {
        return $this->_returnEmailHelper;
    }


    /**
     * Get order link (href address)
     *
     * @return string
     */
    public function getOrderLink()
    {
        return $this->getUrl('sales/order/view', ['order_id' => $this->getOrder()->getId()]);
    }

    /**
     * Get order increment id
     *
     * @return string
     */
    public function getOrderIncrementId()
    {
        return $this->getOrder()->getIncrementId();
    }

    /**
     * Get Link to Customer's Page
     *
     * Gets address for link to customer's page.
     * Returns null for guest-checkout orders
     *
     * @return string|null
     */
    public function getCustomerLink()
    {
        if ($this->getOrder()->getCustomerIsGuest()) {
            return false;
        }
        return $this->getUrl('customer/index/edit', ['id' => $this->getOrder()->getCustomerId()]);
    }

    /**
     * Get Customer Email
     *
     * @return string
     */
    public function getCustomerEmail()
    {
        return $this->escapeHtml($this->getOrder()->getCustomerEmail());
    }

    /**
     * Get Customer Email
     *
     * @return string
     */
    public function getCustomerContactEmail()
    {
        return $this->escapeHtml($this->getRmaData('customer_custom_email'));
    }


}