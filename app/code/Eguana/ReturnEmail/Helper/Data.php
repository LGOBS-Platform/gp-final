<?php
/**
 * Created by Kenneth.
 * Date: 2018-11-13
 * Time: 오후 5:09
 */
namespace Eguana\ReturnEmail\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;


class Data extends AbstractHelper
{

    const XML_PATH_EMAIL = 'returnemail/';

    protected $_scopeConfig;

    public function __construct(Context $context,
                                \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig)
    {

        $this->_scopeConfig = $scopeConfig;
        parent::__construct($context);
    }

    public function getConfigValue($field, $storeId = null)
    {
        return $this->_scopeConfig->getValue($field, ScopeInterface::SCOPE_STORES, $storeId);
    }

    public function getGeneralConfig($code, $storeId = null)
    {
        return $this->getConfigValue(self::XML_PATH_EMAIL . 'general/' . $code, $storeId);
    }

}