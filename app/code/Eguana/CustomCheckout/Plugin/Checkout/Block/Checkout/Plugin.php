<?php
/**
 * Created by PhpStorm.
 * User: hyuna
 * Date: 2018-05-29
 * Time: 오후 5:33
 */

namespace Eguana\CustomCheckout\Plugin\Checkout\Block\Checkout;


use Magento\Checkout\Block\Checkout\AttributeMerger;
use Magento\Store\Model\StoreManagerInterface;

class Plugin
{
    protected $_storeManager;

    /**
     * Plugin constructor.
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(StoreManagerInterface $storeManager)
    {
        $this->_storeManager = $storeManager;
    }

    public function afterMerge(AttributeMerger $subject, $result)
    {
        $storeCode = $this->_storeManager->getStore()->getCode();

        if ($storeCode == 'br') {
            if (array_key_exists('street', $result)) {
                $result['street']['children'][0]['label'] = __('Street');
                $result['street']['children'][1]['label'] = __('Number');
                $result['street']['children'][1]['validation']['required-entry'] = true;
                $result['street']['children'][2]['label'] = __('Complement');
                $result['street']['children'][3]['label'] = __('District');
                $result['street']['children'][3]['validation']['required-entry'] = true;
            }

            if (array_key_exists('vat_id', $result)) {
                $result['vat_id']['sortOrder'] = 50;
                $result['vat_id']['validation']['required-entry'] = true;
            }

            if (array_key_exists('telephone', $result)) {
                $result['telephone']['sortOrder'] = 55;
            }

            if (array_key_exists('postcode', $result)) {
                $result['postcode']['sortOrder'] = 60;
            }

            if (array_key_exists('country_id', $result)) {
                $result['country_id']['sortOrder'] = 150;
            }
        }

        return $result;
    }
}