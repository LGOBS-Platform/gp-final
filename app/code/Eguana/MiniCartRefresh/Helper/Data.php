<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-10-01
 * Time: 오후 5:37
 */

namespace Eguana\MiniCartRefresh\Helper;


use Magento\Checkout\Model\Session;
use Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{
    const EGUANA_CUSTOM_MINI_CART_REFRESH_FLAG = 'eguana_mini_cart_refresh';

    protected $_checkoutSession;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        Session $checkoutSession
    )
    {
        $this->_checkoutSession = $checkoutSession;
        parent::__construct($context);
    }

    /** @return boolean */
    public function isRefresh(){

        if($this->_checkoutSession->getData(self::EGUANA_CUSTOM_MINI_CART_REFRESH_FLAG)){
            $this->setRefresh(false);
            return true;
        }

        return false;
    }

    public function setRefresh($flag = true){
        $this->_checkoutSession->setData(self::EGUANA_CUSTOM_MINI_CART_REFRESH_FLAG , $flag);
    }

}