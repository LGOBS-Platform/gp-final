<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-09-11
 * Time: 오후 12:16
 */

namespace Eguana\MiniCartRefresh\Block\MiniCart;



use Magento\Checkout\Model\Session;

class Refresh extends \Magento\Framework\View\Element\Template
{

    public function getMiniCartClearUrl(){
        return $this->_urlBuilder->getUrl('minicartrefresh/index/index');
    }

    public function getMiniCartCheckUrl(){
        return $this->_urlBuilder->getUrl('minicartrefresh/index/check');
    }

}