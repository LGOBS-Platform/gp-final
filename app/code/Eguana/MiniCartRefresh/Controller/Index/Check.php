<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-09-11
 * Time: 오후 12:05
 */

namespace Eguana\MiniCartRefresh\Controller\Index;

use Eguana\MiniCartRefresh\Helper\Data;
use Magento\Framework\App\Action\Action;
use Magento\Framework\Controller\ResultFactory;

class Check extends Action
{
    protected $_refreshHelper;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        Data $dataHelper
    )
    {
        $this->_refreshHelper = $dataHelper;
        parent::__construct($context);
    }

    public function execute()
    {

        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultPage->setData(['result' => $this->_refreshHelper->isRefresh()]);

        return $resultPage;

    }

}