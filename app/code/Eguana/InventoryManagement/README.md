Eguana_InventoryManagement v2.0

Website : Main Website (LG Electronics)

Author : Abbas Ali Butt

DB Table Name :

Requirements:

1) Create the salesAvailability API 
2) Parse the products updates 

Explanation :

    1) SalesAvailability API
    
        -> Whenever product will update it change the product attribute transer to 0
    
        -> This API will return all those products which transfer attribute is 0.
    
        -> After send the products information it change their attribute transfer to 1. So on next call
        it will not resend those products
    
        -> Here one important point to keep in mind. If admin add any new custom attribute which is required then
        he should set it's value for all products else this api will give error while change transfer attribute value.
    
        -> It's url is {store url}/inventorymanagement/sales/availability/store_code/pl

2) Parse Product updates

How it's process works:

    -> Client will place zip file of update at var/production-import/pl/ folder, here now prodution-import should be set from settings so it can be different according to the settings
        * So sub folders related to the store should always exist such as pl, it etc

    -> Program will unzip the zip file at var/Inventory/executed/pl folder
        * So related folder such as pl, it etc should always exist

    -> Each unzip folder will contain two sub folder one product and second spec. 

    -> spec sub folder will contain the specification attribute information against model id in the form of xml.

    -> sub folder product will contain the attributes information in the form of xml

    -> Each tag name of xml will be in propercasing. For example <SuperCategoryName>Elettrodomestici</SuperCategoryName> It means update the product attribute which code
    is super_category_name with the value of  Elettrodomestici

    -> There will be some attrbutes which needed the mapping information such as UserFriendlyProductName. which after convert will be user_friendly_product_name. But there 
    is no attribute with the code user_friendly_product_name. So in this case client should let us know in advance about such mapping. For example the attribute code should
    map to the user_friendly_name.
    
    -> If there After process all the xmls in the unzip folder it will remove the related zip file from var/Inventory/updates folder
    
    -> Client always should give a unique name to the zip file so it's unzip folder will not overwrite.
    
    -> For Images parsing it will first delete all the images of the product
    
          -> if Gallery->BasicImage exist then previous thumbnail image will be deleted and update the new one
          -> If Gallery->GalleryImages exist then all gallery images will be delted and update the new one
    -> For Images parsing var/tmp folder should exist





How to Enable:

    1) How to parse product updates and enter Cron
            Stores -> Configuration -> EGUANA Extensions -> Inventory Management
            
    2) Admin can set the cron execution time at Stores -> Configuration -> EGUANA Extensions -> Inventory Management -> General
    
    3) Admin can execute the cron at Stores -> Configuration -> EGUANA Extensions -> Inventory Management -> General
    
    4) For sales Availability API admin should set the public key etc at Stores -> Configuration -> EGUANA Extensions -> Security -> Security
    
    5) If Stores -> Configuration -> EGUANA Extensions -> Security -> Security -> Enable will no then api can be accessibel without the public key

    6)  Stores -> Configuration -> EGUANA Extensions -> Security -> General -> Log Enable will save the call information in log file 

    
