<?php
/**
 * Created by PhpStorm.
 * User: abbas
 * Date: 16. 10. 26
 * Time: 오후 2:32
 */
namespace Eguana\InventoryManagement\Model;

use Eguana\InventoryManagement\Api\UpdateRepositoryInterface;
use Eguana\InventoryManagement\Api\Data;
use Eguana\InventoryManagement\Model\ResourceModel\Updates as ResourceUpdates;
use Eguana\InventoryManagement\Model\ResourceModel\Updates\CollectionFactory as updatesCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class LogRepository
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class UpdateRepository implements UpdateRepositoryInterface
{
    /**
     * @var ResourceUpdates
     */
    protected $resource;

    /**
     * @var UpdatesFactory
     */
    protected $updatesFactory;

    /**
     * @var updatesCollectionFactory
     */
    protected $updatesCollectionFactory;

    /**
     * @var Data\UpdateSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * @var \Eguana\InventoryManagement\Api\Data\UpdateInterfaceFactory
     */
    protected $dataUpdateFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;


    /**
     * @param ResourceUpdates $resource
     * @param UpdatesFactory $updatesFactory
     * @param Data\UpdateInterfaceFactory $dataFaqFactory
     * @param updatesCollectionFactory $updatesCollectionFactory
     * @param Data\UpdateSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ResourceUpdates $resource,
        UpdatesFactory $updatesFactory,
        \Eguana\InventoryManagement\Api\Data\UpdateInterfaceFactory $dataUpdateFactory,
        updatesCollectionFactory $updatesCollectionFactory,
        Data\UpdateSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager
    ) {
        $this->resource = $resource;
        $this->updatesFactory = $updatesFactory;
        $this->updatesCollectionFactory = $updatesCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataUpdateFactory = $dataUpdateFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
    }

    /**
     * Save Block data
     *
     * @param \Eguana\InventoryManagement\Api\Data\UpdateInterface $update
     * @return Updates
     * @throws CouldNotSaveException
     */
    public function save(Data\UpdateInterface $update)
    {
        if (empty($update->getStoreId())) {
            $update->setStoreId($this->storeManager->getStore()->getId());
        }

        try {
            $this->resource->save($update);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        return $update;
    }

    /**
     * Load Block data by given Block Identity
     *
     * @param int $updateId
     * @return Updates
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById($updateId)
    {
        $update = $this->updatesFactory->create();
        $this->resource->load($update, $updateId);
        if (!$update->getId()) {
            throw new NoSuchEntityException(__('Update with id "%1" does not exist.', $updateId));
        }
        return $update;
    }

    /**
     * Load Block data collection by given search criteria
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @param \Magento\Framework\Api\SearchCriteriaInterface $criteria
     * @return \Eguana\InventoryManagement\Api\Data\UpdateSearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $criteria)
    {
        /** @var \Eguana\InventoryManagement\Model\ResourceModel\Updates\Collection $collection */
        $collection = $this->updateCollectionFactory->create();

        //$this->collectionProcessor->process($criteria, $collection);

        /** @var Data\UpdateSearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * Delete Block
     *
     * @param \Eguana\InventoryManagement\Api\Data\UpdateInterface $update
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(Data\UpdateInterface $update)
    {
        try {
            $this->resource->delete($update);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * Delete Log by given Block Identity
     *
     * @param int $updateId
     * @return bool
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById($updateId)
    {
        return $this->delete($this->getById($updateId));
    }

}
