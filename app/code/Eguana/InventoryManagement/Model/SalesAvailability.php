<?php
/**
 * Created by PhpStorm.
 * User: abbas
 * Date: 9/28/18
 * Time: 3:07 PM
 */

namespace Eguana\InventoryManagement\Model;

use Eguana\InventoryManagement\Api\SalesAvailabilityInterface;

class SalesAvailability implements SalesAvailabilityInterface
{
    /**
     * @var \Eguana\InventoryManagement\Helper\Product\Status
     */
    private $statusHelper;

    public function __construct( \Eguana\InventoryManagement\Helper\Product\Status $statusHelper)
    {
        $this->statusHelper = $statusHelper;
    }

    /**
     * Returns the products statuses
     *
     * @api
     * @param string $storeCode
     * @param string $fromDate
     * @return \Eguana\InventoryManagement\Api\Data\StatusDataInterface[]|string[]
     */
    public function getStatus($storeCode, $fromDate = null)
    {
        return $this->statusHelper->getStatus($storeCode, $fromDate);
    }

}