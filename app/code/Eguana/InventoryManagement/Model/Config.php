<?php
/**
 * Created by PhpStorm.
 * User: abbas
 * Date: 17. 5. 25
 * Time: 오후 4:31
 */

namespace Eguana\InventoryManagement\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\Encryption\EncryptorInterface;

class Config extends \Magento\Framework\Model\AbstractModel
{
    const LOW_CREDITS_WARNING_MESSAGE = 'Warning: Low credit level at your Storesms account. Buy credits.';
    const API_HOST = 'api.infobip.com';
    const WRONG_AUTH_DATA = 'Storesms API: Wrong Password and/or Username' ;
    const DR_LIMIT = 7654;
    const PLUGIN_VERSION = "2.0.2";

    public $contacts;

    /** @var $_scopeConfig ScopeConfigInterface   */
    protected $_scopeConfig;
    /**
     * @var EncryptorInterface
     */
    private $encryptorInterface;
    

    function __construct(ScopeConfigInterface $scopeConfig,
                         EncryptorInterface $encryptorInterface)
    {
        $this->_scopeConfig = $scopeConfig;

        $this->contacts = array (
                            'en_US'=>'http://www.infobip.com/contact', //default international
                            'pt_BR'=>'http://br.infobip.com/contact', //brasil
                            'fr_FR'=>'http://www.infobip.fr/contact', //france
                            'ru_RU'=>'http://www.infobip.com.ru/contact', //russia
                            'af_ZA'=>'http://www.infobip.co.za/contact', //south africa
                            'pl_PL'=>'http://www.infobip.com.pl/contact', //poland
                            'tr_TR'=>'http://www.infobip.com.tr/contact'//Turkey
                        );
        $this->encryptorInterface = $encryptorInterface;
    }

    public function getContactUrl($localeCode) {

        return (array_key_exists($localeCode, $this->contacts)) ? $this->contacts[$localeCode] : $this->contacts['en_US'];

    }



    /**
     * getting API login from main configuration
     * @return string
     */
    public function getLogin() {
        return $this->_scopeConfig->getValue('storesms_section/main_conf/apilogin', ScopeInterface::SCOPE_STORE);
    }


    /**
     * getting API password from main configuration
     * @return string
     */
    public function getPassword() {
        $encrypted_pass =  $this->_scopeConfig->getValue('storesms_section/main_conf/apipassword', ScopeInterface::SCOPE_STORE);
        return $this->encryptorInterface->decrypt($encrypted_pass);//Mage::helper('core')->decrypt($encrypted_pass);

    }

    /**
     * getting message sender from main configuration
     * @return string
     */
    public function getSender() {
        return $this->_scopeConfig->getValue('storesms_section/main_conf/sender', ScopeInterface::SCOPE_STORE);
    }

    /**
     * getting API login from main configuration
     * @return string
     */
    public function getPromotionLogin() {
        return $this->_scopeConfig->getValue('storesms_section/main_conf/promotionapilogin', ScopeInterface::SCOPE_STORE);
    }


    /**
     * getting API password from main configuration
     * @return string
     */
    public function getPromotionPassword() {
        $encrypted_pass =  $this->_scopeConfig->getValue('storesms_section/main_conf/promotionapipassword', ScopeInterface::SCOPE_STORE);
        return $this->encryptorInterface->decrypt($encrypted_pass);//Mage::helper('core')->decrypt($encrypted_pass);

    }

    /**
     * getting message sender from main configuration
     * @return string
     */
    public function getPromotionSender() {
        return $this->_scopeConfig->getValue('storesms_section/main_conf/promotionsender', ScopeInterface::SCOPE_STORE);
    }

    public function isPro() {
        return $this->_scopeConfig->getValue('storesms_section/main_conf/sender_active', ScopeInterface::SCOPE_STORE);
    }



    public function getCountryPrefix() {

        return $this->_scopeConfig->getValue('storesms_section/main_conf/country_prefix', ScopeInterface::SCOPE_STORE);

    }

    public function getStoreName() {

        return $this->_scopeConfig->getValue('storesms_section/main_conf/storename', ScopeInterface::SCOPE_STORE);

    }


    /**
     * checks if Storesms API module is enabled
     * @return boolean
     */
    public function isApiEnabled() {

        return $this->_scopeConfig->getValue('storesms_section/main_conf/active', ScopeInterface::SCOPE_STORE)==0 ? 0:1;

    }



    public function creditAlertLimit() {
        return floatval(str_replace(',','.',$this->_scopeConfig->getValue('storesms_section/main_conf/credit_alert_limit', ScopeInterface::SCOPE_STORE)));
    }

    public function getPromotionSMSCustomerGroups() {
        return $this->_scopeConfig->getValue('storesms_section/templates/event_promotion_customer_group', ScopeInterface::SCOPE_STORE);
    }

    public function getTestPromotionSMSCustomerGroups() {
        return $this->_scopeConfig->getValue('storesms_section/test_templates/event_promotion_customer_group', ScopeInterface::SCOPE_STORE);
    }

    public function getTestNumbers() {
        return $this->_scopeConfig->getValue('storesms_section/test_templates/event_promotion_test_numbers', ScopeInterface::SCOPE_STORE);
    }

    public function forTestUseCustomerNumbers() {
        return $this->_scopeConfig->getValue('storesms_section/test_templates/event_promotion_test_use_customers', ScopeInterface::SCOPE_STORE);
    }

    /**
     * getting SMS templates from config
     * @return string
     */
    public function getMessageTemplate($template) {

       /* $templateContent = Mage::getStoreConfig('storesms/templates/status_'.$template);

        if (Mage::getStoreConfig('storesms/templates/status_'. $template .'_active') && !empty($templateContent))
            return $templateContent;*/

    }
}