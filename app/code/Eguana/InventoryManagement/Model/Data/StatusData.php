<?php
/**
 * Created by PhpStorm.
 * User: abbas
 * Date: 10/1/18
 * Time: 4:39 PM
 */

namespace Eguana\InventoryManagement\Model\Data;

use Magento\Framework\Api\AbstractExtensibleObject;

class StatusData extends AbstractExtensibleObject implements \Eguana\InventoryManagement\Api\Data\StatusDataInterface
{
    /**
     * Set Currency Code
     * $param string $currencyCode
     * @return void
     */
    public function setCurrencyCode($currencyCode)
    {
        $this->setData(self::CURRENCY_CODE, $currencyCode);
    }


    /**
     * Set Sku
     * $param string $sku
     *
     * @return void
     */
    public function setSku($sku)
    {
        $this->setData(self::SKU, $sku);
    }

    /**
     * Set Model Id
     * $param string $modelId
     * @return void
     */
    public function setModelId($modelId)
    {
        $this->setData(self::MODEL_ID, $modelId);
    }

    /**
     * Set Stock
     * $param bool $stock
     * @return void
     */
    public function setStock($stock)
    {
        $this->setData(self::STOCK, $stock);
    }

    /**
     * Set Sales Availability
     * $param bool $salesAvailability
     * @return void
     */
    public function setSalesAvailability($salesAvailability)
    {
        $this->setData(self::SALES_AVAILABILITY, $salesAvailability);
    }

    /**
     * Set Sales Model
     * $param string $salesModel
     * @return void
     */
    public function setSalesModel($salesModel)
    {
        $this->setData(self::SALES_MODEL, $salesModel);
    }

    /**
     * Set Sales Suffix
     * $param string $salesSuffix
     * @return void
     */
    public function setSalesSuffix($salesSuffix)
    {
        $this->setData(self::SALES_SUFFIX, $salesSuffix);
    }

    /**
     * Set Price
     * $param float $price
     * @return void
     */
    public function setPrice($price)
    {
        $this->setData(self::PRICE, $price);
    }


    /**
     * Get Currency Code
     *
     * @return string
     */
    public function getCurrencyCode()
    {
        return $this->_get(self::CURRENCY_CODE);
    }

    /**
     * get Sku
     *
     * @return string
     */
    public function getSku()
    {
        return $this->_get(self::SKU);
    }

    /**
     * Get Model Id
     *
     * @return string
     */
    public function getModelId()
    {
      return $this->_get(self::MODEL_ID);
    }

    /**
     * Get Stock
     *
     * @return bool
     */
    public function getStock()
    {
        return $this->_get(self::STOCK);
    }

    /**
     * Get Sales Availability
     *
     * @return bool
     */
    public function getSalesAvailability()
    {
        return $this->_get(self::SALES_AVAILABILITY);
    }

    /**
     * get Sales Model
     *
     * @return string
     */
    public function getSalesModel()
    {
        return $this->_get(self::SALES_MODEL);
    }

    /**
     * Get Sales Suffix
     *
     * @return string
     */
    public function getSalesSuffix()
    {
        return $this->_get(self::SALES_SUFFIX);
    }

    /**
     * Get Price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->_get(self::PRICE);
    }


}