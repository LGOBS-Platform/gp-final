<?php
/**
 * Created by PhpStorm.
 * User: abbas
 * Date: 16. 7. 25
 * Time: 오후 5:21
 */

namespace Eguana\InventoryManagement\Model\ResourceModel\Updates;

use Eguana\InventoryManagement\Model\Updates;
use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = Updates::UPDATE_ID;

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Eguana\InventoryManagement\Model\Updates', 'Eguana\InventoryManagement\Model\ResourceModel\Updates');
    }

    /**
     * Resource initialization
     * @return $this
     */
    protected function _initSelect()
    {
        parent::_initSelect();

        /*$this->join(
            [$this->getTable('customer_group')],
            'main_table.customer_group_id = '.$this->getTable('customer_group').'.customer_group_id',
            array('customer_group_code')
        );*/

        return $this;
    }
}