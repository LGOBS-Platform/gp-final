<?php
/**
 * Created by PhpStorm.
 * User: abbas
 * Date: 16. 7. 26
 * Time: 오후 2:37
 */

namespace Eguana\InventoryManagement\Model\ResourceModel\Updates;


use Magento\Framework\ObjectManagerInterface;

class CollectionFactory
{
    /**
     * Object Manager instance
     *
     * @var ObjectManagerInterface
     */
    protected $_objectManager = null;

    /**
     * Instance name to create
     *
     * @var string
     */
    protected $_instanceName = null;

    /**
     * Factory constructor
     *
     * @param ObjectManagerInterface $objectManager
     * @param string $instanceName
     */
    public function __construct(ObjectManagerInterface $objectManager, $instanceName = '\\Eguana\\InventoryManagement\\Model\\ResourceModel\\Updates\\Collection')
    {
        $this->_objectManager = $objectManager;
        $this->_instanceName = $instanceName;
    }

    /**
     * Create class instance with specified parameters
     *
     * @param array $data
     * @return \Eguana\CustomerStatus\Model\ResourceModel\Rule\Collection
     */
    public function create(array $data = array())
    {
        return $this->_objectManager->create($this->_instanceName, $data);
    }
}