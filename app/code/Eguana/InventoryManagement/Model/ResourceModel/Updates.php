<?php
/**
 * Created by PhpStorm.
 * User: abbas
 * Date: 16. 10. 26
 * Time: 오후 2:37
 */

namespace Eguana\InventoryManagement\Model\ResourceModel;



use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Updates extends AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        // Table Name and Primary Key column
        $this->_init('eguana_inventory_management_update_records', 'update_id');
    }
}