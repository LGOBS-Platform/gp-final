<?php
/**
 * Created by PhpStorm.
 * User: abbas
 * Date: 16. 10. 26
 * Time: 오후 2:32
 */

namespace Eguana\InventoryManagement\Model;

use Eguana\InventoryManagement\Api\Data\UpdateInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

class Updates extends AbstractModel implements UpdateInterface,IdentityInterface
{

    const CACHE_TAG = 'eguana_inventory_management_updates';

    const STATUS_ENABLED = 1;

    const STATUS_DISABLED = 0;

    protected $_cacheTag = self::CACHE_TAG;

    /**
     * Name of object id field
     *
     * @var string
     */
    protected $_idFieldName = self::UPDATE_ID;

    protected function _construct()
    {
        $this->_init('Eguana\InventoryManagement\Model\ResourceModel\Updates');
    }

    /**
     * Return unique ID(s) for each object in system
     *
     * @return string[]
     */
    public function getIdentities(){
        return [self::CACHE_TAG . '_' . $this->getId(), self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @param int $update_id
     * @return $this
     */
    public function setUpdateId($update_id)
    {
        return $this->setData(self::UPDATE_ID, $update_id);
    }

    /**
     * @return int
     */
    public function getUpateId()
    {
        return $this->getData(self::UPDATE_ID);
    }

    /**
     * @param string $zipFileName
     * @return $this
     */
    public function setZipFileName($zipFileName)
    {
        return $this->setData(self::ZIP_FILE_NAME, $zipFileName);
    }

    /**
     * @return string
     */
    public function getZipFileName()
    {
        return $this->getData(self::ZIP_FILE_NAME);
    }


    /**
     * @param string $sku
     * @return $this
     */
    public function setProductSku($sku)
    {
        return $this->setData(self::PRODUCT_SKU, $sku);
    }

    /**
     * @return string
     */
    public function getProductSku()
    {
        return $this->getData(self::PRODUCT_SKU);
    }

    /**
     * @param string $attributesXml
     * @return $this
     */
    public function setAttributesXml($attributesXml)
    {
        return $this->setData(self::ATTRIBUTES_XML, $attributesXml);
    }

    /**
     * @return string
     */
    public function getAttributesXml()
    {
        return $this->getData(self::ATTRIBUTES_XML);
    }

    /**
     * @param string $specsXml
     * @return $this
     */
    public function setSpecsXml($specsXml)
    {
        return $this->setData(self::SPECS_XML, $specsXml);
    }

    /**
     * @return string
     */
    public function getSpecsXml()
    {
        return $this->getData(self::SPECS_XML);
    }

    /**
     * @param string $receivedAt
     * @return $this
     */
    public function setReceivedAt($receivedAt)
    {
        return $this->setData(self::RECEIVED_AT, $receivedAt);
    }

    /**
     * @return string
     */
    public function getReceivedAt()
    {
        return $this->getData(self::RECEIVED_AT);
    }

    /**
     * @param string $parsedAt
     * @return $this
     */
    public function setParsedAt($parsedAt)
    {
        return $this->setData(self::PARSED_AT, $parsedAt);
    }

    /**
     * @return string
     */
    public function getParsedAt()
    {
        return $this->getData(self::PARSED_AT);
    }

    /**
     * @param string $parseError
     * @return $this
     */
    public function setParseError($parseError)
    {
        return $this->setData(self::PARSE_ERROR, $parseError);
    }

    /**
     * @return string
     */
    public function getParseError()
    {
        return $this->getData(self::PARSE_ERROR);
    }

    /**
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->getData(self::STATUS);
    }

    /**
     * @param int $storeId
     * @return $this
     */
    public function setStoreId($storeId)
    {
        return $this->setData(self::STORE_ID, $storeId);
    }

    /**
     * @return int
     */
    public function getStoreId()
    {
        return $this->getData(self::STORE_ID);
    }

}