<?php
/**
 * Created by PhpStorm.
 * User: abbas
 * Date: 16. 7. 26
 * Time: 오후 3:31
 */

namespace Eguana\InventoryManagement\Ui\Component\Form;


use Magento\Framework\View\Element\UiComponent\DataProvider\FilterPool;
use Eguana\InventoryManagement\Model\ResourceModel\Updates\CollectionFactory as UpdatesCollectionFactory;
use Magento\Ui\DataProvider\AbstractDataProvider;

class DataProvider extends AbstractDataProvider
{
    /**
     * @var Collection
     */
    protected $collection;

    /**
     * @var Config
     */
    protected $eavConfig;

    /**
     * @var FilterPool
     */
    protected $filterPool;

    /**
     * @var array
     */
    protected $loadedData;


    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        UpdatesCollectionFactory $collectionFactory,
        FilterPool $filterPool,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
        $this->filterPool = $filterPool;
    }


    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (!$this->loadedData) {
            $items = $this->collection->getItems();
            $result = array();
            foreach ($items as $item) {
                $result['logs'] = $item->getData();
                $this->loadedData[$item->getLogId()] = $result;
                break;
            }
        }
        return $this->loadedData;
    }
}