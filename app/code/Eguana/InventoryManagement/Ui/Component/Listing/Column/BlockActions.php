<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Eguana\InventoryManagement\Ui\Component\Listing\Column;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

/**
 * Class BlockActions
 */
class BlockActions extends Column
{
    /**
     * Url path
     */
    const URL_PATH_EDIT = 'eguana_inventorymanagement/updates/edit';
    const URL_PATH_DELETE = 'eguana_inventorymanagement/updates/delete';
    const URL_PATH_EXECUTE = 'eguana_inventorymanagement/updates/execute';

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * Constructor
     *
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * @param array $items
     * @return array
     */
    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                if (isset($item['update_id'])) {
                    $item[$this->getData('name')] = [
                        'edit' => [
                            'href' => $this->urlBuilder->getUrl(
                                static::URL_PATH_EDIT,
                                [
                                    'update_id' => $item['update_id']
                                ]
                            ),
                            'label' => __('View')
                        ],
                        'delete' => [
                            'href' => $this->urlBuilder->getUrl(
                                static::URL_PATH_DELETE,
                                [
                                    'update_id' => $item['update_id']
                                ]
                            ),
                            'label' => __('Delete'),
                            'confirm' => [
                                'title' => __('Delete "${ $.$data.update_id }"'),
                                'message' => __('Are you sure you wan\'t to delete a "${ $.$data.update_id }" record?')
                            ]
                        ]
                    ];
                }
                if (!isset($item['status'])) {
                    $executeURL = $this->urlBuilder->getUrl(
                        static::URL_PATH_EXECUTE,
                        [
                            'update_id' => $item['update_id']
                        ]
                    );
                    $executeLink = [
                        'execute' => [
                            'href' => 'javascript:jQuery("div[data-role=\'spinner\']").show(); window.location = "'.$executeURL.'"',
                            'label' => __('Execute')
                        ]
                    ];
                    $item[$this->getData('name')] = array_merge($item[$this->getData('name')], $executeLink);
                }
            }
        }

        return $dataSource;
    }
}
