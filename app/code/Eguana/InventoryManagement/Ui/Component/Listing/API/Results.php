<?php
/**
 * Created by PhpStorm.
 * User: abbas
 * Date: 11/14/18
 * Time: 6:04 PM
 */

namespace Eguana\InventoryManagement\Ui\Component\Listing\API;


class Results implements \Magento\Framework\Data\OptionSourceInterface
{
    const INVENTORY_UPDATE_PARSE_SUCCESS = 1;
    const INVENTORY_UPDATE_PARSE_FAILED = 0;


    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $availableOptions = $this->getAvailableStatuses();
        $options = [];
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $options;
    }

    /**
     * @return array
     */
    public function getAvailableStatuses()
    {
        return [self::INVENTORY_UPDATE_PARSE_SUCCESS => __('SUCCESSFUL'), self::INVENTORY_UPDATE_PARSE_FAILED => __('FAILED')];
    }
}