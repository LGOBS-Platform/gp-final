<?php
/**
 * Created by PhpStorm.
 * User: abbas
 * Date: 8/7/18
 * Time: 1:36 PM
 */

namespace Eguana\InventoryManagement\Setup;

use Magento\Catalog\Model\Resource\Eav\Attribute;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;


/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
    /**
     * EAV setup factory
     *
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * Init
     *
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {

        $booleanAttributes = array(
            'transferred'=>'Transferred'

        );


        $setup->startSetup();

        // TO CREATE ATTRIBUTE SETS

        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);


        foreach ($booleanAttributes as $code=>$booleanAttribute)
        {
            $this->addBooleanField($eavSetup, $code, $booleanAttribute);
        }


        $setup->endSetup();
    }


    private function addBooleanField($eavSetup, $attributeCode, $attributeName)
    {
        $eavSetup->removeAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            $attributeCode);
        /**
         * Add attributes to the eav/attribute
         */
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            $attributeCode,
            [
                'type' => 'text',
                'frontend' => '',
                'label' => $attributeName,
                'input' => 'select',
                'backend' => '',
                'class' => '',
                'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => 0,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => true,
                'used_in_product_listing' => true,
                'unique' => false,
                'apply_to' => 'simple,configurable,virtual,bundle,downloadable,grouped',
                'group'=> 'LG Attributes'
            ]
        );

    }

}