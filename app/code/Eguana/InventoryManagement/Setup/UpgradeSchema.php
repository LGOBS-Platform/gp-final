<?php
/**
 * Created by PhpStorm.
 * User: Abbas
 * Date: 2018-11-23
 * Time: 오후 02:51
 */

namespace Eguana\InventoryManagement\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $this->_createParseRecordsTable($setup);
        }



        $setup->endSetup();
    }

    protected function _createParseRecordsTable(SchemaSetupInterface $setup){

        /**
         * Create table 'eguana_inventory_management_update_records'
         */
        $lgcomReceiveTable = $setup->getConnection()->newTable($setup->getTable('eguana_inventory_management_update_records'))
            ->addColumn(
                'update_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Update ID'
            )->addColumn(
                'zip_file_name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Zip File Name'
            )->addColumn(
                'product_sku',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => true],
                'Product SKU'
            )->addColumn(
                'attributes_xml',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                ['nullable' => true],
                'Attributes XML'
            )->addColumn(
                'specs_xml',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                ['nullable' => true],
                'Specs XML'
            )->addColumn(
                'received_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                'Record Received AT'
            )->addColumn(
                'parsed_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => true, 'default' => null],
                'Parsed At'
            )->addColumn(
                'parse_error',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                ['nullable' => true],
                'Parse Error'
            )->addColumn(
                'status',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                2,
                [
                    'nullable' => true,
                    'default'   => null
                ],
                'Parse status'
            )->addColumn(
                'store_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                ['nullable' => true],
                'Record related store id'
            )->addIndex(
                $setup->getIdxName(
                    $setup->getTable('eguana_inventory_management_update_records'),
                    ['store_id','status']
                ),['store_id','status']
            );

        $setup->getConnection()->createTable($lgcomReceiveTable);
    }
}