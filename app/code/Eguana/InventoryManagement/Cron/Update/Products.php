<?php
/**
 * Created by PhpStorm.
 * User: abbas
 * Date: 10/26/18
 * Time: 10:46 AM
 */

namespace Eguana\InventoryManagement\Cron\Update;

use \Psr\Log\LoggerInterface;

class Products
{

    /**
     * @var \Eguana\InventoryManagement\Helper\Product\ZipParser
     */
    private $parser;

    /** @var $_logger LoggerInterface */
    protected $_logger;

    /**
     * Coupon constructor.
     * @param LoggerInterface $logger
     * @param \Eguana\InventoryManagement\Helper\Product\ZipParser $parser
     */
    public function __construct(
        LoggerInterface $logger,
        \Eguana\InventoryManagement\Helper\Product\ZipParser $parser
    )
    {
        $this->_logger = $logger;
        $this->parser = $parser;

    }

    /**
     * This function will execute to update the products by cron
     */
    public function execute()
    {
        try{
            $this->_logger->addDebug('Called the update products cron');
            $message = $this->parser->parseProducts();
            $this->_logger->addDebug($message);
        }catch (\Exception $e){
            $this->_logger->addDebug($e->getMessage());
        }


    }
}