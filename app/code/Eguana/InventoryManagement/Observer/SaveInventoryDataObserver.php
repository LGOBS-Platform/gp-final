<?php
/**
 * Created by PhpStorm.
 * User: abbas
 * Date: 10/19/18
 * Time: 11:22 AM
 */

namespace Eguana\InventoryManagement\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;


class SaveInventoryDataObserver implements ObserverInterface
{
    /**
     * Saving product inventory data. Product qty calculated dynamically.
     *
     * @param EventObserver $observer
     * @return $this
     */
    public function execute(EventObserver $observer)
    {
        $product = $observer->getEvent()->getProduct();
        if($product->getData('transferred') == $product->getOrigData('transferred')) {
            $product->setData('transferred', 0);
            $product->setData('last_update_date', time());
        }
        return $this;
    }
}