<?php
/**
 * Created by PhpStorm.
 * User: abbas
 * Date: 17. 7. 14
 * Time: 오후 4:02
 */

namespace Eguana\InventoryManagement\Logger;

class Handler extends \Magento\Framework\Logger\Handler\Base
{
    /**
     * File name
     * @var string
     */
    protected $fileName = '/var/log/Eguana_InventoryManagement_Log.log';

    /**
     * Logging level
     * @var int
     */
    protected $loggerType = \Monolog\Logger::INFO;
}
