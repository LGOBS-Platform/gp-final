<?php
/**
 * Created by PhpStorm.
 * User: abbas
 * Date: 17. 7. 14
 * Time: 오후 4:02
 */

namespace Eguana\InventoryManagement\Logger;

class Logger extends \Monolog\Logger
{

    public function InventoryLog($message)
    {
        try {
            if(is_null($message)){
                $message = "NULL";
            }
            if (is_array($message) || is_object($message)) {
                $message = json_encode($message, JSON_PRETTY_PRINT);
            }

            if(!empty(json_last_error())){
                $message = (string)json_last_error();
            }
            
            $message = (string)$message;

        } catch (\Exception $e) {
            $message = "Now Data";
        }
        $message .= "\r\n";
        $this->info($message);
    }
}