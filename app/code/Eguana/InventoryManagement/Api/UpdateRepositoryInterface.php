<?php
/**
 * Created by PhpStorm.
 * User: Abbas
 * Date: 2018-11-15
 * Time: 오후 1:51
 */
namespace Eguana\InventoryManagement\Api;

use Eguana\InventoryManagement\Api\Data\UpdateInterface;
use Eguana\InventoryManagement\Model\Updates;
use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * CMS block CRUD interface.
 * @api
 * @since 100.0.2
 */
interface UpdateRepositoryInterface
{
    /**
     * Save log.
     *
     * @param UpdateInterface $update
     * @return UpdateInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(UpdateInterface $update);

    /**
     * Retrieve update.
     *
     * @param int $updateId
     * @return UpdateInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($updateId);

    /**
     * Retrieve blocks matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Magento\Cms\Api\Data\BlockSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete udpate.
     *
     * @param UpdateInterface $update
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(UpdateInterface $update);

    /**
     * Delete udpate by ID.
     *
     * @param int $updateId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($updateId);
}
