<?php
namespace Eguana\InventoryManagement\Api;

interface SalesAvailabilityInterface
{
    /**
     * Returns the products statuses
     *
     * @param string $storeCode
     * @param string $fromDate
     * @return \Eguana\InventoryManagement\Api\Data\StatusDataInterface[]|string[]
     */
    public function getStatus($storeCode, $fromDate = null);
}