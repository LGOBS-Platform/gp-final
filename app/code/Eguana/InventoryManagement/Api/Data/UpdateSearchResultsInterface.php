<?php
/**
 * Created by PhpStorm.
 * User: Abbas
 * Date: 2018-11-15
 * Time: 오후 1:51
 */
namespace Eguana\InventoryManagement\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;


interface UpdateSearchResultsInterface extends SearchResultsInterface
{
    /**
     * @return UpdateInterface[]
     */
    public function getItems();

    /**
     * @param UpdateInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
