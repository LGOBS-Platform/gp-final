<?php
/**
 * Created by PhpStorm.
 * User: Abbas
 * Date: 2018-11-15
 * Time: 오후 1:51
 */

namespace Eguana\InventoryManagement\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

interface UpdateInterface
{
    const UPDATE_ID = 'update_id';

    const ZIP_FILE_NAME = 'zip_file_name';

    const PRODUCT_SKU = 'product_sku';

    const ATTRIBUTES_XML = 'attributes_xml';

    const SPECS_XML = 'specs_xml';

    const RECEIVED_AT = 'received_at';

    const PARSED_AT = 'parsed_at';

    const PARSE_ERROR = 'parse_error';

    const STATUS = 'status';

    const STORE_ID = 'store_id';



    /**
     * @param int $update_id
     * @return $this
     */
    public function setUpdateId($update_id);

    /**
     * @return int
     */
    public function getUpateId();

    /**
     * @param string $zipFileName
     * @return $this
     */
    public function setZipFileName($zipFileName);

    /**
     * @return string
     */
    public function getZipFileName();


    /**
     * @param string $sku
     * @return $this
     */
    public function setProductSku($sku);

    /**
     * @return string
     */
    public function getProductSku();

    /**
     * @param string $attributesXml
     * @return $this
     */
    public function setAttributesXml($attributesXml);

    /**
     * @return string
     */
    public function getAttributesXml();

    /**
     * @param string $specsXml
     * @return $this
     */
    public function setSpecsXml($specsXml);

    /**
     * @return string
     */
    public function getSpecsXml();

    /**
     * @param string $receivedAt
     * @return $this
     */
    public function setReceivedAt($receivedAt);

    /**
     * @return string
     */
    public function getReceivedAt();

    /**
     * @param string $parsedAt
     * @return $this
     */
    public function setParsedAt($parsedAt);

    /**
     * @return string
     */
    public function getParsedAt();

    /**
     * @param string $parseError
     * @return $this
     */
    public function setParseError($parseError);

    /**
     * @return string
     */
    public function getParseError();

    /**
     * @param string $status
     * @return $this
     */
    public function setStatus($status);

    /**
     * @return string
     */
    public function getStatus();

    /**
     * @param int $storeId
     * @return $this
     */
    public function setStoreId($storeId);

    /**
     * @return int
     */
    public function getStoreId();

}