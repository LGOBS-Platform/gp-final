<?php
/**
 * Created by PhpStorm.
 * User: abbas
 * Date: 10/1/18
 * Time: 4:27 PM
 */

namespace Eguana\InventoryManagement\Api\Data;


interface StatusDataInterface
{
    const SKU = 'sku';
    const MODEL_ID = 'MODEL_ID';
    const STOCK = 'STOCK';
    const SALES_AVAILABILITY = 'SALES_AVAILABILITY';
    const SALES_MODEL = 'SALES_MODEL';
    const SALES_SUFFIX = 'SALES_SUFFIX';
    const PRICE = 'PRICE';
    const CURRENCY_CODE = 'CURRENCY_CODE';

    /**
     * Set Currency Code
     * $param string $currencyCode
     * @return void
     */
    public function setCurrencyCode($currencyCode);

    /**
     * Set Sku
     * $param string $sku
     *
     * @return void
     */
    public function setSku($sku);

    /**
     * Set Model Id
     * $param string $modelId
     * @return void
     */
    public function setModelId($modelId);

    /**
     * Set Stock
     * $param bool $stock
     * @return void
     */
    public function setStock($stock);

    /**
     * Set Sales Availability
     * $param bool $salesAvailability
     * @return void
     */
    public function setSalesAvailability($salesAvailability);

    /**
     * Set Sales Model
     * $param string $salesModel
     * @return void
     */
    public function setSalesModel($salesModel);

    /**
     * Set Sales Suffix
     * $param string $salesSuffix
     * @return void
     */
    public function setSalesSuffix($salesSuffix);

    /**
     * Set Price
     * $param float $price
     * @return void
     */
    public function setPrice($price);


    /**
     * Get Currency Code
     *
     * @return string
     */
    public function getCurrencyCode();


    /**
     * get Sku
     *
     * @return string
     */
    public function getSku();

    /**
     * Get Model Id
     *
     * @return string
     */
    public function getModelId();

    /**
     * Get Stock
     *
     * @return bool
     */
    public function getStock();

    /**
     * Get Sales Availability
     *
     * @return bool
     */
    public function getSalesAvailability();

    /**
     * get Sales Model
     *
     * @return string
     */
    public function getSalesModel();

    /**
     * Get Sales Suffix
     *
     * @return string
     */
    public function getSalesSuffix();

    /**
     * Get Price
     *
     * @return float
     */
    public function getPrice();


}