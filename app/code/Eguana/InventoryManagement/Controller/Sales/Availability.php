<?php
/**
 * Created by PhpStorm.
 * User: abbas
 * Date: 10/23/18
 * Time: 4:11 PM
 */

namespace Eguana\InventoryManagement\Controller\Sales;

use Eguana\Security\Controller\AbstractAction;
use Eguana\Security\Helper\Data;
use Magento\Framework\App\Action\Context;

class Availability extends AbstractAction
{
    /**
     * @var \Eguana\InventoryManagement\Helper\Product\Inventory
     */
    private $inventoryHelper;

    public function __construct(Context $context,
                                Data $helper,
                                \Magento\Framework\Webapi\Rest\Response\Renderer\Xml $xmlResult,
                                \Eguana\InventoryManagement\Helper\Product\Inventory $inventoryHelper)
    {
        parent::__construct($context, $helper, $xmlResult);
        $this->inventoryHelper = $inventoryHelper;
    }

    /*public function execute(){
        echo 'JOk'; die;
    }*/

    public function _execute(){
        $storeCode = $this->getRequest()->getParam('store_code');
        return $this->getResultPage($this->inventoryHelper->getInventory($storeCode));
    }

}