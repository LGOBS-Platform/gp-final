<?php

/**
 * Created by PhpStorm.
 * User: abbas
 * Date: 17. 7. 14
 * Time: 오후 3:13
 */

namespace Eguana\InventoryManagement\Controller\Adminhtml\System\Config\Parse;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;

class Run extends Action
{
    protected $resultJsonFactory;

    /**
     * @var \Eguana\InventoryManagement\Helper\Product\ZipParser
     */
    private $parser;

    /**
     * Coupon constructor.
     * @param Context $context
     * @param JsonFactory $resultJsonFactory
     * @param \Eguana\InventoryManagement\Helper\Product\Status $statusHelper
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        \Eguana\InventoryManagement\Helper\Product\ZipParser $parser
    )
    {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->parser = $parser;
        parent::__construct($context);

    }

    /**
     * Collect relations data
     *
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        //$message = $this->statusHelper->getStatusFile();
        $message = $this->parser->parseProducts();

        /** @var \Magento\Framework\Controller\Result\Json $result */
        $result = $this->resultJsonFactory->create();

        return $result->setData(['success' => true, 'message' => $message]);
    }
}