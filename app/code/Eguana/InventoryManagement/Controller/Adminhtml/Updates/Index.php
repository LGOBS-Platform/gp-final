<?php
/**
 * Created by PhpStorm.
 * User: Abbas
 * Date: 2018-11-13
 * Time: 오후 5:10
 */

namespace Eguana\InventoryManagement\Controller\Adminhtml\Updates;


use Eguana\InventoryManagement\Controller\Adminhtml\AbstractController;
use Magento\Framework\Controller\ResultFactory;

class Index extends AbstractController
{

    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage
            ->setActiveMenu('Eguana_InventoryManagement::updates')
            ->getConfig()->getTitle()->prepend(__('Product Updates'));

        return $resultPage;
    }

}