<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Eguana\InventoryManagement\Controller\Adminhtml\Updates;

use Eguana\InventoryManagement\Controller\Adminhtml\AbstractController;

class Delete extends AbstractController
{
    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('update_id');
        if ($id) {
            try {
                // init model and delete
                $this->_updateRepository->deleteById($id);
                // display success message
                $this->messageManager->addSuccess(__('You deleted the update.'));
                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addError($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['update_id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addError(__('We can\'t find a update to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}
