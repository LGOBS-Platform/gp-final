<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Eguana\InventoryManagement\Controller\Adminhtml\Updates;

use Eguana\InventoryManagement\Controller\Adminhtml\AbstractController;

class Edit extends AbstractController
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;


    public function __construct(\Magento\Backend\App\Action\Context $context,
                                \Magento\Framework\Registry $coreRegistry,
                                \Magento\Framework\View\Result\PageFactory $resultPageFactory,
                                \Eguana\InventoryManagement\Model\UpdateRepository $updateRepository)
    {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context, $coreRegistry, $resultPageFactory, $updateRepository);
    }

    /**
     * Edit CMS block
     *
     * @return \Magento\Framework\Controller\ResultInterface
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('update_id');
        $update = $this->_updateRepository->getById($id);

        // 2. Initial checking
        if ($id) {
            if (!$update->getUpateId()) {
                $this->messageManager->addError(__('This update no longer exists.'));
                /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }

        $this->_coreRegistry->register('update', $update);

        // 5. Build edit form
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $this->initPage($resultPage)->addBreadcrumb(
            $id ? __('Edit Update') : __('New Update'),
            $id ? __('Edit Update') : __('New Update')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Updates'));
        $resultPage->getConfig()->getTitle()->prepend($update->getUpateId() ? $update->getProductSku() : __('New Update'));
        return $resultPage;
    }
}
