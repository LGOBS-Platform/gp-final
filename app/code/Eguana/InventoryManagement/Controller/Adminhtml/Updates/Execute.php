<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Eguana\InventoryManagement\Controller\Adminhtml\Updates;

use Eguana\InventoryManagement\Controller\Adminhtml\AbstractController;

class Execute extends AbstractController
{
    /**
     * @var \Eguana\InventoryManagement\Helper\Product\Update
     */
    private $_updateHelper;

    protected $_date;

    public function __construct(\Magento\Backend\App\Action\Context $context,
                                \Magento\Framework\Registry $coreRegistry,
                                \Magento\Framework\View\Result\PageFactory $resultPageFactory,
                                \Eguana\InventoryManagement\Model\UpdateRepository $updateRepository,
                                \Magento\Framework\Stdlib\DateTime\DateTime $date,
                                \Eguana\InventoryManagement\Helper\Product\Update $updateHelper)
    {
        $this->_updateHelper = $updateHelper;
        $this->_date = $date;
        parent::__construct($context, $coreRegistry, $resultPageFactory, $updateRepository);
    }

    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('update_id');
        if ($id) {
            try {
                $update = $this->_updateRepository->getById($id);
                $result = $this->_updateHelper->parseFile($update->getAttributesXml(), $update->getSpecsXml());
                $updateStatus = $result['status']?1:0;
                $update->setStatus($updateStatus);
                if(isset($result['error_message'])) {
                    $update->setParseError($result['error_message']);
                }
                $update->setParsedAt($this->_date->gmtTimestamp());
                $this->_updateRepository->save($update);
                // init model and delete
               // $this->_updateRepository->deleteById($id);
                // display success message
                //$this->messageManager->addSuccess(__('You deleted the update.'));
                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addError($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/', ['update_id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addError(__('We can\'t find a update to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}
