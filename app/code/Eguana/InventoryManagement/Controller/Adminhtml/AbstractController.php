<?php
/**
 * Created by PhpStorm.
 * User: Abbas
 * Date: 2018-08-24
 * Time: 오후 1:51
 */

namespace Eguana\InventoryManagement\Controller\Adminhtml;


use Magento\Backend\App\Action;

abstract class AbstractController extends Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_resultPageFactory;

    protected $_coreRegistry;

    /**
     * @var \Eguana\InventoryManagement\Model\UpdateRepository
     */
    protected $_updateRepository;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Eguana\InventoryManagement\Model\UpdateRepository $updateRepository
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $coreRegistry;
        $this->_updateRepository = $updateRepository;
        parent::__construct($context);
    }

    public function _init($resultPage){

        $resultPage->setActiveMenu('Eguana_InventoryManagement');
        $resultPage->addBreadcrumb(__('Inventory Management'), __('Inventory Management'));
        $resultPage->addBreadcrumb(__('Manage Updates'), __('Manage Updates'));
        $resultPage->getConfig()->getTitle()->prepend(__('Manage Updates'));

        return $resultPage;
    }

    /**
     * Check the permission to run it
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Eguana_InventoryManagement::updates');
    }

    /**
     * Init page
     *
     * @param \Magento\Backend\Model\View\Result\Page $resultPage
     * @return \Magento\Backend\Model\View\Result\Page
     */
    protected function initPage($resultPage)
    {
        $resultPage->setActiveMenu('Eguana_InventoryManagement::updates')
            ->addBreadcrumb(__('Inventory'), __('Inventory'))
            ->addBreadcrumb(__('Updates'), __('Updates'));
        return $resultPage;
    }

}