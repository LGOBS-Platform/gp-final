<?php
/**
 * Created by PhpStorm.
 * User: abbas
 * Date: 17. 3. 27
 * Time: 오후 3:49
 */

namespace Eguana\InventoryManagement\Block\Adminhtml\System\Config;


use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;

class Run extends \Magento\Config\Block\System\Config\Form\Field
{
    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
   // protected $timezone;
    public function __construct(\Magento\Backend\Block\Template\Context $context,
                                array $data = [])
    {

       // $this->timezone = $context->getLocaleDate();
        parent::__construct($context, $data);
    }

    /**
     * Set template to itself
     *
     * @return \Magento\Customer\Block\Adminhtml\System\Config\Validatevat
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (!$this->getTemplate()) {
            $this->setTemplate('system/config/run.phtml');
        }
        return $this;
    }

    /**
     * Unset some non-related element parameters
     *
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     */
    public function render(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();
        return parent::render($element);
    }

    /**
     * Get the button and scripts contents
     *
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        /** @var  \DateTime $currentTime */
        //$currentTime = (array)$this->timezone->date();
        //$timeMessage = $currentTime['date'].' '.$currentTime['timezone'];
        $uri = '';
        $originalData = $element->getOriginalData();
        if($originalData['id'] == 'run_parse')
        {
            $uri = 'eguana_inventorymanagement/system_config_parse/run';
        }

        $buttonLabel = !empty($originalData['button_label']) ? $originalData['button_label'] : $this->_vatButtonLabel;
        $this->addData(
            [
                'button_label' => __($buttonLabel),
                'html_id' => $element->getHtmlId(),
                'ajax_url' => $this->_urlBuilder->getUrl($uri, ['form_key' => $this->getFormKey()]),
                //'time_message' => $timeMessage,
                'result_div_id' => $originalData['id']
            ]
        );

        return $this->_toHtml();
    }
}