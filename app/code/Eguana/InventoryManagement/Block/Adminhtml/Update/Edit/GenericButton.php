<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Eguana\InventoryManagement\Block\Adminhtml\Update\Edit;

use Magento\Backend\Block\Widget\Context;
use Eguana\InventoryManagement\Api\UpdateRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class GenericButton
 */
class GenericButton
{
    /**
     * @var Context
     */
    protected $context;

    /**
     * @var UpdateRepositoryInterface
     */
    protected $updateRepository;

    /**
     * @param Context $context
     * @param UpdateRepositoryInterface $blockRepository
     */
    public function __construct(
        Context $context,
        UpdateRepositoryInterface $blockRepository
    ) {
        $this->context = $context;
        $this->updateRepository = $blockRepository;
    }

    /**
     * Return Inventory update ID
     *
     * @return int|null
     */
    public function getUpdateId()
    {
        try {
            return $this->updateRepository->getById(
                $this->context->getRequest()->getParam('update_id')
            )->getUpateId();
        } catch (NoSuchEntityException $e) {
        }
        return null;
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
