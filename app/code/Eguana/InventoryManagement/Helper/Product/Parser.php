<?php
namespace Eguana\InventoryManagement\Helper\Product;

/**
 * Created by PhpStorm.
 * User: abbas
 * Date: 17. 7. 14
 * Time: 오후 4:02
 */

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Xml\Parser as xmlParser;
use Magento\Setup\Exception;

class Parser extends AbstractHelper
{

    /**
     * @var DirectoryList
     */
    private $directoryList;

    protected $_storeManager;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $productFactory;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\Action
     */
    private $productAction;

    /**
     * @var \Magento\Indexer\Model\IndexerFactory
     */
    protected $indexerFactory;

    /**
     * @var \Eguana\InventoryManagement\Helper\Product\Attributes\Map
     */
    protected $_attributesMap;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $_jsonHelper;

    /**
     * @var \Eguana\InventoryManagement\Helper\Data
     */
    protected $_commanHelper;

    /**
     * @var \Magento\Framework\Filesystem\Driver\File
     */
    protected $_file;

    /**
     * @var xmlParser
     */
    private $_xmlParser;

    private $productRepository;
    private $imageHelper;

    private $parseError;

    /**
     * @var \Eguana\InventoryManagement\Logger\Logger
     */
    protected $_parserLogger;

    /**
     * @var \Magento\Catalog\Api\Data\Product
     */
    protected $_productData;

    /**
     * Status constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param DirectoryList $directoryList
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        DirectoryList $directoryList,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Catalog\Model\ResourceModel\Product\Action $productAction,
        \Magento\Indexer\Model\IndexerFactory $indexerFactory,
        \Eguana\InventoryManagement\Helper\Product\Attributes\Map $attributesMap,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Eguana\InventoryManagement\Helper\Data $commanHelper,
        \Magento\Framework\Filesystem\Driver\File $file,
        xmlParser $xmlParser,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Eguana\InventoryManagement\Helper\Product\Image $imageHelper,
         \Eguana\InventoryManagement\Logger\Logger $logger,
        \Magento\Catalog\Api\Data\ProductInterface $productData
    )
    {
        parent::__construct($context);
        $this->directoryList = $directoryList;
        $this->_storeManager = $storeManager;
        $this->productFactory = $productFactory;
        $this->productAction = $productAction;
        $this->indexerFactory = $indexerFactory;
        $this->_attributesMap = $attributesMap;
        $this->_jsonHelper = $jsonHelper;
        $this->_commanHelper = $commanHelper;
        $this->_file = $file;
        $this->_xmlParser = $xmlParser;
        $this->productRepository = $productRepository;
        $this->imageHelper = $imageHelper;
        $this->parseError = '';
        $this->_parserLogger = $logger;
    }

    public function parseProducts()
    {
        if(!$this->_commanHelper->getInventoryManagementGeneralConfig('enabled'))
        {
            return 'Product Update is disabled';
        }
        $newUpdates = $this->getNewUpdates();

        if(sizeof($newUpdates) == 0)
        {
            return 'There are no new updates';
        }
        try {
            foreach ($newUpdates as $newUpdate) {
                $inventoryUpdatesDir = $this->getFilePath();
                $newUpdate = str_replace($inventoryUpdatesDir . '/', '', $newUpdate);
                if ($this->copyFile($newUpdate)) {
                    $this->_parserLogger->InventoryLog('Parsing File '.$newUpdate);
                    if ($this->parseFile($newUpdate)) {
                        $this->_parserLogger->InventoryLog('Deleting File '.$newUpdate);
                        $this->_file->deleteFile($this->getFilePath() . '/' . $newUpdate);
                    }else{
                        $this->_parserLogger->InventoryLog($this->parseError);
                        return $this->parseError;
                    }
                }
            }
            $this->_parserLogger->InventoryLog('Finished');
            return 'Done';
        }catch (\Exception $e){
            $this->_parserLogger->InventoryLog($e->getMessage());
            return $e->getMessage();
        }
        /*$indexer = $this->indexerFactory->create();
        $indexer->load('catalog_product_flat');
        $indexer->reindexAll();*/
    }

    private function copyFile($update)
    {
        $inventoryUpdatesDir = $this->getFilePath();
        return $this->_file->copy($inventoryUpdatesDir.'/'.$update, $this->getExecutedFilePath().'/'.$update);
    }

    private function parseFile($update)
    {
        $result = true;
        if(!$this->unzipFile($update))
        {
            $this->parseError = 'Unable to unzip file';
            return false;
        }
        $unZipPath = $this->getExecutedFilePath().'/'.str_replace('.zip', '', $update);
        $attributesXML = $unZipPath.'/'.'product';
        $specsXML = $unZipPath.'/'.'spec';

        $attributesXMLFiles = $this->_file->readDirectoryRecursively($attributesXML);

        foreach ($attributesXMLFiles as $xmlFile)
        {

            $updateXML = simplexml_load_file($xmlFile);
            $productAttributes = $updateXML->Product->children();
            $gallery = $updateXML->Gallery->children();
            $sku = (string)$productAttributes->LocaleCode.'.'.(string)$productAttributes->SalesModels->SalesModel->SalesModelCode;
            $this->_parserLogger->InventoryLog('Parsing File '.$xmlFile);
            if(isset($sku))
            {
                /**
                 * @var $relatedProduct \Magento\Catalog\Model\Product
                 */
                $relatedProduct = $this->productFactory->create();
                $skuId = $relatedProduct->getIdBySku($sku);

                if($skuId) {
                    $this->_parserLogger->InventoryLog('Updating Product '.$skuId);
                    $currentProduct = $this->productRepository->getById($skuId);
                    foreach ($productAttributes as $key => $value) {
                        if($key == 'EnergyLabel')
                        {
                            $currentProduct->setData('doc_id',(string)$value->DocId);
                            $currentProduct->setData('original_name_b1_a1',(string)$value->OriginalName);
                            $currentProduct->setData('file_name', (string)$value->FileName);
                            $currentProduct->setData('tc', (string)$value->TC);
                            $currentProduct->setData('gsri_doc', (string)$value->GsriDoc);
                        }elseif($key == 'ProductFiche')
                        {
                            $currentProduct->setData('doc_id_2', (string)$value->DocId);
                            $currentProduct->setData('original_name_b1_a1_2', (string)$value->OriginalName);
                            $currentProduct->setData('file_name_2', (string)$value->FileName);
                            $currentProduct->setData('tc_2', (string)$value->TC);
                            $currentProduct->setData('gsri_doc_2', (string)$value->GsriDoc);
                        }
                        $updatedKey = strtolower(preg_replace('/\B([A-Z])/', '_$1', $key));
                        try {
                            if((string)$value != '' && $mappedKey = $this->_attributesMap->getMappedCode($updatedKey)) {
                                $currentProduct->setData($mappedKey,$this->_attributesMap->getSelectValue($mappedKey, (string)$value));
                            }
                        }catch (\Exception $e){
                            $this->parseError = $sku.': '.$e->getMessage();
                            $result = false;
                        }
                    }

                    try {

                        $specFile = $specsXML.'/'.(string)$productAttributes->ProductCode.'.xml';
                        if(file_exists($specFile))
                        {
                            //$updatedValues['specifications'] = $this->getProductSpecs($specFile);
                            $currentProduct->setData('specifications', $this->getProductSpecs($specFile));
                        }
                        $store = $this->_storeManager->getStore(strtolower($currentProduct->getData('locale_code')));
                        $this->_storeManager->setCurrentStore($store->getId());
                        //$currentProduct->setData($updatedValues);
                        $this->productRepository->save($currentProduct);
                        $this->imageHelper->updateImages($currentProduct, $gallery);
                        /*
                        $this->productAction->updateAttributes([$skuId], $updatedValues, $store->getId());
                        $currentProduct = $this->productRepository->getById($skuId);//get($sku);
                        $this->imageHelper->updateImages($currentProduct, $gallery);*/
                    }catch (\Exception $e){
                        $this->parseError =  $sku.': '.$e->getMessage();
                        $result = false;
                    }
                }
            }
        }
        return $result;
    }

    private function unzipFile($update)
    {
        $filePath = $this->getExecutedFilePath().'/'.$update;
        $result = FALSE;
        $zip = new \ZipArchive();
        $res = $zip->open($filePath);
        if($res === TRUE)
        {
            $zip->extractTo($this->getExecutedFilePath().'/'.str_replace('.zip', '', $update));
            $zip->close();
            $result = TRUE;
        }
        return $result;
    }

    private function getNewUpdates()
    {
        $inventoryUpdatesDir = $this->getFilePath();
        $newUpdates = $this->_file->readDirectoryRecursively($inventoryUpdatesDir);//scandir($inventoryUpdatesDir);
        $newZipUpdates = array();
        foreach ($newUpdates as $newUpdate)
        {
            if($this->_file->isFile($newUpdate) && pathinfo($newUpdate, PATHINFO_EXTENSION) == 'zip')
            $newZipUpdates[] = $newUpdate;
        }
        //$filePath = $inventoryStatusDir.'/'.str_replace(" ", "-", $this->date->gmtDate()).'.xml';
        return $newZipUpdates;
    }

    private function getFilePath()
    {
        $varDir = $this->directoryList->getPath('var');
        $inventoryUpdatesDir = $varDir.'/'.$this->_commanHelper->getInventoryManagementGeneralConfig('zippath');

        if (!file_exists($inventoryUpdatesDir)) {
            mkdir($inventoryUpdatesDir, 0777, true);
        }
        return $inventoryUpdatesDir;
    }

    private function getExecutedFilePath()
    {
        $varDir = $this->directoryList->getPath('var');
        $inventoryUpdatesDir = $varDir.'/Inventory/executed';

        if (!file_exists($inventoryUpdatesDir)) {
            mkdir($inventoryUpdatesDir, 0777, true);
        }
        return $inventoryUpdatesDir;
    }

    private function getProductSpecs($path)
    {
        $specs = array();
        $specXML = simplexml_load_file($path);
        foreach($specXML->group as $group)
        {
            $specs[(string)$group['name']] = array();
            foreach ($group->item as $item)
            {
                $specs[(string)$group['name']][(string)$item['name']] = (string)$item;
            }
        }
        return sizeof($specs)?$this->_jsonHelper->jsonEncode($specs):'';
    }
}