<?php
namespace Eguana\InventoryManagement\Helper\Product;

/**
 * Created by PhpStorm.
 * User: abbas
 * Date: 17. 7. 14
 * Time: 오후 4:02
 */

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Xml\Parser as xmlParser;
use Magento\Setup\Exception;

class ZipParser extends AbstractHelper
{

    /**
     * @var DirectoryList
     */
    private $directoryList;

    protected $_storeManager;


    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $_jsonHelper;

    /**
     * @var \Eguana\InventoryManagement\Helper\Data
     */
    protected $_commanHelper;

    /**
     * @var \Magento\Framework\Filesystem\Driver\File
     */
    protected $_file;

    /**
     * @var xmlParser
     */
    private $_xmlParser;


    private $parseError;

    /**
     * @var \Eguana\InventoryManagement\Logger\Logger
     */
    protected $_parserLogger;

    /**
     * @var \Eguana\InventoryManagement\Model\UpdateRepository
     */
    protected $_updateRepository;


    /** @var $_log \Eguana\InventoryManagement\Api\Data\UpdateInterface   */
    protected $_updateFactory;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;


    /**
     * Status constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param DirectoryList $directoryList
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        DirectoryList $directoryList,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Eguana\InventoryManagement\Helper\Data $commanHelper,
        \Magento\Framework\Filesystem\Driver\File $file,
        xmlParser $xmlParser,
         \Eguana\InventoryManagement\Logger\Logger $logger,
        \Eguana\InventoryManagement\Model\UpdateRepository $updateRepository,
        \Eguana\InventoryManagement\Api\Data\UpdateInterfaceFactory $updateFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory
    )
    {
        parent::__construct($context);
        $this->directoryList = $directoryList;
        $this->_storeManager = $storeManager;
        $this->_jsonHelper = $jsonHelper;
        $this->_commanHelper = $commanHelper;
        $this->_file = $file;
        $this->_xmlParser = $xmlParser;
        $this->parseError = '';
        $this->_parserLogger = $logger;
        $this->_updateRepository = $updateRepository;
        $this->_updateFactory = $updateFactory;
        $this->_productFactory = $productFactory;
    }

    public function parseProducts()
    {
        if(!$this->_commanHelper->getInventoryManagementGeneralConfig('enabled'))
        {
            return 'Product Update is disabled';
        }

        if(!$this->_commanHelper->getInventoryManagementGeneralConfig('zippath'))
        {
            return 'Please set the zip path';
        }

        $newUpdates = $this->getNewUpdates();

        if(sizeof($newUpdates) == 0)
        {
            return 'There are no new updates';
        }

        try {


            foreach ($newUpdates as $newUpdate) {
                $inventoryUpdatesDir = $this->getFilePath();
                $newUpdate = str_replace($inventoryUpdatesDir . '/', '', $newUpdate);
                if ($this->copyFile($newUpdate)) {

                    $this->_parserLogger->InventoryLog('Parsing File '.$newUpdate);
                    if ($this->parseFile($newUpdate)) {
                        $this->_parserLogger->InventoryLog('Deleting File '.$newUpdate);
                        $this->_file->deleteFile($this->getFilePath() . '/' . $newUpdate);
                    }else{
                        $this->_parserLogger->InventoryLog($this->parseError);
                        return $this->parseError;
                    }
                }
            }
            $this->_parserLogger->InventoryLog('Finished');
            return $this->parseError?'Successful But got the following errors <br/>'.$this->parseError:'Done';
        }catch (\Exception $e){
            $this->_parserLogger->InventoryLog($e->getMessage());
            return $e->getMessage();
        }
    }

    private function copyFile($update)
    {
        $inventoryUpdatesDir = $this->getFilePath();
        return $this->_file->copy($inventoryUpdatesDir.'/'.$update, $this->getExecutedFilePath().'/'.$update);
    }

    private function parseFile($update)
    {
        $result = true;
        if(!$this->unzipFile($update))
        {
            $this->parseError = 'Unable to unzip file';
            return false;
        }
        $unZipPath = $this->getExecutedFilePath().'/'.str_replace('.zip', '', $update);
        $attributesXML = $unZipPath.'/'.'product';
        $specsXML = $unZipPath.'/'.'spec';

        $attributesXMLFiles = $this->_file->readDirectoryRecursively($attributesXML);

        foreach ($attributesXMLFiles as $xmlFile)
        {
            try{
                $parsedArray = $this->_xmlParser->load($xmlFile)->xmlToArray();
                $lgAttributes = $parsedArray['PRODUCTCONTAINER']['Product'];
                $salesModel = $lgAttributes['SalesModels']['SalesModel'];
                if(isset($salesModel['SalesModelCode'])){
                    $sku = $lgAttributes['LocaleCode'] . '.' . $salesModel['SalesModelCode'];
                }elseif(is_array($salesModel)){//To fix the issue of multiple sales model code in the xml
                        foreach ($salesModel as $salesModelCode)
                        {
                            $tempSku = $lgAttributes['LocaleCode'] . '.' . $salesModelCode['SalesModelCode'];
                            $tempProduct = $this->_productFactory->create();
                            $tempSkuId = $tempProduct->getIdBySku($tempSku);
                            if($tempSkuId)
                            {
                                $sku = $tempSku;
                                break;
                            }
                        }
                }
                if(isset($sku)) {
                    /**
                     * @var $relatedProduct \Magento\Catalog\Model\Product
                     */
                    $relatedProduct = $this->_productFactory->create();
                    $skuId = $relatedProduct->getIdBySku($sku);
                    if($skuId) {
                        $varPath = $this->directoryList->getPath('var');
                        $updateRecord = $this->_updateFactory->create();
                        $specFile = $specsXML . '/' . $lgAttributes['ProductCode'] . '.xml';
                        if (file_exists($specFile)) {
                            $specFile = str_replace($varPath, '', $specFile);
                            $updateRecord->setSpecsXml($specFile);
                        }
                        $xmlFile = str_replace($varPath, '', $xmlFile);
                        $store = $this->_storeManager->getStore(strtolower($lgAttributes['LocaleCode']));
                        $updateRecord->setAttributesXml($xmlFile);
                        $updateRecord->setZipFileName($update);
                        $updateRecord->setProductSku($sku);
                        $updateRecord->setStoreId($store->getId());
                        $this->_updateRepository->save($updateRecord);
                    }
                    unset($sku);
                }
            }catch (\Exception $e){
                $this->_parserLogger->InventoryLog('For File: '.$xmlFile);
                $this->_parserLogger->InventoryLog('For File: '.$e->getMessage());
                $this->parseError .= 'For file: '.$xmlFile.'<br/>';
                $this->parseError .= 'Error: '.$e->getMessage().'<br/>';
            }
        }
        return $result;
    }

    private function unzipFile($update)
    {
        $filePath = $this->getExecutedFilePath().'/'.$update;
        $result = FALSE;
        $zip = new \ZipArchive();
        $res = $zip->open($filePath);
        if($res === TRUE)
        {
            $zip->extractTo($this->getExecutedFilePath().'/'.str_replace('.zip', '', $update));
            $zip->close();
            $result = TRUE;
        }
        return $result;
    }

    private function getNewUpdates()
    {
        $inventoryUpdatesDir = $this->getFilePath();
        $newUpdates = $this->_file->readDirectoryRecursively($inventoryUpdatesDir);//scandir($inventoryUpdatesDir);
        $newZipUpdates = array();
        foreach ($newUpdates as $newUpdate)
        {
            if($this->_file->isFile($newUpdate) && pathinfo($newUpdate, PATHINFO_EXTENSION) == 'zip')
            $newZipUpdates[] = $newUpdate;
        }
        //$filePath = $inventoryStatusDir.'/'.str_replace(" ", "-", $this->date->gmtDate()).'.xml';
        return $newZipUpdates;
    }

    private function getFilePath()
    {
        $varDir = $this->directoryList->getPath('var');
        $inventoryUpdatesDir = $varDir.'/'.$this->_commanHelper->getInventoryManagementGeneralConfig('zippath');

        if (!file_exists($inventoryUpdatesDir)) {
            mkdir($inventoryUpdatesDir, 0777, true);
        }
        return $inventoryUpdatesDir;
    }

    private function getExecutedFilePath()
    {
        $varDir = $this->directoryList->getPath('var');
        $inventoryUpdatesDir = $varDir.'/Inventory/executed';

        if (!file_exists($inventoryUpdatesDir)) {
            mkdir($inventoryUpdatesDir, 0777, true);
        }
        return $inventoryUpdatesDir;
    }

    private function getProductSpecs($path)
    {
        $specs = array();
        $specXML = simplexml_load_file($path);
        foreach($specXML->group as $group)
        {
            $specs[(string)$group['name']] = array();
            foreach ($group->item as $item)
            {
                $specs[(string)$group['name']][(string)$item['name']] = (string)$item;
            }
        }
        return sizeof($specs)?$this->_jsonHelper->jsonEncode($specs):'';
    }
}