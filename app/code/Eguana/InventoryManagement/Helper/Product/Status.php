<?php
namespace Eguana\InventoryManagement\Helper\Product;

/**
 * Created by PhpStorm.
 * User: abbas
 * Date: 17. 7. 14
 * Time: 오후 4:02
 */

use Magento\Framework\App\Helper\AbstractHelper;
use Vertex\Tax\Model\DomDocumentFactory;
use Magento\Framework\Controller\Result\JsonFactory;

class Status extends AbstractHelper
{

    /** @var $productCollection \Magento\Catalog\Model\ResourceModel\Product\Collection  */
    protected $productCollection;

    /**
     * @var \Eguana\InventoryManagement\Model\Config
     */
    private $config;

    /**
     * @var \Magento\Framework\App\Filesystem\DirectoryList
     */
    private $directoryList;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    private $date;

    /**
     * @var \Vertex\Tax\Model\DomDocument
     */
    private $domDocumentFactory;

    /**
     * @var \Magento\CatalogInventory\Model\Stock\StockItemRepository
     */
    protected $stockItemRepository;


    protected $resultJsonFactory;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var \Eguana\InventoryManagement\Api\Data\StatusDataInterfaceFactory
     */
    private $statusDataFactory;

    protected $_storeManager;

    /**
     * @var \Magento\CatalogInventory\Api\StockRegistryInterface
     */
    protected $_stockRegistry;

    /**
     * Issue constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory  $productCollectionFactory
     * @param \Eguana\InventoryManagement\Model\Config $config
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Eguana\InventoryManagement\Model\Config $config,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Magento\CatalogInventory\Model\Stock\StockItemRepository $stockItemRepository,
        \Vertex\Tax\Model\DomDocumentFactory $domDocumentFactory,
        \Eguana\InventoryManagement\Api\Data\StatusDataInterfaceFactory $statusDataFactory,
        JsonFactory $resultJsonFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
    )
    {
        parent::__construct($context);
        $this->productCollection   = $productCollectionFactory->create();
        $this->config = $config;
        $this->directoryList = $directoryList;
        $this->date = $date;
        $this->stockItemRepository = $stockItemRepository;
        $this->domDocumentFactory = $domDocumentFactory->create();
        $this->resultJsonFactory = $resultJsonFactory;
        $this->logger = $context->getLogger();
        $this->statusDataFactory = $statusDataFactory;
        $this->_storeManager = $storeManager;
        $this->_stockRegistry = $stockRegistry;
    }

    public function getStatus($storeCode, $fromDate = null){
        try {
            $result = array();
            $collection = $this->productCollection
                                ->addAttributeToFilter('type_id', array('eq' => 'simple'))
                                ->addStoreFilter($storeCode)
                                ->addAttributeToSelect('*');
            if($fromDate)
            {
                $collection->addFieldToFilter('updated_at', array('from'=>$fromDate));
            }
            $collection = $collection->load();

            $store = $this->_storeManager->getStore($storeCode);
            //$i = 0;
            /**
             * @var \Magento\Catalog\Model\Product $product
             */
            foreach ($collection as $product){
                /**
                 * @var $statusData \Eguana\InventoryManagement\Api\Data\StatusDataInterface
                 */
                $statusData = $this->statusDataFactory->create();

                $productStock = $this->_stockRegistry->getStockItem($product->getId());//$this->stockItemRepository->get($product->getId());

                $statusData->setCurrencyCode($store->getCurrentCurrency()->getCode());
                $statusData->setSku($product->getSku());
                $statusData->setModelId($product->getModelId());
                $statusData->setStock($productStock->getIsInStock());
                $statusData->setSalesAvailability( $product->isSaleable());
                $statusData->setSalesModel($product->getSalesModelCode());
                $statusData->setSalesSuffix($product->getSalesSuffixCode());
                $statusData->setPrice($product->getPrice());

                $result[] = $statusData;
                //$this->logger->info(100, (array)$statusData);

                //$this->logger->info(100, (array)$statusData);
                /*$result[$i]['sku'] = $product->getSku();
                $result[$i]['model_id'] = $product->getModelId();
                $result[$i]['stock'] = $productStock->getIsInStock()?'True':'False';
                $result[$i]['sales_availability'] = $product->isSaleable();
                $result[$i]['sales_model'] = $product->getSalesModelCode();
                $result[$i]['sales_suffix'] = $product->getSalesSuffixCode();
                $result[$i]['price'] = $product->getPrice();
                $i++;*/
            }
        }
        catch (\Exception $e){
            $success = false;
            $result = $e->getMessage();
            $this->logger->info($e->getMessage());
        }

        /** @var \Magento\Framework\Controller\Result\Json $result */
       // $response = $this->resultJsonFactory->create();

        return $result;//$response->setData(['success' => $success, 'result' => $result]);

    }

    public function getStatusFile(){
        try {

            $filePath = $this->getFilePath();

            $collection = $this->productCollection
                                ->addAttributeToSelect('*')
                                ->load();

            $xml_products = $this->domDocumentFactory->createElement("products");

            foreach ($collection as $product){
                $productStock = $this->stockItemRepository->get($product->getId());

                $xml_product = $this->domDocumentFactory->createElement("product");

                $xml_sku = $this->domDocumentFactory->createElement("sku");
                $xml_sku->nodeValue = $product->getSku();

                $xml_modelId = $this->domDocumentFactory->createElement("model_id");
                $xml_modelId->nodeValue = $product->getModelId();

                $xml_stock = $this->domDocumentFactory->createElement("stock");
                $xml_stock->nodeValue = $productStock->getIsInStock()?'True':'False';

                $xml_salesAvailability = $this->domDocumentFactory->createElement("sales_availability");
                $xml_salesAvailability->nodeValue = $product->getStatus();

                $xml_salesModel = $this->domDocumentFactory->createElement("sales_model");
                $xml_salesModel->nodeValue = $product->getSalesModelCode();

                $xml_salesSuffix = $this->domDocumentFactory->createElement("sales_suffix");
                $xml_salesSuffix->nodeValue = $product->getSalesSuffixCode();

                $xml_price = $this->domDocumentFactory->createElement("price");
                $xml_price->nodeValue = $product->getPrice();

                $xml_product->appendChild( $xml_sku );
                $xml_product->appendChild( $xml_modelId );
                $xml_product->appendChild( $xml_stock );
                $xml_product->appendChild( $xml_salesAvailability );
                $xml_product->appendChild( $xml_salesModel );
                $xml_product->appendChild( $xml_salesSuffix );
                $xml_product->appendChild( $xml_price );

                $xml_products->appendChild( $xml_product );
            }



            $this->domDocumentFactory->appendChild( $xml_products );

            $this->domDocumentFactory->save($filePath);

           // $xmlfile = fopen($filePath, "w");

            //fwrite($xmlfile, $productsXML);
            //fclose($xmlfile);

            $message = __('Executed successfully');
        }
        catch (\Exception $e){
            $message = $e->getMessage();
        }

        return $message;
    }

    private function getFilePath()
    {
        $varDir = $this->directoryList->getPath('var');
        $inventoryStatusDir = $varDir.'/Inventory/Status';
        if (!file_exists($inventoryStatusDir)) {
            mkdir($inventoryStatusDir, 0777, true);
        }

        $filePath = $inventoryStatusDir.'/'.str_replace(" ", "-", $this->date->gmtDate()).'.xml';

        return $filePath;
    }
}