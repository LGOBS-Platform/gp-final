<?php
/**
 * Created by PhpStorm.
 * User: abbas
 * Date: 10/25/18
 * Time: 11:30 AM
 */

namespace Eguana\InventoryManagement\Helper\Product\Attributes;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;

class Map extends AbstractHelper
{
    /**
     * @var \Magento\Eav\Api\AttributeRepositoryInterface
     */
    protected $attributeRepository;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    private $customMap = array(
                            'e_a_n_code' => 'ean_code',
                            'product_name' => 'model_name',
                            'product_code' => 'model_id',
                            'product_url' => 'url',
                            'user_friendly_product_name' => 'user_friendly_name',
                            'product_description' => 'model_desc',
                            'super_category_code' => 'super_category_id',
                            'category_code' => 'category_id',
                            'sub_category_code' => 'sub_category_id1',
                            'sub_category_name' => 'sub_category_name',
                            'status' => 'model_status_code',
                            );

    private $attributes = array();
    private $_selectAttributesOptions = array();

    public function __construct(Context $context,
                                \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
                                \Magento\Eav\Api\AttributeRepositoryInterface $attributeRepository
                                )
    {
        parent::__construct($context);
        $this->attributeRepository = $attributeRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;

       // $searchCriteria = $this->searchCriteriaBuilder->addFilter('is_user_defined', 1)->create();
        $searchCriteria = $this->searchCriteriaBuilder->create();
        $attributeRepository = $this->attributeRepository->getList(
            \Magento\Catalog\Api\Data\ProductAttributeInterface::ENTITY_TYPE_CODE,
            $searchCriteria
        );
        try {
            /**
             * @var $item \Magento\Catalog\Model\ResourceModel\Eav\Attribute
             */
            foreach ($attributeRepository->getItems() as $item) {
                if ($item->getFrontendInput() == 'select' || $item->getFrontendInput() == 'multiselect') {
                    $options = $item->getSource()->getAllOptions();
                    foreach ($options as $option) {
                        if($option['value'] != '' && gettype($option['label']) == 'string') {
                            $this->_selectAttributesOptions[$item->getAttributeCode()][$option['label']] = $option['value'];
                        }
                    }
                }
                $this->attributes[$item->getAttributeCode()] = $item->getAttributeCode();

            }
        }catch (\Exception $e)
        {
            $error = $e->getMessage();
        }
    }


    public function getMappedCode($code)
    {
        $attributeCode = isset($this->customMap[$code])?$this->customMap[$code]:false;
        if(!$attributeCode){
            $attributeCode = isset($this->attributes[$code])?$this->attributes[$code]:false;
        }
        return $attributeCode;
    }

    public function getSelectValue($attributeCode, $label)
    {
        if(isset($this->_selectAttributesOptions[$attributeCode]) && isset($this->_selectAttributesOptions[$attributeCode][$label]))
        {
            return $this->_selectAttributesOptions[$attributeCode][$label];
        }
        else
            return $label;
    }
}