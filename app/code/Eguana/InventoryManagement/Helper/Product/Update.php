<?php
namespace Eguana\InventoryManagement\Helper\Product;

/**
 * Created by PhpStorm.
 * User: abbas
 * Date: 17. 7. 14
 * Time: 오후 4:02
 */

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\Xml\Parser as xmlParser;

class Update extends AbstractHelper
{

    /**
     * @var DirectoryList
     */
    private $directoryList;

    protected $_storeManager;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $productFactory;


    /**
     * @var \Eguana\InventoryManagement\Helper\Product\Attributes\Map
     */
    protected $_attributesMap;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $_jsonHelper;



    /**
     * @var xmlParser
     */
    private $_xmlParser;

    private $productRepository;
    private $imageHelper;

    private $parseError;

    /**
     * @var \Eguana\InventoryManagement\Logger\Logger
     */
    protected $_parserLogger;

    /**
     * @var \Magento\Catalog\Api\Data\Product
     */
    protected $_productData;

    /**
     * Status constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param DirectoryList $directoryList
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        DirectoryList $directoryList,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Eguana\InventoryManagement\Helper\Product\Attributes\Map $attributesMap,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        xmlParser $xmlParser,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Eguana\InventoryManagement\Helper\Product\Image $imageHelper,
         \Eguana\InventoryManagement\Logger\Logger $logger,
        \Magento\Catalog\Api\Data\ProductInterface $productData
    )
    {
        parent::__construct($context);
        $this->directoryList = $directoryList;
        $this->_storeManager = $storeManager;
        $this->productFactory = $productFactory;
        $this->_attributesMap = $attributesMap;
        $this->_jsonHelper = $jsonHelper;
        $this->_xmlParser = $xmlParser;
        $this->productRepository = $productRepository;
        $this->imageHelper = $imageHelper;
        $this->parseError = '';
        $this->_parserLogger = $logger;
    }

    public function parseFile($attributesXmlFile, $specsXMLFile)
    {
        $result = array('status'=>true);
        $varDir = $this->directoryList->getPath('var');
        $xmlFileFullPath = $varDir.$attributesXmlFile;
        if(isset($specsXMLFile)){
            $specsXMLFile = $varDir.$specsXMLFile;
        }
        if(!file_exists($xmlFileFullPath))
        {
            $result['status'] = false;
            $result['error_message'] = 'XML File '.$xmlFileFullPath.' does not exist';
            return $result;
        }
        $parsedArray = $this->_xmlParser->load($xmlFileFullPath)->xmlToArray();
        $updatedAttributes = $parsedArray['PRODUCTCONTAINER']['Product'];
        $salesModel = $updatedAttributes['SalesModels']['SalesModel'];
        if(isset($updatedAttributes['SalesModels']['SalesModel']['SalesModelCode'])){
            $sku = $updatedAttributes['LocaleCode'] . '.' . $updatedAttributes['SalesModels']['SalesModel']['SalesModelCode'];
        }elseif(is_array($salesModel)){//To fix the issue of multiple sales model code in the xml
            foreach ($salesModel as $salesModelCode)
            {
                $tempSku = $updatedAttributes['LocaleCode'] . '.' . $salesModelCode['SalesModelCode'];
                $tempProduct = $this->productFactory->create();
                $tempSkuId = $tempProduct->getIdBySku($tempSku);
                if($tempSkuId)
                {
                    $sku = $tempSku;
                    break;
                }
            }
        }
        $this->_parserLogger->InventoryLog('Parsing File '.$attributesXmlFile);
        if(isset($sku))
        {
            $gallery = $parsedArray['PRODUCTCONTAINER']['Gallery'];
            /**
             * @var $relatedProduct \Magento\Catalog\Model\Product
             */
            $relatedProduct = $this->productFactory->create();
            $skuId = $relatedProduct->getIdBySku($sku);

            if($skuId) {
                $this->_parserLogger->InventoryLog('Updating Product '.$skuId);
                $currentProduct = $this->productRepository->getById($skuId);
                foreach ($updatedAttributes as $key => $value) {
                    if($key == 'EnergyLabel')
                    {
                        $currentProduct->setData('doc_id',$value['DocId']);
                        $currentProduct->setData('original_name_b1_a1',$value['OriginalName']);
                        $currentProduct->setData('file_name', $value['FileName']);
                        $currentProduct->setData('tc', $value['TC']);
                        $currentProduct->setData('gsri_doc', $value['GsriDoc']);
                    }elseif($key == 'ProductFiche')
                    {
                        $currentProduct->setData('doc_id_2', $value['DocId']);
                        $currentProduct->setData('original_name_b1_a1_2', $value['OriginalName']);
                        $currentProduct->setData('file_name_2', $value['FileName']);
                        $currentProduct->setData('tc_2', $value['TC']);
                        $currentProduct->setData('gsri_doc_2', $value['GsriDoc']);
                    }
                    $updatedKey = strtolower(preg_replace('/\B([A-Z])/', '_$1', $key));
                    try {
                        if($value != '' && $mappedKey = $this->_attributesMap->getMappedCode($updatedKey)) {
                            $currentProduct->setData($mappedKey,$this->_attributesMap->getSelectValue($mappedKey, $value));
                        }
                    }catch (\Exception $e){
                        $this->parseError = $sku.': '.$e->getMessage();
                        $result['status'] = false;
                        $result['error_message'] = $sku.': '.$e->getMessage();
                        return $result;
                    }
                }

                try {
                    if(isset($specsXMLFile) && file_exists($specsXMLFile))
                    {
                        $currentProduct->setData('specifications', $this->getProductSpecs($specsXMLFile));
                    }
                    $store = $this->_storeManager->getStore(strtolower($currentProduct->getData('locale_code')));
                    $this->_storeManager->setCurrentStore(0);
                    $this->imageHelper->deleteExistingMediaEntries($currentProduct);//Clearing the all store Images else it become duplicated in next update
                    $this->_storeManager->setCurrentStore($store->getId());
                    $currentProduct->setName($currentProduct->getData('model_name'));
                    $this->productRepository->save($currentProduct);
                    //$this->imageHelper->deleteExistingMediaEntries($currentProduct);
                    $this->imageHelper->updateImages($currentProduct, $gallery);
                    $imagesArray =  $this->imageHelper->getGalleryStatuses($currentProduct);

                    //Setting the images statuses for all store view
                   $this->_storeManager->setCurrentStore(0);
                   $currentProduct->setMediaGalleryEntries($imagesArray);
                   $this->productRepository->save($currentProduct);
                   /*foreach ($imagesArray as $imageArray)
                   {
                       $imagePath  = $imageArray['path'];
                       $currentProduct->addImageToMediaGallery( $imagePath, $imageArray['types'], false, false );
                   }*/

                    //$this->imageHelper->assignDefaultImages($currentProduct, $imagesStatuses);
                }catch (\Exception $e){
                    $this->parseError =  $sku.': '.$e->getMessage();
                    $result['status'] = false;
                    $result['error_message'] = $sku.': '.$e->getMessage();
                }
            }else{
                $result['status'] = false;
                $result['error_message'] = 'SKU '.$sku.' does not exist anymore in Magento';
                return $result;
            }
        }else{
            $result['status'] = false;
            $result['error_message'] = 'SKU is not set in the XML file';
            return $result;
        }

        return $result;
    }



    private function getProductSpecs($path)
    {
        $specs = array();
        $specXML = simplexml_load_file($path);
        foreach($specXML->group as $group)
        {
            $specs[(string)$group['name']] = array();
            foreach ($group->item as $item)
            {
                $specs[(string)$group['name']][(string)$item['name']] = (string)$item;
            }
        }
        return sizeof($specs)?$this->_jsonHelper->jsonEncode($specs):'';
    }
}