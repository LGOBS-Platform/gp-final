<?php
namespace Eguana\InventoryManagement\Helper\Product;

/**
 * Created by PhpStorm.
 * User: abbas
 * Date: 17. 7. 14
 * Time: 오후 4:02
 */

use Magento\Framework\App\Helper\AbstractHelper;

class Inventory extends AbstractHelper
{

    /** @var $productCollection \Magento\Catalog\Model\ResourceModel\Product\Collection  */
    protected $productCollection;



    protected $_storeManager;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\Action
     */
    private $productAction;

    /**
     * @var \Magento\CatalogInventory\Api\StockRegistryInterface
     */
    protected $_stockRegistry;


    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * Status constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
     * @param \Magento\Catalog\Model\ResourceModel\Product\Action $productAction
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\Catalog\Model\ResourceModel\Product\Action $productAction,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
    )
    {
        parent::__construct($context);
        $this->productCollection   = $productCollectionFactory->create();
        $this->_storeManager = $storeManager;
        $this->_stockRegistry = $stockRegistry;
        $this->productAction = $productAction;
        $this->productRepository = $productRepository;
    }

    public function getInventory($storeCode){
        try {
            $transferredProducts = array();
            $result = array();
            $this->_storeManager->setCurrentStore($storeCode);
            $store = $this->_storeManager->getStore($storeCode);
            $collection = $this->productCollection
                ->addAttributeToFilter('type_id', array('eq' => 'simple'))
                ->addAttributeToFilter(
                    array(
                        array('attribute' => 'transferred', 'eq' => 0),
                        array('attribute' => 'transferred', 'null' => true)
                    )
                )
                ->addWebsiteFilter()
                ->addAttributeToSelect('*');

            $collection = $collection->load();


            $vat = $this->scopeConfig->getValue('inventorymanagement/availability/vat', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeCode);
            /**
             * @var \Magento\Catalog\Model\Product $product
             */
            foreach ($collection as $product){
                if($product->getModelId() != null) {
                    $statusData = array();
                    $productStock = $this->_stockRegistry->getStockItem($product->getId());
                    $storeProduct = $this->productRepository->getById($product->getId(),false,$store->getId());
                    if(!$storeProduct->getData('transferred')) {
                        $statusData['currencyCode'] = $store->getCurrentCurrency()->getCode();
                        $statusData['sku'] = $product->getSku();
                        $statusData['modelId'] = $product->getModelId();
                        $statusData['stock'] = $productStock->getIsInStock();
                        $statusData['salesAvailability'] = false;
                        if ($productStock->getQty() > 0 && $storeProduct->getStatus() == 1) {
                            $statusData['salesAvailability'] = true;
                        }
                        $statusData['salesModel'] = $product->getSalesModelCode() ? $product->getSalesModelCode() : $product->getSku();
                        $statusData['salesSuffix'] = $product->getSalesSuffixCode() ? $product->getSalesSuffixCode() : $product->getSku();
                        $statusData['price'] = ($storeProduct->getSpecialPrice() > 0) ? $storeProduct->getSpecialPrice() : $storeProduct->getPrice();
                        if ($vat) {
                            $statusData['price'] = $statusData['price'] * $vat;
                        }
                        $statusData['price'] = round($statusData['price'], 2);
                        settype($statusData['price'], 'string');
                        $result[] = $statusData;
                    }
                    $transferredProducts[] = $product->getId();
                    // $product->setData('transferred', 1);
                }
            }
            if(sizeof($transferredProducts)) {
                $this->productAction->updateAttributes($transferredProducts, array('transferred' => 1), $store->getId());
               /* $indexer = $this->indexerFactory->create();
                $indexer->load('catalog_product_flat');
                $indexer->reindexAll();*/
            }
        }
        catch (\Exception $e){
            if(isset($product)) {
                $result = $e->getMessage() . ' == ' . $product->getSku();
            }else{
                $result = $e->getMessage();
            }
        }


        return $result;

    }

}