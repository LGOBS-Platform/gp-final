<?php
/**
 * Created by PhpStorm.
 * User: abbas
 * Date: 11/6/18
 * Time: 5:39 PM
 */

namespace Eguana\InventoryManagement\Helper\Product;

use Magento\Framework\App\Helper\AbstractHelper;

class Image extends AbstractHelper
{
    /**
     * @var \Magento\Framework\Filesystem\Driver\File
     */
    protected $_file;


    private $_mediaEntryFactory;
    private $_mediaEntry;
    private $_contentFactory;

    /**
     * @var \Magento\Catalog\Model\Product\Gallery\GalleryManagement
     */
    private $_galleryManagement;

    /**
     * @var \Eguana\InventoryManagement\Logger\Logger
     */
    protected $_parserLogger;

    /**
     * @var \Magento\Framework\Math\Random
     */
    protected $_math;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Catalog\Api\ProductAttributeMediaGalleryManagementInterface $galleryManagement,
        \Magento\Catalog\Model\Product\Gallery\EntryFactory $entryFactory,
        \Magento\Catalog\Model\Product\Gallery\Entry $entry,
        \Magento\Framework\Api\ImageContentFactory $contentFactory,
        \Magento\Framework\Filesystem\Driver\File $file,
        \Eguana\InventoryManagement\Logger\Logger $logger,
        \Magento\Framework\Math\Random $math
    )
    {
        $this->_galleryManagement = $galleryManagement;
        $this->_parserLogger = $logger;
        $this->_mediaEntryFactory = $entryFactory;
        $this->_mediaEntry = $entry;
        $this->_contentFactory = $contentFactory;
        $this->_file = $file;
        $this->_math = $math;
        parent::__construct($context);
    }

    /**
     * @param $product
     * @param $imagesData
     * @param $urls
     * @return mixed
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\StateException
     */
    public function updateImages($product,$images){
        /** 1. Download new images and save them to temp folder @see TEMP_IMAGES_PATH */
        $this->_parserLogger->InventoryLog('Downloading Images');
        $imagesData = $this->downloadImages($images);

        if (empty($imagesData)) {
            return $product;
        }
        /** 2. Delete existing media Entries */
        $this->_parserLogger->InventoryLog('Delete Existing Product Images');
        $product = $this->deleteExistingMediaEntries($product);

        //return $product;
        /** 3. Update with new images */
        $this->_parserLogger->InventoryLog('Update Product Images');
        $product = $this->updateProductMediaEntry($product, $imagesData);

        return $product;
    }

    protected function downloadImages($gallery)
    {
        $result = array();
        $plpImage = '';
        $pdpImages = array();

        if(isset($gallery['BasicImages']['Small'])) {
            $plpImage = $gallery['BasicImages']['Small'];
            $plpImage = str_replace(" ", "%20", $plpImage);
        }

        if(isset($gallery['BasicImages']['Large'])) {
            $imagePath = $gallery['BasicImages']['Large'];
            $imagePath = str_replace(" ", "%20", $imagePath);
            $result['images'][] = 'https://www.lg.com' . $imagePath;
            $pdpImages[] = $imagePath;
        }

        if(isset($gallery['GalleryImages'])) {
            if(isset($gallery['GalleryImages']['Small']) || isset($gallery['GalleryImages']['Large']))
            {
                $imagePath = $gallery['GalleryImages']['Large'] ? $gallery['GalleryImages']['Large'] : $gallery['GalleryImages']['Small'];
                $imagePath = str_replace(" ", "%20", $imagePath);
                $result['images'][] = 'https://www.lg.com' . $imagePath;//$this->downloadImage('https://www.lg.com'.$imagePath);
                $pdpImages[] = $imagePath;
            }else {
                foreach ($gallery['GalleryImages'] as $galleryImage) {
                    $imagePath = $galleryImage['Large'] ? $galleryImage['Large'] : $galleryImage['Small'];
                    $imagePath = str_replace(" ", "%20", $imagePath);
                    $result['images'][] = 'https://www.lg.com' . $imagePath;//$this->downloadImage('https://www.lg.com'.$imagePath);
                    $pdpImages[] = $imagePath;
                }
            }

            if ($plpImage == '' && sizeof($pdpImages)) {
                $plpImage = $pdpImages[0];
            }
        }

        if($plpImage != '') {

            $result['smallImage'] = 'https://www.lg.com' . $plpImage;//$this->downloadImage('https://www.lg.com' . $plpImage);
        }

        return $result;
    }

    public function deleteExistingMediaEntries($product){
        $existingGallery = $this->_galleryManagement->getList($product->getSku());
        foreach ($existingGallery as $existingImage) {
            $this->_galleryManagement->remove($product->getSku(), $existingImage->getId());
        }
        return $product;
    }

    /**
     * @param array $imgData
     * @return null
     */
    protected function createMediaEntry($sku, $imgData = []){

        if(empty($imgData)){
            return null;
        }

        $entry = $this->_mediaEntryFactory->create();
        $entry->setFile($imgData['path'])
            ->setMediaType($imgData['type'])
            ->setDisabled($imgData['disabled'])
            ->setTypes($imgData['types']);

        $imageContent = $this->_contentFactory->create();

        $imageInfo = getimagesize($imgData['path']);
        $imageName = $this->_math->getRandomString(10).'_'.str_replace('.', '_', $sku).'_'.basename($imgData['path']);
        $imageName = preg_replace("/[^A-Za-z0-9 .]/", '_', $imageName);
        $imageContent->setType($imageInfo['mime'])
            ->setName($imageName)
            ->setBase64EncodedData(base64_encode($this->_file->fileGetContents($imgData['path'])));
        $entry->setContent($imageContent);

        return $entry;

    }

    /**
     * @param $product
     * @param $imagesData
     * @return mixed
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\StateException
     */
    protected function updateProductMediaEntry($product,$imagesData){

        $mediaGalleryEntries = [];


        if(isset($imagesData['smallImage'])){
            if (@is_array(getimagesize( $imagesData['smallImage']))) {
                $mediaGalleryEntries[] = $this->createMediaEntry($product->getSku(), [
                    'path' => $imagesData['smallImage'],
                    'type'=>'image',
                    'disabled'=> isset($imagesData['images'])?true:false,
                    'types'=>[
                        'small_image', 'thumbnail'
                    ]
                ]);
            }
        }

        if(!empty($imagesData['images'])){
            $i = 0;
            foreach ($imagesData['images'] as $imageFileName) {
                $types = $i==0?['image']:null;
                if (@is_array(getimagesize($imageFileName))) {
                    $mediaGalleryEntries[] = $this->createMediaEntry($product->getSku(), [
                        'path' =>  $imageFileName,
                        'type'=>'image',
                        'disabled'=> false,
                        'types'=> $types,
                    ]);
                }
                $i++;
            }
        }

        foreach ($mediaGalleryEntries as $mediaGalleryEntry){
            $this->_galleryManagement->create($product->getSku(), $mediaGalleryEntry);
        }

        return $product;
    }

    /**
     * Return the images of product with statuses etc.
     *
     * @param $product
     * @return array
     */
    public function getGalleryStatuses($product)
    {
        $imagesArray = array();
        $images =  $this->_galleryManagement->getList($product->getSku());
        /**
         * @var $image \Magento\Catalog\Model\Product\Gallery\Entry
         */
        foreach ($images as $image) {
            $imagesArray[] = $image;
        }
        return $imagesArray;
    }

    public function assignDefaultImages($product, $statuses)
    {

        $images =  $this->_galleryManagement->getList($product->getSku());
        /**
         * @var $image \Magento\Catalog\Model\Product\Gallery\Entry
         */
        foreach ($images as $image) {
            $image->setTypes(array());
            $this->_galleryManagement->create($product->getSku(), $image);
        }

        /*foreach ($images as $image) {
            $image->setTypes($statuses[$image->getId()]);
            $this->_galleryManagement->update($product->getSku(), $image);
        }*/
    }
}