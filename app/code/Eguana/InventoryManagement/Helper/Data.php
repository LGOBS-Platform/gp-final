<?php

/**
 * Created by Eguana.
 * User: jason
 * Date: 2018-08-27
 * Time: 오전 9:59
 */

namespace Eguana\InventoryManagement\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{

    public function getInventoryManagementConfig($path){
        return $this->scopeConfig->getValue('inventorymanagement/'.$path);
    }

    public function getInventoryManagementGeneralConfig($path){
        return $this->getInventoryManagementConfig('general/'.$path);
    }

}