<?php

/**
 * Created by PhpStorm.
 * User: abbas
 * Date: 8/23/18
 * Time: 8:26 PM
 */

namespace Eguana\ProductTab\Block;

use Magento\Catalog\Api\ProductRepositoryInterface;

class Feature extends \Magento\Catalog\Block\Product\View
{
    private $_filterProvider;

    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\Url\EncoderInterface $urlEncoder,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Framework\Stdlib\StringUtils $string,
        \Magento\Catalog\Helper\Product $productHelper,
        \Magento\Catalog\Model\ProductTypes\ConfigInterface $productTypeConfig,
        \Magento\Framework\Locale\FormatInterface $localeFormat,
        \Magento\Customer\Model\Session $customerSession,
        ProductRepositoryInterface $productRepository,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        array $data = [],
        \Magento\Cms\Model\Template\FilterProvider $filterProvider
    )
    {
        parent::__construct($context, $urlEncoder, $jsonEncoder, $string, $productHelper, $productTypeConfig, $localeFormat, $customerSession, $productRepository, $priceCurrency, $data);
        $this->_filterProvider = $filterProvider;
    }

    public function getHtmlContent($content)
    {
        return $this->_filterProvider->getBlockFilter()->filter($content);
    }

}