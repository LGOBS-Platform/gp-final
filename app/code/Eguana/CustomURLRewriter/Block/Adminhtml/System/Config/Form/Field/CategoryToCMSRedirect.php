<?php
/**
 * Created by PhpStorm.
 * User: abbas
 * Date: 17. 10. 30
 * Time: 오후 3:17
 */

namespace Eguana\CustomURLRewriter\Block\Adminhtml\System\Config\Form\Field;


class CategoryToCMSRedirect extends \Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray
{
    /**
     * @var \Magento\Framework\Data\Form\Element\Factory
     */
    protected $_elementFactory;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Data\Form\Element\Factory $elementFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Data\Form\Element\Factory $elementFactory,
        array $data = []
    )
    {
        $this->_elementFactory  = $elementFactory;
        parent::__construct($context,$data);
    }

    protected function _construct()
    {
        $this->addColumn('from', ['label' => __('From (Category)')]);
        $this->addColumn('to', ['label' => __('To (CMS)')]);
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add');
        parent::_construct();
    }

}