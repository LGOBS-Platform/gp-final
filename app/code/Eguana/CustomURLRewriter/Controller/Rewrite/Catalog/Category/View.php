<?php
/**
 * Created by PhpStorm.
 * User: abbas
 * Date: 16. 10. 14
 * Time: 오후 4:56
 */

namespace Eguana\CustomURLRewriter\Controller\Rewrite\Catalog\Category;

use Eguana\CustomURLRewriter\Model\Config;
use Magento\Catalog\Model\Design;
use Magento\Catalog\Model\Layer\Resolver;
use Magento\Catalog\Model\Session;
use Magento\CatalogUrlRewrite\Model\CategoryUrlPathGenerator;
use Magento\Cms\Helper\Page;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\ForwardFactory;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Exception\NoSuchEntityException;

class View extends \Magento\Catalog\Controller\Category\View
{


    /**
     * @var  Rewrites
     */
    protected $_cmsPageHelper;
    /**
     * @var Config
     */
    private $config;


    /**
     * View constructor.
     * @param Context $context
     * @param Design $catalogDesign
     * @param Session $catalogSession
     * @param Registry $coreRegistry
     * @param StoreManagerInterface $storeManager
     * @param CategoryUrlPathGenerator $categoryUrlPathGenerator
     * @param PageFactory $resultPageFactory
     * @param ForwardFactory $resultForwardFactory
     * @param Resolver $layerResolver
     * @param CategoryRepositoryInterface $categoryRepository
     * @param Rewrites $urlRewrites
     */
    public function __construct(Context $context,
                                Design $catalogDesign,
                                Session $catalogSession,
                                Registry $coreRegistry,
                                StoreManagerInterface $storeManager,
                                CategoryUrlPathGenerator $categoryUrlPathGenerator,
                                PageFactory $resultPageFactory,
                                ForwardFactory $resultForwardFactory,
                                Resolver $layerResolver,
                                CategoryRepositoryInterface $categoryRepository,
                                Page $cmsPageHelper,
                                Config $config)
    {
        $this->_cmsPageHelper = $cmsPageHelper;
        parent::__construct($context, $catalogDesign, $catalogSession, $coreRegistry, $storeManager, $categoryUrlPathGenerator, $resultPageFactory, $resultForwardFactory, $layerResolver, $categoryRepository);
        $this->config = $config;
    }


    /**
     * It is a overridden function and it will call whenever customer will click the category
     *
     * @return $this|bool|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $categoryId = (int)$this->getRequest()->getParam('id', false);

        if (!$categoryId) {
            return false;
        }else {

            if($cmsPageID = $this->config->haveRedirectCMSPage($categoryId))
            {
                try {
                    /**
                     * @var \Magento\Catalog\Model\Category $category
                     */
                    return $this->resultRedirectFactory->create()->setUrl($this->_cmsPageHelper->getPageUrl($cmsPageID));

                } catch (NoSuchEntityException $e) {
                    return false;
                }
            }

            if($customURL = $this->config->haveRedirectCustomURL($categoryId))
            {
                try {
                    /**
                     * @var \Magento\Catalog\Model\Category $category
                     */
                    return $this->resultRedirectFactory->create()->setUrl($customURL);

                } catch (NoSuchEntityException $e) {
                    return false;
                }
            }
        }

        return parent::execute();
    }
}