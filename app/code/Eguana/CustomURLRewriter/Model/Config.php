<?php
/**
 * Created by PhpStorm.
 * User: abbas
 * Date: 17. 5. 25
 * Time: 오후 4:31
 */

namespace Eguana\CustomURLRewriter\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\Encryption\EncryptorInterface;

class Config extends \Magento\Framework\Model\AbstractModel
{

    /** @var $_scopeConfig ScopeConfigInterface   */
    protected $_scopeConfig;

    /** @var $_jsonUnserialize \Magento\Framework\Serialize\Serializer\Json   */
    protected $_jsonUnserialize;

    public function __construct(ScopeConfigInterface $scopeConfig,
                         \Magento\Framework\Serialize\Serializer\Json $jsonUnserialize
                            )
    {
        $this->_scopeConfig = $scopeConfig;
        $this->_jsonUnserialize = $jsonUnserialize;

    }

    /**
     * getting API login from main configuration
     * @return string
     */
    private function getCategoriesToCMSRedirect() {
        return $this->_scopeConfig->getValue('url_rewrite_section/main_conf/categorytocmsredirect', ScopeInterface::SCOPE_STORE);
    }

    /**
     * getting API login from main configuration
     * @return string
     */
    private function getCategoriesToCustomURLRedirect() {
        return $this->_scopeConfig->getValue('url_rewrite_section/main_conf/categorytocustomurlredirect', ScopeInterface::SCOPE_STORE);
    }


    /**
     * It will return if there is any redirect category for category to module url
     * @param $categoryId
     * @return string
     */
    public function haveRedirectCustomURL($categoryId)
    {
        $redirectCustomURL = '';
        if($this->getCategoriesToCustomURLRedirect()) {
            $redirection = $this->_jsonUnserialize->unserialize($this->getCategoriesToCustomURLRedirect());
            if ($redirection) {
                foreach ($redirection as $option) {
                    if ($option['from'] == $categoryId) {

                        $redirectCustomURL = $option['to'];
                        break;
                    }
                }
            }
        }
        return $redirectCustomURL;
    }

    /**
     * It will return if there is any redirect category
     * @param $categoryId
     * @return int
     */
    public function haveRedirectCMSPage($categoryId)
    {
        $redirectCMSPage = 0;
        if($this->getCategoriesToCMSRedirect()) {
            $redirection = $this->_jsonUnserialize->unserialize($this->getCategoriesToCMSRedirect());
            if ($redirection) {
                foreach ($redirection as $option) {
                    if ($option['from'] == $categoryId) {

                        $redirectCMSPage = $option['to'];
                        break;
                    }
                }
            }
        }
        return $redirectCMSPage;
    }

}