<?php
/**
 * Created by PhpStorm.
 * User: sonia
 * Date: 18. 9. 29
 * Time: 오전 10:32
 */
namespace Eguana\EnhancedExport\Plugin\Block\Widget\Grid;

class ExtendedPlugin
{


    public function afterGetMainButtonsHtml(\Magento\Backend\Block\Widget\Grid\Extended $subject, $result)
    {
        $result .= '<button id="" title="Select All" type="button" class="action-default scalable action-reset action-tertiary" onclick="export_filter_gridJsObject.selectAll()" data-action="grid-filter-reset" data-ui-id="widget-button-3"><span>Select All</span></button>';

        return $result;
    }
}