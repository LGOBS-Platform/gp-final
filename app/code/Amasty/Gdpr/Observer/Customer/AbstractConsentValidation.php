<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Gdpr
 */


namespace Amasty\Gdpr\Observer\Customer;

use Amasty\Gdpr\Model\Consent;
use http\Env\Request;
use Magento\Customer\Model\Session;
use Magento\Framework\App\RequestInterface;

abstract class AbstractConsentValidation
{
    /**
     * @var Consent
     */
    private $consent;

    /**
     * @var Session
     */
    private $session;

    /**
     * @var RequestInterface
     */
    private $request;

    public function __construct(
        Consent $consent,
        Session $session,
        RequestInterface $request
    ) {
        $this->consent = $consent;
        $this->session = $session;
        $this->request = $request;
    }

    protected function confirmConcent($param)
    {
        $customerId = $this->session->getCustomerId();

        if (!$this->request->getParam($param) || !$customerId) {
            return;
        }

        $this->consent->acceptLastVersion($customerId);
    }
}
