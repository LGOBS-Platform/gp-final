<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Gdpr
 */


namespace Amasty\Gdpr\Model;

use Amasty\Gdpr\Api\PolicyRepositoryInterface;
use Amasty\Gdpr\Api\WithConsentRepositoryInterface;
use Amasty\Gdpr\Model\ResourceModel\WithConsent as WithConsentResource;

class Consent
{
    /**
     * @var WithConsentRepositoryInterface
     */
    private $withConsentRepository;

    /**
     * @var WithConsentFactory
     */
    private $consentFactory;

    /**
     * @var PolicyRepositoryInterface
     */
    private $policyRepository;

    /**
     * @var ActionLogger
     */
    private $logger;

    /**
     * @var WithConsentResource
     */
    private $withConsent;

    public function __construct(
        WithConsentRepositoryInterface $withConsentRepository,
        WithConsentFactory $consentFactory,
        PolicyRepositoryInterface $policyRepository,
        ActionLogger $logger,
        WithConsentResource $withConsent
    ) {
        $this->withConsentRepository = $withConsentRepository;
        $this->consentFactory = $consentFactory;
        $this->policyRepository = $policyRepository;
        $this->logger = $logger;
        $this->withConsent = $withConsent;
    }

    public function acceptLastVersion($customerId)
    {
        if (!$customerId) {
            return;
        }

        if ($policy = $this->policyRepository->getCurrentPolicy()) {
            $consents = $this->withConsent->getConsentsByCustomerId($customerId);
            foreach ($consents as $consent) {
                if ($consent['policy_version'] === $policy->getPolicyVersion()) {
                    return;
                }
            }
            /** @var WithConsent $consent */
            $consent = $this->consentFactory->create();
            $consent->setPolicyVersion($policy->getPolicyVersion());
        }

        $consent->setCustomerId($customerId);

        $this->withConsentRepository->save($consent);

        $this->logger->logAction('consent_given', $customerId);
    }
}
