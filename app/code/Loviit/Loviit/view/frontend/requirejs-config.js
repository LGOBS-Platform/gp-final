var config = {
    map: {
        '*': {
            'Magento_Checkout/js/action/select-payment-method':
                'Loviit_Loviit/js/action/payment/select-payment-method',
            'Magento_Checkout/js/view/billing-address':
                'Loviit_Loviit/js/view/billing-address',
            'Magento_SalesRule/js/action/set-coupon-code':
                'Loviit_Loviit/js/action/payment/set-coupon-code',
            'Magento_SalesRule/js/action/cancel-coupon':
                'Loviit_Loviit/js/action/payment/cancel-coupon',
        }
    }
};