/**
 * Copyright � 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*browser:true*/
/*global define*/
define(
    [
        'Loviit_Loviit/js/view/payment/method-renderer/methods-abstract',
        'ko'
    ],
    function (
        Component,
        ko
    ) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Loviit_Loviit/checkout/directdebit',
                checkPolicy:  ko.observable(window.checkoutConfig.settings.privacy_policy ? false : true),
                checkConditions:  ko.observable(window.checkoutConfig.settings.general_conditions ? false : true)
            },
        });
    }
);
