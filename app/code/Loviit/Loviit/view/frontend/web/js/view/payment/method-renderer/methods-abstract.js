/**
 * Copyright  2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*browser:true*/
/*global define*/
define(
    [
        'jquery',
        'Magento_Checkout/js/view/payment/default',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/action/set-payment-information',
        'Magento_Customer/js/customer-data',
        'mage/url'
    ],
    function (
        $,
        Component,
        quote,
        setPaymentMethodAction,
        customerData,
        url
    ) {
        'use strict';
        
        return Component.extend({
            defaults: {  
            },
            
            /** Init observable variables */
            initObservable: function () {
                this._super()
                    .observe({
                        submitButtonEnabled: false,
                        taxid: window.customerData.taxvat,
                        birthday: window.customerData.dob,
                        gender: window.customerData.gender,
                        iban: "",
                        bic: "",
                        bankAccountOpeningDate:"",
                        showBirthday: false,
                        showTaxId: false,
                        showGender: false,
                        showBic: false,
                        showBankAccountOpeningDate:false
                    });
                return this;
            },
            
            initialize: function () {
                this._super();
                this.checkRequiredFields();
                quote.billingAddress.subscribe(function () {
                        this.checkRequiredFields();
                }, this);
            },

            
            getRequiredFields: function () {
                return window.checkoutConfig.settings.required_fields;
            },
            
            getGeneralConditions: function() {
                return window.checkoutConfig.settings.general_conditions;
            },

            getPrivacyPolicy: function() {
                return window.checkoutConfig.settings.privacy_policy;
            },
            
             /** Returns payment information data */
            getData: function () {
                var parent = this._super(),
                    additionalData = null;
                    additionalData = {};
                    additionalData['iban'] =  this.iban();
                    additionalData['bic'] = this.bic();
                    additionalData['bank_account_opening_date'] = this.bankAccountOpeningDate();
                    additionalData['taxid'] = this.taxid();
                    additionalData['gender'] = this.gender();
                    additionalData['birthday'] = this.birthday();
                    additionalData['general_conditions'] = this.checkConditions();
                    additionalData['privacy_policy'] = this.checkPolicy();

                return $.extend(true, parent, {
                    'additional_data': additionalData
                });
            },
            

            checkRequiredFields: function(){
                if(quote.billingAddress()==null)
                        return;
                var countryId = quote.billingAddress().countryId;
                var requiredFields = window.checkoutConfig.settings.required_fields[countryId];

                //check tax id
                if(countryId=='ES' || countryId=='IT' || (!this.isSecurePaymentMethod() && typeof requiredFields !== 'undefined' && requiredFields.taxid == 1)){
                    this.showTaxId(true);
                }
                else{
                    this.showTaxId(false);
                }

    
                if(typeof requiredFields !== 'undefined'){
                        this.showBirthday(requiredFields.birthday == 1 && (window.customerData.dob == "" || window.customerData.dob == null) ? true : false);
                        this.showGender(requiredFields.gender == 1 && (window.customerData.gender == "" || window.customerData.gender == null) ? true : false);
                        this.showBic(requiredFields.bic == 1 ? true : false);
                        this.showBankAccountOpeningDate(requiredFields.bank_account_opening_date == 1 ? true : false);
                }
                else{
                        this.showBirthday(false);
                        this.showGender(false);
                        this.showBic(false);
                        this.showBankAccountOpeningDate(false);
                }

                this.showBirthday.valueHasMutated();
                this.showTaxId.valueHasMutated();
                this.showGender.valueHasMutated();
                this.showBic.valueHasMutated();
                this.showBankAccountOpeningDate.valueHasMutated();

            },

			/*eguana error fix*/
			isSecurePaymentMethod: function(paymentCode){
				if(paymentCode==null){
					if(quote.paymentMethod()==null || quote.paymentMethod().method == null){
						return false;
					}
					paymentCode=quote.paymentMethod().method;
				}
				var re= /loviit(cc|dc|su|va|pp)/;
				return re.test(paymentCode);
			},

            continueToPayon: function () {
                var self = this;
                /*update payment method information if additional data was changed*/
                this.selectPaymentMethod();
                setPaymentMethodAction(this.messageContainer, self.getData()).done(
                    function () {
                        customerData.invalidate(['cart']);
                        $.mage.redirect(
                            url.build('loviit/payment/finish?t='+(new Date().getTime()))
                        );
                    }
                );
                return false;
            }
           
            
        });
    }
);