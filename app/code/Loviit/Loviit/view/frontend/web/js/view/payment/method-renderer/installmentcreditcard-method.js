/**
 * Copyright  2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*browser:true*/
/*global define*/
define(
    [
        'jquery',
        'Loviit_Loviit/js/view/payment/method-renderer/installment-abstract',
        'Magento_Checkout/js/model/payment/additional-validators',
        'Magento_Checkout/js/model/quote',
        'Magento_Customer/js/customer-data',
         'mage/url',
         'ko'
    ],
    function (
        $,
        Component,
        additionalValidators,
        quote,
        customerData,
        url,
        ko
    ) {
        'use strict';

        return Component.extend({
        	defaults: {
                rates: ko.observableArray([]),
                lastInstallmentUpdate: "",
                checkPolicy:  ko.observable(window.checkoutConfig.settings.privacy_policy ? false : true),
                checkConditions:  ko.observable(window.checkoutConfig.settings.general_conditions ? false : true),
                selectedRate: ko.observable(false),
                showInterestColumn: ko.observable(false),
                showAdminFeeColumn: ko.observable(false),
                showDiscountColumn: ko.observable(false),
                installmentTableIsLoading: ko.observable(false)
            },
            bankDetailsRequired:function(){
            	return false;
            },
            

            /** Returns payment information data */
            getData: function () {
                var parent = this._super(),
                    additionalData = {};

                additionalData['taxid'] = this.taxid();
                additionalData['gender'] = this.gender();
                additionalData['birthday'] = this.birthday();
                additionalData["plan_selected"]=this.selectedRate();
                additionalData['general_conditions'] = this.checkConditions();
                additionalData['privacy_policy'] = this.checkPolicy();
                
                return $.extend(true, parent, {
                    'additional_data': additionalData
                });
            },
            
           
        });
    }
);
