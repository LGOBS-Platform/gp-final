/**
 * Copyright � 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*browser:true*/
/*global define*/
define(
    [
        'jquery',
        'Loviit_Loviit/js/view/payment/method-renderer/installment-deposit-abstract',
        'Magento_Checkout/js/model/payment/additional-validators',
        'Magento_Checkout/js/model/quote',
        'Magento_Customer/js/customer-data',
         'mage/url',
         'ko'
    ],
    function (
        $,
        Component,
        additionalValidators,
        quote,
        customerData,
        url,
        ko
    ) {
        'use strict';

        return Component.extend({
        	defaults: {
                rates: ko.observableArray([]),
                lastInstallmentUpdate: "",
                disableContinue: ko.observable(true),
                deposit: ko.observable(0),
                checkPolicy:  ko.observable(window.checkoutConfig.settings.privacy_policy ? false : true),
                checkConditions:  ko.observable(window.checkoutConfig.settings.general_conditions ? false : true),
                selectedRate: ko.observable(false),
                showInterestColumn: ko.observable(false),
                showAdminFeeColumn: ko.observable(false),
                showDiscountColumn: ko.observable(false),
                forceLoading: ko.observable(true),
                installmentTableIsLoading: ko.observable(false)
            }
        });
    }
);
