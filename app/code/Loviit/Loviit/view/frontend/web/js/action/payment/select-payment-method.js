define(
    [
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/model/full-screen-loader',
        'jquery',
        'Magento_Checkout/js/action/get-totals',
        'mage/url'
    ],
    function (quote, fullScreenLoader, jQuery, getTotalsAction, url) {
        'use strict';
        return function (paymentMethod) {
        	
        	quote.paymentMethod(paymentMethod);
        	
        	if(paymentMethod.method.match('^loviit(rp|rpiv|ic|rpcc|rpot|rpva|rppp)$')){
        		
        		var plan_selected = jQuery("input[name='installment_plan_"+paymentMethod.method+"']:checked").val();
        		if(!plan_selected  || plan_selected == null ){
        			return;
        		}
        	}

            fullScreenLoader.startLoader();

            jQuery.ajax( url.build('loviit/payment/applyPaymentMethod'), {
                data: {
                	payment_method: paymentMethod,
                	plan_selected: jQuery("input[name='installment_plan_"+paymentMethod.method+"']:checked").val()
                	},
                complete: function () {
                    getTotalsAction([]);
                    fullScreenLoader.stopLoader();
                }
            });

        }
    }
);