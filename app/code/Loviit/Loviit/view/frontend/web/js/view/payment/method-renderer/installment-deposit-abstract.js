/**
 * Copyright  2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*browser:true*/
/*global define*/
define(
    [
        'jquery',
        'Loviit_Loviit/js/view/payment/method-renderer/installment-abstract',
        'Magento_Checkout/js/model/payment/additional-validators',
        'Magento_Checkout/js/model/quote',
        'Magento_Customer/js/customer-data',
         'mage/url',
         'ko'
    ],
    function (
        $,
        Component,
        additionalValidators,
        quote,
        customerData,
        url,
        ko
    ) {
        'use strict';

        return Component.extend({
        	
        	defaults: {
                rates: ko.observableArray([]),
                lastInstallmentUpdate: "",
                disableContinue: ko.observable(true),
                deposit:  ko.observable(0)
            },

            /** Init observable variables */
            initObservable: function () {
            	//this.rates(Object.values(window.checkoutConfig.payment[this.getCode()].installment_plans));
            	this.rates.valueHasMutated();
            	this.disableContinue(true);
            	this.disableContinue.valueHasMutated();
                this._super().observe([]);
                this.deposit.valueHasMutated();
                return this;
            },
            
            showDeposit:function(){
            	return true;
            },
            showDepositColumn:function(){
            	return true;
            },
            depositChanged:function(){
         	    return this.disableContinue();
            },
            disableContinueButton: function(){
            	this.disableContinue(true);
            	this.disableContinue.valueHasMutated();
            },


            /** Returns payment information data */
            getData: function () {
                var parent = this._super(),
                    additionalData = {};

                var code= this.getCode();
                additionalData['iban'] =  this.iban();
                additionalData['bic'] = this.bic();
                additionalData['bank_account_opening_date'] = this.bankAccountOpeningDate();
                additionalData['taxid'] = this.taxid();
                additionalData['gender'] = this.gender();
                additionalData['birthday'] = this.birthday();
                additionalData["plan_selected"]=this.selectedRate();
                additionalData["deposit"]= this.deposit();
                additionalData['general_conditions'] = this.checkConditions();
                additionalData['privacy_policy'] = this.checkPolicy();
                additionalData['force_loading'] = this.forceLoading();

                return $.extend(true, parent, {
                    'additional_data': additionalData
                });
            },

        });
    }
);
