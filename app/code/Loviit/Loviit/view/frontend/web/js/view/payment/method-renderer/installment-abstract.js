/**
 * Copyright  2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*browser:true*/
/*global define*/
define(
    [
        'jquery',
        'Loviit_Loviit/js/view/payment/method-renderer/methods-abstract',
        'Magento_Checkout/js/model/payment/additional-validators',
        'Magento_Checkout/js/model/quote',
        'Magento_Customer/js/customer-data',
         'mage/url',
         'ko',
         'Magento_Checkout/js/model/full-screen-loader'
    ],
    function (
        $,
        Component,
        additionalValidators,
        quote,
        customerData,
        url,
        ko,
        fullScreenLoader
    ) {
        'use strict';
        
        return Component.extend({
            defaults: {
                template: 'Loviit_Loviit/checkout/installment',
                rates: ko.observableArray([]),
                lastInstallmentUpdate: "",
                disableContinue: ko.observable(false),
                iban: ko.observable(""),
                bic: ko.observable(""),
                taxid: ko.observable(""),
                bankAccountOpeningDate: ko.observable(""),
                birthday:ko.observable(""),
                gender: ko.observable(""),
                checkPolicy:  ko.observable(false),
                checkConditions:  ko.observable(false),
                showBirthday: ko.observable(false),
                showTaxId: ko.observable(false),
                showGender: ko.observable(false),
                showBic: ko.observable(false),
                showBankAccountOpeningDate: ko.observable(false),
                selectedRate: ko.observable(false),
                showMinimumDepositColumn: ko.observable(false),
                showInterestColumn: ko.observable(false),
                showAdminFeeColumn: ko.observable(false),
                showDiscountColumn: ko.observable(false),
                showDepositColumn: ko.observable(false),
                forceLoading: ko.observable(false),
                installmentTableIsLoading: ko.observable(false)
            },
            
            initialize: function () {
                this._super();
                this.checkRequiredFields();
                this.recalculatePlan();
                this.checkHideColumns();
                quote.totals.subscribe(function () {
                	this.recalculatePlan(true);
                }, this);
                if (window.checkoutConfig.reloadOnBillingAddress) {
                	quote.billingAddress.subscribe(function () {
                		this.recalculatePlan(true);
                    }, this);
                }
                quote.billingAddress.subscribe(function () {
            		this.checkRequiredFields();
                }, this);
                this.rates.subscribe(function () {
                	this.checkHideColumns();
                }, this);
            },
            
            checkHideColumns:function(){
            	var self=this;
            	this.showMinimumDepositColumn(this.rates().some(function(plan){ 
            		if(plan['minimum_deposit']=='' || plan['minimum_deposit']==null)
            			return false;
            		return Number(plan['minimum_deposit'])>0;
            	}));
            	this.showInterestColumn(this.rates().some(function(plan){ 
            		if(plan['interest_amount']=='' || plan['interest_amount']==null)
            			return false;
            		return Number(plan['interest_amount'])>0;
            	}));
            	
            	this.showAdminFeeColumn(this.rates().some(function(plan){ 
            		if(plan['administration_fee_amount']=='' || plan['administration_fee_amount']==null)
            			return false;
            		return Number(plan['administration_fee_amount'])>0;
            	}));
            	
            	this.showDiscountColumn(this.rates().some(function(plan){ 
            		if(plan['discount']=='' || plan['discount']==null)
            			return false;
            		return Number(plan['discount'])< 0;
            	}));
                this.showDepositColumn(this.rates().some(function(plan){ 
            		if(plan['deposit'] > 0)
                                return Number(plan['deposit']) > 0;
                        return false;
            	}));
            },

            /** Init observable variables */
            initObservable: function () {
            	//this.rates(Object.values(window.checkoutConfig.payment[this.getCode()].installment_plans));
            	this.rates.valueHasMutated();
            	this._super().observe({
	            		taxid: window.customerData.taxvat,
	                    birthday: window.customerData.dob,
	                    gender: window.customerData.gender,
                        iban: "",
                        bic: "",
                        showBirthday: false,
                        showTaxId: false,
                        showGender: false,
                        showBic: false,
                        showBankAccountOpeningDate:false
                    });
                return this;
            },
            
            checkRequiredFields: function(){
            	if(quote.billingAddress()==null)
            		return;
            	var countryId = quote.billingAddress().countryId;
            	var requiredFields = window.checkoutConfig.settings.required_fields[countryId];
            	if(typeof requiredFields !== 'undefined'){
            		this.showBirthday(requiredFields.birthday == 1 && (window.customerData.dob == "" || window.customerData.dob == null) ? true : false);
            		this.showTaxId(requiredFields.taxid == 1 && (window.customerData.taxvat == "" || window.customerData.taxvat == null) ? true : false);
            		this.showGender(requiredFields.gender == 1 && (window.customerData.gender == "0" || window.customerData.gender == "" || window.customerData.gender == null) ? true : false);
            		this.showBic(requiredFields.bic == 1 ? true : false);
            		this.showBankAccountOpeningDate(requiredFields.bank_account_opening_date == 1 ? true : false);
            	}
            	else{
            		this.showBirthday(false);
            		this.showTaxId(false);
            		this.showGender(false);
            		this.showBic(false);
            		this.showBankAccountOpeningDate(false);
            	}
            	
            	this.showBirthday.valueHasMutated();
        		this.showTaxId.valueHasMutated();
        		this.showGender.valueHasMutated();
        		this.showBic.valueHasMutated();
        		this.showBankAccountOpeningDate.valueHasMutated();
            	
            },
            
            getRequiredFields: function () {
                return window.checkoutConfig.settings.required_fields;
            },
            
            showDeposit:function(){
            	return false;
            },
            depositChanged:function(){
         	    return false;
            },
            bankDetailsRequired:function(){
            	return true;
            },

            getInstallmentPlans: function() {
            	return this.rates();
            },
            
            getGeneralConditions: function() {
                return window.checkoutConfig.settings.general_conditions;
            },

            getPrivacyPolicy: function() {
                return window.checkoutConfig.settings.privacy_policy;
            },
            
            selectRate: function(parent_ref){
            	return parent_ref.selectPaymentMethod();
            },
            
            selectInstallmentPaymentMethod: function(){
            	this.selectPaymentMethod()
            	this.recalculatePlan(true);
            	return true;
            },
            
            recalculatePlan: function(executeChecks){
            	if(this.getCode() != this.isChecked()){
            		return;
            	}
            	if(executeChecks===true){
            		if(!this.forceLoading() && this.showDeposit() && (this.deposit()=="" || this.deposit()==0 ) ){
            			this.disableContinue(true);
            			return;
            		}
	            	var lastInstallmentUpdate=window.checkoutConfig.lastInstallmentUpdate;
		            	if(typeof lastInstallmentUpdate === 'undefined' || this.lastInstallmentUpdate==lastInstallmentUpdate){
		            		return;
		            	}
		            	else{
		            		this.lastInstallmentUpdate=lastInstallmentUpdate;
		            	}
            	}
            	var self = this;
            	
//            	fullScreenLoader.startLoader();
            	this.installmentTableIsLoading(true);
            	$.ajax({
                    url:  url.build('loviit/payment/RecalculatePlans'),
                    data: this.getData(),
                    type: "GET",
                    dataType: 'json'
                }).done(function (data) {
                	
                	self.forceLoading(false);
                	
                   if(data.success){
                	   window.checkoutConfig.payment[self.getCode()]=[];
                	   window.checkoutConfig.payment[self.getCode()]['installment_plans']=data.result;
                	   self.rates(Object.keys(data.result).map(function(key) {return data.result[key];}));
                	   self.rates.valueHasMutated();
                	   self.disableContinue(false);
                	   self.disableContinue.valueHasMutated();
                   }
                   else{
                	   self.messageContainer.addErrorMessage({
                            'message': data.message
                        });
                   }
                   self.installmentTableIsLoading(false);
                }).fail(
                       
                 ).always(
                         function () {
                        	 self.installmentTableIsLoading(false);
//                             fullScreenLoader.stopLoader();
                         }
                     );
            },

            /** Returns payment information data */
            getData: function () {
                var parent = this._super(),
                    additionalData = {};

                return $.extend(true, parent, {
                    'additional_data': additionalData
                });
            },

            
        });
    }
);
