/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*browser:true*/
/*global define*/
define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'loviitcc',
                component: 'Loviit_Loviit/js/view/payment/method-renderer/creditcard-method'
            },
            {
                type: 'loviitdc',
                component: 'Loviit_Loviit/js/view/payment/method-renderer/debitcard-method'
            },
            {
                type: 'loviitiv',
                component: 'Loviit_Loviit/js/view/payment/method-renderer/invoice-method'
            },
            {
                type: 'loviitppal',
                component: 'Loviit_Loviit/js/view/payment/method-renderer/paypal-method'
            },
            {
                type: 'loviitpp',
                component: 'Loviit_Loviit/js/view/payment/method-renderer/prepayment-method'
            },
            {
                type: 'loviitsu',
                component: 'Loviit_Loviit/js/view/payment/method-renderer/sofort-method'
            },
            {
                type: 'loviitidl',
                component: 'Loviit_Loviit/js/view/payment/method-renderer/ideal-method'
            },
            {
                type: 'loviitdd',
                component: 'Loviit_Loviit/js/view/payment/method-renderer/directdebit-method'
            },
            {
                type: 'loviitpl',
                component: 'Loviit_Loviit/js/view/payment/method-renderer/przelewy-method'
            },

            
            // INSTALLMENTS
            {
                type: 'loviitic',
                component: 'Loviit_Loviit/js/view/payment/method-renderer/installmentcreditcard-method'
            },
            {
                type: 'loviitrpiv',
                component: 'Loviit_Loviit/js/view/payment/method-renderer/installmentmanual-method'
            },
            {
                type: 'loviitrp',
                component: 'Loviit_Loviit/js/view/payment/method-renderer/installment-method'
            },
            {
                type: 'loviitrpcc',
                component: 'Loviit_Loviit/js/view/payment/method-renderer/installmentccdeposit-method'
            },
            {
                type: 'loviitrpva',
                component: 'Loviit_Loviit/js/view/payment/method-renderer/installmentpaypaldeposit-method'
            },
            {
                type: 'loviitrppp',
                component: 'Loviit_Loviit/js/view/payment/method-renderer/installmentprepaymentdeposit-method'
            },
            {
                type: 'loviitrpot',
                component: 'Loviit_Loviit/js/view/payment/method-renderer/installmentsofortdeposit-method'
            }
        );
        
        return Component.extend({});
    }
);