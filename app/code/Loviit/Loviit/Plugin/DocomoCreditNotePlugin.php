<?php
namespace Loviit\Loviit\Plugin;

use Loviit\Loviit\Helper\Payment;
use \Magento\Framework\Exception\CouldNotSaveException;
use \Magento\Framework\Phrase;
use Loviit\Loviit\Model\LoviitPaymentMethodAbstract;
use Magento\Sales\Model\Order\Payment\Transaction;
use Magento\SalesSequence\Model\Manager;
use Magento\Sales\Model\EntityInterface;

class DocomoCreditNotePlugin
{    
    protected $_paymentHelper;

    protected $sequenceManager;


    /**
     * @var \Magento\Sales\Model\Order\Payment\Transaction\ManagerInterface
     */
    protected $transactionManager;

     /**
     * @var \Magento\Framework\Event\ManagerInterface
     */
    protected $eventManager;
     
	public function __construct(
        Payment $paymentHelper,
        \Magento\Sales\Model\Order\Payment\Transaction\ManagerInterface $transactionManager,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        Manager $sequenceManager) {
        $this->_paymentHelper = $paymentHelper;
        $this->transactionManager = $transactionManager;
        $this->eventManager = $eventManager;
        $this->sequenceManager = $sequenceManager;
        
    }
    
   
    public function aroundRefund(\Magento\Sales\Model\Order\Payment $payment, callable $proceed,$creditmemo){

        $gateway = $payment->getMethodInstance();
        
        if(($gateway instanceof LoviitPaymentMethodAbstract) && $creditmemo->getDoTransaction()){
            
            //this is needed before doing the call to have the same CN number in Magento and Docomo
            $creditmemo->setIncrementId(
                "DCMONL".$this->sequenceManager->getSequence(
                    $creditmemo->getEntityType(),
                    $creditmemo->getStore()->getGroup()->getDefaultStoreId()
                )->getNextValue()
            );

            $baseAmountToRefund = $payment->formatAmount($creditmemo->getBaseGrandTotal());
            $payment->setTransactionId(
                $this->transactionManager->generateTransactionId($payment, Transaction::TYPE_REFUND)
            );
    
            $isOnline = true;
            $payment->setCreditmemo($creditmemo);
            $additionalData =array();
            $additionalData['ShippingCosts']=$creditmemo->getShippingInclTax();
            $additionalData['AdministrationFee']=$creditmemo->getData('loviit_administration_fee');
            $additionalData['CreditId']=$creditmemo->getIncrementId();

            if($gateway->isPSP()){
                $result = $this->_paymentHelper->doPspRefund($creditmemo->getOrder(),$creditmemo->getItems(),$additionalData);
            }
            else{
                $result = $this->_paymentHelper->doCreditNoteRequest($creditmemo->getOrder(),$creditmemo->getItems(),$additionalData);
            }
            
            if (!$result["success"]) {
                throw new CouldNotSaveException( new Phrase("DOCOMO Credit Note was not successful - ".$result["message"]));
            }
            
    
            // update self totals from creditmemo
            $totalsToUpdate=[
                    'amount_refunded' => $creditmemo->getGrandTotal(),
                    'base_amount_refunded' => $baseAmountToRefund,
                    'base_amount_refunded_online' => $isOnline ? $baseAmountToRefund : null,
                    'shipping_refunded' => $creditmemo->getShippingAmount(),
                    'base_shipping_refunded' => $creditmemo->getBaseShippingAmount()
                ];

            foreach ($totalsToUpdate as $key => $amount) {
                    if (null !== $amount) {
                        $was = $payment->getDataUsingMethod($key);
                        $payment->setDataUsingMethod($key, $was + $amount);
                    }
            }

            //update order refunded docomo amounts
            $order = $payment->getOrder();

            $order->setData('loviit_administration_fee_refunded', $order->getData('loviit_administration_fee_refunded')+$creditmemo->getData('loviit_administration_fee'));
            $order->setData('loviit_interest_fee_refunded', $order->getData('loviit_interest_fee_refunded')+$creditmemo->getData('loviit_interest_fee'));
            
            $creditmemo->addComment("DOCOMO credit memo refunded online",false,false);

            // update transactions and order state
            $transaction = $payment->addTransaction(
                Transaction::TYPE_REFUND,
                $creditmemo,
                $isOnline
            );

            
            /*$payment->setOrderStateProcessing($message);*/
            $this->eventManager->dispatch(
                'sales_order_payment_refund',
                ['payment' => $payment, 'creditmemo' => $creditmemo]
            );
            return $payment;
        }
        else{//default
            return $proceed($creditmemo);
        }

    }
}