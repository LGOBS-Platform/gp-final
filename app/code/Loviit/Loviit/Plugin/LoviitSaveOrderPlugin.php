<?php

namespace Loviit\Loviit\Plugin;

use Loviit\Loviit\Helper\Payment;
use \Magento\Quote\Api\CartRepositoryInterface;
use Loviit\Loviit\Exception\LoviitException;
use \Magento\Framework\Exception\CouldNotSaveException;
use \Magento\Framework\Phrase;

class LoviitSaveOrderPlugin {
	protected $_paymentHelper;
	protected $quoteRepository;
	public function __construct(CartRepositoryInterface $quoteRepository, Payment $paymentHelper, \Magento\Quote\Model\QuoteFactory $quoteFactory) {
		$this->quoteRepository = $quoteRepository;
		$this->_paymentHelper = $paymentHelper;
	}
	
	//NOT USED
	public function beforeSavePaymentInformationAndPlaceOrder(
			\Magento\Checkout\Api\PaymentInformationManagementInterface $subject, 
			$cartId, 
			\Magento\Quote\Api\Data\PaymentInterface $paymentMethod, 
			\Magento\Quote\Api\Data\AddressInterface $billingAddress = null) {
		$additionalData = $paymentMethod->getAdditionalData ();
		$quote = $this->quoteRepository->get ( $cartId );
		$this->_paymentHelper->verifyAndSetAdditionalPaymentData ( $additionalData, $quote,$paymentMethod->getMethod() );
		
		unset ( $additionalData ["plan_selected"] );
		unset ( $additionalData ["iban"] );
		unset ( $additionalData ["bic"] );
		unset ( $additionalData ["general_conditions"] );
		unset ( $additionalData ["privacy_policy"] );
		unset ( $additionalData ["gender"] );
		unset ( $additionalData ["taxid"] );
		unset ( $additionalData ["birthday"] );
		$paymentMethod->setAdditionalData ( $additionalData );
		return [ 
				$cartId,
				$paymentMethod,
				$billingAddress 
		];
	}
	
	//NOT USED
	public function beforeSavePaymentInformation(
			\Magento\Checkout\Api\PaymentInformationManagementInterface $subject, 
			$cartId, 
			\Magento\Quote\Api\Data\PaymentInterface $paymentMethod, 
			\Magento\Quote\Api\Data\AddressInterface $billingAddress = null) {
		$additionalData = $paymentMethod->getAdditionalData ();
		$quote = $this->quoteRepository->get ( $cartId );
		$this->_paymentHelper->verifyAndSetAdditionalPaymentData ( $additionalData, $quote,$paymentMethod->getMethod() );

		unset ( $additionalData ["plan_selected"] );
		unset ( $additionalData ["iban"] );
		unset ( $additionalData ["bic"] );
		unset ( $additionalData ["general_conditions"] );
		unset ( $additionalData ["privacy_policy"] );
		unset ( $additionalData ["gender"] );
		unset ( $additionalData ["taxid"] );
		unset ( $additionalData ["birthday"] );
		$paymentMethod->setAdditionalData ( $additionalData );
		return [ 
				$cartId,
				$paymentMethod,
				$billingAddress 
		];
	}
	
	//set payment api
	public function beforeSet(
			\Magento\Quote\Api\PaymentMethodManagementInterface $subject,
			$cartId,
			\Magento\Quote\Api\Data\PaymentInterface $method) {

				if(strpos($method->getMethod(),'loviit')!==0)
	            	return [
							$cartId,
							$method
					];
				
				$additionalData = $method->getAdditionalData ();
				$quote = $this->quoteRepository->get ( $cartId );
				try{
					$this->_paymentHelper->verifyAndSetAdditionalPaymentData ( $additionalData, $quote,$method->getMethod() );
					unset ( $additionalData ["plan_selected"] );
					unset ( $additionalData ["iban"] );
					unset ( $additionalData ["bic"] );
					unset ( $additionalData ["general_conditions"] );
					unset ( $additionalData ["privacy_policy"] );
					unset ( $additionalData ["gender"] );
					unset ( $additionalData ["taxid"] );
					unset ( $additionalData ["birthday"] );
					unset ( $additionalData ["bank_account_opening_date"] );
					$method->setAdditionalData ( $additionalData );
				}
				catch (LoviitException $e){
					throw new CouldNotSaveException( new Phrase($e->getMessage ()), $e);
				}
				return [
						$cartId,
						$method
				];
	}
	
	
}