<?php

namespace Loviit\Loviit\Plugin;

use Loviit\Loviit\Helper\Payment;
use \Magento\Quote\Api\CartRepositoryInterface;

class LoviitSaveOrderPluginGuestCart {
	protected $_paymentHelper;
	protected $quoteRepository;
	protected $quoteIdMaskFactory;
	
	public function __construct(CartRepositoryInterface $quoteRepository, Payment $paymentHelper, \Magento\Quote\Model\QuoteFactory $quoteFactory,
			\Magento\Quote\Model\QuoteIdMaskFactory $quoteIdMaskFactory) {
		$this->quoteRepository = $quoteRepository;
		$this->_paymentHelper = $paymentHelper;
		$this->quoteIdMaskFactory = $quoteIdMaskFactory;
	}
	public function beforeSavePaymentInformationAndPlaceOrder(
			\Magento\Checkout\Api\GuestPaymentInformationManagementInterface $subject, 
			$cartId, 
			$email,
			\Magento\Quote\Api\Data\PaymentInterface $paymentMethod, 
			\Magento\Quote\Api\Data\AddressInterface $billingAddress = null) {
		$additionalData = $paymentMethod->getAdditionalData ();
		$quoteIdMask = $this->quoteIdMaskFactory->create()->load($cartId, 'masked_id');
		
		$quote = $this->quoteRepository->get ( $quoteIdMask->getQuoteId() );
		
		$this->_paymentHelper->verifyAndSetAdditionalPaymentData ( $additionalData, $quote,$paymentMethod->getMethod()  );
		
		
		unset ( $additionalData ["plan_selected"] );
		unset ( $additionalData ["iban"] );
		unset ( $additionalData ["bic"] );
		unset ( $additionalData ["general_conditions"] );
		unset ( $additionalData ["privacy_policy"] );
		unset ( $additionalData ["gender"] );
		unset ( $additionalData ["taxid"] );
		unset ( $additionalData ["birthday"] );
		$paymentMethod->setAdditionalData ( $additionalData );
		return [ 
				$cartId,
				$email,
				$paymentMethod,
				$billingAddress 
		];
	}
	public function beforeSavePaymentInformation(
			\Magento\Checkout\Api\GuestPaymentInformationManagementInterface $subject,  
			$cartId, 
			$email,
			\Magento\Quote\Api\Data\PaymentInterface $paymentMethod, 
			\Magento\Quote\Api\Data\AddressInterface $billingAddress = null) {
		$additionalData = $paymentMethod->getAdditionalData ();
		
		$quoteIdMask = $this->quoteIdMaskFactory->create()->load($cartId, 'masked_id');
		
		$quote = $this->quoteRepository->get ( $quoteIdMask->getQuoteId() );
		
		$this->_paymentHelper->verifyAndSetAdditionalPaymentData ( $additionalData, $quote,$paymentMethod->getMethod()  );
		
		unset ( $additionalData ["plan_selected"] );
		unset ( $additionalData ["iban"] );
		unset ( $additionalData ["bic"] );
		unset ( $additionalData ["general_conditions"] );
		unset ( $additionalData ["privacy_policy"] );
		unset ( $additionalData ["gender"] );
		unset ( $additionalData ["taxid"] );
		unset ( $additionalData ["birthday"] );
		$paymentMethod->setAdditionalData ( $additionalData );
		return [ 
				$cartId,
				$email,
				$paymentMethod,
				$billingAddress 
		];
	}
}