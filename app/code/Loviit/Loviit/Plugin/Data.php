<?php

namespace Loviit\Loviit\Plugin;
/**
 * Description of Data
 *
 * @author tim.zwinkels
 */

class Data extends \Magento\Payment\Helper\Data {
    
    public function aroundGetPaymentMethodList($sorted = true, $asLabelValue = false, $withGroups = false, $store = null)
    {
        $methods = [];
        $groups = [];
        $groupRelations = [];
        foreach ($this->getPaymentMethods() as $code => $data) {

            if (isset($data['title'])) {
                $methods[$code] = $data['title'];
            } else {
                $methods[$code] = $this->getMethodInstance($code)->getConfigData('title', $store);
            }
            if ($asLabelValue && $withGroups && isset($data['group'])) {
                $groupRelations[$code] = $data['group'];
            }
            
            if (in_array(strtolower($code), array('loviitrp', 'loviitrpiv', 'loviitic'))) {
                $installmentPlans = array();
                if (!$installmentPlans = @unserialize($this->getMethodInstance($code)->getConfigData('installment_plans', $store))) {
                    $installmentPlans = json_decode($this->getMethodInstance($code)->getConfigData('installment_plans', $store), true);
                }

                if (is_array($installmentPlans) && count($installmentPlans) > 0) {
                    foreach ($installmentPlans as $installmentPlan) {
                        $nbOfInstallments = $installmentPlan['number_of_installments'];

                        $newMethodCode = $code . $nbOfInstallments;

                        $methods[$newMethodCode] = $methods[$code]. " (".$nbOfInstallments." ".__("months").")";
                        if ($asLabelValue && $withGroups && isset($data['group'])) {
                            $groupRelations[$newMethodCode] = $data['group'];
                        }
                    }
                }
            }
        }
        if ($asLabelValue && $withGroups) {
            $groups = $this->_paymentConfig->getGroups();
            foreach ($groups as $code => $title) {
                $methods[$code] = $title;
            }
        }

        if ($sorted) {
            asort($methods);
        }
        if ($asLabelValue) {
            $labelValues = [];
            foreach ($methods as $code => $title) {
                $labelValues[$code] = [];
            }
            foreach ($methods as $code => $title) {
                if (isset($groups[$code])) {
                    $labelValues[$code]['label'] = $title;
                } elseif (isset($groupRelations[$code])) {
                    unset($labelValues[$code]);
                    $labelValues[$groupRelations[$code]]['value'][$code] = ['value' => $code, 'label' => $title];
                } else {
                    $labelValues[$code] = ['value' => $code, 'label' => $title];
                }
            }
            return $labelValues;
        }

        return $methods;
    }    
}

