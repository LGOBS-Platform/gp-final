<?php
namespace Loviit\Loviit\Plugin;

use Magento\Backend\Block\Widget\Button\Toolbar as ToolbarContext;
use Magento\Framework\View\Element\AbstractBlock;
use Magento\Backend\Block\Widget\Button\ButtonList;

class LoviitButtonList
{    
    /**
     * @param ToolbarContext $toolbar
     * @param AbstractBlock $context
     * @param ButtonList $buttonList
     * @return array
     */
    public function beforePushButtons(
        ToolbarContext $toolbar,
        AbstractBlock $context,
        ButtonList $buttonList
    ) {
    	
        //ADMIN ORDER DETAIL TOOLBAR
        if($context instanceof \Magento\Sales\Block\Adminhtml\Order\View){

			if (!$this->wasPaidWithDocomo($context) ) {
				return [$context, $buttonList];
			}
        
	        $ommRequestStatus = $this->getOmmRequestStatus($context);
	        $orderIsCancelled = $this->orderIsCancelled($context);
	
	        if ($ommRequestStatus['order'] && !$ommRequestStatus['delivery'] && !$ommRequestStatus['credit'] && !$orderIsCancelled) {
	            $message = __('Send delivery request. Confirm?');
	            $buttonList->add(
	                'loviit_delivery_request',
	                [
	                    'label' => __('DOCOMO Delivery Request'),
	                    'onclick' => "var trkid = prompt('Tracking ID:'); var newLoc = '" . $context->getReviewPaymentUrl('loviitdelivery') . "?trkid='+trkid; confirmSetLocation('{$message}', newLoc)",
	                ]
	            );
	        }
			 
			//show the button only if not delivery has been sent since now credit requests are sent using standard magento functionalities
	        if ($ommRequestStatus['order'] && !$ommRequestStatus['credit'] && !$ommRequestStatus['delivery'] && !$orderIsCancelled) {
	            $message = __('Send creditnote request. Confirm?');
	            $buttonList->add(
	                'loviit_creditnote_request',
	                [
	                    'label' => __('DOCOMO Cancel Order Request'),
	                    'onclick' => "confirmSetLocation('{$message}', '{$context->getReviewPaymentUrl('loviitcreditnote')}')"
	                ]
	            );
	        }
	        
	        if (!$ommRequestStatus['order'] && !( $ommRequestStatus['psp_pa'] || $ommRequestStatus['psp_db'] )  && !$orderIsCancelled) {
	            $message = __('Resend order request. Confirm?');        
	            $buttonList->add(
	                'loviit_order_request',
	                [
	                    'label' => __('DOCOMO Order Request'),
	                    'onclick' => "confirmSetLocation('{$message}', '{$context->getReviewPaymentUrl('loviitorder')}')"
	                ]
	            );
			}
			
			//remove invoice button for non psp
			if(!($ommRequestStatus['psp_pa'] || $ommRequestStatus['psp_cp'] || $ommRequestStatus['psp_db'] || $ommRequestStatus['psp_rv']) ){
				$buttonList->remove('order_invoice');
			}
	        
	        if ($ommRequestStatus['psp_pa'] && !( $ommRequestStatus['psp_cp'] || $ommRequestStatus['psp_rv'] )   && !$orderIsCancelled) {
	            $message = __('Pre-authorized amount will be captured. Confirm?');        
	            $buttonList->add(
	                'loviit_psp_cp',
	                [
	                    'label' => __('DOCOMO Capture Funds'),
	                    'onclick' => "confirmSetLocation('{$message}', '{$context->getReviewPaymentUrl('loviitcapture')}')"
	                ]
	            );
	            $message = __('Order will be cancelled and the pre authorized amount will be reverted. Confirm?');        
	            $buttonList->add(
	                'loviit_psp_rv',
	                [
	                    'label' => __('DOCOMO Reversal'),
	                    'onclick' => "confirmSetLocation('{$message}', '{$context->getReviewPaymentUrl('loviitreversal')}')"
	                ]
	            );
	        }
	        
	        /*if ( ($ommRequestStatus['psp_cp'] || $ommRequestStatus['psp_db']) && !$ommRequestStatus['psp_rf']  && !$orderIsCancelled) {
	            $message = __('Submit Refund');        
	            $buttonList->add(
	                'loviit_psp_rf',
	                [
	                    'label' => __('DOCOMO Refund'),
	                    'onclick' => "confirmSetLocation('{$message}', '{$context->getReviewPaymentUrl('loviitrefund')}')"
	                ]
	            );
	        }*/
	        
	        if ( !$ommRequestStatus['psp_pa'] && !$ommRequestStatus['psp_db'] ) {
				$message = __('Retrieve Order Status');        
				$buttonList->add(
					'loviit_order_status',
					[
						'label' => __('DOCOMO Order Status'),
						'onclick' => "confirmSetLocation('{$message}', '{$context->getReviewPaymentUrl('loviitstatus')}')"
				] );
			}


		} 		// ADMIN ORDER SHIPMENT DETAIL TOOLBAR
		else if ($context instanceof \Magento\Shipping\Block\Adminhtml\View) {

			if (!$this->wasPaidWithDocomo($context) ) {
				return [$context, $buttonList];
			}
			
			//check if request has already been sent
			$docomo_notified = $context->getShipment()->getData('docomo_notified_tracking');
			
			if( empty($docomo_notified)){
				$message = __('Docomo Digital will be notified');
				$buttonList->add ( 'docomo_tracking_request', [ 
					'label' => __ ( 'Send Docomo Tracking Request'),
	        		'onclick' => "confirmSetLocation('{$message}', '{$this->getTrackingRequestUrl($context)}')"
	        		]
	        	);
			}
        }
        
        return [$context, $buttonList];
    }
    
    /**
     * @return string
     */
    public function getTrackingRequestUrl($context)
    {
    	return $context->getUrl('loviit_admin/shipment/docomotrackingrequest', ['shipment_id' => $context->getShipment()->getId()]);
    }
    
    
    private function getOmmRequestStatus($context)
    {
        $order = $context->getOrder();
        
        return array('order' => $order->getData('loviit_order_request'), 
            'delivery' => $order->getData('loviit_delivery_request'), 
            'credit' => $order->getData('loviit_creditnote_request'), 
            'psp_pa' => $order->getData('loviit_psp_pa'), 
            'psp_cp' => $order->getData('loviit_psp_cp'), 
            'psp_db' => $order->getData('loviit_psp_db'), 
            'psp_rf' => $order->getData('loviit_psp_rf'), 
            'psp_rv' => $order->getData('loviit_psp_rv')
                );
    }
    
    private function wasPaidWithDocomo($context){
    	$order = $context->getOrder();
    	if(empty($order)){
    		$order= $context->getShipment()->getOrder();
    	}
    	$paymentMethod = $order->getPayment()->getMethod();
    	return strrpos($paymentMethod,"loviit")===0;
    }
    
    private function orderIsCancelled($context){
    	return $context->getOrder()->getStatus()==\Magento\Sales\Model\Order::STATE_CANCELED;
    }
}