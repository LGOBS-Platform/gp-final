<?php
namespace Loviit\Loviit\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\DB\Adapter\AdapterInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        $table = $installer->getConnection()
            ->newTable($installer->getTable('loviit_customer_registration'))
            ->addColumn(
                'registration_id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'nullable' => false, 'unsigned'=>true, 'primary' => true],
                'ID'
            )
              ->addColumn(
                'customer_id',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false, 'unsigned'=>true ],
                'Customer ID'
            )
              ->addColumn(
                'registration',
                Table::TYPE_TEXT,
                32,
                ['nullable' => false, 'default' => ''],
                'Registration'
            )
              ->addColumn(
                'channel_id',
                Table::TYPE_TEXT,
                32,
                ['nullable' => false, 'default' => ''],
                'Channel'
            )
              ->addColumn(
                'pm_id',
                Table::TYPE_TEXT,
                4,
                ['nullable' => false, 'default' => ''],
                'PM ID'
            )
              ->addIndex(
				$setup->getIdxName(
					$installer->getTable( 'loviit_customer_registration' ),
					[ 'customer_id', 'registration'],
					AdapterInterface::INDEX_TYPE_UNIQUE
				),
				[ 'customer_id', 'registration'],
				[ 'type' => AdapterInterface::INDEX_TYPE_UNIQUE ]
			)
            ->setComment('Loviit oneclick checkout payment registrations')
			->setOption('type', 'InnoDB')
			->setOption('charset', 'utf8');
        $installer->getConnection()->createTable($table);

        $installer->endSetup();
    }

}
