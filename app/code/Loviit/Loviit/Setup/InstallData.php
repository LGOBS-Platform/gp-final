<?php

namespace Loviit\Loviit\Setup;

use Magento\Customer\Model\Customer;
use Magento\Framework\Encryption\Encryptor;
use Magento\Framework\Indexer\IndexerRegistry;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Catalog\Setup\CategorySetupFactory;
use Magento\Quote\Setup\QuoteSetupFactory;
use Magento\Sales\Setup\SalesSetupFactory;

/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface {
	/**
	 * EAV setup factory
	 *
	 * @var EavSetupFactory
	 */
	private $eavSetupFactory;
	
	/**
	 * Category setup factory
	 *
	 * @var CategorySetupFactory
	 */
	protected $categorySetupFactory;
	
	/**
	 * Quote setup factory
	 *
	 * @var QuoteSetupFactory
	 */
	protected $quoteSetupFactory;
	
	/**
	 * Sales setup factory
	 *
	 * @var SalesSetupFactory
	 */
	protected $salesSetupFactory;
	
	/**
	 * Init
	 *
	 * @param CategorySetupFactory $categorySetupFactory        	
	 * @param SalesSetupFactory $salesSetupFactory        	
	 */
	public function __construct(SalesSetupFactory $salesSetupFactory, QuoteSetupFactory $quoteSetupFactory) {
		$this->salesSetupFactory = $salesSetupFactory;
		$this->quoteSetupFactory = $quoteSetupFactory;
	}
	
	/**
	 *
	 * {@inheritdoc} @SuppressWarnings(PHPMD.ExcessiveMethodLength)
	 */
	public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context) {
		$setup->startSetup();
		
		/** @var \Magento\Sales\Setup\SalesSetup $salesSetup */
		$salesSetup = $this->salesSetupFactory->create ( [ 'setup' => $setup ] );
		
		$quoteInstaller = $this->quoteSetupFactory->create(['resourceName' => 'quote_setup', 'setup' => $setup]);
		
		/**
		 * Remove previous attributes
		 */
// 		$attributes = [ 
// 				'number_of_installments' 
// 		];
// 		foreach ( $attributes as $attr_to_remove ) {
// 			$salesSetup->removeAttribute ( \Magento\Sales\Model\Order::ENTITY, $attr_to_remove );
// 		}
		
		/**
		 * Add 'number_of_installments' attributes for order
		 */
		$options = [ 
                    'type' => 'int',
                    'visible' => true,
                    'required' => false 
		];
		$quoteInstaller->addAttribute('quote_payment', 'number_of_installments', $options);
		$salesSetup->addAttribute ( 'order', 'number_of_installments', $options );
		
		$options = [
                    
                    'type' => 'decimal',
                    'visible' => true,
                    'required' => false
		];
		
		$quoteInstaller->addAttribute('quote', 'loviit_administration_fee', $options);
		$quoteInstaller->addAttribute('quote', 'loviit_interest_fee', $options);
		$quoteInstaller->addAttribute('quote', 'loviit_interest_fee_perc', $options );
		$quoteInstaller->addAttribute ( 'quote', 'loviit_deposit', $options );
		$quoteInstaller->addAttribute ( 'quote', 'loviit_discount', $options );
		
		$salesSetup->addAttribute ( 'order', 'loviit_administration_fee', $options );
		$salesSetup->addAttribute ( 'order', 'loviit_interest_fee', $options );
		$salesSetup->addAttribute ( 'order', 'loviit_interest_fee_perc', $options );
		$salesSetup->addAttribute ( 'order', 'loviit_deposit', $options );
		$salesSetup->addAttribute ( 'order', 'loviit_discount', $options );
		
		$options = [ 
                    'type' => 'int',
                    'visible' => true,
                    'required' => false,
                    'default' => '0'
		];
                
		$salesSetup->addAttribute(  'order', 'loviit_order_request', $options);
        $salesSetup->addAttribute(  'order', 'loviit_delivery_request', $options);
        $salesSetup->addAttribute(  'order', 'loviit_creditnote_request', $options);
        
        $salesSetup->addAttribute(  'order', 'loviit_psp_pa', $options);
        $salesSetup->addAttribute(  'order', 'loviit_psp_cp', $options);
        $salesSetup->addAttribute(  'order', 'loviit_psp_db', $options);
        $salesSetup->addAttribute(  'order', 'loviit_psp_rf', $options);
        $salesSetup->addAttribute(  'order', 'loviit_psp_rv', $options);
                		
        $options = [
        		'type' => 'text',
        		'visible' => true,
        		'required' => false
        ];
        $salesSetup->addAttribute(  'order', 'loviit_uniqueid', $options);
        
		$setup->endSetup();
	}
}