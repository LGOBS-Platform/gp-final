<?php

namespace Loviit\Loviit\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\DB\Ddl\Table;

class UpgradeSchema implements UpgradeSchemaInterface {
	public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context) {

		$installer = $setup;

		$setup->startSetup ();
		if (version_compare ( $context->getVersion (), '1.4.10' ) < 0) {
			$installer->getConnection()->addColumn(
					$installer->getTable('sales_shipment'),
					'docomo_notified_tracking',
					[
							'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
							'nullable' => true,
							'comment' => 'Tracking number notified to Docomo'
					]
					);
		}

		if (version_compare ( $context->getVersion (), '1.5.0' ) <= 0) {
			$installer->getConnection()->addColumn(
					$installer->getTable('sales_creditmemo'),
					'loviit_administration_fee',
					[
							'type' => \Magento\Framework\DB\Ddl\Table::TYPE_FLOAT,
							'nullable' => true,
							'comment' => 'DOCOMO Administration fee refunded'
					]
					);

			//used only for keeping track on credit memo, automatically calculated when a new CN is created
			$installer->getConnection()->addColumn(
				$installer->getTable('sales_creditmemo'),
				'loviit_interest_fee',
				[
						'type' => \Magento\Framework\DB\Ddl\Table::TYPE_FLOAT,
						'nullable' => true,
						'comment' => 'DOCOMO Agio fee refunded'
				]
				);

			$installer->getConnection()->addColumn(
				$installer->getTable('sales_order'),
				'loviit_interest_fee_refunded',
				[
						'type' => \Magento\Framework\DB\Ddl\Table::TYPE_FLOAT,
						'nullable' => true,
						'comment' => 'DOCOMO Agio fee refunded'
				]
				);

			$installer->getConnection()->addColumn(
				$installer->getTable('sales_order'),
				'loviit_administration_fee_refunded',
				[
						'type' => \Magento\Framework\DB\Ddl\Table::TYPE_FLOAT,
						'nullable' => true,
						'comment' => 'DOCOMO Administration fee refunded'
				]
				);
		}

		if (version_compare ( $context->getVersion (), '1.5.3' ) <= 0) {
			$installer->getConnection()->addColumn(
				$installer->getTable('loviit_customer_registration'),
				'channel_id',
				[
					'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					'length' => 32,
					'nullable' => false,
					'comment' => 'Channel'
				]
				);

		}

		$setup->endSetup ();
	}
}
