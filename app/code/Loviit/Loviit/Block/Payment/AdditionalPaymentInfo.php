<?php

/**
 * Copyright � 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Loviit\Loviit\Block\Payment;

/**
 * Base payment information block
 */
class AdditionalPaymentInfo extends \Magento\Payment\Block\Info 
{

    protected function _prepareSpecificInformation($transport = null)
    {
        if (null === $this->_paymentSpecificInformation) {
            if (null === $transport) {
                $transport = new \Magento\Framework\DataObject();
            } elseif (is_array($transport)) {
                $transport = new \Magento\Framework\DataObject($transport);
            }
            $this->_paymentSpecificInformation = $transport;
        }

        $paymentMethod = $this->getMethod()->getCode();
        $parent = $this->getParentBlock();
        
        if ($parent!==false && preg_match("/^loviit(rp|rpiv|ic|rpcc|rpot|rpva|rppp)$/", $paymentMethod)) {
            
            $this->_order = $parent->getOrder();
            
            $numberOfInstallments = $this->_order->getData("number_of_installments");
                        
            if (isset($numberOfInstallments) && !empty($numberOfInstallments)) {
                $label = __('Number of installments')->render();
                $this->_paymentSpecificInformation[$label] = $numberOfInstallments;
            }
           
            if (preg_match("/^loviit(rp|rpcc|rpot|rpva|rppp)$/", $paymentMethod)) {
                $deposit = $this->_order->getData("loviit_deposit");
                if (!empty($deposit) && $deposit > 0) {
                    $label = __('Deposit')->render();
                    $this->_paymentSpecificInformation[$label] = $this->getFormattedPrice($deposit);
                }
            }
        }
        return $this->_paymentSpecificInformation;
    }
    
    
    public function getFormattedPrice($price)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $priceHelper = $objectManager->create('Magento\Framework\Pricing\Helper\Data');
        return $priceHelper->currency($price, true, false);
    }

}
