<?php

namespace Loviit\Loviit\Block\Payment;

/**
 * implement pixel in payment page for fraudcheck
 */
class Pixel extends \Magento\Framework\View\Element\Template {

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
            array $data = []
    ) {
        parent::__construct($context, $data);
    }

    public function getCid() {
        return $this->_scopeConfig->getValue('loviit/settings/fraudcheck_cid', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    public function getTid() {
        return 'CO'; //CheckOut ID
        //return $this->_scopeConfig->getValue('loviit/settings/fraudcheck_tid', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getSessionId() {
        if (!$this->getCid()) {
            $this->_session->setPixelSessionId(null);
            return null;
        }
        if (!$this->_session->getPixelSessionId()) {
            $prefix = $this->_scopeConfig->getValue('loviit/settings/fraudcheck_prefix', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $this->_session->setPixelSessionId($prefix.$this->_session->getSessionId()); // \Magento\Framework\Session\SessionManagerInterface
        }
        return $this->_session->getPixelSessionId();
    }

}
