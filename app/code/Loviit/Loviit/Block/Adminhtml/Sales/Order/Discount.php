<?php

namespace Loviit\Loviit\Block\Adminhtml\Sales\Order;

use \Magento\Sales\Model\Order;
use Loviit\Loviit\Helper\Payment;

class Discount extends \Magento\Framework\View\Element\Template {

    /**
     * @var Order
     */
    protected $_order;
    protected $_paymentHelper;
    protected $_quote;

    /**
     * @var \Magento\Framework\DataObject
     */
    protected $_source;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
    \Magento\Framework\View\Element\Template\Context $context, array $data = [], Payment $paymentHelper
    ) {
        parent::__construct($context, $data);
        $this->_paymentHelper = $paymentHelper;
    }

    public function getSource() {
        return $this->_source;
    }

    public function displayFullSummary() {
        return true;
    }

    public function initTotals() {
        $parent = $this->getParentBlock();
        $this->_order = $parent->getOrder();
        $this->_source = $parent->getSource();
       	
        $discountAmount = $this->_order->getData('loviit_discount');

        if ($discountAmount < 0) {
            $discount = new \Magento\Framework\DataObject(
                    [
                'code' => 'loviit_discount',
                'strong' => false,
                'value' => $discountAmount,
                'label' => __('Discount'),
                    ]
            );
            $parent->addTotal($discount, 'loviit_discount');
        }
        return $this;
    }

    /**
     * Get order store object
     *
     * @return \Magento\Store\Model\Store
     */
    public function getStore() {
        return $this->_order->getStore();
    }

    /**
     * @return Order
     */
    public function getOrder() {
        return $this->_order;
    }

    /**
     * @return array
     */
    public function getLabelProperties() {
        return $this->getParentBlock()->getLabelProperties();
    }

    /**
     * @return array
     */
    public function getValueProperties() {
        return $this->getParentBlock()->getValueProperties();
    }

}
