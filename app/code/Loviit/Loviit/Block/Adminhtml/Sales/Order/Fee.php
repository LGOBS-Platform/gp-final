<?php

namespace Loviit\Loviit\Block\Adminhtml\Sales\Order;

use \Magento\Sales\Model\Order;
use Loviit\Loviit\Helper\Payment;

class Fee extends \Magento\Framework\View\Element\Template {

    /**
     * @var Order
     */
    protected $_order;
    protected $_paymentHelper;

    /**
     * @var \Magento\Framework\DataObject
     */
    protected $_source;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
    \Magento\Framework\View\Element\Template\Context $context, array $data = [], Payment $paymentHelper
    ) {
        parent::__construct($context, $data);
        $this->_paymentHelper = $paymentHelper;
    }

    public function getSource() {
        return $this->_source;
    }

    public function displayFullSummary() {
        return true;
    }

    public function initTotals() {
        $parent = $this->getParentBlock();
        $this->_order = $parent->getOrder();
        $this->_source = $parent->getSource();

        $administrationFee = $this->_order->getData('loviit_administration_fee');

        if ($administrationFee > 0) {
            $feeAmount = new \Magento\Framework\DataObject(
                    [
                'code' => 'loviit_administration_fee',
                'strong' => false,
                'value' => $administrationFee,
                'label' => __('Administration Fee'),
                    ]
            );
            $parent->addTotal($feeAmount, 'loviit_administration_fee');
        }
        return $this;
    }

    /**
     * Get order store object
     *
     * @return \Magento\Store\Model\Store
     */
    public function getStore() {
        return $this->_order->getStore();
    }

    /**
     * @return Order
     */
    public function getOrder() {
        return $this->_order;
    }

    /**
     * @return array
     */
    public function getLabelProperties() {
        return $this->getParentBlock()->getLabelProperties();
    }

    /**
     * @return array
     */
    public function getValueProperties() {
        return $this->getParentBlock()->getValueProperties();
    }

}
