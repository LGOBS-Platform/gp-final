<?php

namespace Loviit\Loviit\Block\Adminhtml\Sales\Order\Creditmemo\Create;

use Loviit\Loviit\Model\LoviitPaymentMethodAbstract;

class Adjustments extends \Magento\Sales\Block\Adminhtml\Order\Creditmemo\Create\Adjustments
{
    
    protected function _toHtml()
    {
        $methodInstance = $this->getSource()->getOrder()->getPayment()->getMethodInstance();
        if($methodInstance instanceof LoviitPaymentMethodAbstract){
            //do not show adjustment and use custom template, adjustments cannot be handled from docomo
        }
        else{
            //use default template
            $this->setModuleName($this->extractModuleName('Magento\Sales\Block\Adminhtml\Order\Creditmemo\Create\Adjustments'));
        }
        return parent::_toHtml();
        
    }
}
