<?php

namespace Loviit\Loviit\Block\Adminhtml\Sales\Order\Creditmemo;

use Loviit\Loviit\Model\LoviitPaymentMethodAbstract;

class Totals extends \Magento\Sales\Block\Adminhtml\Order\Creditmemo\Totals
{

    
    
    protected function _initTotals()
    {
        parent::_initTotals();

        
        $methodInstance = $this->getCreditmemo()->getOrder()->getPayment()->getMethodInstance();
        if($methodInstance instanceof LoviitPaymentMethodAbstract){

            $total = new \Magento\Framework\DataObject(
                [
                    'code' => 'loviit_administration_fee',
                    'value' => $this->getCreditmemo()->getOrder()->getData('loviit_administration_fee')-$this->getCreditmemo()->getOrder()->getData('loviit_administration_fee_refunded'),
                    'label' => __('DOCOMO Administration Fee'),
                ]
            );
            $this->addTotal($total);

            $total = new \Magento\Framework\DataObject(
                [
                    'code' => 'loviit_interest_fee',
                    'value' => $this->getCreditmemo()->getData('loviit_interest_fee'),
                    'label' => __('DOCOMO Agio Fee'),
                ]
            );
            $this->addTotal($total);
            
        }
        return $this;
        
        
    }

    protected function _toHtml()
    {
        $this->setModuleName($this->extractModuleName('Magento\Sales\Block\Adminhtml\Order\Creditmemo\Totals'));
        return parent::_toHtml();
    }


}
