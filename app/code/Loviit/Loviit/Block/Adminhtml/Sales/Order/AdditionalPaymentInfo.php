<?php

namespace Loviit\Loviit\Block\Adminhtml\Sales\Order;

class AdditionalPaymentInfo extends \Magento\Backend\Block\Template {

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
    \Magento\Backend\Block\Template\Context $context, \Magento\Framework\Registry $registry, array $data = []
    ) {
        $this->registry = $registry;
        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    protected function _toHtml() {
        $order = $this->registry->registry('current_order');
        $paymentMethod = $order->getPayment()->getMethod();
        $html = '';
        if (preg_match("/^loviit(rp|rpiv|ic|rpcc|rpot|rpva|rppp)$/", $paymentMethod)) {
            $number_of_installments = $order->getData("number_of_installments");
            if (isset($number_of_installments) && !empty($number_of_installments)) {
                $html .= "<div class='order_payment_loviit_number_of_installments'>" . __('Number of installments') . ": " . $number_of_installments . "</div>";
            }

            if (preg_match("/^loviit(rp|rpcc|rpot|rpva|rppp)$/", $paymentMethod)) {
                $deposit = $order->getData("loviit_deposit");
                if (!empty($deposit) && $deposit > 0) {
                    $html .= "<div class='order_payment_loviit_deposit'>" . __('Deposit') . ": " . $order->formatPrice($deposit) . "</div>";
                }
            }
        }
        return $html;
    }

}
