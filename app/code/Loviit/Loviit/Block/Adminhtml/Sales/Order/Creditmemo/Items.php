<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
 namespace Loviit\Loviit\Block\Adminhtml\Sales\Order\Creditmemo;
 
 use Loviit\Loviit\Helper\Payment  as PaymentHelper;
 use Loviit\Loviit\Model\LoviitPaymentMethodAbstract;
/**
 * Adminhtml credit memo items grid
 */
class Items extends \Magento\Sales\Block\Adminhtml\Order\Creditmemo\Create\Items
{
  
    protected $_paymentHelper;


    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
     * @param \Magento\CatalogInventory\Api\StockConfigurationInterface $stockConfiguration
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Sales\Helper\Data $salesData
     * @param array $data
     * @param PaymentHelper $paymentHelper
     */
     public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\CatalogInventory\Api\StockConfigurationInterface $stockConfiguration,
        \Magento\Framework\Registry $registry,
        \Magento\Sales\Helper\Data $salesData,
        array $data = [],
        PaymentHelper $paymentHelper
    ) {
        $this->_paymentHelper = $paymentHelper;
        parent::__construct($context, $stockRegistry, $stockConfiguration, $registry,$salesData, $data);
    }

    //Necessary to add a new refund button, default online refund button cannot be displayed since no invoice is generated with docomo
    public function _prepareLayout()
    {
        $methodInstance = $this->getCreditmemo()->getOrder()->getPayment()->getMethodInstance();
        if($methodInstance instanceof LoviitPaymentMethodAbstract){
            //$this->unsetChild('submit_button');
            $this->addChild(
                'submit_button',
                'Magento\Backend\Block\Widget\Button',
                [
                    'label' => __('DOCOMO Online Refund'),
                    'class' => 'save submit-button refund primary',
                    'onclick' => 'disableElements(\'submit-button\');if ($("creditmemo_do_offline")) $("creditmemo_do_offline").value="0"; jQuery("#edit_form").triggerHandler("save");'
                ]
            );
        }

        return parent::_prepareLayout();
        
        
    }

    protected function _toHtml()
    {
        $this->setModuleName($this->extractModuleName('Magento\Sales\Block\Adminhtml\Order\Creditmemo\Create\Items'));
        return parent::_toHtml();
    }

}
