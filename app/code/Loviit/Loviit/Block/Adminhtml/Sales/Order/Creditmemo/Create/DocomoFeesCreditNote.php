<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Loviit\Loviit\Block\Adminhtml\Sales\Order\Creditmemo\Create;
 
use \Magento\Framework\Pricing\PriceCurrencyInterface;
use Loviit\Loviit\Model\LoviitPaymentMethodAbstract;

class DocomoFeesCreditNote extends \Magento\Backend\Block\Template
{
    /**
     * Source object
     *
     * @var \Magento\Framework\DataObject
     */
    protected $_source;

    /**
     * Tax config
     *
     * @var \Magento\Tax\Model\Config
     */
    protected $_taxConfig;

    /**
     * @var PriceCurrencyInterface
     */
    protected $priceCurrency;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Tax\Model\Config $taxConfig
     * @param PriceCurrencyInterface $priceCurrency
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Tax\Model\Config $taxConfig,
        PriceCurrencyInterface $priceCurrency,
        array $data = []
    ) {
        $this->_taxConfig = $taxConfig;
        $this->priceCurrency = $priceCurrency;
        parent::__construct($context, $data);
    }

    /**
     * Initialize creditmemo docomo fees totals
     *
     * @return $this
     */
    public function initTotals()
    {
        $parent = $this->getParentBlock();
        $this->_source = $parent->getSource();
        $parent->removeTotal('loviit_interest_fee');
        $total = new \Magento\Framework\DataObject([
            'code' => 'loviit_administration_fee', 
            'block_name' => $this->getNameInLayout()
        ]);
        $parent->addTotal($total);
        
        return $this;
    }

    /**
     * Get source object
     *
     * @return \Magento\Framework\DataObject
     */
    public function getSource()
    {
        return $this->_source;
    }

    /**
     * Get credit memo admin fee amount
     *
     * @return float
     */
    public function getAdministrationFee()
    {
        $source = $this->getSource(); //creditnote
        if(!is_null($source->getData('loviit_administration_fee'))){
            $adminFee=$source->getData('loviit_administration_fee');
        }
        else{
            $adminFee = $source->getOrder()->getData('loviit_administration_fee')-$source->getOrder()->getData('loviit_administration_fee_refunded');
        }
        
        return $this->priceCurrency->round($adminFee) * 1;
    }

    public function getInterestFee(){
        $source = $this->getSource(); //creditnote
        return $source->getOrder()->formatPrice($source->getData('loviit_interest_fee'));
    }


    public function getCreditMemoAdministrationFee()
    {
        $source = $this->getSource();
        return $source->getOrder()->formatPrice($source->getData('loviit_administration_fee'));
    }

    /**
     * Get label for shipping total based on configuration settings
     *
     * @return string
     */
    public function getAdministrationFeeLabel()
    {
        return __('DOCOMO Administration fee');
    }

    public function orderHasInterestFee(){
        $source = $this->getSource();
        return $source->getOrder()->getData('loviit_interest_fee')>0;
    }

    public function creditNoteHasInterestFee(){
        $source = $this->getSource();
        return $source->getData('loviit_interest_fee')>0;
    }

    public function orderHasAdministrationFee(){
        $source = $this->getSource();
        return $source->getOrder()->getData('loviit_administration_fee')>0;
    }

    public function creditNoteHasAdministrationFee(){
        $source = $this->getSource();
        return $source->getData('loviit_administration_fee')>0;
    }

    public function isDocomoMethod(){
        $source = $this->getSource();
        $methodInstance = $source->getOrder()->getPayment()->getMethodInstance();
        return $methodInstance instanceof LoviitPaymentMethodAbstract;
    }
}
