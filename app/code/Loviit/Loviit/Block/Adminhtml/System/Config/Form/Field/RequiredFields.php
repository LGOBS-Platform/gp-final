<?php

namespace Loviit\Loviit\Block\Adminhtml\System\Config\Form\Field;

use \Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;

/**
 * Backend system config array field renderer
 */
class RequiredFields extends AbstractFieldArray
{
    protected $config;
        
    /**
     * @var \Magento\Framework\Data\Form\Element\Factory
     */
    protected $_elementFactory;

    protected $_countryCollectionFactory;
    
    protected $_template = 'Loviit_Loviit::system/config/form/field/array.phtml';

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Data\Form\Element\Factory $elementFactory
     * @param \Magento\Framework\View\Design\Theme\LabelFactory $labelFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Data\Form\Element\Factory $elementFactory,
        \Magento\Directory\Model\ResourceModel\Country\CollectionFactory $countryCollectionFactory,
        array $data = []
    ) {
        $this->config = $context->getScopeConfig();
        $this->_elementFactory = $elementFactory;
        $this->_countryCollectionFactory = $countryCollectionFactory;
        parent::__construct($context, $data);
    }

    /**
     * Initialise form fields
     *
     * @return void
     */
    protected function _construct()
    {
        $this->addColumn('country', ['label' => __('Country')]);
        $this->addColumn('taxid', ['label' => __('Tax id')]);
        $this->addColumn('gender', ['label' => __('Gender')]);
        $this->addColumn('birthday', ['label' => __('Data of Birth')]);
        $this->addColumn('bic', ['label' => __('BIC')]);
        $this->addColumn('bank_account_opening_date', ['label' => __('Bank Account Opening Date')]);
 
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add');
        parent::_construct();
    }

    /**
     * Render array cell for prototypeJS template
     *
     * @param string $columnName
     * @return string
     */
    public function renderCellTemplate($columnName)
    {
        if (in_array($columnName, array('taxid', 'gender', 'birthday', 'bic','bank_account_opening_date')) && isset($this->_columns[$columnName])) {
            $element = $this->_elementFactory->create('select');
            $element->setForm(
                $this->getForm()
            )->setName(
                $this->_getCellInputElementName($columnName)
            )->setHtmlId(
                $this->_getCellInputElementId('<%- _id %>', $columnName)
            )->setValues(
                array('0' => __('No'), '1' => __('Yes'))
            );
            
            if (!$this->loviitConfigurationIsEditable()) {
                $element->setDisabled('disabled');
            }
            return str_replace("\n", '', $element->getElementHtml());
        }
        if ($columnName == 'country' && isset($this->_columns[$columnName])) {
            $options = $this->_countryCollectionFactory->create()->loadByStore()->toOptionArray(); 
            
            $element = $this->_elementFactory->create('select');
            $element->setForm(
                $this->getForm()
            )->setName(
                $this->_getCellInputElementName($columnName)
            )->setHtmlId(
                $this->_getCellInputElementId('<%- _id %>', $columnName)
            )->setValues(
                $options
            );
            
            if (!$this->loviitConfigurationIsEditable()) {
                $element->setDisabled('disabled');
            }
            
            return str_replace("\n", '', $element->getElementHtml());
        }

        return parent::renderCellTemplate($columnName);
    }
    
    /**
     * are loviit configuration fields configurable.
     *
     * @return boolean
     */
    public function loviitConfigurationIsEditable()
    {
        $editable = $this->config->getValue('loviit/settings/configuration_editable', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        
        if (isset($editable) && !$editable) {
            return false;
        }
        return true;
    }
}