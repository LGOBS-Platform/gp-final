<?php

namespace Loviit\Loviit\Block\Adminhtml\System\Config\Form\Field;

use \Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;

/**
 * Backend system config array field renderer
 */
class InstallmentPlan extends AbstractFieldArray
{
    
    protected $config;
    
    protected $_elementFactory;
    
    protected $_template = 'Loviit_Loviit::system/config/form/field/array.phtml';
        
    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param array $data
     */
    public function __construct(
            \Magento\Framework\Data\Form\Element\Factory $elementFactory,
        \Magento\Backend\Block\Template\Context $context,
        array $data = []
    ) {
        $this->config = $context->getScopeConfig();
        $this->_elementFactory = $elementFactory;
        parent::__construct($context, $data);
    }

    /**
     * Initialise form fields
     *
     * @return void
     */
    protected function _construct()
    {
        $this->addColumn('number_of_installments', ['label' => __('Number of installments'), 'style' => 'width:100px']);
        $this->addColumn('interest', ['label' => __('Agio Fee'). "(%)", 'style' => 'width:100px']);
        $this->addColumn('fee', ['label' => __('Administration Fee'), 'style' => 'width:100px']);
        $this->addColumn('discount', ['label' => __('Discount'), 'style' => 'width:100px']);

        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add');    
        
        parent::_construct();
    }

    /**
     * Render array cell for prototypeJS template
     *
     * @param string $columnName
     * @return string
     */
    public function renderCellTemplate($columnName)
    {
        $element = $this->_elementFactory->create('text');
        $element->setForm(
            $this->getForm()
        )->setName(
            $this->_getCellInputElementName($columnName)
        )->setHtmlId(
            $this->_getCellInputElementId('<%- _id %>', $columnName)
        );

        if (!$this->loviitConfigurationIsEditable()) {
            $element->setDisabled('disabled');
        }
        return str_replace("\n", '', $element->getElementHtml());
    }
    
    /**
     * are loviit configuration fields configurable.
     *
     * @return boolean
     */
    public function loviitConfigurationIsEditable()
    {
        $editable = $this->config->getValue('loviit/settings/configuration_editable', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        if (isset($editable) && !$editable) {
            return false;
        }
        return true;
    }
    
}