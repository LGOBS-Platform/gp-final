<?php
namespace Loviit\Loviit\Block\Adminhtml\System\Config\Form\Field;

use \Magento\Framework\Data\Form\Element\AbstractElement;
use \Magento\Config\Block\System\Config\Form\Field;
use \Magento\Backend\Block\Template\Context;
use \Magento\Framework\Module\ModuleListInterface;

class Disable extends Field
{    
    
    protected $config;
    
    protected $moduleList;
    
    /**
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        ModuleListInterface $moduleList,
        Context $context,
        array $data = []
        
    ) {
        $this->config = $context->getScopeConfig();
        $this->moduleList = $moduleList;
        parent::__construct($context, $data);
    }
    
    
    protected function _getElementHtml(AbstractElement $element)
    {
        $editable = $this->config->getValue('loviit/settings/configuration_editable', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        
        $disabledFields = array('loviit_settings_configuration_version', 'loviit_settings_plugin_version');
        $elementId = $element->getId();
        
        if ((isset($editable) && !$editable) || in_array($elementId, $disabledFields)) {
            $element->setDisabled('disabled');
        }
        
        if ($elementId == 'loviit_settings_plugin_version') {
            $element->setValue($this->getVersion());
        }
        
        
        return $element->getElementHtml();

    }
    
    public function getVersion()
    {
        return $this->moduleList
            ->getOne('Loviit_Loviit')['setup_version'];
    }
}