<?php
namespace Loviit\Loviit\Block\Onepage;

class Failure extends \Magento\Checkout\Block\Onepage\Failure
{

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Checkout\Model\Session $checkoutSession,
        array $data = []
    ) {
        parent::__construct($context, $checkoutSession,$data);
    }

    /**
     * @return mixed
     */
    public function getRealOrderId()
    {
        return false;
    }

    /**
     *  Payment custom error message
     *
     * @return string
     */
    public function getErrorMessage()
    {
        $error = $this->_checkoutSession->getErrorMessage();
        return $error;
    }

    /**
     * Continue shopping URL
     *
     * @return string
     */
    public function getContinueShoppingUrl()
    {
        return $this->getUrl('checkout/cart');
    }
    
    public function getCheckoutUrl()
    {
    	return $this->getUrl('checkout');
    }
}
