<?php

namespace Loviit\Loviit\Block\Onepage;

use \Magento\Framework\View\Element\Template\Context;
use \Magento\Checkout\Model\Session;
use \Magento\Sales\Model\Order\Config;
use \Magento\Framework\App\Http\Context as HttpContext;
use Loviit\Loviit\Helper\Payment;
use Magento\Sales\Model\Order;

class Success extends \Magento\Checkout\Block\Onepage\Success {

    protected $paymentHelper;
    
    public function __construct(
        Context $context,
        Session $checkoutSession,
        Config $orderConfig,
        HttpContext $httpContext,
        Payment $paymentHelper
    )
    {
        parent::__construct($context, $checkoutSession, $orderConfig, $httpContext, array());
        $this->paymentHelper = $paymentHelper;
    }
    
    public function getOrder() {
        return $this->_checkoutSession->getLastRealOrder();
    }
    
    public function isOrderOnHold() {
    	return $this->_checkoutSession->getLastRealOrder()->getStatus()==Order::STATE_PROCESSING;
    }
    
    
    public function getUniqueIdSubstring() {
    	$order = $this->getOrder();
    	return $order->getData('loviit_uniqueid');
    }
    
    public function getLoviitDeposit() {
    	$order = $this->getOrder();
    	return $this->paymentHelper->getFormattedPrice($order->getData('loviit_deposit'));
    }
    
    public function getErrorMessage()
    {
    	$error = $this->_checkoutSession->getErrorMessage();
    	return empty($error) ? '' : $error;
    }

}