<?php
namespace Loviit\Loviit\Block\Loviit\Payment\Finish;
  
use \Magento\Framework\Registry;
use \Magento\Framework\View\Element\Template\Context;
use \Magento\Framework\Locale\ResolverInterface;
use Loviit\Loviit\Helper\Payment;
use Loviit\Loviit\Helper\Payon;
use \Magento\Checkout\Model\Session;
use \Magento\Framework\App\Config\ScopeConfigInterface;

class Main extends \Magento\Framework\View\Element\Template
{   
    protected $_coreRegistry;
    
    protected $_localeResolver;

    protected $_checkoutSession;
    

    public function __construct(
        Context $context,
        Registry $coreRegistry,
        Payment $paymentHelper,
        Payon $payonHelper,
        ResolverInterface $localeResolver,
        Session $checkoutSession
    )
    {
        parent::__construct($context);
        $this->paymentHelper = $paymentHelper;
        $this->payonHelper = $payonHelper;
        $this->_coreRegistry = $coreRegistry;
        $this->_localeResolver = $localeResolver;
        $this->_checkoutSession = $checkoutSession;
    }
    protected function _prepareLayout()
    {

        $quote = $this->_checkoutSession->getQuote();
        $payment = $quote->getPayment()->getMethodInstance();

        $this->setPaymentCode($this->_coreRegistry->registry('paymentCode'));
        $this->setCheckoutId($this->_coreRegistry->registry('checkoutId'));
        $brands = $payment->getBrand();
        error_log("empty '$brands'");
        $this->setPaymentBrand($brands);
        $this->setHasBrands(!empty($brands));
        $this->setCountry(strtolower(\Locale::getRegion($this->_localeResolver->getLocale())));
        $this->setBillingCountry($quote->getBillingAddress()->getCountry());
        $this->setPaymentUrlBase($this->payonHelper->getUrl());

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->get('Magento\Customer\Model\Session');
        $this->setIsUserLoggedIn($customerSession->isLoggedIn());
        $this->setSaveCreditCardToken($this->_scopeConfig->getValue ( 'loviit/settings/save_cc_token', \Magento\Store\Model\ScopeInterface::SCOPE_STORE ));
        $this->setLegalAddressForCC($this->_scopeConfig->getValue ( 'loviit/settings/legal_address', \Magento\Store\Model\ScopeInterface::SCOPE_STORE ));
        

        //check CC expiry date
        $isPSP = $payment->isPSP();
        if(!$isPSP){
            $numberOfInstallments = $quote->getPayment()->getData ('number_of_installments');
            //if installment with deposit or non installment method check if the date is greater than 1 month from now
            if(!isset($numberOfInstallments) || empty($numberOfInstallments) || $this->_checkoutSession->getQuote()->getData ( 'loviit_deposit' )>0 ){
                $numberOfInstallments=0;
            }

            //always add one month to all payments to allow first capture
            $numberOfInstallments+=1;
            
            //minimum expiration date
            $now = new \DateTime('now');
            $month = $now->format('n');
            $year = $now->format('y');

            $month=$month+$numberOfInstallments;
            
            $this->setMinimumExpiryDateInMonths($month+12*(2000+$year));

            while($month>12){
                $year++;
                $month-=12;
            }
            $month=sprintf("%02d", $month);
    
            if($numberOfInstallments==1) {//non installments
                $this->setExpiryDateMessage(__('Expiry Date must be equal or greater than %1/%2',$month,$year));
            }
            else{
                $this->setExpiryDateMessage(__('Expiry Date is before the end of the installment plan. Please use a card with expiry date equal or greater than %1/%2',$month,$year));
            }

        }
        $this->setExecuteExpiryDateCheck(!$isPSP);
        
        
    }
}
