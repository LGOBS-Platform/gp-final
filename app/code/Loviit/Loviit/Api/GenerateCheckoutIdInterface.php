<?php
namespace Loviit\Loviit\Api;
/**
 * Interface GenerateCheckoutIdInterface
 * @package Loviit\Loviit\Api
 */
interface GenerateCheckoutIdInterface
{
    /**
     * return checkoutid for secure payment gateway
     *
     * @param int $cartId
     * @return string
     */
    public function getCheckoutId($cartId);
    
    /**
     * return checkoutid for secure payment gateway using masked id
     *
     * @param string $maskedId
     * @return string
     */
    public function getCheckoutIdGuestCart($maskedId);
    
}