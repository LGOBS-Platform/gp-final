<?php
namespace Loviit\Loviit\Api;
/**
 * Interface InstallmentsPlansAPIInterface
 * @package Loviit\Loviit\Api
 * @api
 */
interface InstallmentsPlansAPIInterface
{
	/**
	 * calculate installment plans for the given payment method for logged in user
	 *
	 * @param int $cartId
	 * @param \Magento\Quote\Api\Data\PaymentInterface $paymentMethod
	 * @return array
	 */
	public function getPlans($cartId,$paymentMethod);//:array;
	
    /**
     * calculate installment plans for the given payment method for guest user
     *
     * @param string $maskedId
     * @param \Magento\Quote\Api\Data\PaymentInterface $paymentMethod
     * @return array
     */
    public function getPlansGuestCart($maskedId,$paymentMethod);//:array;
    
    
}