<?php
namespace Loviit\Loviit\Api;
/**
 * Interface ProcessSecurePaymentResponseInterface
 * @package Loviit\Loviit\Api
 * @api
 */
interface ProcessSecurePaymentResponseInterface
{
    /**
     * placeOrder after transaction on payment gateway
     *
     * @param int $cartId
     * @param string $id
     * @param string $resourcePath
     * @return \Loviit\Loviit\Model\Resource\BasicRestResponseObject
     */
    public function placeOrder($cartId,$id,$resourcePath); //:\Loviit\Loviit\Model\Resource\BasicRestResponseObject;
    
    /**
     * placeOrder after transaction on payment gateway
     *
     * @param string $maskedId
     * @param string $id
     * @param string $resourcePath
     * @return \Loviit\Loviit\Model\Resource\BasicRestResponseObject
     */
    public function placeOrderGuestCart($maskedId,$id,$resourcePath); //:\Loviit\Loviit\Model\Resource\BasicRestResponseObject;
    
    
}