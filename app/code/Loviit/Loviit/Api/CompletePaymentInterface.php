<?php
namespace Loviit\Loviit\Api;
/**
 * Interface CompletePaymentInterface
 * @package Loviit\Loviit\Api
 */
interface CompletePaymentInterface
{
    /**
     * complete server to server payment
     *
     * @param int $cartId
     * @param mixed $additionalData
     * @return \Loviit\Loviit\Model\Resource\BasicRestResponseObject
     */
    public function completePayment($cartId,$additionalData); //:\Loviit\Loviit\Model\Resource\BasicRestResponseObject;
    
    /**
     * complete server to server payment with maskedId
     *
     * @param string $maskedId
     * @param mixed $additionalData
     * @return \Loviit\Loviit\Model\Resource\BasicRestResponseObject
     */
    public function completePaymentGuestCart($maskedId,$additionalData); //:\Loviit\Loviit\Model\Resource\BasicRestResponseObject;
    
}