<?php
namespace Loviit\Loviit\Api;
/**
 * Interface SendLoviitRequestInterface
 * @package Loviit\Loviit\Api
 * @api
 */
interface SendLoviitRequestInterface
{
    /**
     * sendOrderRequest
     *
     * @param int $orderId
     * @return \Loviit\Loviit\Model\Resource\BasicRestResponseObject
     */
    public function sendOrderRequest($orderId);//:\Loviit\Loviit\Model\Resource\BasicRestResponseObject;
    
    /**
     * sendDeliveryRequest
     *
     * @param int $orderId
     * @param string $trackingCode
     * @return \Loviit\Loviit\Model\Resource\BasicRestResponseObject
     */
    public function sendDeliveryRequest($orderId,$trackingCode);//:\Loviit\Loviit\Model\Resource\BasicRestResponseObject;
    
    /**
     * sendCreditNote
     *
     * @param int $orderId
     * @return \Loviit\Loviit\Model\Resource\BasicRestResponseObject
     */
    public function sendCreditNote($orderId);//:\Loviit\Loviit\Model\Resource\BasicRestResponseObject;
    
    
}