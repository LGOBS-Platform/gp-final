<?php
namespace Loviit\Loviit\Api;

interface BasicRestResponseObjectInterface{

    /**
     * @return mixed
     */
	public function getOrderId();

    /**
     * @return mixed
     */
	public function getMessage();

    /**
     * @return mixed
     */
	public function getSuccess();
}