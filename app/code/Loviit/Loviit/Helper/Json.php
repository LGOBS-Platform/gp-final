<?php

namespace Loviit\Loviit\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Psr\Log\LoggerInterface;

use Loviit\Loviit\Helper\Payment;

/**
 * Class which handles request and response to payon
 *
 * @author tim.zwinkels
 */
class Json extends AbstractHelper {
    
    private $config;
    
    protected $_logger;
    
    protected $paymentHelper;
    
    var $errorMessage;
       
    var $demo_url = 'http://stagingservices.fine-trade.org';
    
    var $live_url = 'http://services.fine-trade.org';
    
    public function __construct(
            LoggerInterface $logger,
            ScopeConfigInterface $config,
            Payment $paymentHelper
            ) {
        $this->_logger = $logger;
        $this->config = $config;
        $this->paymentHelper = $paymentHelper;
    }
    
    /**
     * Confirm and get the status of a checkout on payon with curl
     *
     * @param Array $user
     * @param String $resourcePath
     * @return Object
     * @throws Exception
     */
    public function doJsonCall($userData, $route)
    {
        try {
            $url = $this->demo_url;
            $transactionmode = $this->config->getValue('loviit/settings/transaction_mode', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            
            if ($transactionmode == "LIVE") {
                $url = $this->live_url;
            }
            $url .= $route;
            
            $ch = curl_init($url);
            //curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
            curl_setopt($ch, CURLOPT_FAILONERROR, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($userData));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                                                    'Content-Type: application/json',
                                                    'Content-Length: ' . strlen(json_encode($userData)))
                                            );
            $responseData = curl_exec($ch);
            
            $this->paymentHelper->loviitLogger("Url: " . $url);
            $this->paymentHelper->loviitLogger("Request: ".print_r($userData,1));
            $this->paymentHelper->loviitLogger("Response: ".print_r($responseData,1));
            
            if (curl_errno($ch)) {
                $this->errorMessage = curl_error($ch);
            }
            curl_close($ch);
                                
        } catch (\Exception $ex) {
            $this->errorMessage = $ex->getMessage();
        }
        return json_decode($responseData, true);
    }
    
    public function isGoodCall($result)
    {
        if(isset($result['success']) && $result['success'] == true){
            return true;
        } else if (isset($result['message'])) {
            $this->errorMessage = $result['message'];
        }
        return false;
    }
}
