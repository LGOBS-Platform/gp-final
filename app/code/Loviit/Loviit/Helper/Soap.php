<?php

namespace Loviit\Loviit\Helper;

use \Psr\Log\LoggerInterface;
use \Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Class which handle the soap request to fine trade platform
 *
 * @author tim.zwinkels
 */
class Soap {
    
    private $config;
    
    protected $_logger;
    
    protected $paymentHelper;
    
    var $live_soap_url = 'http://services.fine-trade.org/PayOnOrderHandling.svc?WSDL';
    var $demo_soap_url = 'http://stagingservices.fine-trade.org/PayOnOrderHandling.svc?WSDL';
    
    public $lastCode = '';
    
    public $lastError = '';
    public $lastErrorCode = '';
    
    const CODE_WAITING_FOR_PREPAYMENT = '111.111.117';
    const CODE_TRANSACTION_NOT_FOUND = '111.111.123';
    const CODE_OR_ALREADY_SUBMITTED = '111.111.115';
    
    /**
     * Constructor
     *
     * @param ScopeConfigInterface $config
     * @param LoggerInterface $logger
     */
    public function __construct(
        ScopeConfigInterface $config,
        LoggerInterface $logger
    ) {
        $this->config = $config;
        $this->_logger = $logger;
    }
    
    /**
     * prepare action and doSoapCall
     *
     * @param string $action
     * @param array $orderData
     * @return array $result
     */
    public function doSoap($action, $orderData,$paymentHelper=null,$order = null)
    {
        $data = $this->$action($orderData);
        $data['identification'] = $this->getIdentification($order);
        $result = $this->doSoapCall($action, $data,$paymentHelper);

        return $result;
    }
    
    /**
     * do soap call
     *
     * @param string $action
     * @param array $orderData
     * @return Object $result
     */
    public function doSoapCall($action, $reqData, $paymentHelper) 
    {
        $url = $this->demo_soap_url;
        $transactionmode = $this->config->getValue('loviit/settings/transaction_mode', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        ini_set('default_socket_timeout', 60);
        if ($transactionmode == "LIVE") {
            $url = $this->live_soap_url;
            ini_set('default_socket_timeout', 30);
        }
        
        if($paymentHelper!=null){
	        $paymentHelper->loviitLogger("SOAP $action call to ".$url);
	        $paymentHelper->loviitLogger("Request: ".print_r($reqData,1));
        }

        try {
            $client = new \SoapClient($url);
            $result = $client->$action($reqData);
            
            if($paymentHelper!=null)
            	$paymentHelper->loviitLogger("Response: ".print_r($result, 1));
            
        } catch (Exception $e) {
            return $e->getMessage();
        }
        return $result;
    }
    
    /**
     * verify the result of soap request
     *
     * @param Object $result
     * @return boolean 
     */
    public function isGoodCall($result) {
        $obj = '';
        if (!empty($result->SubmitNewOrderResult)) {
            $obj = $result->SubmitNewOrderResult;
        }
        if (!empty($result->SubmitNewDeliveryResult)) {
            $obj = $result->SubmitNewDeliveryResult;
        }
        if (!empty($result->SubmitNewCreditResult)) {
            $obj = $result->SubmitNewCreditResult;
        }
        if (!empty($result->SubmitTrackingInformationResult)) {
        	$obj = $result->SubmitTrackingInformationResult;
        }
        if (empty($obj)) {
            $this->lastError = $result;
            $this->lastErrorCode = '999.999.999';
            return false;
        }

        $this->lastErrorCode = $this->lastCode = $obj->Code;
        if ($obj->Code == '000.000.000' || $obj->Code == self::CODE_WAITING_FOR_PREPAYMENT) { //waiting for prepayment
            return true;
        } else {
            $this->lastError = $obj->Description;
        }
        return false;
    }
    
    
    private function submitNewOrder($oDat)
    {
        $reqData = array(
            'order' => $this->getOrderRequest($oDat),
            'importIfPending' => true,
            'IBANRequired' => false,
        );
        return $reqData;
    }

    private function submitNewDelivery($oDat)
    {
        $reqData = array(
            'delivery' => $this->getDeliveryRequest($oDat),
        );
        return $reqData;
    }
    
    private function SubmitTrackingInformation($oDat)
    {
    	$reqData = array(
    			'tracking' => $this->getTrackingRequest($oDat),
    	);
    	return $reqData;
    }

    private function submitNewCredit($oDat)
    {
        $reqData = array(
            'credit' => $this->getCreditRequest($oDat),
        );
        return $reqData;
    }
    
    private function getIdentification($order = null)
    {
        return array(
            'UserName' => $this->config->getValue('loviit/settings/soap_user',  \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $order !=null ? $order->getStore()->getCode() : null),
            'Password' => $this->config->getValue('loviit/settings/soap_pw',  \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $order !=null ? $order->getStore()->getCode() : null),
        );
    }

    private function getOrderRequest($oDat, $data = array())
    {
        $data = $this->getBaseOrderRequest($oDat, $data);
        $data['OrderRequestDate'] = $oDat['OrderRequestDate'];
        return $data;
    }

    private function getBaseOrderRequest($oDat, $data)
    {
        //$data['AdditionalPaymentReferenceId'] = $oDat['AdditionalPaymentReferenceId'];
        $data['AdministrationFee'] = isset($oDat['AdministrationFee']) ? $oDat['AdministrationFee'] : 0;
        $data['Currency'] = $oDat['Currency'];
        $data['CustomerId'] = $oDat['CustomerId'];
        $data['FirstName'] = $oDat['FirstName'];
        $data['LastName'] = $oDat['LastName'];
        $data['Items'] = array();
        $data['OrderRequestId'] = $oDat['OrderRequestId'];
        $data['ShippingCosts'] = $oDat['ShippingCosts'];
        $data['Tax'] = $oDat['Tax'];
        $data['Total'] = $oDat['Total'];
        $data['TransportationCosts'] = $oDat['TransportationCosts'];
        $data['UniqueId'] = $oDat['UniqueId'];
        return $this->getOrderItems($oDat, $data);
    }

    private function getOrderItems($oDat, $data)
    {
        foreach ($oDat['Items'] AS $v) {
            $orderItem = array(
                'ArticleNumber' => $v['ArticleNumber'],
                'Description' => $v['Description'],
                'Price' => $v['Price'],
                'Quantity' => $v['Quantity'],
                'SerialNumber' => $v['SerialNumber'],
                'Tax' => $v['Tax'],
            );
            if ($v['DiscountAmount'] > 0) {
                $orderItem['DiscountAmount'] = $v['DiscountAmount'];
            }
            if ($v['DiscountPercentage'] > 0) {
                $orderItem['DiscountPercentage'] = $v['DiscountPercentage'];
            }
            $data['Items'][] = $orderItem;
        }
        return $data;
    }

    private function getDeliveryRequest($oDat, $data = array())
    {
        $data = $this->getBaseOrderRequest($oDat, $data);
        if (!empty($oDat['Carrier']))
            $data['Carrier'] = $oDat['Carrier'];
        if (!empty($oDat['DeliveryDate']))
            $data['DeliveryDate'] = $oDat['DeliveryDate'];
        if (!empty($oDat['DeliveryId']))
            $data['DeliveryId'] = $oDat['DeliveryId'];
        if (!empty($oDat['IBAN']))
            $data['IBAN'] = $oDat['IBAN'];
        if (!empty($oDat['InvoiceNumber']))
            $data['InvoiceNumber'] = $oDat['InvoiceNumber'];
        if (!empty($oDat['InvoiceNumberExternal']))
            $data['InvoiceNumberExternal'] = $oDat['InvoiceNumberExternal'];
        if (!empty($oDat['TrackingNumber']))
            $data['TrackingNumber'] = $oDat['TrackingNumber'];
		return $data;
	}
	
	private function getTrackingRequest($oDat, $data = array())
    {
        $data = $this->getBaseOrderRequest($oDat, $data);
        if (!empty($oDat['Carrier']))
            $data['Carrier'] = $oDat['Carrier'];
        if (!empty($oDat['TrackingDate']))
            $data['TrackingDate'] = $oDat['TrackingDate'];
        if (!empty($oDat['TrackingNumber']))
            $data['TrackingNumber'] = $data['TrackingId'] = $oDat['TrackingNumber'];
		return $data;
	}

    private function getCreditRequest($oDat, $data = array())
    {
        $data = $this->getBaseOrderRequest($oDat, $data);
        if (!empty($oDat['CreditDate']))
            $data['CreditDate'] = $oDat['CreditDate'];
        if (!empty($oDat['CreditId']))
            $data['CreditId'] = $oDat['CreditId'];
        if (!empty($oDat['InvoiceNumber']))
            $data['InvoiceNumber'] = $oDat['InvoiceNumber'];
        if (!empty($oDat['InvoiceNumberExternal']))
            $data['InvoiceNumberExternal'] = $oDat['InvoiceNumberExternal'];
        return $data;
    }
}
