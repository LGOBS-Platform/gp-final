<?php
namespace Loviit\Loviit\Helper;

use \Magento\Framework\App\Helper\Context;
use \Magento\Sales\Api\Data\OrderInterface;
use \Magento\Sales\Model\ResourceModel\Order\Tax\Item;
use \Magento\Tax\Model\Config;

class Tax extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\Tax\Item
     */
    protected $taxItem;
    protected $taxConfig;

    public function __construct(
        Context $context,
        Item $taxItem,
        Config $taxConfig
    ) {
        parent::__construct($context);
        $this->taxItem = $taxItem;
        $this->taxConfig = $taxConfig;
    }

    public function getTaxItems(OrderInterface $order)
    {
        $tax_items = $this->taxItem->getTaxItemsByOrderId($order->getId());
        return $tax_items;
    }

    public function getShippingTaxPercent($order)
    {
        $shippingTaxClass = $this->taxConfig->getShippingTaxClass($order->getStore()->getCode());
        if($shippingTaxClass==null){
            return 0;
        }

        if($order instanceof OrderInterface){ //Order

            $useNewLogics = $order->getPayment()->getAdditionalInformation('new_general_tax_logics');

            if(!$useNewLogics){
                $orderItems = $order->getAllItems ();
                if( $orderItems[0] instanceof \Magento\Sales\Model\Order\Creditmemo\Item){
                    return  $orderItems[0]->getOrderItem()->getTaxPercent ();
                }
                else{
                    return $orderItems [0]->getTaxPercent ();
                }
            }
            $tax_items = $this->taxItem->getTaxItemsByOrderId($order->getId());
            if (is_array($tax_items)) {
                foreach ($tax_items as $taxItem) {
                    if ($taxItem['taxable_item_type'] === 'shipping') {
                        return $taxItem['tax_percent'];
                    }
                }
            }
        }
        else{ //Quote
            
            $shippingAppliedTaxes = $order->getShippingAddress ()->getAppliedTaxes();
            
            if (is_array($shippingAppliedTaxes)) {
                $productTaxPercent = 0;
                foreach ($shippingAppliedTaxes as $taxItem) {
                    if ($taxItem['item_type'] === 'shipping') {
                        return $taxItem['percent'];
                    }
                    else $productTaxPercent=$taxItem['percent'];
                }
                return $productTaxPercent;
            }
        }
        return 0;
    }
}
