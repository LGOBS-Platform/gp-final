<?php

namespace Loviit\Loviit\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;
use \Magento\Quote\Model\Quote;
use \Magento\Quote\Api\CartManagementInterface;
use \Magento\Sales\Model\Order;
use \Magento\Checkout\Helper\Data;
use \Magento\Checkout\Model\Type\Onepage;
use \Magento\Customer\Model\Group;
use \Magento\Customer\Model\Session;
use \Magento\Customer\Model\CustomerFactory;
use \Magento\Checkout\Model\Session as CheckoutSession;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Framework\Module\ResourceInterface;
use \Magento\Store\Model\StoreManagerInterface;
use \Psr\Log\LoggerInterface;
use Loviit\Loviit\Model\RegistrationFactory;
use \Magento\Quote\Api\CartRepositoryInterface;
use Loviit\Loviit\Helper\OrderPlace;
use Loviit\Loviit\Helper\Soap;
use Loviit\Loviit\Helper\Tax;
use Loviit\Loviit\Exception\LoviitException;
use \Magento\Framework\Exception\CouldNotSaveException;
use \Magento\Sales\Model\Order\Email\Sender\OrderSender;
use \Magento\Framework\Session\SessionManagerInterface;
use \Magento\Framework\Phrase;

/**
 * Class which contain many important function, the raw data is prepared for display, to be used in soap/curl request ect.
 *
 * @author tim.zwinkels
 */
class Payment extends AbstractHelper {

	/**
	 * @var OrderSender
	 */
	protected $orderSender;


	/**
	 *
	 * @var StoreManagerInterface
	 */
	protected $storeManager;

	/**
	 *
	 * @var CartManagementInterface
	 */
	private $cartManagement;

	/**
	 *
	 * @var Session
	 */
	private $customerSession;

	/**
	 *
	 * @var Data
	 */
	private $checkoutHelper;
	private $checkoutSession;
	private $config;
	protected $_logger;
	var $paymentCode;
	var $paymentMethod;
	var $discountDifference;
	var $disagioItemDiscount;
	var $disagioTotalDiscount;
	protected $customerFactory;
	protected $registrationFactory;
	protected $quoteRepository;
	protected $soapHelper;
        protected $taxHelper;
        protected $coreSession;


	/**
	 *
	 * @var \Magento\Framework\Module\ResourceInterface
	 */
	protected $moduleResource;

	/**
	 * Constructor
	 *
	 * @param CartManagementInterface $cartManagement
	 * @param Session $customerSession
	 * @param Data $checkoutHelper
	 * @param ScopeConfigInterface $config
	 * @param LoggerInterface $logger
	 * @param ResourceInterface $moduleResource
	 * @param CustomerFactory $customerFactory
	 * @param StoreManagerInterface $storeManager
	 */
	public function __construct(
			CartManagementInterface $cartManagement,
			Session $customerSession,
			Data $checkoutHelper,
			ScopeConfigInterface $config,
			LoggerInterface $logger,
			ResourceInterface $moduleResource,
			CustomerFactory $customerFactory,
			StoreManagerInterface $storeManager,
			CheckoutSession $checkoutSession,
			RegistrationFactory $registrationFactory,
			CartRepositoryInterface $quoteRepository,
			OrderPlace $orderPlace,
			Soap $soapHelper,
            Tax $taxHelper,
			Order $order,
			OrderSender $orderSender = null,
                        SessionManagerInterface $coreSession) {
		$this->cartManagement = $cartManagement;
		$this->customerSession = $customerSession;
		$this->checkoutHelper = $checkoutHelper;
		$this->config = $config;
		$this->_logger = $logger;
		$this->moduleResource = $moduleResource;
		$this->customerFactory = $customerFactory;
		$this->storeManager = $storeManager;
		$this->checkoutSession = $checkoutSession;
		$this->registrationFactory = $registrationFactory;
		$this->quoteRepository = $quoteRepository;
		$this->orderPlace = $orderPlace;
		$this->soapHelper = $soapHelper;
        $this->taxHelper = $taxHelper;
		$this->order = $order;
		$this->orderSender = $orderSender;
        $this->coreSession = $coreSession;
	}

	/**
	 * Get checkout method
	 *
	 * @param Quote $quote
	 * @return string
	 */
	private function getCheckoutMethod(Quote $quote) {
		if ($this->customerSession->isLoggedIn ()) {
			return Onepage::METHOD_CUSTOMER;
		}
		if (! $quote->getCheckoutMethod ()) {
			if ($this->checkoutHelper->isAllowedGuestCheckout ( $quote )) {
				$quote->setCheckoutMethod ( Onepage::METHOD_GUEST );
			} else {
				$quote->setCheckoutMethod ( Onepage::METHOD_REGISTER );
			}
		}

		return $quote->getCheckoutMethod ();
	}

	/**
	 * Prepare quote for guest checkout order submit
	 *
	 * @param Quote $quote
	 * @return void
	 */
	private function prepareGuestQuote(Quote $quote) {
		$quote->setCustomerId ( null )->setCustomerEmail ( $quote->getBillingAddress ()->getEmail () )->setCustomerIsGuest ( true )->setCustomerGroupId ( Group::NOT_LOGGED_IN_ID );
	}

	/**
	 * Prepare payment gateway checkout
	 *
	 * @param Quote $quote
	 * @return array
	 */
	public function prepareCheckout(Quote $quote) {
		$this->setPaymentMethodFromQuote ( $quote );

		$data = $this->buildAuthenticationInfo ($quote);
		$data = $this->buildCustomerInfo ( $quote, $data );
		$data = $this->buildCartInfo ( $quote, $data );
		$data = $this->buildCustomInfo ( $quote, $data );

		return $data;
	}


	/**
	 * Prepare payon payment status request
	 *
	 * @param Quote $quote
	 * @return array
	 */
	public function finishSecureCheckout(Quote $quote) {
		$this->setPaymentMethodFromQuote ( $quote );
		$data = $this->buildAuthenticationInfo ($quote);
		return $data;
	}



	public function storeCustomerRegistration(Quote $quote, $registration) {

		$saveCCTokenEnabled = $this->config->getValue( 'loviit/settings/save_cc_token', \Magento\Store\Model\ScopeInterface::SCOPE_STORE );
		if(!$saveCCTokenEnabled){
			return;
		}

        $domocoPaymentCode = $quote->getPayment ()->getMethodInstance ()->getCode ();
        $channelId = $this->config->getValue ( 'payment/' . $domocoPaymentCode . '/channel_id', \Magento\Store\Model\ScopeInterface::SCOPE_STORE );

		$customerId = $quote->getCustomer ()->getId ();
		if ($this->paymentCode == 'CC') {
			$registrations = $this->registrationFactory->create ()->getCollection ()->addFieldToFilter ( 'registration', ( string ) $registration )->getData ();
			if (empty ( $registrations )) {
				$this->registrationFactory->create ()
                ->setData ( 'pm_id', 'CC' )
                ->setData ( 'registration', ( string ) $registration )
                ->setData ( 'channel_id', ( string ) $channelId )
                ->setData ( 'customer_id', $customerId )
                ->save ();
			}
		}
	}

	public function calculateTotalFromItems($items,$additionalAmounts = array()){
		$sum = 0;
		foreach ( $items as $item ) {
			$sum += $item['Price']*$item['Quantity']; //discount is always included in the price
		}

		if($additionalAmounts && sizeof($additionalAmounts)>0){
			if(isset($additionalAmounts['ShippingCosts'])){
				$sum += $additionalAmounts['ShippingCosts'];
			}
			if(isset($additionalAmounts['AdministrationFee'])){
				$sum += $additionalAmounts['AdministrationFee'];
			}
		}

		return $sum;
	}

	/**
	 * Prepare clean order data to be used for OR and DR
	 *
	 * @param Order $order
	 * @param Array $config
	 * @param Array $items is set in case of CN and TR and contains the list of items to refund or to track
	 * @param Array $additionalData used for CN, it contains additional information to create the request like shipping and fee amounts to refund
	 * @return Array
	 * @throws Exception
	 */
	public function prepareSoapRequest(Order $order, $config = array(),$items = null,$additionalData = null) {
		$data = array ();

		try {

			$data ['FirstName'] = $order->getBillingAddress ()->getFirstname ();
			$data ['LastName'] = $order->getBillingAddress ()->getLastname ();
			$data ['CustomerId'] = $order->getCustomerId ();
			$data ['Currency'] = $order->getOrderCurrencyCode ();
			$data ['OrderRequestId'] = $order->getIncrementId ();

			$data ['ShippingCosts'] = $additionalData != null ? $additionalData['ShippingCosts'] : $order->getShippingInclTax ();

			$data ['TransportationCosts'] = '';
			$data ['UniqueId'] = isset ( $config ['uniqueid'] ) ? $config ['uniqueid'] : '';

			$orderItems = $items ? $items : $order->getAllItems ();


			/*if( $orderItems[0] instanceof \Magento\Sales\Model\Order\Creditmemo\Item){
				$data ['Tax'] =  $orderItems[0]->getOrderItem()->getTaxPercent ();
			}
			else{*/
				$data ['Tax'] = $this->taxHelper->getShippingTaxPercent($order);
			//}

			$data ['Items'] = $this->getCartItemsData ( $orderItems, $order->getData ( "loviit_interest_fee_perc" ), $order->getData ( "number_of_installments" ) );

			$data ['Total'] = $items ? $this->calculateTotalFromItems($data ['Items'],$additionalData) : $order->getGrandTotal ();
			// add loviit discount as extra item
			$loviitDiscount = $order->getData ( "loviit_discount" );
			$data = $this->getLoviitDiscountItem ( $data, $loviitDiscount, $data ['Tax'] );

			if($additionalData && isset($additionalData['AdministrationFee'])){
				$data ['AdministrationFee'] = $additionalData['AdministrationFee']>0 ? $additionalData['AdministrationFee'] : 0;
			}
			else if ($order->getData ( "loviit_administration_fee" ) > 0) {
				$data ['AdministrationFee'] = $order->getData ( "loviit_administration_fee" );
			}
			$data ['OrderRequestDate'] = date ( 'Y-m-d\TH:i:s\Z' );
			$data ['Carrier'] = (isset($config['Carrier']) && !empty($config['Carrier'])) ? $config['Carrier'] : $order->getShippingDescription ();
			if(isset($config['TrackingDate'])){ //tracking request
				$data ['TrackingDate'] = $config['TrackingDate'];
			}
			else{ //delivery request
				$data ['DeliveryDate'] = date ( 'Y-m-d\TH:i:s\Z' );
			}

			$data ['DeliveryId'] = '';
			$data ['InvoiceNumber'] = '';
			$data ['InvoiceNumberExternal'] = '';

			if (isset ( $config ['trackingid'] )) {
				$data ['TrackingNumber'] = $config ['trackingid'];
			}

			// Credit
			$data ['CreditDate'] = date ( 'Y-m-d\TH:i:s\Z' );
			$data ['CreditId'] = !is_null($additionalData) && isset($additionalData['CreditId'])?  $additionalData['CreditId'] : '';
			$data ['InvoiceNumber'] = '';
			$data ['InvoiceNumberExternal'] = '';
		} catch ( Exception $ex ) {
			$this->_logger->addError ( $ex->getMessage () );
		}
		return $data;
	}

	/**
     * Send payon request for PSP methods
     *
     * @param String $action
     * @param Order $order
     * @return array
     */
	 public function requestPspAction($action, $order,$itemsToRefund = null,$additionalData=null){
    	$config = array('uniqueid' => $order->getData("loviit_uniqueid"));
        $requestData = $this->prepareSoapRequest($order, $config,$itemsToRefund,$additionalData);

   		$amount = $requestData['Total'];
   		$currency = $requestData['Currency'];

   		$uniqueid = $config['uniqueid'];

   		$payment = $order->getPayment();
   		$methodCode = $payment->getMethodInstance()->getCode();

   		$resourcePath = "/v1/payments/".$uniqueid;

   		$data = [
   			'paymentType' => $action,
   			'amount' => sprintf('%1.2f',$amount),
   			'currency' => $currency
   		];

   		$data['authentication.userId'] = $this->config->getValue('loviit/settings/psp_login', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
   		$data['authentication.password'] = $this->config->getValue('loviit/settings/psp_password', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
   		$data['authentication.entityId'] = $this->config->getValue('payment/' . $methodCode . '/channel_id', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

   		try {
            $url = $this->config->getValue('loviit/settings/live_url', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $transactionmode = $this->config->getValue('loviit/settings/transaction_mode', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

            ini_set('default_socket_timeout', 10);

            $verifypeer = true;
            if ($transactionmode != "LIVE") {
                $url = $this->config->getValue('loviit/settings/demo_url', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                ini_set('default_socket_timeout', 60);
                $verifypeer = false;
                if ($transactionmode == "CONNECTOR_TEST") {
                    $data['testMode'] = "EXTERNAL";
                }
            }

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url. $resourcePath);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $verifypeer);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $responseData = curl_exec($ch);

            if (curl_errno($ch)) {
                return curl_error($ch);
            }
            curl_close($ch);

            $this->loviitLogger("Url: " . $url . $resourcePath);
            $this->loviitLogger("Request: ".print_r($data,1));
            $this->loviitLogger("Response: ".print_r($responseData,1));
        } catch (Exception $ex) {
            $this->_logger->addError($ex->getMessage());
        }
        return json_decode($responseData);

	}

	/**
     * Check if PSP response is success
     *
     * @param Array $result
     * @return bool
     */
	 public function isGoodPspResponse($result){
    	$validCodes = array(
			'000.000.000',
			'000.000.100',
			'000.100.110',
			'000.100.111',
			'000.100.112');

    	if (!empty($result->result->code) && in_array($result->result->code, $validCodes))
    		return true;
    	else
    		return false;
    }

	/**
	 * build payon array with authentication parameters
	 *
	 * @return Array
	 * @throws Exception
	 */
	private function buildAuthenticationInfo($quote) {

		$payment = $quote->getPayment ()->getMethodInstance ();

		$authentication = array ();
		try {
			if ($payment->isPSP()) {
				$authentication ['authentication.userId'] = $this->config->getValue ( 'loviit/settings/psp_login', \Magento\Store\Model\ScopeInterface::SCOPE_STORE );
				$authentication ['authentication.password'] = $this->config->getValue ( 'loviit/settings/psp_password', \Magento\Store\Model\ScopeInterface::SCOPE_STORE );
			} else {
				$authentication ['authentication.userId'] = $this->config->getValue ( 'loviit/settings/user_login', \Magento\Store\Model\ScopeInterface::SCOPE_STORE );
				$authentication ['authentication.password'] = $this->config->getValue ( 'loviit/settings/user_password', \Magento\Store\Model\ScopeInterface::SCOPE_STORE );
			}
			$authentication ['authentication.entityId'] = $this->config->getValue ( 'payment/' . $this->paymentMethod . '/channel_id', \Magento\Store\Model\ScopeInterface::SCOPE_STORE );
		} catch ( Exception $ex ) {
			$this->_logger->addError ( $ex->getMessage () );
		}
		return $authentication;
	}

	/**
	 * build soap array with authentication parameters
	 *
	 * @return Array
	 * @throws Exception
	 */
	public function buildSimpleAuthenticationArray() {
		return array (
				'distributorId' => $this->config->getValue ( 'loviit/settings/merchant_id', \Magento\Store\Model\ScopeInterface::SCOPE_STORE ),
				'username' => $this->config->getValue ( 'loviit/settings/soap_user', \Magento\Store\Model\ScopeInterface::SCOPE_STORE ),
				'password' => $this->config->getValue ( 'loviit/settings/soap_pw', \Magento\Store\Model\ScopeInterface::SCOPE_STORE )
		);
	}

	/**
	 * build payon array with customer parameters
	 *
	 * @param Quote $quote
	 * @param Array $data
	 * @return Array
	 * @throws Exception
	 */
	private function buildCustomerInfo(Quote $quote, $data = array()) {
		try {
            $domocoPaymentCode = $quote->getPayment ()->getMethodInstance ()->getCode ();

			$customer = $this->getCustomer ( $quote );
			$channelId = $this->config->getValue ( 'payment/' . $domocoPaymentCode . '/channel_id', \Magento\Store\Model\ScopeInterface::SCOPE_STORE );

			$saveCCTokenEnabled = $this->config->getValue( 'loviit/settings/save_cc_token', \Magento\Store\Model\ScopeInterface::SCOPE_STORE );

			if ($saveCCTokenEnabled && $this->paymentCode == 'CC' && ( int ) $customer ['userid'] > 0) {
				$registrations = $this->registrationFactory
                ->create ()->getCollection ()
                ->addFieldToFilter ( 'customer_id', ( int ) $customer ['userid'] )
                ->addFieldToFilter ( 'pm_id', 'CC' )
                ->addFieldToFilter ( 'channel_id' , $channelId )
                ->getData ();

				foreach ( $registrations as $k => $v ) {
					$data ['registrations[' . $k . '].id'] = $v ['registration'];
				}
			}

			$data ['customer.merchantCustomerId'] = $customer ['userid'];
			$data ['customer.givenName'] = $customer ['billing'] ['firstname'];
			$data ['customer.surname'] = $customer ['billing'] ['lastname'];
			if (! empty ( $customer ['gender'] )) {
				$data ['customer.sex'] = $customer ['gender'] == '1' ? 'M' : 'F';
				$data ['customParameters[NAME.SALUTATION]'] = ($customer ['gender'] == '1' ? 'MR' : 'MS');
			}
			if (! empty ( $customer ['dob'] )) {
				$data ['customer.birthDate'] = $customer ['dob'];
				$data ['customParameters[NAME.BIRTHDATE]'] = $customer ['dob'];
			}
			$data ['customer.phone'] = $customer ['billing'] ['phone'];
			$data ['customer.mobile'] = $customer ['billing'] ['phone'];
			$data ['customer.email'] = $customer ['billing'] ['email'];

			if (! empty ( $customer ['vat_id'] )) {
				$data ['customer.identificationDocType'] = 'TAXSTATEMENT';
				$data ['customer.identificationDocId'] = $customer ['vat_id'];
			}
			$data ['customer.ip'] = $customer ['ip'];

			$data ['billing.street1'] = $customer ['billing'] ['street'] [0];
			$data ['billing.city'] = $customer ['billing'] ['city'];
			$data ['billing.state'] = $customer ['billing'] ['state'];
			$data ['billing.postcode'] = $customer ['billing'] ['zipcode'];
			$data ['billing.country'] = $customer ['billing'] ['country'];

			/*$data ['shipping.street1'] = $customer ['shipping'] ['street'] [0];
			// $customer['shipping.street2'] = $customer['shipping']['street'][0];
			$data ['shipping.city'] = $customer ['shipping'] ['city'];
			$data ['shipping.state'] = $customer ['shipping'] ['state'];
			$data ['shipping.postcode'] = $customer ['shipping'] ['zipcode'];
			$data ['shipping.country'] = $customer ['shipping'] ['country'];*/

			$data ['customParameters[CUSTOMER.BILLINGADDRESS.STREET]'] = $customer ['billing'] ['street'] [0];
			$data ['customParameters[CUSTOMER.BILLINGADDRESS.CITY]'] = $customer ['billing'] ['city'];
			$data ['customParameters[CUSTOMER.BILLINGADDRESS.STATE]'] =  $customer ['billing'] ['state'];
			$data ['customParameters[CUSTOMER.BILLINGADDRESS.ZIP]'] = $customer ['billing'] ['zipcode'];
			$data ['customParameters[CUSTOMER.BILLINGADDRESS.COUNTRY]'] = $customer ['billing'] ['country'];
			$data ['customParameters[CUSTOMER.BILLINGADDRESS.FIRSTNAME]'] = $customer ['billing'] ['firstname'];
			$data ['customParameters[CUSTOMER.BILLINGADDRESS.LASTNAME]'] = $customer ['billing'] ['lastname'];

			$data ['customParameters[CUSTOMER.DELIVERYADDRESS.STREET]'] = $customer ['shipping'] ['street'] [0];
			$data ['customParameters[CUSTOMER.DELIVERYADDRESS.CITY]'] = $customer ['shipping'] ['city'];
			$data ['customParameters[CUSTOMER.DELIVERYADDRESS.STATE]'] =  $customer ['shipping'] ['state'];
			$data ['customParameters[CUSTOMER.DELIVERYADDRESS.ZIP]'] = $customer ['shipping'] ['zipcode'];
			$data ['customParameters[CUSTOMER.DELIVERYADDRESS.COUNTRY]'] = $customer ['shipping'] ['country'];
			$data ['customParameters[CUSTOMER.DELIVERYADDRESS.FIRSTNAME]'] = $customer ['shipping'] ['firstname'];
			$data ['customParameters[CUSTOMER.DELIVERYADDRESS.LASTNAME]'] = $customer ['shipping'] ['lastname'];

			$iban = $this->checkoutSession->getData ( "iban" );
			$bic = $this->checkoutSession->getData ( "bic" );


			$this->loviitLogger ( print_r ( "PAYMENT IBAN $iban", 1 ) );
			$this->loviitLogger ( print_r ( "PAYMENT BIC $bic", 1 ) );

			if (! empty ( $iban ) || ! empty ( $iban )) {
				if (! empty ( $iban )) {
					$data ['customParameters[CUSTOMER.BANK.IBAN]'] = $iban;
                                        if (!$this->isInstallmentWithDepositMethod($domocoPaymentCode)) {
                                            $data ['bankAccount.iban'] = $iban;
                                        }
				}
				if (! empty ( $bic )) {
					$data ['customParameters[CUSTOMER.BANK.BIC]'] = $bic;
                                        if (!$this->isInstallmentWithDepositMethod($domocoPaymentCode)) {
                                            $data ['bankAccount.bic'] = $bic;
                                        }
				}
				$bank_account_opening_date = $this->checkoutSession->getData ( "bank_account_opening_date" );
				if (! empty ( $bank_account_opening_date )) {
					$this->loviitLogger ( print_r ( "PAYMENT BANK ACCOUNT OPENING DATE $bank_account_opening_date", 1 ) );
					$date_parts = explode('-',$bank_account_opening_date);
					$data['customParameters[CUSTOMER.BANK.OPENDATE]'] = sprintf('%02d-%02d-%04d', $date_parts[0], $date_parts[1], $date_parts[2]);
				}
				$data ['bankAccount.holder'] = trim ( $customer ['billing'] ['firstname'] ) . ' ' . trim ( $customer ['billing'] ['lastname'] );
			}

			$dueDays = null;
			if ($customer ['is_company']) {
				if($domocoPaymentCode=='loviitdd'){
					$dueDays = $this->config->getValue('loviit/settings/due_date_b2b_dd', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
				}
				else if($domocoPaymentCode=='loviitiv'){
					$dueDays = $this->config->getValue('loviit/settings/due_date_b2b', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
				}
				if(!empty($dueDays)){
					$data['customParameters[ORDER.DUE_DAYS]'] = $dueDays;
					// $customerInfo['transactionDueDate'] = date('Y-m-d', strtotime(date('Y-m-d'). ' + '. $dueDays .' days'));
				}

				$data ['customer.companyName'] = $customer ['billing'] ['company'];
				$data ['customParameters[COMPANY.TAXID]'] = $customer ['vat_id'];
				$data ['customParameters[COMPANY.NAME]'] = $customer ['billing'] ['company'];
				$data ['customParameters[COMPANY.STREET]'] = $customer ['billing'] ['street'] [0];
				$data ['customParameters[COMPANY.STATE]'] = $customer ['billing'] ['state'];
				$data ['customParameters[COMPANY.ZIP]'] = $customer ['billing'] ['zipcode'];
				$data ['customParameters[COMPANY.COUNTRY]'] = $customer ['billing'] ['country'];
				$data ['customParameters[COMPANY.ADDITIONAL_DATA]'] = '';
				$data ['customParameters[CUSTOMER.ISCOMPANY]'] = 1;
				$data ['customParameters[COMPANY.COMPANYTYPE]'] = $customer ['company_type'];
			} else {
				if($domocoPaymentCode=='loviitdd'){
					$dueDays = $this->config->getValue('loviit/settings/due_date_b2c_dd', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
				}
				else if($domocoPaymentCode=='loviitiv'){
					$dueDays = $this->config->getValue('loviit/settings/due_date_b2c', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
				}
				if(!empty($dueDays)){
					$data['customParameters[ORDER.DUE_DAYS]'] = $dueDays;
					// $customerInfo['transactionDueDate'] = date('Y-m-d', strtotime(date('Y-m-d'). ' + '. $dueDays .' days'));
				}


				if (isset ( $customer ['vat_id'] )) {
					$data ['customParameters[CUSTOMER.TAXID]'] = $customer ['vat_id'];
					$data ['customParameters[CUSTOMER.ISCOMPANY]'] = 0;
				}
			}
		} catch ( Exception $ex ) {
			error_log ( $ex->getMessage () );
			$this->_logger->addError ( $ex->getMessage () );
		}
		return $data;
	}

	/**
	 * build payon array with cart parameters
	 *
	 * @param Quote $quote
	 * @param Array $data
	 * @return Array
	 * @throws Exception
	 */
	private function buildCartInfo(Quote $quote, $data = array()) {
		try {
			$cartData = $this->generateCartData ( $quote );

			$data ['amount'] = sprintf ( '%1.2f', $cartData ['Total'] );
			$data ['customParameters[MERCHANT.ID]'] = $cartData ['MerchantId'];
			$data ['currency'] = $cartData ['Currency'];

			$data ['merchantTransactionId'] = $cartData ['OrderRequestId'];
			$data ['customParameters[ORDER.TOTAL_AMOUNT]'] = $cartData ['Total'];
			$data ['customParameters[ORDER.PRC_VAT]'] = sprintf ( '%1.2f', $cartData ['Tax'] );
			$data ['customParameters[ORDER.SHIPPING_COSTS]'] = $cartData ['ShippingCosts'];
			$data ['customParameters[ORDER.ADMINISTRATION_FEE]'] = sprintf ( '%1.2f', $cartData ['AdministrationFee'] );
			$data ['customParameters[ORDER.CURRENCY]'] = $cartData ['Currency'];
			$data ['customParameters[ORDER.ORDER_POSITIONS_COUNT]'] = $cartData ['ItemCount'];
			// $customInfo['customParameters[ORIG.AMOUNT]'] = $cartData['OrigalTotal'];

			$payment = $quote->getPayment()->getMethodInstance();

			$autocaptureDays = $payment->getConfigData('autocapture_days');
			if(!empty($autocaptureDays)){
				$data ['customParameters[ORDER.TRIGGER_CP_DAYS]'] = $autocaptureDays;
			}

			$methodCode = $this->paymentMethod;
			$this->loviitLogger ( "INSTALLMENT_SECTION: methodCode $methodCode" );
			if ($this->isInstallmentMethod ( $methodCode )) {
				$installment_data = $this->calculateInstallmentPlans ( $quote, $methodCode, $quote->getPayment ()->getData ( 'number_of_installments' ), $quote->getData ( 'loviit_deposit' ) );

				$data ['customParameters[ORDER.INSTALLMENTS.IIR]'] = $quote->getData ( 'loviit_interest_fee_perc' );
				$data ['customParameters[ORDER.INSTALLMENTS.SECOND_INSTALLMENT_AMOUNT]'] = sprintf ( '%1.2f', $installment_data ['second_rate'] );

				if ($this->isInstallmentWithDepositMethod ( $methodCode ) || $payment->pushDeposit()) {
					$data ['customParameters[PRESENTATION.AMOUNT]'] = $installment_data ['deposit'];
					$data ['customParameters[ORDER.INSTALLMENTS.FIRST_INSTALLMENT_AMOUNT]'] = sprintf ( '%1.2f', $installment_data ['deposit'] );
					$data ['amount'] = $installment_data ['deposit'];
					$data ['customParameters[ORDER.INSTALLMENTS.COUNT]'] = $quote->getPayment ()->getData ( 'number_of_installments' ) + 1;
				} else {
					$data ['customParameters[ORDER.INSTALLMENTS.COUNT]'] = $quote->getPayment ()->getData ( 'number_of_installments' );
					$data ['customParameters[ORDER.INSTALLMENTS.FIRST_INSTALLMENT_AMOUNT]'] = sprintf ( '%1.2f', $installment_data ['first_rate'] );
				}

				if ($methodCode == "loviitic") {
					$data ['customParameters[PRESENTATION.AMOUNT]'] = $data ['customParameters[ORDER.INSTALLMENTS.FIRST_INSTALLMENT_AMOUNT]'];
					$data ['amount'] = $data ['customParameters[ORDER.INSTALLMENTS.FIRST_INSTALLMENT_AMOUNT]'];
					$data ['createRegistration']=true;
				}
			}

			if (! isset ( $interest_factor )) {
				$interest_factor = 1;
			}

			foreach ( $cartData ['Items'] as $key => $item ) {
				$position = $key + 1;
				$discountPct = $item ['DiscountPercentage'];

				$cartItem = array ();
				$cartItem ['customParameters[ORDERPOSITION.' . $position . '.ARTICLE_NUMBER]'] = $item ['ArticleNumber'];
				$cartItem ['customParameters[ORDERPOSITION.' . $position . '.ARTICLE_DESCRIPTION]'] = $item ['Description'];
				$cartItem ['customParameters[ORDERPOSITION.' . $position . '.PRICE]'] = sprintf ( '%1.2f', $item ['Price'] );
				$cartItem ['customParameters[ORDERPOSITION.' . $position . '.QUANTITY]'] = $item ['Quantity'];
				$cartItem ['customParameters[ORDERPOSITION.' . $position . '.TAXRATE]'] = sprintf ( '%1.2f', $item ['Tax'] );
				$cartItem ['customParameters[ORDERPOSITION.' . $position . '.DISCOUNT]'] = (isset ( $item ['DiscountAmount'] ) && strlen ( $item ['DiscountAmount'] ) > 0 ? $item ['DiscountAmount'] : 0);
				// $cartItem['customParameters[ORDERPOSITION.'. $position .'.DISCOUNT_PCT]'] = ($discountPct > 0 ? $discountPct : '0');
				$cartItem ['customParameters[ORDERPOSITION.' . $position . '.SerialNumber]'] = $item ['SerialNumber'];

				$data [] = $cartItem;
			}
		} catch ( Exception $ex ) {
			$this->loviitLogger ( $ex->getMessage (), "error" );
			$this->_logger->addError ( $ex->getMessage () );
		}
		return $data;
	}

	/**
	 * build payon array with custom parameters
	 *
	 * @param Array $data
	 * @return Array
	 * @throws Exception
	 */
	private function buildCustomInfo($quote, $data = array()) {
		try {

			$payment = $this->checkoutSession->getQuote()->getPayment()->getMethodInstance();

			if ($payment->isServerToServer()) {
				$data ['paymentBrand'] = $payment->getBrand();
				$data ['shopperResultUrl'] = $this->storeManager->getStore ()->getUrl ( 'loviit/payment/response' );
			}

            $paymentCode = $this->paymentCode;
			$data ['paymentType'] = $payment->getPSPType();


			if ($paymentCode == 'CC') {
				$data ['customParameters[presentation.currency3D]'] = $data ['currency'];
				$data ['customParameters[presentation.amount3D]'] = $data ['amount'];
			}

//                        only for testing PRZELEWY
//                        $data ['customParameters[TEST_TIMEOUT_LATE_PAYMENT_TYPE]'] = 'PRZELEWY';
//                        $data ['customParameters[TEST_TIMEOUT_LATE_PAYMENT_INITIAL_DELAY]'] = 20;
//                        $data ['customParameters[TEST_TIMEOUT_LATE_PAYMENT_FINAL_DELAY]'] = 120;

                        $data ['customParameters[ADDEGREESESSIONID]'] = $this->coreSession->getPixelSessionId();

			$data ['customParameters[ORDER.PAYMENT_METHOD]'] = $payment->getDocomoMethodId();
			$data ['customParameters[SHOPMODULE.VERSION]'] = $this->moduleResource->getDbVersion ( 'Loviit_Loviit' );
		} catch ( Exception $ex ) {
			$this->loviitLogger ( $ex->getMessage (), "error" );
			$this->_logger->addError ( $ex->getMessage () );
		}
		return $data;
	}

	/**
	 * Get clean cart data to be use on payon and omm
	 *
	 * @param Quote $cartItems
	 * @param Array $orderConfig
	 * @return Array
	 * @throws Exception
	 */
	public function generateCartData(Quote $quote, $orderConfig = array()) {
		$cartData = array ();

		try {
			// $cartTotals = $quote->getTotals();
			$shippingTotals = $quote->getShippingAddress ()->getTotals ();
			$shippingData = $quote->getShippingAddress ()->getData ();

			$cartData ['Tax'] = $this->taxHelper->getShippingTaxPercent($quote);

			$cartData ['Total'] = $shippingTotals ['grand_total']->getValue ();
			$cartData ['ShippingCosts'] = $shippingData ['shipping_incl_tax'];
            $cartData ['AdministrationFee'] = $shippingTotals ['loviit_administration_fee']->getValue ();

			$this->disagioTotalDiscount = $shippingTotals ['loviit_discount']->getValue ();

			$cartData ['Currency'] = $quote->getQuoteCurrencyCode ();
			$cartData ['MerchantId'] = $this->config->getValue ( 'loviit/settings/merchant_id', \Magento\Store\Model\ScopeInterface::SCOPE_STORE );
			$cartData ['OrderRequestId'] = $quote->getReservedOrderId ();

			$cartData ['TransportationCosts'] = 0;
			$cartData ['OrderRequestDate'] = date ( 'Y-m-d\TH:i:s\Z' );

			$allItems = $quote->getAllItems ();
			$cartData ['Items'] = $this->getCartItemsData ( $allItems, $quote->getData ( "loviit_interest_fee_perc" ), $quote->getPayment ()->getData ( "number_of_installments" ) );

			$cartData = $this->getLoviitDiscountItem ( $cartData, $this->disagioTotalDiscount, $cartData ['Tax'] );

			$cartData ['ItemCount'] = count ( $cartData ['Items'] );
		} catch ( Exception $ex ) {
			$this->_logger->addError ( $ex->getMessage () );
		}
		return $cartData;
	}

	/**
	 * Get clean product data to be use on payon and omm
	 *
	 * @param Object $cartItems
	 * @param String $interestPercentage
	 * @param String $numberOfInstallments
	 * @return Array
	 * @throws Exception
	 */
	private function getCartItemsData($cartItems, $interestPercentage = null, $numberOfInstallments = null) {
		$items = array ();
		try {
			$taxAfterDiscount = $this->config->getValue ( 'tax/calculation/apply_after_discount', \Magento\Store\Model\ScopeInterface::SCOPE_STORE );
			$discountIncludingTax = $this->config->getValue ( 'tax/calculation/discount_tax', \Magento\Store\Model\ScopeInterface::SCOPE_STORE );

			if (! empty ( $numberOfInstallments ) && ! empty ( $interestPercentage )) {
				$interest_factor = (($interestPercentage / 100) * ($numberOfInstallments / 12) + 1);
			} else {
				$interest_factor = 1;
			}


			$totalItems = 0;
			foreach ( $cartItems as $cartItem ) {
				if($cartItem->getProductType()=='configurable'){
					continue;
				}

				$quantity = $cartItem->getQty ();
				//calculate bundle's child quantity
				$parentItem = $cartItem->getParentItem();
				if($parentItem!=null){
					$quantity=$quantity*$parentItem->getQty();
				}

				if (empty ( $quantity )) {
					$quantity = $cartItem->getQtyOrdered (); //already contains the correct quantity for bundle
				}
				$totalItems += $quantity;
			}

			foreach ( $cartItems as $cartItem ) {
				//only children of configurable products are processed

				$isCN = false;

				//CREDIT NOTE ITEMS
				unset($creditMemoQuantity);
				if($cartItem instanceof \Magento\Sales\Model\Order\Creditmemo\Item) {
					$creditMemoQuantity = $cartItem->getQty ();
					$creditMemoItem=$cartItem;
					$cartItem = $cartItem->getOrderItem(); // use order item to retrieve the info except for quantity
					//configurable items take quantity from cn parent
					$parentItem=$cartItem->getParentItem();
					if($parentItem && $parentItem->getProductType() == 'configurable'){
						foreach ($cartItems as $cnitem) {
							if ($cnitem->getOrderItem()->getId() == $parentItem->getId()) {
								$creditMemoQuantity=$creditMemoQuantity*$cnitem->getQty();
							}
						}

					}

					$isCN = true;
				}

				$productType = $cartItem->getProductType();
				if($productType=='configurable'){
					continue;
				}
				if($productType == 'bundle'){
					$productPriceType = $cartItem->getProduct()->getPriceType();
				}

				$taxRate =  $cartItem->getTaxPercent();

				if(!$isCN){
					$quantity = isset($creditMemoQuantity) ? $creditMemoQuantity : $cartItem->getQty ();

					//calculate bundle's child quantity
					$parentItem = $cartItem->getParentItem();
					if($parentItem!=null){
						$quantity=$quantity*$parentItem->getQty();
					}

					if (empty ( $quantity )) {
						$quantity = $cartItem->getQtyOrdered (); //already contains the correct quantity for bundle
					}
				}
				else{
					$quantity=$creditMemoQuantity;
				}



				$discountPrice = empty ( $quantity )? 0 : ($cartItem->getDiscountAmount () / $quantity);

				$itemPrice = $cartItem->getPrice ();


				//for bundle items with dynamic price, price is set on children
				if($productType == 'bundle' && $productPriceType == \Magento\Bundle\Model\Product\Price::PRICE_TYPE_DYNAMIC){
					$itemPrice = 0;
				}
				//configurable products use parent prices
				else if($cartItem->getParentItem() && $cartItem->getParentItem()->getProductType()=='configurable'){
					$itemPrice = $cartItem->getParentItem()->getPrice ();
					$taxRate = $cartItem->getParentItem()->getTaxPercent();
					$discountPrice = empty ( $quantity )? 0 : ($cartItem->getParentItem()->getDiscountAmount()/$quantity);
					if(empty($discountPrice)) $discountPrice = 0;
				}

				if ( $taxAfterDiscount && !$discountIncludingTax )
					$_priceIncludingTax = ($itemPrice - $discountPrice) + (($itemPrice - $discountPrice) * $taxRate / 100);
				else
					$_priceIncludingTax = $itemPrice + ($itemPrice * $taxRate / 100) - $discountPrice;

				//workaround to fix 1 cent rounding miscalculation
				if($cartItem->getProduct()!=null && abs($cartItem->getProduct()->getFinalPrice()-$discountPrice-$_priceIncludingTax)<0.02){
					$_priceIncludingTax = $cartItem->getProduct()->getFinalPrice()-$discountPrice;
				}

				$this->calculateItemDiscount ( $itemPrice + ($itemPrice * $taxRate / 100), $numberOfInstallments, $totalItems );

				$itemInterest = (($interestPercentage / 100) * ($numberOfInstallments / 12) + 1) * ($_priceIncludingTax - $this->disagioItemDiscount) - ($_priceIncludingTax - $this->disagioItemDiscount);


				$orderItem = array (
						'ArticleNumber' => $cartItem->getProductId (),
						'Name' => $cartItem->getName (),
						'Description' => $cartItem->getName (),
						'Price' =>  empty($quantity)? 0 :number_format ( $_priceIncludingTax + $itemInterest, 2, '.', '' ),
						'Quantity' => empty($quantity)? 0 : $quantity,
						'SerialNumber' => 'SKU:' . $cartItem->getSku (),
						'Tax' => $taxRate,
						'Weight' => $cartItem->getWeight ()
				);
				// $orderItem ['DiscountAmount'] = $this->getDiscountAmountByProduct ($v,$order,true);
				$orderItem ['DiscountPercentage'] = '0';
				$orderItem ['DiscountAmount'] = '0'; // $discountPrice;

				$items [] = $orderItem;
			}
		} catch ( Exception $ex ) {
			$this->_logger->addError ( $ex->getMessage () );
		}

		//error_log(print_r($items,1));
		//throw new \Exception("stop");

		return $items;
	}

	/**
	 * Add the loviit discount as a new product item
	 *
	 * @param Array $data
	 * @param String $loviitDiscount
	 * @param String $tax
	 * @return Array
	 * @throws Exception
	 */
	private function getLoviitDiscountItem($data = array(), $loviitDiscount, $tax) {
		try {
			if (preg_match ( "/^-?[0-9]\d*(\.\d+)?$/", $loviitDiscount ) && $loviitDiscount != "-0.00" && $loviitDiscount != "0.00") {
				$data ['Items'] [] = array (
						'ArticleNumber' => 'loviit_discount',
						'Name' => __ ( 'Loviit Discount' ),
						'Description' => __ ( 'Loviit Discount' ),
						'Price' => $loviitDiscount,
						'Quantity' => 1,
						'SerialNumber' => 'loviit_discount',
						'Tax' => $tax,
						'DiscountPercentage' => '0',
						'DiscountAmount' => '0'
				);
			}
		} catch ( Exception $ex ) {
			$this->_logger->addError ( $ex->getMessage () );
		}
		return $data;
	}

	/**
	 * Get customer data from quote
	 *
	 * @param Quote $quote
	 * @return Array
	 * @throws Exception
	 */
	public function getCustomer(Quote $quote) {
		$customerData = array ();
		try {
			$customer = $quote->getCustomer ();
			$customerBillingAddress = $quote->getBillingAddress ();
			$customerShippingAddress = $quote->getShippingAddress ();

			$customerBillingCompany = $customerBillingAddress->getCompany ();
			$cusotmerBillingVatId = $customerBillingAddress->getVatId ();

			$date_of_birth = $customer->getDob () ? $customer->getDob () : $quote->getCustomerDob();
			if(!empty($date_of_birth) && strlen($date_of_birth)>=10){
				$date_of_birth = substr($date_of_birth,0,10);
			}
			else{
				$date_of_birth = null;
			}

			$customerData = array (
					"userid" => $customer->getId (),
					"groupid" => $quote->getCustomerGroupId (),
					"billing" => array (
							"firstname" => $customerBillingAddress->getFirstname (),
							"lastname" => $customerBillingAddress->getLastname (),
							"company" => $customerBillingAddress->getCompany (),
							"street" => $customerBillingAddress->getStreet (),
							"city" => $customerBillingAddress->getCity (),
							"state" => $customerBillingAddress->getRegion (),
							"zipcode" => $customerBillingAddress->getPostcode (),
							"country" => $customerBillingAddress->getCountry (),
							"phone" => $customerBillingAddress->getTelephone (),
							"email" => $customerBillingAddress->getEmail ()
					),
					"shipping" => array (
							"firstname" => $customerShippingAddress->getFirstname (),
							"lastname" => $customerShippingAddress->getLastname (),
							"company" => $customerShippingAddress->getCompany (),
							"street" => $customerShippingAddress->getStreet (),
							"city" => $customerShippingAddress->getCity (),
							"state" => $customerShippingAddress->getRegion (),
							"zipcode" => $customerShippingAddress->getPostcode (),
							"country" => $customerShippingAddress->getCountry (),
							"phone" => $customerShippingAddress->getTelephone (),
							"email" => $customerShippingAddress->getEmail ()
					),

					"ip" => $this->getIpAddress (),
					"dob" => $date_of_birth,
					"company_type" => '',
					"gender" => $customer->getGender () ? $customer->getGender () : $quote->getCustomerGender()
			);
			$customerData ["is_company"] = 0;
			if (isset ( $customerBillingCompany ) && isset ( $cusotmerBillingVatId ) && $customerBillingCompany != "" && $cusotmerBillingVatId != "") {
				$customerData ["is_company"] = 1;
			}

			$vatid = $customerBillingAddress->getVatId();
			$taxvat = $customer->getTaxvat() ? $customer->getTaxvat () : $quote->getCustomerTaxvat();

			if (isset ( $vatid ) && $vatid != "") {
				$customerData ["vat_id"] = $vatid;
			} else if (isset ( $taxvat ) && $taxvat != "") {
				$customerData ["vat_id"] = $taxvat;
			}
		} catch ( Exception $ex ) {
			$this->_logger->addError ( $ex->getMessage () );
		}
		return $customerData;
	}

	/**
	 * Calculate installment plans over cart from loviit configuration
	 *
	 * @param Quote $quote
	 * @param String $paymentCode
	 * @param String $numberOfInstallments
	 * @param String $deposit
	 * @return Array
	 * @throws Exception
	 */
	public function calculateInstallmentPlans(Quote $quote, $paymentCode = null, $numberOfInstallments = null, $deposit = 0) {
		$installmentPlans = array ();
		try {
			// $this->loviitLogger("calculateInstallmentPlans - elaborazione $paymentCode $numberOfInstallments $deposit");

			if ($this->isInstallmentMethod ( $paymentCode )) {
                                $configInstallmentPlan = $this->getInstallmentPlansFromConfig($paymentCode);

				if (is_array ( $configInstallmentPlan ) && count ( $configInstallmentPlan ) > 0) {

					foreach ( $configInstallmentPlan as $installmentPlan ) {
						$nbOfInstallments = $installmentPlan ['number_of_installments'];
						if (! isset ( $numberOfInstallments ) || (isset ( $numberOfInstallments ) && $numberOfInstallments == $nbOfInstallments)) {
							// $this->loviitLogger("calculateInstallmentPlans - processing $paymentCode $numberOfInstallments / $nbOfInstallments");

							// recalculate totals for each plan
							// $quote->setPaymentMethod($code);

							// $quote->getShippingAddress()->setPaymentMethod($code);
							// $quote->getBillingAddress()->setPaymentMethod($code);
							$quote->getPayment ()->addData ( array (
									'number_of_installments' => $nbOfInstallments
							) );
							$quote->getPayment ()->setMethod ( $paymentCode );
							$quote->setTotalsCollectedFlag ( false );
							$quote->getShippingAddress ()->setCollectShippingRates ( true );
							$quote->collectTotals ();
							$cartTotals = $quote->getTotals ();
							$subTotal = $quote->getSubtotal ();

							// retrieve installment config
							$interest = empty ( $installmentPlan ['interest'] ) ? 0 : $installmentPlan ['interest']; // percentage
							$administrationFee = $installmentPlan ['fee']; // percentage or amount

							// calculate fee amounts
							$interest_amount = isset ( $cartTotals ['loviit_interest_fee'] ) ? $cartTotals ['loviit_interest_fee']->getValue () : 0;
							$administrationFeeAmount = isset ( $cartTotals ['loviit_administration_fee'] ) ? $cartTotals ['loviit_administration_fee']->getValue () : 0;

							$totals = array (
									'subtotal' => $cartTotals ['subtotal']->getValue ()
							);
							$discountAmount = $this->calculateDiscount ( $quote, $totals, $paymentCode, $nbOfInstallments );

							$total = $cartTotals ['grand_total']->getValue ();
							$payment = $this->checkoutSession->getQuote()->getPayment()->getMethodInstance();


							$maxInstallmentAmount = $this->config->getValue ( 'loviit/settings/max_dd_installment', \Magento\Store\Model\ScopeInterface::SCOPE_STORE );

							if ($this->isInstallmentWithDepositMethod ( $paymentCode )) {
								//calculation using deposit chosen by the user
								$deposit = number_format ( floatval ( $deposit ), 2, '.', '' );
								//calculation of the minimum deposit
								$minimum_deposit_percentage = $this->config->getValue ( "payment/$paymentCode/min_first_installment", \Magento\Store\Model\ScopeInterface::SCOPE_STORE );

								if(!empty($minimum_deposit_percentage)){
									$minimum_deposit=number_format ( floatval ( $total*$minimum_deposit_percentage/100.0 ), 2, '.', '' );
									if($total<$deposit){
										$deposit=$total;
									}
									else{
										$deposit=max($deposit,$minimum_deposit);
									}


								}
								$base_rate = $first_rate = ($total - $deposit) / $nbOfInstallments;
							} else if ($payment->pushDeposit()) {

								$deposit = $total - $maxInstallmentAmount;
								$deposit = number_format ( floatval ( $deposit ), 2, '.', '' );
								$first_rate = $deposit;
								$base_rate = ($total - $deposit) / $nbOfInstallments;
							} else {
								$minimum_first_installment = $this->config->getValue ( "payment/$paymentCode/min_first_installment", \Magento\Store\Model\ScopeInterface::SCOPE_STORE );

								$deposit=0;
								$base_rate = ($total + $interest_amount - $administrationFeeAmount - $quote->getShippingAddress ()->getShippingAmount ()) / $nbOfInstallments;
								$first_rate = $base_rate + $administrationFeeAmount + $quote->getShippingAddress ()->getShippingAmount ();
								if(!empty($minimum_first_installment)){
									$minimum_first_installment_amount = number_format ( floatval ( $total*$minimum_first_installment/100.0 ), 2, '.', '' );
									$first_rate = max($minimum_first_installment_amount,$first_rate);
									$base_rate = ($total - $first_rate) / ($nbOfInstallments -1);
								}
							}

							$shippingData = $quote->getShippingAddress ()->getData ();

							$installmentPlans [$nbOfInstallments] ['shipping'] = $shippingData ['shipping_incl_tax'];
							$installmentPlans [$nbOfInstallments] ['deposit'] = $deposit;
							$installmentPlans [$nbOfInstallments] ['minimum_deposit'] = isset($minimum_deposit)? $minimum_deposit : null;
							$installmentPlans [$nbOfInstallments] ['number_of_months'] = $nbOfInstallments;
							$installmentPlans [$nbOfInstallments] ['interest'] = $interest;
							$installmentPlans [$nbOfInstallments] ['interest_factor'] = ($subTotal + $interest_amount) / $subTotal; // used to recalculate items prices
							$installmentPlans [$nbOfInstallments] ['administration_fee'] = $administrationFee;
							$installmentPlans [$nbOfInstallments] ['discount'] = $discountAmount;
							$installmentPlans [$nbOfInstallments] ['interest_amount'] = $interest_amount;
							$installmentPlans [$nbOfInstallments] ['administration_fee_amount'] = $administrationFeeAmount;
							$installmentPlans [$nbOfInstallments] ['total'] = $total;
							$installmentPlans [$nbOfInstallments] ['first_rate'] = number_format ( $first_rate, 2, '.', '' );
							$installmentPlans [$nbOfInstallments] ['second_rate'] = number_format ( $base_rate, 2, '.', '' );

							foreach ( array (
									'first_rate',
									'second_rate',
									'interest_amount',
									'administration_fee_amount',
									'total',
									'discount',
									'deposit' ,
									'minimum_deposit'
							) as $key ) {
								$installmentPlans [$nbOfInstallments] [$key . "_formatted"] = $this->getFormattedPrice ( $installmentPlans [$nbOfInstallments] [$key] );
							}
						}
					}
				}
			}
		} catch ( Exception $ex ) {
			error_log ( $ex->getMessage () );
		}

		if (isset ( $numberOfInstallments )) {
			return $installmentPlans [$numberOfInstallments];
		}
		return $installmentPlans;
	}

	/**
	 * Calculate administration fee over cart from loviit configuration
	 *
	 * @param Quote $quote
	 * @param Array $totals
	 * @param String $paymentCode
	 * @param String $numberOfInstallments
	 * @return String
	 * @throws Exception
	 */
	public function calculateAdministrationFee($quote, $totals, $paymentCode, $numberOfInstallments = null) {
		$administrationFee = 0;
		try {
			if (! isset ( $paymentCode ) || empty ( $paymentCode )) {
				return $administrationFee;
			}
			if ($this->isInstallmentWithDepositMethod ( $paymentCode )) {
				$paymentCode = 'loviitrp';
			}
			$paymentModel = $this->config->getValue ( 'loviit/settings/payment_model', \Magento\Store\Model\ScopeInterface::SCOPE_STORE );
			if (isset ( $paymentModel ) && strtolower ( $paymentModel ) == "agio") {

				if ($this->isInstallmentMethod ( $paymentCode )) {
					$configInstallmentPlan = $this->getInstallmentPlansFromConfig($paymentCode);
					foreach ( $configInstallmentPlan as $plan ) {
						if ($plan ['number_of_installments'] == $numberOfInstallments) {
							$administrationFee = $plan ['fee'];
							break;
						}
					}
				} else {
					$administrationFee = $this->config->getValue ( 'payment/' . $paymentCode . '/administration_fee', \Magento\Store\Model\ScopeInterface::SCOPE_STORE );
				}

				if (strpos ( $administrationFee, '%' ) !== false) {
					// subtotal calculations
					$subtotal = $this->calculateCartSubtotal ( $quote, $totals );
					$administrationFee = str_replace ( '%', '', $administrationFee ) / 100 * ($subtotal);
				}
			}
		} catch ( Exception $ex ) {
			$this->_logger->addError ( $ex->getMessage () );
		}
		return number_format ( empty ( $administrationFee ) ? 0 : $administrationFee, 2, '.', '' );
	}

	/**
	 * Calculate interest fee over cart from loviit configuration
	 *
	 * @param Quote $quote
	 * @param Array $totals
	 * @param String $paymentCode
	 * @param String $numberOfInstallments
	 * @return Array
	 * @throws Exception
	 */
	public function calculateInterestFee($quote, $totals, $paymentCode, $numberOfInstallments = null) {
		$return = array (
				"percentage" => 0,
				"amount" => 0
		);
		try {
			if (! isset ( $paymentCode ) || empty ( $paymentCode ) || ! $this->isInstallmentMethod ( $paymentCode )) {
				return $return;
			}
			if ($this->isInstallmentWithDepositMethod ( $paymentCode )) {
				$paymentCode = 'loviitrp';
			}

			$interest_fee = $interest_fee_amount = 0;

			$paymentModel = $this->config->getValue ( 'loviit/settings/payment_model', \Magento\Store\Model\ScopeInterface::SCOPE_STORE );
			if (isset ( $paymentModel ) && strtolower ( $paymentModel ) == "agio") {
				// subtotal calculations
				$subtotal = $this->calculateCartSubtotal ( $quote, $totals );
				$cartDiscount = $this->calculateDiscount ( $quote, $totals, $paymentCode, $numberOfInstallments );
				$subtotal += $cartDiscount;

				$configInstallmentPlan = $this->getInstallmentPlansFromConfig($paymentCode);
				foreach ( $configInstallmentPlan as $plan ) {
					if ($plan ['number_of_installments'] == $numberOfInstallments) {
						$interest_fee = $plan ['interest'];
						break;
					}
				}
				$interest_fee_amount = (($interest_fee / 100) * ($numberOfInstallments / 12) + 1) * $subtotal - $subtotal;
			}
			$return = array (
					"percentage" => $interest_fee,
					"amount" => number_format ( empty ( $interest_fee_amount ) ? 0 : $interest_fee_amount, 2, '.', '' )
			);

			return $return;
		} catch ( Exception $ex ) {
			$this->_logger->addError ( $ex->getMessage () );
		}
		return $return;
	}

	/**
	 * Calculate cart subtotal
	 *
	 * @param Quote $quote
	 * @param Array $totals
	 * @return String
	 * @throws Exception
	 */
	public function calculateCartSubtotal($quote, $totals) {
		try {
			// subtotal calculations
			$subtotal = $totals ['subtotal'];

			foreach ( $quote->getAllItems () as $item ) {
				$subtotal += $item->getTaxAmount () - $item->getDiscountAmount ();
			}
			return $subtotal;
		} catch ( Exception $ex ) {
			$this->_logger->addError ( $ex->getMessage () );
		}
		return false;
	}

	public function calculateCreditNoteInterestToRefund($creditmemo){
		$order=$creditmemo->getOrder();
		$interest_perc = $order->getData('loviit_interest_fee_perc');
		$number_of_installment = $order->getData('number_of_installments');
		if(empty($interest_perc) || empty($number_of_installment)){
			return 0;
		}
		$items = $creditmemo->getItems();
        $subtotal=0;
        foreach ( $items as $item ) {
            $subtotal+=$item->getRowTotalInclTax();
        }
        $interest_fee_amount = (($interest_perc / 100) * ($number_of_installment / 12) + 1) * $subtotal - $subtotal;
		$interest_fee_amount = $interest_fee_amount<0 ? 0 : $interest_fee_amount;
		return $interest_fee_amount;

	}

	/**
	 * Get required fields for this store country
	 *
	 * @param Quote $quote
	 * @param Array $totals
	 * @param String $paymentCode
	 * @param String $numberOfInstallments
	 * @return array
	 * @throws Exception
	 */
	public function calculateDiscount($quote, $totals, $paymentCode, $numberOfInstallments = null) {
		if (! isset ( $paymentCode ) || empty ( $paymentCode )) {
			return 0;
		}
		if ($this->isInstallmentWithDepositMethod ( $paymentCode )) {
			$paymentCode = 'loviitrp';
		}
		$discountAmount = $discount = 0;
		try {
			$subtotal = $this->calculateCartSubtotal ( $quote, $totals );
			if ($this->isInstallmentMethod ( $paymentCode ) && ! $this->isInstallmentWithDepositMethod ( $paymentCode )) {
				$configInstallmentPlan = $this->getInstallmentPlansFromConfig($paymentCode);
				foreach ( $configInstallmentPlan as $plan ) {
					if ($plan ['number_of_installments'] == $numberOfInstallments) {
						if (! empty ( $plan ['discount'] )) {
							$discount = $plan ['discount'];
						} else {
							return 0;
						}
						break;
					}
				}
			} else {
				$discount = $this->config->getValue ( 'payment/' . $paymentCode . '/discount', \Magento\Store\Model\ScopeInterface::SCOPE_STORE );
			}

			$discount = str_replace ( '-', '', $discount );
			if (strpos ( $discount, '%' ) !== false) {
				$discountAmount = str_replace ( '%', '', $discount ) / 100 * ($subtotal);
			} else {
				if ($discount >= $subtotal) {
					$discountAmount = $subtotal;
				} else {
					$discountAmount = $discount;
				}
			}
			$discountAmount = '-' . $discountAmount;
		} catch ( Exception $ex ) {
			$this->_logger->addError ( $ex->getMessage () );
		}
		return $discountAmount;
	}
	public function calculateItemDiscount($itemPrice, $numberOfInstallments = null, $itemsQuantity) {
		$paymentCode = $this->paymentMethod;
		if (! isset ( $paymentCode ) || empty ( $paymentCode )) {
			return 0;
		}
		if ($this->isInstallmentWithDepositMethod ( $paymentCode )) {
			$paymentCode = 'loviitrp';
		}
		$discountAmount = $discount = 0;

		try {
			if ($this->isInstallmentMethod ( $paymentCode ) && ! $this->isInstallmentWithDepositMethod ( $paymentCode )) {
				$configInstallmentPlan = $this->getInstallmentPlansFromConfig($paymentCode);

                                foreach ( $configInstallmentPlan as $plan ) {
					if ($plan ['number_of_installments'] == $numberOfInstallments) {
						if (! empty ( $plan ['discount'] )) {
							$discount = $plan ['discount'];
						} else {
							return 0;
						}
						break;
					}
				}
			} else {
				$discount = $this->config->getValue ( 'payment/' . $paymentCode . '/discount', \Magento\Store\Model\ScopeInterface::SCOPE_STORE );
			}

			$discount = str_replace ( '-', '', $discount );
			if (strpos ( $discount, '%' ) !== false) {
				$this->disagioItemDiscount = str_replace ( '%', '', $discount ) / 100 * ($itemPrice);
			} else {
				$this->disagioItemDiscount = empty($itemsQuantity)? 0: number_format ( $discount / $itemsQuantity, 2, '.', '' );

				$discountTotal = $this->disagioItemDiscount * $itemsQuantity;

				$this->discountDifference = $discountTotal - $this->disagioTotalDiscount;
			}
		} catch ( Exception $ex ) {
			$this->_logger->addError ( $ex->getMessage () );
		}
	}

	/**
	 * Get required fields for this store country
	 *
	 * @param Quote $quote
	 * @return array
	 * @throws Exception
	 */
	public function getRequiredFieldsForThisStore($quote) {
            try {
                $requiredFields = array();
                if (!$requiredFields = @unserialize ( $this->getLoviitConfig ( 'required_fields' ) )) {
                    $requiredFields = json_decode($this->getLoviitConfig ( 'required_fields' ), true);
                }

                $return = array ();
                if (isset ( $requiredFields ) && is_array ( $requiredFields )) {
                        foreach ( $requiredFields as $key => $row ) {
                                $return [$requiredFields [$key] ['country']] = $row;
                        }
                }
                $return["ES"]["taxid"]=1; //in spain tax id is always required
                return $return;
            } catch ( Exception $ex ) {
                $this->_logger->addError ( $ex->getMessage () );
            }
            return false;
	}

	/**
	 * Get customer IP address
	 *
	 * @return string
	 * @throws Exception
	 */
	protected function getIpAddress() {
		try {
			$om = \Magento\Framework\App\ObjectManager::getInstance ();
			$obj = $om->get ( 'Magento\Framework\HTTP\PhpEnvironment\RemoteAddress' );
			$ip = $obj->getRemoteAddress ();
			return $ip;
		} catch ( Exception $ex ) {
			$this->_logger->addError ( $ex->getMessage () );
		}
		return false;
	}

	/**
	 * Update customer field when logged
	 *
	 * @param Quote $quote
	 * @param string $field
	 * @param string $value
	 * @return void
	 */
	public function updateCustomerField($quote, $field, $value) {
		$customerFactory = $this->customerFactory->create ()->load ( $quote->getCustomerId () );
		if ($this->customerSession->isLoggedIn ()) {
			$customerFactory->setData ( $field, $value )->save ();
		}
	}

	/**
	 * Set customer group id when logged in and return group id
	 *
	 * @param Quote $quote
	 * @return string
	 * @throws Exception
	 */
	public function setCustomerGroupId($quote) {
		try {
			$companyGroupId = $this->config->getValue ( 'loviit/settings/company_group_id', \Magento\Store\Model\ScopeInterface::SCOPE_STORE );
			$autoGroupId = $this->config->getValue ( 'customer/create_account/auto_group_assign', \Magento\Store\Model\ScopeInterface::SCOPE_STORE );

			$customerBillingAddress = $quote->getBillingAddress ();
			$customerBillingCompanyName = $customerBillingAddress->getCompany ();
			$customerBillingVatId = $customerBillingAddress->getVatId ();

			$customerFactory = $this->customerFactory->create ()->load ( $quote->getCustomerId () );
			$customerData = $customerFactory->getData ();
			$customerGroupId = $this->customerSession->isLoggedIn () ? $customerData ['group_id'] : $quote->getCustomerGroupId ();

			if (isset ( $customerBillingCompanyName ) && $customerBillingCompanyName != "" && isset ( $customerBillingVatId ) && $customerBillingVatId != "") {
				if ($customerGroupId != $companyGroupId) {
					if ($this->customerSession->isLoggedIn ()) {
						$customerFactory->setData ( 'group_id', $companyGroupId )->save ();
					}
					return $companyGroupId;
				}
			} else {
				if ($customerGroupId == $companyGroupId) {
					if ($this->customerSession->isLoggedIn ()) {
						$customerFactory->setData ( 'group_id', $autoGroupId )->save ();
					}
					return $autoGroupId;
				}
			}
			return $customerGroupId;
		} catch ( Exception $ex ) {
			$this->_logger->addError ( $ex->getMessage () );
		}
		return false;
	}

	public function getLoviitConfig($param) {
		return $this->config->getValue ( 'loviit/settings/' . strtolower ( $param ), \Magento\Store\Model\ScopeInterface::SCOPE_STORE );
	}

	public function setPaymentMethodFromQuote($quote) {
		try {
			$this->paymentMethod = $quote->getPayment ()->getMethodInstance ()->getCode ();

			// remove loviit
			$paymentCode = preg_replace ( '/loviit/', '', $this->paymentMethod );
			$this->paymentCode = strtoupper ( $paymentCode );

			return true;
		} catch ( Exception $ex ) {
			$this->_logger->addError ( $ex->getMessage () );
		}
	}
	public function loviitLogger($message, $level = "info") {
		try {
			$fileName = $this->getLoviitConfig ( "log_file_path" ); // "var/log/loviit.log";
			if (empty ( $fileName ))
				return;

			$date = date ( 'Y-m-d h:i:s' );
			$pid = getmypid ();

			$message = "[" . $date . "] [" . $level . "] [" . $pid . "] [" . $this->getIpAddress () . "] " . $message . "\n";

			if (! file_exists ( $fileName ))
				fopen ( $fileName, "w+" );

			error_log ( $message, 3, $fileName );
		} catch ( Exception $ex ) {
			$this->_logger->addError ( $ex->getMessage () );
		}
	}
	private function getTaxRatesFromTaxClassId($taxClassId, $productTaxClassId) {
		try {
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance ();
			$taxRateManagement = $objectManager->get ( 'Magento\Tax\Api\TaxRateManagementInterface' );

			$rates = $taxRateManagement->getRatesByCustomerAndProductTaxClassId ( $taxClassId, $productTaxClassId );
			if (is_array ( $rates )) {
			}
			foreach ( $rates as $rate ) {
				$this->loviitLogger ( " rate = " . $rate->getRate () . " country = " . $rate->getTaxCountryId () );
			}
		} catch ( Exception $ex ) {
		}
	}
	public function getFormattedPrice($price) {
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance ();
		$priceHelper = $objectManager->create ( 'Magento\Framework\Pricing\Helper\Data' );
		return $priceHelper->currency ( $price, true, false );
	}
	public function isInstallmentMethod($paymentMethod) {
		return preg_match ( "/^loviit(rp|rpiv|ic|rpcc|rpot|rpva|rppp)$/", strtolower ( $paymentMethod ) );
	}
	public function isInstallmentWithDepositMethod($paymentMethod) {
		return preg_match ( "/^loviit(rpcc|rpot|rpva|rppp)$/", strtolower ( $paymentMethod ) );
	}
	public function isInstallmentNoDepositMethod($paymentMethod) {
		return preg_match ( "/^loviit(rp|rpiv|ic)$/", strtolower ( $paymentMethod ) );
	}
	public static function getInstallmentMethodsCodes() {
		return array (
				"loviitrp",
				"loviitic",
				"loviitrpiv",
				"loviitrpcc",
				"loviitrpva",
				"loviitrpot",
				"loviitrppp"
		);
	}
	public function ibanRequired($paymentMethod) {
		return preg_match ( "/^loviit(dd|rp|rpcc|rpot|rpva|rppp)$/", strtolower ( $paymentMethod ) );
	}


	function serializeAddress($address)  {
		return serialize(
				array(
						'firstname' => $address->getFirstname(),
						'lastname'  => $address->getLastname(),
						'street'    => $address->getStreet(),
						'city'      => $address->getCity(),
						'postcode'  => $address->getPostcode(),
						'country_id'=> $address->getCountryId(),
				)
			);
	}

	public function verifyAndSetAdditionalPaymentData($additionalData, $quote,$paymentCode = null) {
		try {
			$quote->collectTotals();
			$quote->reserveOrderId();
			$this->setCustomerGroupId ( $quote );

			if (! is_array ( $additionalData )) {
				return;
			}

			$billingAddressCountry = $quote->getBillingAddress ()->getCountryId ();

			$allRequiredFields = $this->getRequiredFieldsForThisStore ( $quote );

			$billingCountryRequiredFields = isset ( $allRequiredFields [$billingAddressCountry] ) ? $allRequiredFields [$billingAddressCountry] : null;

			$generalConditions = $this->getLoviitConfig ( 'general_conditions' );
			$privacyPolicy = $this->getLoviitConfig ( 'privacy_policy' );

			$payment = $quote->getPayment()->getMethodInstance();

			if($paymentCode==null){
				$paymentCode = $payment->getCode ();
			}

			$isUnsecure = !$payment->isSecure();

			//SHIPMENT AND BILLING ADDRESS
			if($this->getLoviitConfig ( 'allow_different_shipping_address' )==0 && !$payment->isSecure()){
				if (strcmp($this->serializeAddress($quote->getBillingAddress()), $this->serializeAddress($quote->getShippingAddress())) != 0) {
					throw new CouldNotSaveException ( new Phrase("Shipping and Billing address must be the same") );
					return false;
				}

			}

			//BANK ACCOUNT
			if($this->ibanRequired ( $paymentCode )){
				//IBAN
				if (!isset($additionalData['iban']) || ! preg_match ( "/^[a-zA-Z]{2}[0-9]{2}[a-zA-Z0-9]{11,27}$/", $additionalData['iban'] )) {
					throw new CouldNotSaveException ( new Phrase("IBAN is not in the right format.") );
					return false;
				}

				//BIC
				if ((!isset($additionalData['bic'] ) || ! preg_match ( "/^[a-zA-Z0-9]{8}|[a-zA-Z0-9]{11}$/", $additionalData['bic'] )) && isset ( $billingCountryRequiredFields ) && isset ( $billingCountryRequiredFields ['bic'] ) && $billingCountryRequiredFields ['bic']) {
					throw new CouldNotSaveException ( new Phrase("BIC is not in the right format.") );
					return false;
				}

				//BANK ACCOUNT OPENING DATE
				if (isset ( $billingCountryRequiredFields ) && isset ( $billingCountryRequiredFields ['bank_account_opening_date'] ) && $billingCountryRequiredFields ['bank_account_opening_date']) {
					if (empty ( $additionalData['bank_account_opening_date'] )) {
						throw new CouldNotSaveException ( new Phrase("Bank account opening date cannot be empty.") );
						return false;
					}
					else if (!preg_match("/([012]?[1-9]|[12]0|3[01])-(0?[1-9]|1[012])-([0-9]{4})/", $additionalData['bank_account_opening_date'])) {
						throw new CouldNotSaveException ( new Phrase("Bank account opening date is not in the right format (dd-mm-yyyy).") );
						return false;
					}
					else {
						$date_parts = explode ( '-', $additionalData['bank_account_opening_date']);
						if (! checkdate ( $date_parts [1], $date_parts [0], $date_parts [2] ) || $date_parts [2]<1900) {
							throw new CouldNotSaveException ( new Phrase("Bank account opening date is not correct.") );
							return false;
						}
					}
				}
			}

			if ( isset($generalConditions) && $generalConditions != "" && (!isset($additionalData['general_conditions']) || !$additionalData['general_conditions']  )) {
				throw new CouldNotSaveException ( new Phrase("Accepting the conditions is mandatory.") );
				return false;
			}
			if ( isset ( $privacyPolicy ) && $privacyPolicy != "" &&  (!isset($additionalData['privacy_policy']) || !$additionalData['privacy_policy'])) {
				throw new CouldNotSaveException ( new Phrase("Accepting the conditions is mandatory.") );
				return false;
			}

			//GENDER
			if ($isUnsecure && isset ( $billingCountryRequiredFields ) && isset ( $billingCountryRequiredFields ['gender'] ) && $billingCountryRequiredFields ['gender']) {
				if (isset($additionalData['gender']) && $additionalData['gender'] != "") {
					$this->updateCustomerField ( $quote, 'gender', $additionalData['gender'] );
					$quote->getCustomer ()->setGender ( $additionalData['gender'] );
					$quote->setCustomerGender( $additionalData['gender'] );
				}
				else{
					throw new CouldNotSaveException ( new Phrase("Gender is mandatory.") );
					return false;
				}
			}

			//TAXID - in spain always required
			if (($isUnsecure || $billingAddressCountry=='ES' || $billingAddressCountry=='IT') && isset ( $billingCountryRequiredFields ) && isset ( $billingCountryRequiredFields ['taxid'] ) && $billingCountryRequiredFields ['taxid']) {
				if (isset($additionalData['taxid']) && $additionalData['taxid'] != "") {
					$this->updateCustomerField ( $quote, 'taxvat', $additionalData['taxid'] );
					$quote->getCustomer ()->setTaxvat ( $additionalData['taxid'] );
					$quote->setCustomerTaxvat( $additionalData['taxid'] );
				}
				else{
					throw new CouldNotSaveException ( new Phrase("Tax id is mandatory.") );
					return false;
				}
			}

			//BIRTHDAY
			if ($isUnsecure && isset ( $billingCountryRequiredFields ) && isset ( $billingCountryRequiredFields ['birthday'] ) && $billingCountryRequiredFields ['birthday']) {
				if (isset($additionalData['birthday']) && $additionalData['birthday'] != "") {
					$date = date ($additionalData['birthday'] );
					$date = substr($date,0,10);
					$this->updateCustomerField ( $quote, 'dob', $date );
					$quote->getCustomer ()->setDob ( $date );
					$quote->setCustomerDob( $date );
				}
				else{
					throw new CouldNotSaveException ( new Phrase("Date of birth is mandatory.") );
					return false;
				}
			}

			//INSTALLMENT PLAN
			if($this->isInstallmentMethod($paymentCode)){
				if (isset($additionalData['plan_selected']) && $additionalData['plan_selected']) {
					$quote->getPayment ()->addData ( array (
							'number_of_installments' => $additionalData['plan_selected']
					) );
				}
				else{
					throw new CouldNotSaveException ( new Phrase("Plan selected is mandatory." ) );
					return false;
				}
			}

			//if payment method with deposit check minimum deposit and set it in the quote
			if($this->isInstallmentWithDepositMethod($paymentCode)){
				if(!isset($additionalData["deposit"])){
					throw new CouldNotSaveException ( new Phrase("deposit is not set." ) );
				}

				$planDetails = $this->calculateInstallmentPlans($quote,$paymentCode,$additionalData['plan_selected'],$additionalData["deposit"]);
				if($planDetails['minimum_deposit']>$additionalData["deposit"] || $additionalData["deposit"]>$planDetails['total']){
					throw new LoviitException ( __("Deposit must be greater than %1 and lesser then %2",$planDetails['minimum_deposit'] ,$planDetails['total']));
				}
				$quote->addData(array('loviit_deposit' => $additionalData["deposit"]));
			}else if($payment->pushDeposit()){
				$maxInstallmentAmount = $this->getLoviitConfig('max_dd_installment');
				$total = $quote->getTotals ();
				$total = $total['grand_total']->getValue();
				$quote->addData(array('loviit_deposit' => ($total - $maxInstallmentAmount)));
			}
			else{
				$quote->addData(array('loviit_deposit' => 0));
			}

			$this->quoteRepository->save ( $quote );
			$quoteId = $quote->getId ();
			$customer = $quote->getCustomer ();
			$customerId = $customer->getId ();

			$iban = ! empty ( $additionalData ['iban'] ) ? $additionalData ['iban'] : '';
			$bic = ! empty ( $additionalData ['bic'] ) ? $additionalData ['bic'] : '';

			$this->checkoutSession->setData ( 'iban', $iban );
			$this->checkoutSession->setData ( 'bic', $bic );
			$this->checkoutSession->setData ( 'bank_account_opening_date', (! empty ( $additionalData ['bank_account_opening_date'] ) ? $additionalData ['bank_account_opening_date'] : '') );

			return $quote;
		} catch ( Exception $ex ) {
			error_log ( $ex->getMessage () );
		}
	}

	public function placeOrderAfterPaymentGatewayResponse($quote, $result, $resultRedirect = null,$messageManager = null) {
		try {
			$this->checkoutSession->setErrorMessage(null);
			$this->loviitLogger(__FUNCTION__." paymentMethod = ".$this->paymentMethod);
            if ((!empty($result->result->code) && in_array($result->result->code, array(
                        '000.000.000',
                        '000.000.100',
                        '000.100.110',
                        '000.100.111',
                        '000.100.112'
                    ))) || ($this->paymentMethod == 'loviitpl' && in_array($result->result->code, array('800.400.100', '900.300.600')))) {
                $uniqueid = $result->id;

				$config = array (
						'uniqueid' => $uniqueid
				);

				// create order in magento
				$orderId = $this->orderPlace->execute ( $quote );

				// load order from magento
				$order = $this->order->load ( $orderId );
				$order->addStatusToHistory ( $this->order->getStatus(), 'DOCOMO Unique id: ' . $uniqueid );

				$order->addData ( array (
						'number_of_installments' => $quote->getPayment ()->getData ( 'number_of_installments' ),
						'loviit_administration_fee' => $quote->getData ( 'loviit_administration_fee' ),
						'loviit_interest_fee' => $quote->getData ( 'loviit_interest_fee' ),
						'loviit_interest_fee_perc' => $quote->getData ( 'loviit_interest_fee_perc' ),
						'loviit_discount' => $quote->getData ( 'loviit_discount' ),
						'loviit_deposit' => $quote->getData ( 'loviit_deposit' ),
						'loviit_uniqueid' => $uniqueid
				) );

				$order->save ();

				$payment = $quote->getPayment()->getMethodInstance();

				// prepare soap request
				$this->setPaymentMethodFromQuote ( $quote );

				if (isset ( $result->registrationId )) {
					$this->storeCustomerRegistration ( $quote, $result->registrationId );
				}
				// skip OMM for PSP methods
				if ($payment->isPSP()) {

					$orderStatus = Order::STATE_PROCESSING;
					if ($result->paymentType == 'DB') {
						$order->addStatusToHistory ( $orderStatus, 'DOCOMO PSP Direct Billing was successful' );
						$this->order->addData ( array (
								'loviit_psp_db' => '1'
						) );
					} else {
						$order->addStatusToHistory ( $orderStatus, 'DOCOMO PSP Pre-Authorization was successful' );
						$this->order->addData ( array (
								'loviit_psp_pa' => '1'
						) );
					}

					$this->order->save ();
					if($resultRedirect!=null){
						return $resultRedirect->setPath ( 'checkout/onepage/success', [
								'_secure' => true
						] );
					}
					else {
						return $orderId;
					}
				}

				$requestData = $this->prepareSoapRequest ( $order, $config );

				// make soap request
				$result = $this->soapHelper->doSoap ( 'submitNewOrder', $requestData,$this );

				$validOrderRequest = false;
				$orderStatus = $this->getLoviitConfig('failed_or_order_status');// Order::STATE_HOLDED;
				if ($this->soapHelper->isGoodCall ( $result )) {
					$validOrderRequest = true;
					$orderStatus = Order::STATE_PROCESSING;
				} else if ($this->soapHelper->lastCode == Soap::CODE_TRANSACTION_NOT_FOUND) {
					sleep ( 10 ); // retry in 10 seconds due to payon delay
					           // make soap request
					$result = $this->soapHelper->doSoap ( 'submitNewOrder', $requestData,$this );
					if ($this->soapHelper->isGoodCall ( $result ) || $this->soapHelper->lastCode == Soap::CODE_OR_ALREADY_SUBMITTED) {
						$validOrderRequest = true;
						$orderStatus = Order::STATE_PROCESSING;
					}
					$this->loviitLogger ( " Result after second soap request [" . $this->soapHelper->lastCode . "]", "error" );
				}

				if ($validOrderRequest && $this->soapHelper->lastCode == Soap::CODE_WAITING_FOR_PREPAYMENT) {
					if($messageManager!=null)
						$messageManager->addSuccessMessage ( 'Order will be checked.' );
					$orderStatus = Order::STATE_PENDING_PAYMENT;
				}

				$msg="";

				if ($validOrderRequest) {
					$order->setCanSendNewEmailFlag (true);
					$this->loviitLogger("OR is valid");
					try {
						if(!is_null($this->orderSender)){
							$this->orderSender->send($order);
							$this->loviitLogger("email has been sent");
						}
						else{
							$this->loviitLogger("orderSender is null");
							$om = \Magento\Framework\App\ObjectManager::getInstance ();
							$this->orderSender = $om->get ( '\Magento\Sales\Model\Order\Email\Sender\OrderSender' );
							$this->orderSender->send($order);
							$this->loviitLogger("email has been sent using generated instance of orderSender");

						}
						$this->coreSession->setPixelSessionId(null);
					} catch (\Exception $e) {
						$this->loviitLogger("Exception sending confirmation email: ".$e->getMessage() );
						// do not cancel order if we couldn't send email
					}
					$order->addStatusToHistory ( $orderStatus, 'DOCOMO Order Request was successful',true ); //customer notified
					$this->order->addData ( array (
							'loviit_order_request' => '1'
					) );
				} else {
					$order->addStatusToHistory ( $orderStatus, 'DOCOMO Order Request failed:' . $this->soapHelper->lastErrorCode . " - " . $this->soapHelper->lastError );
					if($orderStatus!=Order::STATE_CANCELED){
						$msg='Your payment was successful but due to technical issues the order can\'t be confirmed. Please contact assistance.';
						if($messageManager!=null)
							$messageManager->addErrorMessage ($msg);
					}
				}

				$this->order->save ();

				if($orderStatus!=Order::STATE_CANCELED){
					if($resultRedirect!=null){
                                                $this->checkoutSession->setLastSuccessQuoteId($quote->getId());
						return $resultRedirect->setPath ( 'checkout/onepage/success', [
								'_secure' => true
						] );
					}

					//rest call
					return [
							"orderId"=>$orderId,
							"message"=>$msg
						];
				}
				else{ //order has been cancelled
					$msg="Payment was not successful";
					if($resultRedirect!=null){
						$this->checkoutSession->setErrorMessage($msg);
						//necessary to avoid redirection to card
						$this->checkoutSession->setLastOrderId(1);
						$this->checkoutSession->setLastQuoteId($quote->getId());
						$this->loviitLogger ( "redirect to checkout/onepage/failure $msg", "error" );
						return $resultRedirect->setPath('checkout/onepage/failure', ['_secure' => true]);
					}
					else{ //rest call
						throw new LoviitException($msg); //failed OR
					}
				}
			}
		} catch ( \Exception $e ) {
			$this->loviitLogger ( print_r ( $e->getMessage (), 1 ), "error" );
			if($messageManager!=null)
				$messageManager->addExceptionMessage ( $e, $e->getMessage () );
			else
				throw new LoviitException($e->getMessage ()); //failed payment
		}


		$message = $this->redirectError($result->result);
		if($resultRedirect!=null){
			$this->checkoutSession->setErrorMessage($message);
			//necessary to avoid redirection to card
			$this->checkoutSession->setLastOrderId(1);
			$this->checkoutSession->setLastQuoteId($quote->getId());
			$this->loviitLogger ( "redirect to checkout/onepage/failure $message", "error" );
			return $resultRedirect->setPath('checkout/onepage/failure', ['_secure' => true]);
		}
		else{
			throw new LoviitException($message); //failed payment
		}
	}

	//if payon fail, it does not apply to order request failure
	public function redirectError($payonError,$resultRedirect = null)
	{
		//error_log(print_r("start redirectError\n",1));
		$code = isset ( $payonError->code ) ? $payonError->code : "999.999.999";

		if(in_array($code, array("100.400.307","100.400.308"))){
			$message = __('Offer only secure methods of payment');
		} elseif($code == "100.396.101"){
			$message = __('Operation cancelled by user');
		} elseif (preg_match("/^(000\.400\.[1][0-9][1-9]|000\.400\.2)/", $code)) {
			$message = __('rejections due to 3Dsecure and Intercard risk check');
		} elseif (preg_match("/^(800\.400\.2|100\.380\.4|100\.390)/", $code)) {
			$message = __('rejections due to 3Dsecure');
		} elseif (preg_match("/^(800\.[17]00|800\.800\.[123])/", $code)) {
			$message = __('rejections by the external bank or similar payment system');
		} elseif (preg_match("/^(900\.[1234]00)/", $code)) {
			$message = __('rejections due to communication errors');
		} elseif (preg_match("/^(800\.5|999\.|600\.1|800\.800\.8)/", $code)) {
			$message = __('rejections due to system errors');
		} elseif (preg_match("/^(100\.400|100\.38|100\.370\.100)/", $code)) {
			$message = __('rejections due to checks by external risk systems');
		} elseif (preg_match("/^(800\.400\.1)/", $code)) {
			$message = __('rejections due to address validation');
		} elseif (preg_match("/^(100\.100\.701|800\.[32])/", $code)) {
			$message = __('rejections due to blacklist validation');
		} elseif (preg_match("/^(800\.1[123456]0)/", $code)) {
			$message = __('rejections due to risk validation');
		} elseif (preg_match("/^(600\.2|500\.[12|800\.121])/", $code)) {
			$message = __('rejections due to configuration validation');
		} elseif (preg_match("/^(200\.[12]|100\.[53][07]|800\.900|100\.900\.500)/", $code)) {
			$message = __('rejections due to format validation');
		} elseif (preg_match("/^(100\.800)/", $code)) {
			$message = __('rejections due to address validation');
		} elseif (preg_match("/^(100\.[97]00)/", $code)) {
			$message = __('rejections due to contact validation');
		} elseif (preg_match("/^(100\.100|100.2[01])/", $code)) {
			$message = __('rejections due to account validation');
		} elseif (preg_match("/^(100\.55)/", $code)) {
			$message = __('rejections due to amount validation');
		} elseif (preg_match("/^(200\.300\.404)/", $code)) {
			$message = __('invalid or missing parameter');
			if(isset($payonError->parameterErrors)){
				foreach ($payonError->parameterErrors as $value) {
					$message.=" - ".__($value->name).": ".__($value->message);
				}
			}
		} else {
			$message = __('An error occured');
		}

		$this->loviitLogger(__FUNCTION__. " ". $message . " [".$code."]", "error");

		return $message;

	}

	public function doNewOrderRequest($order)
	{
		$this->setPaymentMethodFromQuote($order);
		$action = "submitNewOrder";

		$config = array('uniqueid' => $order->getData("loviit_uniqueid"));

		$requestData = $this->prepareSoapRequest($order, $config);

		//make soap request
		$result = $this->soapHelper->doSoap($action, $requestData,$this,$order);

		if ($this->soapHelper->isGoodCall($result)) {
			$order->setCanSendNewEmailFlag (true);
			$order->addStatusToHistory(Order::STATE_PROCESSING, 'DOCOMO Order Request was successful.',true);
			$order->setState(Order::STATE_PROCESSING, true);
			$order->addData(array('loviit_order_request' => '1'));
			$order->save();
			return ["success"=>true,"message"=>null];
		}
		$order->addStatusToHistory($this->getLoviitConfig('failed_or_order_status'), 'submitNewOrder failed:'. $this->soapHelper->lastErrorCode . ' - ' . $this->soapHelper->lastError);
		$order->save();
		return [
				"success"=>false,
				"message"=>$this->soapHelper->lastErrorCode . ' - ' . $this->soapHelper->lastError
		];
	}

	public function doDeliveryRequest($order, $trackingId = '')
	{
		$this->setPaymentMethodFromQuote($order);
		$action = "submitNewDelivery";

		$config = array('uniqueid' => $order->getData("loviit_uniqueid"), 'trackingid' => $trackingId);

		$requestData = $this->prepareSoapRequest($order, $config);

		$this->loviitLogger(print_r( $requestData,1));

		//make soap request
		$result = $this->soapHelper->doSoap($action, $requestData,$this,$order);

		if ($this->soapHelper->isGoodCall($result)) {
			$order->addStatusToHistory(Order::STATE_COMPLETE, 'DOCOMO Delivery Request was successful. '.(!empty($trackingId) ? " Tracking code: $trackingId" : ""));
			$order->setState(Order::STATE_COMPLETE, true);
			$order->addData(array('loviit_delivery_request' => '1'));
			//$order->setTotalPaid($requestData['Total']);
			//$order->setBaseTotalPaid($requestData['Total']);
			$order->save();

			//add shipment and tracking to order
			$createShipment =  $this->config->getValue( 'loviit/settings/create_shipping_record_on_dr', \Magento\Store\Model\ScopeInterface::SCOPE_STORE );

			if($createShipment == 1){
				$this->addShipmentAndTrackingToOrder($order, $trackingId);
			}

			//Create invoice without sending it to the customer to allow proper credit memo creation
			$this->createInvoice($order,true,false);


			return ["success"=>true,"message"=>null];
		}
		$order->addStatusToHistory(Order::STATE_HOLDED, 'submitNewDelivery failed:'. $this->soapHelper->lastErrorCode . ' - ' . $this->soapHelper->lastError);
		$order->save();
		return [
				"success"=>false,
				"message"=>$this->soapHelper->lastErrorCode . ' - ' . $this->soapHelper->lastError
		];
	}

	public function doTrackingRequest($shipment,$saveShipping = true,$messageManager = null)
	{
		$tracks = $shipment->getAllTracks();
		if(sizeof($tracks)>0){
			$trackingCode = $tracks[0]->getNumber();
		}

		if(!isset($trackingCode) || empty($trackingCode)){
			$message='DOCOMO Digital Tracking request was not sent: tracking code is empty. Add the tracking code and then press on Send tracking request button';
			$shipment->addComment($message);
			if($saveShipping) //to prevent double execution of AfterShippmentSaveObserver
				$shipment->save();
			if($messageManager!=null){
				$messageManager->addError($message);
			}
			return [
				"success"=>false,
				"message"=>$message
			];
		}

		$order = $shipment->getOrder();

		$this->setPaymentMethodFromQuote($order);
		$action = "SubmitTrackingInformation";
		$config = array(
				'uniqueid' => $order->getData("loviit_uniqueid"),
				'trackingid' => $trackingCode,
				'Carrier' => $tracks[0]->getTitle(),
				'TrackingDate'=> date ( 'Y-m-d\TH:i:s\Z' )
		);

		$requestData = $this->prepareSoapRequest($order, $config,$shipment->getAllItems());

		$this->loviitLogger(print_r( $requestData,1));

		//make soap request
		$result = $this->soapHelper->doSoap($action, $requestData,$this,$order);

		if ($this->soapHelper->isGoodCall($result)) {
			$message = 'Docomo Digital Tracking Request successfully sent. '.(!empty($trackingCode) ? " Tracking code: $trackingCode" : "");
			$order->addStatusToHistory($order->getState(), $message);
			$order->save();
			$shipment->setData('docomo_notified_tracking',$trackingCode);
			$shipment->addComment($message);
			if($saveShipping) //to prevent double execution of AfterShippmentSaveObserver
				$shipment->save();
			if($messageManager!=null)
				$messageManager->addSuccess($message);
			return ["success"=>true,"message"=>$message];
		}
		$message = 'DOCOMO submitNewTracking failed: '. $this->soapHelper->lastErrorCode . ' - ' . $this->soapHelper->lastError;
		$order->addStatusToHistory($order->getState(), $message);
		$order->save();
		$shipment->addComment($message);
		if($saveShipping) //to prevent double execution of AfterShippmentSaveObserver
			$shipment->save();

		if($messageManager!=null){
			$messageManager->addError($message);
		}
		return [
				"success"=>false,
				"message"=>$message
		];
	}

	public function doCreditNoteRequest($order,$itemsToRefund = null,$additionalData=null)
	{
		$this->setPaymentMethodFromQuote($order);
		$action = "submitNewCredit";

		$config = array('uniqueid' => $order->getData("loviit_uniqueid"));

		$requestData = $this->prepareSoapRequest($order, $config, $itemsToRefund,$additionalData);

		//make soap request
		$result = $this->soapHelper->doSoap($action, $requestData,$this,$order);

		if ($this->soapHelper->isGoodCall($result)) {
			if($order->getData('loviit_delivery_request')=='0'){ //cancellation

                if($order->canUnhold())
                    $order->unhold();

                $order->cancel();
                $order->save();

                $order->addStatusToHistory( Order::STATE_CANCELED, 'DOCOMO Order Cancellation request was successful.');
			}
			else{
				$order->addStatusToHistory( $order->getStatus(), 'DOCOMO Online Refund was successful. Refunded amount: '.$order->getBaseCurrency()->formatTxt($requestData['Total']));
				$order->setState( $order->getStatus(), true);
			}

			$order->addData(array('loviit_creditnote_request' => '1'));
			$order->save();
			return ["success"=>true,"message"=>null];
		}
		$order->addStatusToHistory($order->getStatus(), 'submitNewCredit failed:'. $this->soapHelper->lastErrorCode . ' - ' . $this->soapHelper->lastError);
		$order->save();
		return [
				"success"=>false,
				"message"=>$this->soapHelper->lastErrorCode . ' - ' . $this->soapHelper->lastError
		];
	}

	public function doPspRefund($order,$itemsToRefund = null,$additionalData=null)
    {
    	$result = $this->requestPspAction('RF', $order,$itemsToRefund,$additionalData);

    	if ($this->isGoodPspResponse($result)){
			$order->addStatusToHistory($order->getStatus(), 'DOCOMO PSP Refund was successful. Refunded amount: '.$order->getBaseCurrency()->formatTxt($result->amount));
            $order->addData(array('loviit_psp_rf' => '1'));
            $order->save();

            return ["success"=>true,"message"=>null];
        }

        $order->addStatusToHistory($order->getStatus(), 'DOCOMO PSP Refund failed:'. $result->result->code . ' - ' . $result->result->description);
        $order->save();

        return [
			"success"=>false,
			"message"=>"Error Code: ".$result->result->code . ' - ' . $result->result->description
	];
	}



	private function addShipmentAndTrackingToOrder($order, $trackingId)
	{
		try {

			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			if (! $order->canShip()) {
				throw new \Magento\Framework\Exception\LocalizedException(
						__('You can\'t create an shipment.')
						);
			}

			// Initialize the order shipment object
			$convertOrder = $objectManager->create('Magento\Sales\Model\Convert\Order');
			$shipment = $convertOrder->toShipment($order);

			// Loop through order items
			foreach ($order->getAllItems() AS $orderItem) {
				// Check if order item has qty to ship or is virtual
				if (! $orderItem->getQtyToShip() || $orderItem->getIsVirtual()) {
					continue;
				}

				$qtyShipped = $orderItem->getQtyToShip();

				// Create shipment item with qty
				$shipmentItem = $convertOrder->itemToShipmentItem($orderItem)->setQty($qtyShipped);

				// Add shipment item to shipment
				$shipment->addItem($shipmentItem);
			}

			// add tracking to shipment
			if ($trackingId && $trackingId != '') {
				$orderShippingMethod = $order->getShippingMethod(true);
				$trackFactory = $objectManager->create('Magento\Sales\Model\Order\Shipment\TrackFactory');
				$shipment->addTrack(
						$trackFactory->create()
						->setNumber($trackingId)
						->setCarrierCode($orderShippingMethod->getCarrierCode())
						->setTitle($order->getShippingDescription())
						);
			}

			// Register shipment
			$shipment->register();
			$shipment->getOrder()->setIsInProcess(true);

			// Save created shipment and order
			$shipment->save();
			$shipment->getOrder()->save();

			// Send email
			$objectManager->create('Magento\Shipping\Model\ShipmentNotifier') ->notify($shipment);

			$shipment->save();

		} catch (Exception $ex) {
			error_log($ex->getMessage());
		}
	}

	function createInvoice($order,$setAsPaid = true, $notifyCustomer = false){
		//Create invoice without sending it to the customer to allow proper credit memo creation
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$invoice = $objectManager->create('Magento\Sales\Model\Service\InvoiceService')->prepareInvoice($order);

		if ($invoice->getTotalQty()) {

			$invoice->setRequestedCaptureCase(\Magento\Sales\Model\Order\Invoice::CAPTURE_ONLINE);
			$invoice->register();
			if($setAsPaid){
				$invoice->pay();
			}

			$transaction = $objectManager->create('Magento\Framework\DB\Transaction')
			->addObject($invoice)
			->addObject($invoice->getOrder());

			$transaction->save();

			if($notifyCustomer){
				$invoiceSender = $objectManager->create('Magento\Sales\Model\Order\Email\Sender\InvoiceSender');
				$invoiceSender->send($invoice);
			}
		}
	}

        public function getInstallmentPlansFromConfig($paymentCode) {
            $configInstallmentPlan = array();
            if (!$configInstallmentPlan = @unserialize ( $this->config->getValue ( 'payment/' . ($this->isInstallmentWithDepositMethod ( $paymentCode ) ? 'loviitrp' : $paymentCode) . '/installment_plans', \Magento\Store\Model\ScopeInterface::SCOPE_STORE ) )) {
                $configInstallmentPlan = json_decode($this->config->getValue ( 'payment/' . ($this->isInstallmentWithDepositMethod ( $paymentCode ) ? 'loviitrp' : $paymentCode) . '/installment_plans', \Magento\Store\Model\ScopeInterface::SCOPE_STORE ), true);
            }
            return $configInstallmentPlan;
        }

}
