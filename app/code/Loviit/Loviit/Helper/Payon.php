<?php

namespace Loviit\Loviit\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Psr\Log\LoggerInterface;

use Loviit\Loviit\Helper\Payment;

/**
 * Class which handle request and response to payon
 *
 * @author tim.zwinkels
 */
class Payon extends AbstractHelper {
    
    private $config;
    
    protected $_logger;
    
    protected $paymentHelper;
    
    var $lastErrorCode;
    
    var $lastError;
    
    public function __construct(
            LoggerInterface $logger,
            ScopeConfigInterface $config,
            Payment $paymentHelper
            ) {
        $this->_logger = $logger;
        $this->config = $config;
        $this->paymentHelper = $paymentHelper;
    }

    public function getUrl(){
        $url = $this->config->getValue('loviit/settings/live_url', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $transactionmode = $this->config->getValue('loviit/settings/transaction_mode', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        if ($transactionmode != "LIVE") {
            $url = $this->config->getValue('loviit/settings/demo_url', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        }

        return $url;
    }
    
    
    /**
     * Prepare the checkout on payon with curl
     *
     * @param Array $data
     * @param String $resourcePath
     * @return Object
     * @throws Exception
     */
    public function doPrepareRequest($data, $resourcePath)
    {
        try {
            $url = $this->config->getValue('loviit/settings/live_url', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $transactionmode = $this->config->getValue('loviit/settings/transaction_mode', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

            ini_set('default_socket_timeout', 10);

            $verifypeer = true;
            if ($transactionmode != "LIVE") {
                $url = $this->config->getValue('loviit/settings/demo_url', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                ini_set('default_socket_timeout', 60);
                $verifypeer = false;
                if ($transactionmode == "CONNECTOR_TEST") {
                    $data['testMode'] = "EXTERNAL";
                }
            }

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url. $resourcePath);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $this->dataArrayToString($data));
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $verifypeer);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $responseData = curl_exec($ch);

            if (curl_errno($ch)) {
                return curl_error($ch);
            }
            curl_close($ch);

            $this->paymentHelper->loviitLogger("Url: " . $url . $resourcePath);
            $this->paymentHelper->loviitLogger("Request: ".print_r($data,1));
            $this->paymentHelper->loviitLogger("Response: ".print_r($responseData,1));
        } catch (Exception $ex) {
            $this->_logger->addError($ex->getMessage());
        }
        return json_decode($responseData);
    }
    
    /**
     * Confirm and get the status of a checkout on payon with curl
     *
     * @param Array $data
     * @param String $resourcePath
     * @return Object
     * @throws Exception
     */
    public function getRequestCheckoutReponse($data, $resourcePath)
    {
        try {
            $url = $this->config->getValue('loviit/settings/live_url', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $transactionmode = $this->config->getValue('loviit/settings/transaction_mode', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            
            ini_set('default_socket_timeout', 10);

            $verifypeer = true;
            if ($transactionmode != "LIVE") {
                $url = $this->config->getValue('loviit/settings/demo_url', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                ini_set('default_socket_timeout', 60);
                $verifypeer = false;
            }

            $url .= urldecode($resourcePath);
            $url .= "?". $this->dataArrayToString($data);
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $verifypeer);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $responseData = curl_exec($ch);
            if (curl_errno($ch)) {
                return curl_error($ch);
            }
            curl_close($ch);
            
            $responseData = json_decode($responseData);
            $this->paymentHelper->loviitLogger("Url: " . $url . $resourcePath);
            $this->paymentHelper->loviitLogger("Request: ".print_r($data,1));
            $this->paymentHelper->loviitLogger("Response: ".print_r($responseData,1));
        
        } catch (Exception $ex) {
            $this->_logger->addError($ex->getMessage());
        }
        return $responseData;
    }
    
    private function dataArrayToString($data)
    {
        $result = "";
        foreach ($data as $key => $value) {
            
            if (is_array($value)) {
                foreach ($value as $skey => $svalue) {
                    $result .= "&". $skey ."=". urlencode($svalue);
                }
            } else {
                $result .= "&". $key ."=". urlencode($value);
            }
        }
        return substr($result, 1);
    }
    
    
    public function isGoodCall($result)
    {
        $code = $result->result->code;
        if (substr($result->result->code, 0, 3) === "000") {
            return true;
        } else {
//            $errors = $result->result->parameterErrors;
            $this->lastErrorCode = $code;
            $this->lastError = $result->result->description;
//            if (isset($errors)) {
//                $errorMessage = "";
//                foreach ($errors as $error) {
//                    $errorMessage .= $error->name ." ". $error->message;
//                }
//                $this->lastError = $errorMessage;
//            }
        }
        $this->_logger->addError($this->lastErrorCode ." ". $this->lastError);
        return false;
    }
}
