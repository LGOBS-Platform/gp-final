<?php

namespace Loviit\Loviit\Exception;

/**
 * Description of LoviitException
 *
 * @author tim.zwinkels
 */
class LoviitException extends \Exception
{
}