<?php
/**
 * Description of Payment
 *
 * @author tim.zwinkels
 */
namespace Loviit\Loviit\Controller\Payment;

use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Model\Order;

use Loviit\Loviit\Helper\OrderPlace;
use Loviit\Loviit\Helper\Payment;
use Loviit\Loviit\Helper\Payon;
use Loviit\Loviit\Helper\Soap;


/**
 * Class PlaceOrder
 */
class Response extends AbstractAction
{
    /**
     * @var OrderPlace
     */
    private $orderPlace;
    
    private $paymentHelper;
    
    private $payonHelper;
    
    private $soapHelper;
    
    private $order;
        
    private $_messageManager;

    /**
     * Constructor
     *
     * @param Context $context
     * @param Config $config
     * @param Session $checkoutSession
     * @param Helper\OrderPlace $orderPlace
     */
    public function __construct(
        Context $context,
        Session $checkoutSession,
        OrderPlace $orderPlace,
        Payment $paymentHelper,
        Payon $payonHelper,
        Soap $soapHelper,
        Order $order
    ) {
        parent::__construct($context, $checkoutSession);
        $this->orderPlace = $orderPlace;
        $this->paymentHelper = $paymentHelper;
        $this->payonHelper = $payonHelper;
        $this->soapHelper = $soapHelper;
        $this->order = $order;
        $this->_messageManager = $context->getMessageManager();
    }

    /**
     * @inheritdoc
     * @throws LocalizedException
     */
    public function execute()
    {
        
        $resourcePath = $this->getRequest()->getParam('resourcePath');
        $quote = $this->checkoutSession->getQuote();
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $requestData = $this->paymentHelper->finishSecureCheckout ( $quote );
        $result = $this->payonHelper->getRequestCheckoutReponse ( $requestData, $resourcePath );
        return $this->paymentHelper->placeOrderAfterPaymentGatewayResponse($quote, $result,$resultRedirect,$this->_messageManager);
    }
    
   
}