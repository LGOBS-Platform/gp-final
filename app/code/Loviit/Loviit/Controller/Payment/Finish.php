<?php
/**
 * Description of Payment
 *
 * @author tim.zwinkels
 */
namespace Loviit\Loviit\Controller\Payment;


use \Magento\Checkout\Model\Session;
use \Magento\Framework\App\Action\Context;
use \Magento\Framework\Controller\ResultFactory;
use \Magento\Framework\Exception\LocalizedException;
use \Magento\Framework\View\Result\PageFactory;
use \Magento\Framework\Registry;


use Loviit\Loviit\Helper\Payment;
use Loviit\Loviit\Helper\Payon;

class Finish extends AbstractAction {
    
    protected $resultPageFactory;
    /**
     * @var Helper\Payment
     */
    private $paymentHelper;
    
    private $payonHelper;
    
    protected $_coreRegistry;
    
    protected $messageManager;
        
    /**
     * Constructor
     *
     * @param Context $context
     * @param Session $checkoutSession
     * @param Payment $paymentHelper
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        Session $checkoutSession,
        Registry $coreRegistry,
        Payment $paymentHelper,
        Payon $payonHelper,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context, $checkoutSession);
        $this->_coreRegistry = $coreRegistry;
        $this->paymentHelper = $paymentHelper;
        $this->payonHelper = $payonHelper;
        $this->resultPageFactory = $resultPageFactory;
        $this->messageManager = $context->getMessageManager();
    }

    /**
     * @inheritdoc
     * @throws LocalizedException
     */
    public function execute()
    {
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        $quote = $this->checkoutSession->getQuote();
       
        try {
            //$this->validateQuote($quote);
            
            //error_log("payment = ". print_r($quote->getPayment()->getMethodInstance()->getCode(),1));
            //error_log("number of installment = ". print_r($quote->getPayment ()->getData ( 'number_of_installments' ),1));
            
            $quote->collectTotals();
            $payment = $quote->getPayment()->getMethodInstance();
            $requestData = $this->paymentHelper->prepareCheckout($quote);
            //error_log(__FILE__." - ".__FUNCTION__." - ".__FILE__);
            //error_log(print_r($requestData,1));
            $this->paymentHelper->loviitLogger("payment code before prepare request = ".$this->paymentHelper->paymentCode);
            
            if ($requestData) {
                $result = $this->payonHelper->doPrepareRequest($requestData, !$payment->isServerToServer() ? "/v1/checkouts" : "/v1/payments");
                
                if ($this->payonHelper->isGoodCall($result)) {

                    if ($payment->isServerToServer()) {
                        return $resultRedirect->setUrl('response?resourcePath='.urlencode("/v1/payments/".$result->id));
                    }
                    
                    $this->_coreRegistry->register('checkoutId', $result->id);
                    $this->terminateAction();
                } else {
                    $this->paymentHelper->loviitLogger("Finish ".$this->payonHelper->lastError . " [". $this->payonHelper->lastErrorCode ."]" , "error");
                    $this->messageManager->addErrorMessage(__("Payment cannot be processed"));
                    return $resultRedirect->setPath('checkout/cart');
                }
            } else {
                return $resultRedirect->setPath('/checkout/onepage/failure', ['_secure' => true]);
            }  
        } catch (\Exception $e) {
            $this->paymentHelper->loviitLogger(print_r($e->getMessage(),1), "error");
            return $resultRedirect->setPath('/checkout/onepage/failure', ['_secure' => true]);
        }
        return $this->resultPageFactory->create();
    }
    
    public function terminateAction()
    {
        $paymentCode = $this->paymentHelper->paymentCode;
        $this->_coreRegistry->register('paymentCode', $paymentCode);
    }
}