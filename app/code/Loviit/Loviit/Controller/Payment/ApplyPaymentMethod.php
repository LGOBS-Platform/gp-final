<?php

namespace Loviit\Loviit\Controller\Payment;

use Loviit\Loviit\Helper\Payment as PaymentHelper;

class ApplyPaymentMethod extends \Magento\Framework\App\Action\Action {

    /**
     * @var \Magento\Framework\Controller\Result\ForwardFactory
     */
    protected $resultForwardFactory;

    /**
     * @var \Magento\Framework\View\LayoutFactory
     */
    protected $layoutFactory;

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $cart;
    protected $config;
    protected $paymentHelper;
    protected $quoteRepository;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\LayoutFactory $layoutFactory
     * @param \Magento\Framework\Controller\Result\ForwardFactory $resultForwardFactory
     */
    public function __construct(
            \Magento\Framework\App\Action\Context $context, 
            \Magento\Framework\Controller\Result\ForwardFactory $resultForwardFactory, 
            \Magento\Framework\View\LayoutFactory $layoutFactory, 
            \Magento\Checkout\Model\Cart $cart, 
            \Magento\Framework\App\Config\ScopeConfigInterface $config, 
            PaymentHelper $paymentHelper,
            \Magento\Quote\Api\CartRepositoryInterface $quoteRepository

    ) {
        $this->resultForwardFactory = $resultForwardFactory;
        $this->layoutFactory = $layoutFactory;
        $this->cart = $cart;
        $this->config = $config;
        $this->paymentHelper = $paymentHelper;
        $this->quoteRepository = $quoteRepository;

        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {

        $pMethod = $this->getRequest()->getParam('payment_method');

        $quote = $this->cart->getQuote();
        
        $groupId = $this->paymentHelper->setCustomerGroupId($quote);
        $quote->setCustomerGroupId($groupId);
        
        $quote->getPayment()->setMethod($pMethod['method']);
        
        $quote->setTotalsCollectedFlag(false);
        $quote->collectTotals();
        $quote->save();

        $number_of_installment = null;
        
        if ($this->paymentHelper->isInstallmentMethod($pMethod['method'])) {
            $number_of_installment = $this->getRequest()->getParam('plan_selected');
            $quote->getPayment()->addData(array('number_of_installments' => $number_of_installment));
        } else {
            $quote->getPayment()->addData(array('number_of_installments' => null));
        }
        
//         $cartTotals = $quote->getTotals();
//         $total = $cartTotals['grand_total']->getValue();
//         $maxInstallmentAmount = $this->paymentHelper->getLoviitConfig('max_dd_installment');
                
//         if ($this->paymentHelper->isInstallmentWithDepositMethod($pMethod['method'])) {;
// 			$deposit =(!isset($pMethod['additional_data']['deposit']) || empty($pMethod['additional_data']['deposit'])) ? 0 : $pMethod['additional_data']['deposit'];
//             $quote->addData(array('loviit_deposit' => $deposit));
//         } else if (strtolower($pMethod['method']) == 'loviitrp' && $total > $maxInstallmentAmount) {
//             $quote->addData(array('loviit_deposit' => $total - $maxInstallmentAmount));
//         } else {
//             $quote->addData(array('loviit_deposit' => 0));
//         }
        

        //$quote->getPayment()->setMethod($pMethod['method']);

        $quote->setTotalsCollectedFlag(false);
        $quote->collectTotals();
        $quote->save();
        
        $this->quoteRepository->save($quote);
        
    }

}
