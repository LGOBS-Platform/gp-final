<?php
/**
 * Copyright � 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Loviit\Loviit\Controller\Payment;

use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Quote\Api\Data\CartInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\Result\Redirect;

/**
 * Abstract class AbstractAction
 */
abstract class AbstractAction extends Action
{
    /**
     * @var Session
     */
    protected $checkoutSession;

    /**
     * Constructor
     *
     * @param Context $context
     * @param Config $config
     * @param Session $checkoutSession
     */
    public function __construct(
        Context $context,
        Session $checkoutSession
    ) {
        parent::__construct($context);
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * Check whether payment method is enabled
     *
     * @inheritdoc
     */
    public function dispatch(RequestInterface $request)
    {
//        if (!$this->config->isActive() || !$this->config->isDisplayShoppingCart()) {
//            $this->_actionFlag->set('', self::FLAG_NO_DISPATCH, true);
//
//            /** @var Redirect $resultRedirect */
//            $resultRedirect = $this->resultRedirectFactory->create();
//            $resultRedirect->setPath('noRoute');
//
//            return $resultRedirect;
//        }

        return parent::dispatch($request);
    }

    /**
     * @param CartInterface $quote
     * @return void
     * @throws \InvalidArgumentException
     */
    protected function validateQuote($quote)
    {
        if (!$quote || !$quote->getItemsCount()) {
            throw new \InvalidArgumentException(__('We can\'t initialize checkout.'));
        }
    }
}