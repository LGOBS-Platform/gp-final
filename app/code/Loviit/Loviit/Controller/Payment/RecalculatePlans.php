<?php

namespace Loviit\Loviit\Controller\Payment;

use Loviit\Loviit\Helper\Payment as PaymentHelper;

class RecalculatePlans extends \Magento\Framework\App\Action\Action {
	
	public function __construct(
			\Magento\Framework\App\Action\Context $context, 
			\Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory, 
			\Magento\Framework\View\LayoutFactory $layoutFactory, 
			\Magento\Checkout\Model\Cart $cart, 
			\Magento\Framework\App\Config\ScopeConfigInterface $config, 
			PaymentHelper $paymentHelper
			) {
				$this->resultJsonFactory = $resultJsonFactory;
				$this->layoutFactory = $layoutFactory;
				$this->cart = $cart;
				$this->config = $config;
				$this->paymentHelper = $paymentHelper;
	
				parent::__construct($context);
	}
	
	public function execute() {
		$methodCode = $this->getRequest()->getParam('method');
		$additionalData = $this->getRequest()->getParam('additional_data');
		
		$result = $this->resultJsonFactory->create();
		
		$skipCheck = isset($additionalData['force_loading']) && $additionalData['force_loading']=='true';
		
		if(!$skipCheck && (!isset($additionalData['deposit']) || empty($additionalData['deposit'])) && $this->paymentHelper->isInstallmentWithDepositMethod($methodCode)){
			return $result->setData(['success' => false, 'message' =>  __('Deposit cannot be empty')]); 
		}
		
		if($this->paymentHelper->isInstallmentNoDepositMethod($methodCode) || $skipCheck){
			$deposit=0;
		}
		else{
			$deposit=trim($additionalData['deposit']);
			$deposit=str_replace(",", ".", $deposit);
		}

		
		if(!is_numeric($deposit)){
			return $result->setData(['success' => false, 'message' =>  __('Deposit must be a number')]);
		}
		
		$quote = $this->cart->getQuote ();
		
		$newCustomerGroupId = $this->paymentHelper->setCustomerGroupId ( $quote );
		if ($newCustomerGroupId !== false) {
			$quote->setCustomerGroupId ( $newCustomerGroupId )->save ();
		}
		$quote->setTotalsCollectedFlag ( false );
		$quote->collectTotals ();
		$quote->save ();
		
		$plans = $this->paymentHelper->calculateInstallmentPlans ( $quote, $methodCode, null, $deposit );
		
		if($this->paymentHelper->isInstallmentWithDepositMethod($methodCode)){
			if(isset ( $additionalData ['plan_selected'] ) && $additionalData ['plan_selected'] > 0)
				$first_plan=$plans[$additionalData ['plan_selected']];
			else
				$first_plan = current($plans);
			$minimum_amount = $first_plan['minimum_deposit'];
			$maximum_amount = $first_plan['total'];
			
			if(!$skipCheck && ($deposit < $minimum_amount || $deposit>$maximum_amount)){
				return $result->setData ( [ 
						'success' => false,
						'message' => __ ( 'Deposit must be greater than' ) ." $minimum_amount ".__ ( 'and lesser than' )." $maximum_amount "
				] );
			}
		}
		
		return $result->setData(['success' => true, 'result' => $plans]);
	}
}