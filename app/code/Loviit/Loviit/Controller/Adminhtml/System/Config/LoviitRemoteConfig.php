<?php
/**
 * Copyright � 2016 MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
 
namespace Loviit\Loviit\Controller\Adminhtml\System\Config;
 
use \Magento\Backend\App\Action;
use \Magento\Backend\App\Action\Context;
use \Magento\Framework\Controller\Result\JsonFactory;
use \Magento\Config\Model\ResourceModel\Config;
use Loviit\Loviit\Helper\Json;
use Loviit\Loviit\Helper\Payment;
 
class LoviitRemoteConfig extends Action
{
 
    protected $resultJsonFactory;

    protected $jsonHelper;
    
    protected $paymentHelper;
    
    protected $resourceConfig;
     
    /**
     * @param Context $context
     * @param JsonFactory $resultJsonFactory
     * @param Data $helper
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        Json $jsonHelper,
        Payment $paymentHelper,
        Config $resourceConfig
    )
    {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->jsonHelper = $jsonHelper;
        $this->paymentHelper = $paymentHelper;
        $this->resourceConfig = $resourceConfig;
        parent::__construct($context);
    }
 
    /**
     * Collect relations data
     *
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {  
        try {
            
            $username           = $this->getRequest()->getParam('username');
            $password           = $this->getRequest()->getParam('password');
            $merchantId         = $this->getRequest()->getParam('merchantId');
            
            if (!isset($username) || $username == "") {
                throw new \Exception(__('Error : SOAP User is mandatory'));
            }
            if (!isset($password) || $password == "") {
                throw new \Exception(__('Error : SOAP Password is mandatory'));
            }
            if (!isset($merchantId) || $merchantId == "") {
                throw new \Exception(__('Error : Merchant ID is mandatory'));
            }
            
            
            $userData = array('username' => $username,
                        'password' => $password,
                        'distributorId' => $merchantId
                        );
            
            $remoteConfiguration = $this->jsonHelper->doJsonCall(
                    $userData, 
                    '/omgui/ws/RPC/plugin/load');
            
            if ($this->jsonHelper->isGoodCall($remoteConfiguration)) {
                  
                $configuration = $this->readRemoteConfiguration($userData, $remoteConfiguration);

                if (is_array($configuration)) {
                    foreach ($configuration as $param => $value) {
                        if (!is_array($value)) {
                            $this->saveConfig('loviit/settings/'.$param, $value);
                        } else {
                            foreach ($value as $paymentCode => $paymentParams) {
                                foreach ($paymentParams as $paymentParam => $paymentValue) {
                                    $this->saveConfig('payment/loviit'.$paymentCode."/".$paymentParam, $paymentValue);
                                }
                            }
                        }
                    }
                    $this->messageManager->addSuccess(__('Configuration successful updated'));
                }
            } else {
                throw new \Exception($this->jsonHelper->errorMessage);
            }
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            $this->saveConfig('loviit/settings/configuration_editable', 1);
        }
        
        $result = $this->resultJsonFactory->create();
        return $result->setData(['success' => true]);
    }
    
    private function readRemoteConfiguration($userData = array(), $remoteConfiguration = array())
    {
        $configuration = array();
        try {

            //SAVING GENERAL SETTINGS
            $configuration['configuration_version'] = $remoteConfiguration['configuration_version'];
            $configuration['soap_user']             = $userData['username'];
            $configuration['soap_pw']               = $userData['password'];
            $configuration['soap_timeout']          = $remoteConfiguration['soap_timeout'];

            $configuration['live_url']              = $remoteConfiguration['live_url'];
            $configuration['demo_url']              = $remoteConfiguration['demo_url'];
            $configuration['user_login']            = $remoteConfiguration['user_login'];
            $configuration['user_password']         = $remoteConfiguration['user_password'];
            $configuration['merchant_id']           = $userData['distributorId'];

            $configuration['transaction_mode']      = $this->getRequest()->getParam('mode');
            $configuration['cc_types']              = implode(',', $remoteConfiguration['cc_accepted']);
            $configuration['dc_types']              = implode(',', $remoteConfiguration['dc_accepted']);

            $configuration['general_conditions']    = $remoteConfiguration['terms_url'];
            $configuration['privacy_policy']        = $remoteConfiguration['privacy_url']; 

            $configuration['payment_model']         = $remoteConfiguration['payment_model'];
            $configuration['due_date_b2c']          = $remoteConfiguration['due_date_b2c'];
            $configuration['due_date_b2b']          = $remoteConfiguration['due_date_b2b'];
            
            $configuration['psp_login']             = $remoteConfiguration['psp_login'];
            $configuration['psp_password']          = $remoteConfiguration['psp_password'];
            
            //$configuration['autocapture_active']    = $remoteConfiguration['autocapture_active'];
            
            $configuration['max_dd_installment']    = $remoteConfiguration['rpcc_limit'];
            
            $configuration['configuration_editable']= 0;
            if (isset($remoteConfiguration['configuration_editable'])) {
                $configuration['configuration_editable']= $remoteConfiguration['configuration_editable'];
            }
            
            if (is_array($remoteConfiguration['required_fields'])) {
                $requiredFields = array();
                foreach ($remoteConfiguration['required_fields'] as $country => $params) {
                    $requiredFields[] = array('country' => $country, 
                        'taxid' => isset($params['taxid']) ? $params['taxid'] : '0', 
                        'gender' => isset($params['gender']) ? $params['gender'] : '0', 
                        'birthday' => isset($params['dob']) ? $params['dob'] : '0', 
                        'bic' => isset($params['bic']) ? $params['bic'] : '0',
                    	'bank_account_opening_date' => isset($params['bank_account_opening_date']) ? $params['bank_account_opening_date'] : '0');
                } 
                $configuration['required_fields']       = serialize($requiredFields);
            }
            
            //READ PAYMENTS SETTINGS
            
            if (is_array($remoteConfiguration['paymentMethods'])) {
                $index = 1;
                foreach ($remoteConfiguration['paymentMethods'] as $paymentCode => $params) {

                    $configuration['payment'][$paymentCode]['active'] = isset($params['active']) ? $params['active'] : '0';
                    $configuration['payment'][$paymentCode]['channel_id'] = $params['channel'];
                    $configuration['payment'][$paymentCode]['minimum_amount'] = $params['min_amount'];
                    $configuration['payment'][$paymentCode]['maximum_amount'] = $params['max_amount'];

                    if (in_array($paymentCode, array('rp', 'ic', 'rpiv'))) {
                        $installmentPlans = array();
                        foreach ($params['installment_plans'] as $months => $detail){
                            $installmentPlans[$index]['number_of_installments'] = $months;
                            $administrationFee = $detail['administration_fee'];
                            if(!empty($administrationFee)) {
                                $administrationFee .= ($detail['administration_fee_type'] == 'PERC' ? '%' :'');
                            } else {
                                $administrationFee = 0;
                            }
                            $installmentPlans[$index]['fee'] = $administrationFee;
                            $installmentPlans[$index]['interest'] = $detail['interest'];
                            
                            $discount = $detail['discount'];
                            if(!empty($discount)) {
                                $discount .= ($detail['discount_type'] == 'PERC' ? '%' :'');
                            } else {
                                $discount = 0;
                            }
                            $installmentPlans[$index]['discount'] = $discount;
                            
                            $configuration['payment'][$paymentCode]['installment_plans'] = serialize($installmentPlans);
                            $index++;
                        }
                    } else {
                        $configuration['payment'][$paymentCode]['administration_fee']   = $params['administration_fee'].(!empty($params['administration_fee']) && $params['administration_fee_type'] == 'PERC' ? '%' :'');
                        $configuration['payment'][$paymentCode]['discount']             = $params['discount'].(!empty($params['discount']) && $params['discount_type'] == 'PERC' ? '%' :'');

                        if (in_array($paymentCode, array('cc', 'dc', 'ppal'))) {
                            $configuration['payment'][$paymentCode]['psp_type']         = $params['psp_type'];
                            //$configuration['payment'][$paymentCode]['autocapture'] = $params['autocapture'];
                            //$configuration['payment'][$paymentCode]['cp_delay'] = $remoteConfiguration['paymentMethods'][$paymentCode]['cp_delay'];
                        }
                        if (in_array($paymentCode, array('cc', 'dc', 'ppal', 'su'))) {
                            $configuration['payment'][$paymentCode]['psp'] = $remoteConfiguration['paymentMethods'][$paymentCode]['psp'];
                        }

                    }
                }
            } 
        } catch (\Exception $e) {
            throw new \Exception(__('Error')." ".$e->getMessage());
        }
        //error_log(print_r($configuration,1), 3, "var/log/loviit.log");
        return $configuration;
    }
    
    private function saveConfig($path, $value)
    {
//         error_log("path = ".$path ." value = ".$value);
        
        $this->resourceConfig->saveConfig(
                    $path,
                    $value,
                    'default',
                    0
                );
        
    }

}