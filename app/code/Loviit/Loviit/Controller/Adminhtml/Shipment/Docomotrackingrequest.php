<?php
/**
 *
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Loviit\Loviit\Controller\Adminhtml\Shipment;

use \Magento\Backend\App\Action;
use Loviit\Loviit\Helper\Payment;
use \Magento\Backend\App\Action\Context;

class Docomotrackingrequest extends \Magento\Backend\App\Action
{
	
	protected $paymentHelper;
	protected $shipmentLoader;
	protected $shipmentRepository;
	
	/**
	 * @param Context $context
	 * @param JsonFactory $resultJsonFactory
	 * @param Data $helper
	 */
	public function __construct(
			Context $context,
			Payment $paymentHelper,
			\Magento\Shipping\Controller\Adminhtml\Order\ShipmentLoader $shipmentLoader,
			\Magento\Sales\Api\ShipmentRepositoryInterface $shipmentRepository
			)
	{
		$this->shipmentLoader = $shipmentLoader;
		$this->paymentHelper=$paymentHelper;
		$this->shipmentRepository=$shipmentRepository;
		parent::__construct($context);
	}
    
    public function execute()
    {
    	$this->shipmentLoader->setShipmentId($this->getRequest()->getParam('shipment_id'));
    	$shipment = $this->shipmentLoader->load();
    	
        $shipment->setData("docomo_notified_tracking","false");
    	$this->shipmentRepository->save($shipment);
    	
        //notifications are triggered by AfterShippmentSaveObserver
        
        
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setRefererOrBaseUrl();
        return $resultRedirect;
    }
}
