<?php

namespace Loviit\Loviit\Controller\Adminhtml\Order;

/**
 * Description of LoviitReviewPayment
 *
 * @author tim.zwinkels
 */
use \Magento\Sales\Model\Order\Shipment\TrackFactory;
use Magento\Backend\App\Action;
use Magento\Sales\Api\OrderManagementInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Order;
use Psr\Log\LoggerInterface;
use Loviit\Loviit\Exception\LoviitException;
use Loviit\Loviit\Helper\Payment;
use Loviit\Loviit\Helper\Payon;
use Loviit\Loviit\Helper\Json;
use Loviit\Loviit\Helper\Soap;

class LoviitReviewPayment extends \Magento\Sales\Controller\Adminhtml\Order\ReviewPayment {
    
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Magento_Sales::review_payment';
    
    private $paymentHelper;
    private $payonHelper;

    private $soapHelper;
    
    protected $trackFactory;
    
    public function __construct(
        Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\Translate\InlineInterface $translateInline,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        OrderManagementInterface $orderManagement,
        OrderRepositoryInterface $orderRepository,
        LoggerInterface $logger,
        Payment $paymentHelper,
        Payon $payonHelper,
        Soap $soapHelper,
        Json $jsonHelper,
        TrackFactory $trackFactory
    ) {
        $this->paymentHelper = $paymentHelper;
        $this->payonHelper = $payonHelper;
        $this->soapHelper = $soapHelper;
        $this->jsonHelper = $jsonHelper;
        $this->trackFactory = $trackFactory;
        parent::__construct($context, $coreRegistry, $fileFactory, $translateInline, $resultPageFactory, $resultJsonFactory, $resultLayoutFactory, $resultRawFactory, $orderManagement, $orderRepository, $logger);
    }
    
    /**
     * Manage payment state
     *
     * Either denies or approves a payment that is in "review" state
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            $order = $this->_initOrder();
            if ($order) {
                $this->paymentHelper->setPaymentMethodFromQuote($order);
                $action = $this->getRequest()->getParam('action', '');
                switch ($action) {
                    case 'accept':
                        $order->getPayment()->accept();
                        $message = __('The payment has been accepted.');
                        break;
                    case 'deny':
                        $order->getPayment()->deny();
                        $message = __('The payment has been denied.');
                        break;
                    case 'update':
                        $order->getPayment()->update();
                        if ($order->getPayment()->getIsTransactionApproved()) {
                            $message = __('Transaction has been approved.');
                        } else if ($order->getPayment()->getIsTransactionDenied()) {
                            $message = __('Transaction has been voided/declined.');
                        } else {
                            $message = __('There is no update for the transaction.');
                        }
                        break;
                    case 'loviitdelivery':
                        $allItems = $order->getItems();
                        $deliveryCheck = true;
                        $deliveryFailItem = null;

                        foreach ($allItems as $item){

                            if($item->getProductType() == 'configurable')
                                continue;

                            if($order->getStorId() ==2) //only pl(2) use interface
                            {
                                if(!in_array($item->getData('interface_status'),
                                    [
                                        \Eguana\GERP\Model\Source\Status::GERP_ORDER_INVO,
                                    ])){
                                    $deliveryCheck = false;
                                    $deliveryFailItem = $item;
                                    break;
                                }
                            }
                            $deliveryFailItem = $item;
                        }

                        if($deliveryCheck){
                            $trackingId = $this->getRequest()->getParam('trkid', '');
                            $result = $this->paymentHelper->doDeliveryRequest($order,$trackingId);
                            if ($result["success"]) {
                                $message = "DOCOMO Delivery Request was successful";
                            } else {
                                throw new LoviitException('Deliver Request Failed.');
                            }
                        }else{
                            throw new LoviitException(sprintf("%s Item Status Fail.",$deliveryFailItem->getName()));
                        }
                        break;
                    case 'loviitcreditnote':
                        $allItems = $order->getItems();
                        $cancelCheck = true;
                        $cancelFailItem = null;

                        foreach ($allItems as $item){

                            if($item->getProductType() == 'configurable')
                                continue;

                            if($order->getStorId() ==2) //only pl(2) use interface
                            {
                                if(is_null($item->getData('interface_status')) ||
                                    ($item->getData('interface_status') < \Eguana\GERP\Model\Source\Status::GERP_ORDER_ENTERED ||
                                        $item->getData('interface_status') >= \Eguana\GERP\Model\Source\Status::GERP_ORDER_PICK)){
                                    $cancelCheck = false;
                                    $cancelFailItem = $item;
                                    break;
                                }
                            }
                            $cancelFailItem = $item;
                        }

                        if($cancelCheck){
                            $result = $this->paymentHelper->doCreditNoteRequest($order);
                            if ($result["success"]) {
                                $message = "DOCOMO Cancel Order Request was successful";
                            } else {
                                throw new LoviitException('CreditNote Request Failed.');
                            }

                        }else {
                            throw new LoviitException(sprintf("%s Item Cancel Fail.", $cancelFailItem->getName()));
                        }
                        break;
                    case 'loviitorder':
                    	$result = $this->paymentHelper->doNewOrderRequest($order);
                        if ($result["success"]) {
                            $message = "DOCOMO Order Request was successful";
                        } else {
                            throw new LoviitException('Order Request Failed.');
                        }
                        break;
                    case 'loviitcapture':
                    	if ($this->doPspCapture($order)) {
                            $message = "DOCOMO Capture was successful";
                        } else {
                            throw new LoviitException('Capture Failed.');
                        }
                        break;
                    case 'loviitreversal':
                    	if ($this->doPspReversal($order)) {
                            $message = "DOCOMO Reversal was successful";
                        } else {
                            throw new LoviitException('Reversal Failed.');
                        }
                        break;
                    /*case 'loviitrefund':
                    	if ($this->doPspRefund($order)) {
                            $message = "DOCOMO Refund was successful";
                        } else {
                            throw new LoviitException('Refund Failed.');
                        }
                        break;*/
                    case 'loviitstatus':
                    	if ($message = $this->doStatusRequest($order)) {
                        } else {
                            throw new LoviitException('Retrieving status Failed.');
                        }
                        break;
                    default:
                        throw new \Exception(sprintf('Action "%s" is not supported.', $action));
                }
                $this->orderRepository->save($order);
                $this->messageManager->addSuccess($message);
            } else {
                $resultRedirect->setPath('sales/*/');
                return $resultRedirect;
            }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addError($e->getMessage());
        } catch (LoviitException $e) {
            $this->messageManager->addError($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addError(__('We can\'t update the payment right now.'));
            $this->logger->critical($e);
        }
        $resultRedirect->setPath('sales/order/view', ['order_id' => $order->getEntityId()]);
        return $resultRedirect;
    }
    
    private function doPspCapture($order)
    {
    	$result = $this->paymentHelper->requestPspAction('CP', $order);
    	
    	if ($this->paymentHelper->isGoodPspResponse($result)){
            $order->addStatusToHistory(Order::STATE_COMPLETE, 'DOCOMO PSP Capture was successful.');
            $order->addData(array('loviit_psp_cp' => '1'));
            $order->save();
            
            return true;
        } 

        $order->addStatusToHistory(Order::STATE_HOLDED, 'DOCOMO PSP Capture failed:'. $result->result->code . ' - ' . $result->result->description);
        $order->save();
        
        return false;
    }
    
    private function doPspReversal($order)
    {
    	$result = $this->paymentHelper->requestPspAction('RV', $order);
    	
    	if ($this->paymentHelper->isGoodPspResponse($result)){
			$order->addStatusToHistory(Order::STATE_CANCELED, 'DOCOMO PSP Reversal was successful.');
            $order->addData(array('loviit_psp_rv' => '1'));
            $order->save();
            
            return true;
        } 

        $order->addStatusToHistory(Order::STATE_HOLDED, 'DOCOMO PSP Reversal failed:'. $result->result->code . ' - ' . $result->result->description);
        $order->save();
        
        return false;
    }
    
    
    
    private function doStatusRequest($order)
    {
                     
        $results = $this->jsonHelper->doJsonCall($this->paymentHelper->buildSimpleAuthenticationArray(), '/omgui/ws/order/'. $order->getData("loviit_uniqueid"));
        
        $statusMsg = '<table class="table" style="text-align:left"><thead><tr><th width="250">request</th><th width="250">date</th><th width="100">status</th><th width="100">statuscode</th></tr></thead><tbody>';
        foreach ($results as $key => $result) {
            if (in_array($key, array('result', 'orderStatus' ,'orderRequests', 'deliveryRequests', 'creditRequests', 'changeOrderRequests'))) {
                if (in_array($key, array('orderRequests', 'deliveryRequests', 'creditRequests', 'changeOrderRequests'))) {
                    if (is_array($result) && count($result) > 0) {
                        foreach ($result as $request) {
                            $statusMsg .= "<tr><td>".$key."</td><td>". $request['requestTimeStamp']."</td><td>".$request['resultDescription']."</td><td>".$request['resultCode']."</td></tr>";
                        }
                    }  else {
                        $statusMsg .= "<tr><td>".$key."</td><td>-</td><td>-</td><td>-</td></tr>";
                    }
                } else {
                    $statusMsg .= "<tr><td>".$key."</td><td></td><td>".$result['status']."</td><td>-</td></tr>";
                }
            }
        }
        $statusMsg .= '</tbody></table>';
        
   
        return $statusMsg;
    }
    
    
}
