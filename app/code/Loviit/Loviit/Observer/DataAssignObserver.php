<?php
/**
 * Copyright � 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Loviit\Loviit\Observer;

use \Magento\Payment\Observer\AbstractDataAssignObserver;
use \Magento\Framework\Event\Observer as EventObserver;
use \Magento\Quote\Api\Data\PaymentInterface;
use \Magento\Config\Model\ResourceModel\Config;
use \Magento\Payment\Model\Config as PaymentConfig;
use Psr\Log\LoggerInterface as Logger;

use \Magento\Quote\Api\CartRepositoryInterface;
use \Magento\Checkout\Model\Session as CheckoutSession;

use \Magento\Customer\Model\CustomerFactory;

use Loviit\Loviit\Helper\Payment;

/**
 * Class DataAssignObserver
 */
class DataAssignObserver extends AbstractDataAssignObserver
{
    const PAYMENT_METHOD_NONCE = 'payment_method_nonce';
    const DEVICE_DATA = 'device_data';

    /**
     * @var array
     */
    protected $additionalInformationList = [
        self::PAYMENT_METHOD_NONCE,
        self::DEVICE_DATA
    ];
    
    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var \Magento\Payment\Model\Config
     */
    protected $_paymentConfig;
    
    /**
     * @var \Magento\Config\Model\ResourceModel\Config
     */
    protected $_resourceConfig;
    
    protected $_paymentHelper;
    
    /** @var CheckoutSession */
    protected $checkoutSession;
    
    protected $_quoteRepository;
    
    protected $customerFactory;
    
    /**
     * @param Logger $logger
     */
    public function __construct(
        Logger $logger,
        PaymentConfig $paymentConfig,
        Config $resourceConfig,
        Payment $paymentHelper,
        CheckoutSession $checkoutSession,
        CartRepositoryInterface $quoteRepository,
        CustomerFactory $customerFactory 
    ) {
        $this->logger = $logger;
        $this->_paymentConfig = $paymentConfig;
        $this->_resourceConfig = $resourceConfig;
        $this->_paymentHelper = $paymentHelper;
        $this->checkoutSession = $checkoutSession;
        $this->_quoteRepository = $quoteRepository;
        $this->customerFactory = $customerFactory;
    }
    

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(EventObserver $observer)
    {
        try {
            $this->_paymentHelper->loviitLogger(__FUNCTION__);
            $data = $this->readDataArgument($observer);
            $additionalData = $data->getData(PaymentInterface::KEY_ADDITIONAL_DATA);
			$quote = $this->checkoutSession->getQuote();
            $method = $quote->getPayment()->getMethod();
            
            if(strpos($method,'loviit')!==0)
            	return;
            
            $quote->collectTotals();
            $quote->reserveOrderId();
            
            $additionalData = $data->getData(PaymentInterface::KEY_ADDITIONAL_DATA);
            
            $this->_paymentHelper->loviitLogger("additionalData =".print_r($additionalData,1));
            
            if (!is_array($additionalData)) {
            	return;
            }

            $this->_paymentHelper->verifyAndSetAdditionalPaymentData($additionalData, $quote);
            
        } catch (Exception $ex) {
            error_log($ex->getMessage());
        }
    }
    
    private function updateCustomerGroupId($quote)
    {
        $billingAddress = $quote->getBillingAddress();
        $billingCompanyName = $billingAddress->getCompany();
        $billingVatId = $billingAddress->getVatId();

        $customerFactory = $this->customerFactory->create()->load($quote->getCustomerId());
        $customerData = $customerFactory->getData();
        $customerGroupId = $customerData['group_id'];

        $companyGroupId = $this->scopeConfig->getValue('loviit/settings/company_group_id', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $autoGroupId = $this->scopeConfig->getValue('customer/create_account/auto_group_assign', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        if (isset($billingCompanyName) && $billingCompanyName != ""
                && isset($billingVatId) && $billingVatId != "") {
            if ($customerGroupId != $companyGroupId) {
                $customerFactory->setData('group_id', $companyGroupId)->save();
            }
        } else {
            if ($customerGroupId == $companyGroupId) {
                $customerFactory->setData('group_id', $autoGroupId)->save();
            }
        }
    }
}