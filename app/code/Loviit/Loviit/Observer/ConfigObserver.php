<?php

namespace Loviit\Loviit\Observer;

use \Magento\Framework\Event\ObserverInterface;
use \Magento\Framework\Event\Observer as EventObserver;
use \Magento\Config\Model\ResourceModel\Config;
use \Magento\Payment\Model\Config as PaymentConfig;
use Psr\Log\LoggerInterface as Logger;

use Loviit\Loviit\Helper\Payment;

class ConfigObserver implements ObserverInterface
{
    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var \Magento\Payment\Model\Config
     */
    protected $_paymentConfig;
    
    /**
     * @var \Magento\Config\Model\ResourceModel\Config
     */
    protected $_resourceConfig;
    
    protected $_paymentHelper;
    
    /**
     * @param Logger $logger
     */
    public function __construct(
        Logger $logger,
        PaymentConfig $paymentConfig,
        Config $resourceConfig,
        Payment $paymentHelper
            
    ) {
        $this->logger = $logger;
        $this->_paymentConfig = $paymentConfig;
        $this->_resourceConfig = $resourceConfig;
        $this->_paymentHelper = $paymentHelper;
    }

    public function execute(EventObserver $observer)
    {
        $params = array('active', 'title', 'specificcountry', 'allowspecific', 'sort_order');
        $methods = $this->_paymentConfig->getActiveMethods();
        foreach ($methods as $method) {
            //payment/loviitic/active
            //payment/loviitic/title
            //payment/loviitic/installment_plans
            
            if (in_array($method->getCode(), array('loviitrp', 'loviitrpiv', 'loviitic'))) {
                
                $active = $method->getConfigData('active');
                if ($active) {
                    $installmentPlans = array();
                    if (!$installmentPlans = @unserialize($method->getConfigData('installment_plans'))) {
                        $installmentPlans = json_decode($method->getConfigData('installment_plans'), true);
                    }
                    
                    if(is_array($installmentPlans)){
	                    foreach ($installmentPlans as $installmentPlan) {
	                        $nbOfInstallments = $installmentPlan['number_of_installments'];
	                        
	                        $newMethodCode = $method->getCode() . $nbOfInstallments;
	                        
	                        foreach ($params as $param) {
	                            
	                            $value = $method->getConfigData($param);
	                            if ($param == 'sort_order') {
	                                $value += 1; 
	                            } else if ($param == 'title') {
	                                $value .= " (".$nbOfInstallments . " ".__("months").")";
	                            }
	                            
	                            $this->_resourceConfig->saveConfig(
	                                'payment/' . $newMethodCode . '/'.$param,
	                                $value,
	                                'default',
	                                '0'
	                            );
	                        }
	                    }
                    }
                }
            }
        }
    }
}
