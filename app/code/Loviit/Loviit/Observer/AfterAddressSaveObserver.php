<?php
/**
 * Copyright � 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Loviit\Loviit\Observer;

use \Magento\Framework\Event\ObserverInterface;
use \Magento\Framework\Event\Observer as EventObserver;
use \Magento\Checkout\Model\Session as CheckoutSession;
use \Magento\Customer\Model\CustomerFactory;
use \Magento\Framework\App\Config\ScopeConfigInterface;


class AfterAddressSaveObserver implements ObserverInterface
{
    protected $checkoutSession;
    
    protected $customerFactory;
    
    protected $scopeConfig;
    
    public function __construct(
            CheckoutSession $checkoutSession, 
            CustomerFactory $customerFactory,
            ScopeConfigInterface $scopeConfig)
    {
        $this->checkoutSession = $checkoutSession;
        $this->customerFactory = $customerFactory;
        $this->scopeConfig = $scopeConfig;
    }


    public function execute(EventObserver $observer)
    {        
        $quote = $this->checkoutSession->getQuote();
        $customerGroupId = $quote->getCustomerGroupId();
        $customerAddress = $observer->getCustomerAddress();
        
        if ($this->isDefaultBilling($customerAddress)) {
            $billingCompanyName = $customerAddress->getCompany();
            $billingVatId = $customerAddress->getVatid();
            
            $companyGroupId = $this->scopeConfig->getValue('loviit/settings/company_group_id', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $autoGroupId = $this->scopeConfig->getValue('customer/create_account/auto_group_assign', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

            if (isset($billingCompanyName) && $billingCompanyName != ""
                    && isset($billingVatId) && $billingVatId != "") {
                if ($customerGroupId != $companyGroupId) {
                    $this->updateCustomerGroupId($quote->getCustomerId(), $companyGroupId);
                }
            } else {
                if ($customerGroupId == $companyGroupId) {
                    $this->updateCustomerGroupId($quote->getCustomerId(), $autoGroupId);
                }
            }
        }
    }
    
    private function updateCustomerGroupId($customerId, $companyGroupId)
    {
        if ($customerId > 0) {
            $customerFactory = $this->customerFactory->create()->load($customerId);
            $customerFactory->setData('group_id', $companyGroupId)->save();
        }
    }
    
    protected function isDefaultBilling($address)
    {
        return $address->getId() && $address->getId() == $address->getCustomer()->getDefaultBilling()
        || $address->getIsPrimaryBilling()
        || $address->getIsDefaultBilling();
    }
}
