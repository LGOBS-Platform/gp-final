<?php

namespace Loviit\Loviit\Observer;

use \Magento\Framework\Event\ObserverInterface;
use \Magento\Framework\Event\Observer as EventObserver;
use Loviit\Loviit\Helper\Payment;

class AfterShippmentSaveObserver implements ObserverInterface
{
   
	protected $paymentHelper = null;
	protected $messageManager = null;
	
	public function __construct(
			Payment $paymentHelper,
			\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
			\Magento\Framework\Message\ManagerInterface $messageManager
			){
		$this->paymentHelper = $paymentHelper;
		$this->scopeConfig = $scopeConfig;
		$this->messageManager = $messageManager;
	}
    
 
    public function execute(EventObserver $observer)
    {
    	$sendTRonShipmentCreationEnabled = $this->scopeConfig->getValue('loviit/settings/send_tr_on_shipment_creation', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    	if($sendTRonShipmentCreationEnabled!=1){
    		return;
    	}
    	$shipment = $observer->getEvent()->getShipment();
    	
    	//when tracking request is sent manually, in order to trigger this after shipment save event it set the field to false. here it is reset
    	if($shipment->getData('docomo_notified_tracking')=='false'){
    		$shipment->setData('docomo_notified_tracking',null);
    	}
    	
    	/** @var \Magento\Sales\Model\Order $order */
    	$order = $shipment->getOrder();
    	$paymentCode = $order->getPayment()->getMethod();
    	if(strpos($paymentCode, 'loviit')!==0){ // only docomo methods
    		return;
    	}
    	//check if order has a successful order request
    	if( $order->getData('loviit_order_request') != 1 ){
    		return;
    	}
    	$response = $this->paymentHelper->doTrackingRequest($shipment,false,$this->messageManager);
    	
    }
}
