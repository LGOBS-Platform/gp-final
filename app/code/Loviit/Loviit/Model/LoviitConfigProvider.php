<?php

namespace Loviit\Loviit\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Loviit\Loviit\Plugin\Data as LoviitHelper;
use Loviit\Loviit\Helper\Payment as LoviitPaymentHelper;
use Loviit\Loviit\Helper\Payment;
use Magento\Checkout\Model\Session as CheckoutSession;

class LoviitConfigProvider implements ConfigProviderInterface {

    const CODE = 'loviit_config_provider';

    protected $config;
    protected $checkoutSession;
   

    /**
     * Constructor
     *
     * @param ConfigFactory $configFactory
     * @param ResolverInterface $localeResolver
     * @param CurrentCustomer $currentCustomer
     * @param PaypalHelper $paypalHelper
     * @param PaymentHelper $paymentHelper
     * @param UrlInterface $urlBuilder
     */
    public function __construct(
    LoviitHelper $loviitHelper, LoviitPaymentHelper $paymentHelper, CheckoutSession $checkoutSession) {
        $this->loviitHelper = $loviitHelper;
        $this->loviitPaymentHelper = $paymentHelper;
        $this->checkoutSession = $checkoutSession;
//         foreach ($this->loviitPaymentHelper->getInstallmentMethodsCodes() as $code) {
//             $this->methods[$code] = $this->loviitHelper->getMethodInstance($code);
//         }
    }

    public function getConfig() {
        $quote = $this->checkoutSession->getQuote();
        $config = array();
        //plans are now recalculate live as just as page load
//         foreach ($this->loviitPaymentHelper->getInstallmentMethodsCodes() as $code) {
//             if($this->methods[$code]->isActive())
//             	$config['payment'][$code]['installment_plans'] = $this->loviitPaymentHelper->calculateInstallmentPlans($quote, $code);
//         }
        
        $config['settings']['general_conditions'] = $this->loviitPaymentHelper->getLoviitConfig('general_conditions');
        $config['settings']['privacy_policy'] = $this->loviitPaymentHelper->getLoviitConfig('privacy_policy');
        $config['settings']['required_fields'] = $this->loviitPaymentHelper->getRequiredFieldsForThisStore($quote);
        $config['settings']['docomo_allow_different_shipping_address'] = $this->loviitPaymentHelper->getLoviitConfig('allow_different_shipping_address');
        $config['lastInstallmentUpdate'] = 1;
        
        return $config;
    }

}
