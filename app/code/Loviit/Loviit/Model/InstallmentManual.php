<?php
/**
 * Copyright � 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Loviit\Loviit\Model;



/**
 * Pay In Store payment method model
 */
class InstallmentManual extends LoviitPaymentMethodAbstract
{

    /**
     * Payment code
     *
     * @var string
     */
    protected $_code = 'loviitrpiv';

    /**
     * Availability option
     *
     * @var bool
     */
    protected $_isOffline = true;
    
    protected $gateway_brand = 'INVOICE';
    protected $docomo_method_id = '12';
    protected $is_secure = false;
}
