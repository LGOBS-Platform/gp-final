<?php
/**
 * Copyright � 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Loviit\Loviit\Model;



/**
 * Pay In Store payment method model
 */
class Paypal extends LoviitPaymentMethodAbstract
{

    /**
     * Payment code
     *
     * @var string
     */
    protected $_code = 'loviitppal';

    /**
     * Availability option
     *
     * @var bool
     */
    protected $_isOffline = true;
    
    protected $gateway_brand = 'PAYPAL';
    protected $docomo_method_id = '7';
    protected $is_server_to_server = false;
    protected $is_secure = true;
    protected $can_do_psp = true;

    /* Paypal could be processed as PA or DB both for trading and psp mode */
    public function getPSPType(){
        return $this->getConfigData('psp_type');
    }
}
