<?php

namespace Loviit\Loviit\Model\Resource;

use Loviit\Loviit\Helper\Payment;
use \Magento\Quote\Api\CartRepositoryInterface;
use Loviit\Loviit\Api\CompletePaymentInterface;
use Loviit\Loviit\Helper\Payon;
use \Magento\Quote\Model\QuoteFactory;
use \Magento\Quote\Model\QuoteIdMaskFactory;
use Loviit\Loviit\Exception\LoviitException;
use \Magento\Framework\Exception\PaymentException;
use \Magento\Framework\Phrase;
/**
 * Class WebServiceRepository
 *
 * @package Loviit\Loviit\Model
 */
class CompletePayment implements CompletePaymentInterface {
	/**
	 *
	 * @var ResourceConnectionFactory
	 */
	protected $_paymentHelper;
	// protected $_checkoutsession;
	
	/**
	 *
	 * @var \Magento\Quote\Api\CartRepositoryInterface
	 */
	protected $quoteRepository;
	protected $payonHelper;
	protected $quoteIdMaskFactory;
	public function __construct(
		CartRepositoryInterface $quoteRepository, 
		Payment $paymentHelper, 
		QuoteFactory $quoteFactory, 
		Payon $payonHelper,
		QuoteIdMaskFactory $quoteIdMaskFactory
	) {
		$this->quoteRepository = $quoteRepository;
		$this->_paymentHelper = $paymentHelper;
		$this->payonHelper = $payonHelper;
		$this->quoteIdMaskFactory = $quoteIdMaskFactory;
	}
	
	/**
	 *
	 * {@inheritDoc}
	 *
	 */
	public function completePayment($cartId, $additionalData=array()){ //:\Loviit\Loviit\Model\Resource\BasicRestResponseObject {
		try{
			$quote = $this->quoteRepository->get ( $cartId );
			$quote->reserveOrderId();
			$this->_paymentHelper->setPaymentMethodFromQuote ( $quote );
			
			$quote = $this->_paymentHelper->verifyAndSetAdditionalPaymentData ( $additionalData,$quote );
			
	        $requestData = $this->_paymentHelper->prepareCheckout($quote);
	        
			if ($requestData) {
				$result = $this->payonHelper->doPrepareRequest($requestData, "/v1/payments");
				$quote = $this->quoteRepository->get ( $cartId );
				$ret = $this->_paymentHelper->placeOrderAfterPaymentGatewayResponse($quote,$result);
				return new BasicRestResponseObject($ret["orderId"],isset($ret["message"])? $ret["message"] : null);
			} 
			return null;
		}
		catch (LoviitException $e){
			//return new BasicRestResponseObject(null, $e->getMessage (),false);
			throw new PaymentException(
                new Phrase($e->getMessage ())
            );
		}
		
		
	}
	
	/**
	 *
	 * {@inheritDoc}
	 *
	 */
	public function completePaymentGuestCart($maskedId,$additionalData=array()){ //:\Loviit\Loviit\Model\Resource\BasicRestResponseObject{
		$quoteIdMask = $this->quoteIdMaskFactory->create()->load($maskedId, 'masked_id');
		return $this->completePayment($quoteIdMask->getQuoteId(),$additionalData);	
	}
}