<?php
namespace Loviit\Loviit\Model\Resource;

use Loviit\Loviit\Api\ProcessSecurePaymentResponseInterface;
use Loviit\Loviit\Helper\Payment;
use \Magento\Quote\Api\CartRepositoryInterface;
use Loviit\Loviit\Helper\Payon;
use Loviit\Loviit\Exception\LoviitException;
use \Magento\Framework\Exception\PaymentException;
use \Magento\Framework\Phrase;

/**
 * Class ProcessSecurePaymentResponse
 *
 * @package Loviit\Loviit\Model
 */
class ProcessSecurePaymentResponse implements ProcessSecurePaymentResponseInterface
{
	
	protected $paymentHelper;
	protected $quoteRepository;
	protected $payonHelper;
	protected $resultJsonFactory;
	protected $quoteIdMaskFactory;
	
	public function __construct(
			CartRepositoryInterface $quoteRepository, 
			Payment $paymentHelper, 
			\Magento\Quote\Model\QuoteFactory $quoteFactory, 
			Payon $payonHelper,
			\Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
			\Magento\Quote\Model\QuoteIdMaskFactory $quoteIdMaskFactory) {	
		$this->quoteRepository = $quoteRepository;
		$this->paymentHelper = $paymentHelper;
		$this->payonHelper = $payonHelper;
		$this->resultJsonFactory = $resultJsonFactory;
		$this->quoteIdMaskFactory = $quoteIdMaskFactory;
	}
	
    /**
	 *
	 * {@inheritDoc}
	 *
	 */
	//it generates the order using the quote id
    public function placeOrder($cartId,$id,$resourcePath){ //:\Loviit\Loviit\Model\Resource\BasicRestResponseObject{
    	try{
	    	$quote = $this->quoteRepository->get ( $cartId );
	    	$requestData = $this->paymentHelper->finishSecureCheckout ( $quote );
	        $result = $this->payonHelper->getRequestCheckoutReponse ( $requestData, $resourcePath );
	    	$ret = $this->paymentHelper->placeOrderAfterPaymentGatewayResponse($quote, $result);
	    	return new BasicRestResponseObject($ret["orderId"],isset($ret["message"])? $ret["message"] : null);
    	}
    	catch (LoviitException $e){
    		//return new BasicRestResponseObject(null, $e->getMessage (),false);
    		throw new PaymentException(
    			new Phrase($e->getMessage ())
    		);
    	}
    }
    
    /**
     *
     * {@inheritDoc}
     *
     */
    //it generates the order using a masked id
    public function placeOrderGuestCart($maskedId,$id,$resourcePath){ //:\Loviit\Loviit\Model\Resource\BasicRestResponseObject{
    	$quoteIdMask = $this->quoteIdMaskFactory->create()->load($maskedId, 'masked_id');
    	return $this->placeOrder($quoteIdMask->getQuoteId(),$id,$resourcePath);
    }
    
}