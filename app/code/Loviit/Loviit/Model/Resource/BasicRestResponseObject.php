<?php
namespace Loviit\Loviit\Model\Resource;

class BasicRestResponseObject{
	/**
	 * order id
	 *
	 * @var int
	 */
	public $orderId;
	
	public $message;
	
	public $success;
	
	public function __construct($orderId,$message,$success = null){
		$this->orderId=$orderId;
		$this->message=$message;
		$this->success=$success;
	}
	
	/**
	 * Get orderId
	 *
	 * @return string|null
	 */
	public function getOrderId(){
		return $this->orderId;
	}
	
	/**
	 * Get orderId
	 *
	 * @return string|null
	 */
	public function getMessage(){
		return $this->message;
	}
	
	/**
	 * Get success
	 *
	 * @return boolean|null
	 */
	public function getSuccess(){
		return $this->success;
	}
}