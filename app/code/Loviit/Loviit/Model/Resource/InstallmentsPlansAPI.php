<?php
namespace Loviit\Loviit\Model\Resource;

use Loviit\Loviit\Api\InstallmentsPlansAPIInterface;
use Loviit\Loviit\Helper\Payment;
use Loviit\Loviit\Model\Resource\BasicRestResponseObject;
use \Magento\Quote\Api\CartRepositoryInterface;
use \Magento\Quote\Model\QuoteFactory;
use \Magento\Quote\Model\QuoteIdMaskFactory;
use Loviit\Loviit\Exception\LoviitException;
use \Magento\Framework\Exception\PaymentException;
use \Magento\Framework\Phrase;
/**
 * Class InstallmentsPlansAPI
 *
 * @package Loviit\Loviit\Model
 */
class InstallmentsPlansAPI implements InstallmentsPlansAPIInterface
{
	
	protected $_paymentHelper;
	protected $quoteRepository;
	protected $quoteIdMaskFactory;
	
	public function __construct(
		CartRepositoryInterface $quoteRepository, 
		Payment $paymentHelper, 
		QuoteFactory $quoteFactory, 
		QuoteIdMaskFactory $quoteIdMaskFactory
	) {
		$this->quoteRepository = $quoteRepository;
		$this->_paymentHelper = $paymentHelper;
		$this->quoteIdMaskFactory = $quoteIdMaskFactory;
	}
	
	/**
	 *
	 * {@inheritDoc}
	 *
	 */
	public function getPlans($cartId,$paymentMethod){ //:array{
	
		try{
			if(!$this->_paymentHelper->isInstallmentMethod($paymentMethod->getMethod())){
				throw new \Loviit\Loviit\Exception\LoviitException("Only Loviit Intallments methods accepted");
			}
			$quote = $this->quoteRepository->get ( $cartId );
			
			$additionalData = $paymentMethod->getAdditionalData ();
			$deposit = (isset($additionalData['deposit']) && !empty($additionalData['deposit'])) ? $additionalData['deposit'] : 0;
			$numberOfInstallments = (isset($additionalData['number_of_installments']) && !empty($additionalData['number_of_installments'])) ? $additionalData['number_of_installments'] : null;
			
			
			$plans = $this->_paymentHelper->calculateInstallmentPlans($quote,$paymentMethod->getMethod(), $numberOfInstallments, $deposit);
			return $plans;
		}
		catch (LoviitException $e){
			//return new BasicRestResponseObject(null, $e->getMessage (),false);
			throw new PaymentException(
					new Phrase($e->getMessage ())
					);
		}
	}
	
	/**
	 *
	 * {@inheritDoc}
	 *
	 */
	public function getPlansGuestCart($maskedId,$paymentMethod){ //:array{
		$quoteIdMask = $this->quoteIdMaskFactory->create()->load($maskedId, 'masked_id');
		return $this->getPlans($quoteIdMask->getQuoteId(), $paymentMethod);
	}
    
}