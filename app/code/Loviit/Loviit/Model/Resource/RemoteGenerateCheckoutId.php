<?php

namespace Loviit\Loviit\Model\Resource;

use Loviit\Loviit\Helper\Payment;
use \Magento\Quote\Api\CartRepositoryInterface;
use Loviit\Loviit\Api\GenerateCheckoutIdInterface;
use Loviit\Loviit\Helper\Payon;

/**
 * Class WebServiceRepository
 *
 * @package Loviit\Loviit\Model
 */
class RemoteGenerateCheckoutId implements GenerateCheckoutIdInterface {
	/**
	 *
	 * @var ResourceConnectionFactory
	 */
	protected $_paymentHelper;
	// protected $_checkoutsession;
	
	/**
	 *
	 * @var \Magento\Quote\Api\CartRepositoryInterface
	 */
	protected $quoteRepository;
	protected $payonHelper;
	protected $quoteIdMaskFactory;
	public function __construct(
			CartRepositoryInterface $quoteRepository, 
			Payment $paymentHelper, 
			\Magento\Quote\Model\QuoteFactory $quoteFactory, 
			Payon $payonHelper,
			\Magento\Quote\Model\QuoteIdMaskFactory $quoteIdMaskFactory) {
		$this->quoteRepository = $quoteRepository;
		$this->_paymentHelper = $paymentHelper;
		$this->payonHelper = $payonHelper;
		$this->quoteIdMaskFactory = $quoteIdMaskFactory;
	}
	
	/**
	 *
	 * {@inheritDoc}
	 *
	 */
	public function getCheckoutId($cartId) {
		$quote = $this->quoteRepository->get ( $cartId );
		$quote->reserveOrderId();
		//$payment = $quote->getPayment ();
		$this->_paymentHelper->setPaymentMethodFromQuote ( $quote );
		//$quote->collectTotals ();
		$requestData = $this->_paymentHelper->prepareCheckout ( $quote );
		if ($requestData) {
 			$result = $this->payonHelper->doPrepareRequest ( $requestData, "/v1/checkouts"); //always secure
			
 			if ($this->payonHelper->isGoodCall ( $result )) {
 				return $result->id;
 			} else {
 				return null; 
 			}
		} 
		
		return null;
	}
	
	/**
	 *
	 * {@inheritDoc}
	 *
	 */
	public function getCheckoutIdGuestCart($maskedId){
		$quoteIdMask = $this->quoteIdMaskFactory->create()->load($maskedId, 'masked_id');
		return $this->getCheckoutId($quoteIdMask->getQuoteId());	
	}
}