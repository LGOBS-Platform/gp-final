<?php
namespace Loviit\Loviit\Model\Resource;

use Loviit\Loviit\Api\SendLoviitRequestInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Loviit\Loviit\Helper\Payment;
use Loviit\Loviit\Model\Resource\BasicRestResponseObject;

/**
 * Class ProcessSecurePaymentResponse
 *
 * @package Loviit\Loviit\Model
 */
class SendLoviitRequest implements SendLoviitRequestInterface
{
	
	private $orderRepository;
	private $paymentHelper;
	
	public function __construct(
			OrderRepositoryInterface $orderRepository,
			Payment $paymentHelper
			) {	
		$this->orderRepository = $orderRepository;
		$this->paymentHelper = $paymentHelper;
	}
	
    /**
	 *
	 * {@inheritDoc}
	 *
	 */
	public function sendOrderRequest($orderId){ //:BasicRestResponseObject{
		$order = $this->orderRepository->get($orderId);
		$ret = $this->paymentHelper->doNewOrderRequest($order);
		return new BasicRestResponseObject($orderId, $ret["message"],$ret["success"]);
	}
	
	/**
	 *
	 * {@inheritDoc}
	 *
	 */
	public function sendDeliveryRequest($orderId,$trackingCode = null){ //:BasicRestResponseObject{
		$order = $this->orderRepository->get($orderId);
		$ret = $this->paymentHelper->doDeliveryRequest($order,$trackingCode);
		return new BasicRestResponseObject($orderId, $ret["message"],$ret["success"]);
	}
	
	/**
	 *
	 * {@inheritDoc}
	 *
	 */
	public function sendCreditNote($orderId){ //:BasicRestResponseObject{
		$order = $this->orderRepository->get($orderId);
		$ret = $this->paymentHelper->doCreditNoteRequest($order);
		return new BasicRestResponseObject($orderId, $ret["message"],$ret["success"]);
	}
    
}