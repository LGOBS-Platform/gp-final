<?php
/**
 * Copyright � 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Loviit\Loviit\Model;



/**
 * Pay In Store payment method model
 */
class Directdebit extends LoviitPaymentMethodAbstract
{

    /**
     * Payment code
     *
     * @var string
     */
    protected $_code = 'loviitdd';

    /**
     * Availability option
     *
     * @var bool
     */
    protected $_isOffline = true;

    protected $gateway_brand = 'DIRECTDEBIT_SEPA';
    protected $docomo_method_id = '10';
    protected $is_secure = false;
    
   
}
