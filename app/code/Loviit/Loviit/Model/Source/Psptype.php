<?php

namespace Loviit\Loviit\Model\Source;
class Psptype
{
    /**
     * @return array
     */
    public function toOptionArray() {
        return [
            [
                'value' => 'PA',
                'label' => __('PA+CP'),
            ],
            [
                'value' => 'DB',
                'label' => __('DB'),
            ]
        ];
    }
}