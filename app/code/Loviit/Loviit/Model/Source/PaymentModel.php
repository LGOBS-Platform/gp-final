<?php

namespace Loviit\Loviit\Model\Source;
class PaymentModel
{
    /**
     * @return array
     */
    public function toOptionArray() {
        return [
            [
                'value' => 'AGIO',
                'label' => __('Agio'),
            ],
            [
                'value' => 'DISAGIO',
                'label' => __('Disagio')
            ]
        ];
    }
}