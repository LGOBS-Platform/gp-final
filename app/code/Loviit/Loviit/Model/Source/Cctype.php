<?php

namespace Loviit\Loviit\Model\Source;
class Cctype
{
    /**
     * @return array
     */
    public function toOptionArray() {
        return [
            [
                'value' => 'AMEX',
                'label' => __('AMEX'),
            ],
            [
                'value' => 'CHINAUNIONPAY',
                'label' => __('CHINAUNIONPAY'),
            ],
            [
                'value' => 'DINERS',
                'label' => __('DINERS'),
            ],
            [
                'value' => 'MASTER',
                'label' => __('MASTER')
            ],
            [
                'value' => 'VISA',
                'label' => __('VISA'),
            ]
        ];
    }
}