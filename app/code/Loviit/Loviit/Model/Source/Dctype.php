<?php

namespace Loviit\Loviit\Model\Source;
class Dctype
{
    /**
     * @return array
     */
    public function toOptionArray() {
        return [
            [
                'value' => 'CARTEBLEUE',
                'label' => __('CARTEBLEUE'),
            ],
            [
                'value' => 'MASTERDEBIT',
                'label' => __('MASTERDEBIT')
            ],
            [
                'value' => 'VISAELECTRON',
                'label' => __('VISAELECTRON')
            ],
            [
                'value' => 'VISADEBIT',
                'label' => __('VISADEBIT')
            ],
            [
                'value' => 'MAESTRO',
                'label' => __('MAESTRO')
            ]
        ];
    }

}