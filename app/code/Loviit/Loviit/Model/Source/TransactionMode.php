<?php

namespace Loviit\Loviit\Model\Source;
class TransactionMode
{
    /**
     * @return array
     */
    public function toOptionArray() {
        return [
            [
                'value' => 'CONNECTOR_TEST',
                'label' => __('CONNECTOR_TEST'),
            ],
            [
                'value' => 'INTEGRATOR_TEST',
                'label' => __('INTEGRATOR_TEST')
            ],
            [
                'value' => 'LIVE',
                'label' => __('LIVE')
            ]
        ];
    }
}