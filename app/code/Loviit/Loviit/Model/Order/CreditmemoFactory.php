<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Loviit\Loviit\Model\Order;

use \Magento\Sales\Model\Order\Creditmemo;
use  Loviit\Loviit\Helper\Payment;


class CreditmemoFactory extends \Magento\Sales\Model\Order\CreditmemoFactory
{

    protected $paymentHelper;

    public function __construct(
        \Magento\Sales\Model\Convert\OrderFactory $convertOrderFactory,
        \Magento\Tax\Model\Config $taxConfig,
        Payment $paymentHelper
    ) {
        parent::__construct($convertOrderFactory,$taxConfig);
        $this->paymentHelper= $paymentHelper;

    }
    
    protected function initData($creditmemo, $data)
    {
        parent::initData($creditmemo, $data);

        if (isset($data['loviit_administration_fee'])) {
            $creditmemo->setData('loviit_administration_fee',(double)$data['loviit_administration_fee']);
        }
        $creditmemo->setData('loviit_interest_fee',$this->paymentHelper->calculateCreditNoteInterestToRefund($creditmemo));

    }
}
