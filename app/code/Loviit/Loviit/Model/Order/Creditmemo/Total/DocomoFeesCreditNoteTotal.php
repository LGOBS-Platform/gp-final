<?php


namespace Loviit\Loviit\Model\Order\Creditmemo\Total;

use \Magento\Framework\Pricing\PriceCurrencyInterface;
use Loviit\Loviit\Helper\Payment  as PaymentHelper;
use Loviit\Loviit\Model\LoviitPaymentMethodAbstract;

/**
 * Order creditmemo DocomoFeesCreditNoteTotal total calculation model
 */
class DocomoFeesCreditNoteTotal extends \Magento\Sales\Model\Order\Creditmemo\Total\AbstractTotal
{
    /**
     * @var PriceCurrencyInterface
     */
    protected $priceCurrency;

    protected $paymentHelper;

   
    /**
     * @param PriceCurrencyInterface $priceCurrency
     * @param array $data
     */
    public function __construct(
        PaymentHelper $paymentHelper
    ) {
        $this->paymentHelper = $paymentHelper;
    }

    /**
     * @param \Magento\Sales\Model\Order\Creditmemo $creditmemo
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function collect(\Magento\Sales\Model\Order\Creditmemo $creditmemo)
    {
        $order = $creditmemo->getOrder();

        $methodInstance = $order->getPayment()->getMethodInstance();
        if($methodInstance instanceof LoviitPaymentMethodAbstract){
           
                //get order docomo administration fee
                $orderAdministrationFee = $order->getData('loviit_administration_fee');
                $allowedAmount = $orderAdministrationFee - $order->getData('loviit_administration_fee_refunded');
                $desiredAmount = $creditmemo->getData('loviit_administration_fee');
                if($creditmemo->getData('loviit_administration_fee')){
                    if($desiredAmount>$allowedAmount){
                        $maxAllowedAmount = $order->getBaseCurrency()->format($allowedAmount, null, false);
                        throw new \Magento\Framework\Exception\LocalizedException(
                            __('Maximum Docomo Administration fee amount allowed to refund is: %1', $maxAllowedAmount)
                        );
                    }
                }
                $creditmemo->setData('loviit_administration_fee',$desiredAmount);

                //set order docomo interest fee
                $interest_fee_amount=$this->paymentHelper->calculateCreditNoteInterestToRefund($creditmemo);
                $creditmemo->setData('loviit_interest_fee',$interest_fee_amount);
                
                $creditmemo->setGrandTotal($creditmemo->getGrandTotal() + $desiredAmount +$interest_fee_amount);
                $creditmemo->setBaseGrandTotal($creditmemo->getBaseGrandTotal() + $desiredAmount+ $interest_fee_amount);
        }
        
        return $this;
    }

   

    
}
