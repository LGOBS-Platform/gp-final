<?php
/**
* Copyright � 2015 Magento. All rights reserved.
* See COPYING.txt for license details.
*/
namespace Loviit\Loviit\Model;



/**
* Pay In Store payment method model
*/
class Installment extends LoviitPaymentMethodAbstract
{
    
    /**
    * Payment code
    *
    * @var string
    */
    protected $_code = 'loviitrp';
    
    /**
    * Availability option
    *
    * @var bool
    */
    protected $_isOffline = true;
    
    protected $gateway_brand = 'DIRECTDEBIT_SEPA';
    protected $docomo_method_id = '5';
    protected $is_secure = false;
    
    /**
    * Returns true if method is loviirp and cart amount is higher than credit limit. Implemented in Installment.php
    */
    public function pushDeposit(){
        $quote = $this->getInfoInstance()->getQuote();
        $maxInstallmentAmount = $this->_scopeConfig->getValue('loviit/settings/max_dd_installment', \Magento\Store\Model\ScopeInterface::SCOPE_STORE );
        $cartTotals = $quote->getTotals ();
        $total = $cartTotals ['grand_total']->getValue ();
        
        if ($total > $maxInstallmentAmount) {
            return true;
        }
        
        return false;
    }
    
    /**
    * Return the id of docomo method
    *
    * @return string
    */
    
    public function getDocomoMethodId(){
        return $this->pushDeposit() ? '13' : $this->docomo_method_id;
    }
    
    /**
    * Return true if payment use server to server implementation
    *
    * @return string
    */
    
    public function isServerToServer(){
        return $this->pushDeposit() ? false : true;
    }
    
    
    /**
    * Return brand (concatenated brands for cc and dc) associated to the payment method
    *
    * @return string
    */
    
    public function getBrand(){
        if($this->pushDeposit()){
            $brands = explode(',',$this->_scopeConfig->getValue ( 'loviit/settings/cc_types', \Magento\Store\Model\ScopeInterface::SCOPE_STORE ));
            return implode(' ',$brands);
        }
        return $this->gateway_brand;
    }
    
}
