<?php

namespace Loviit\Loviit\Model;

use \Magento\Sales\Model\Order;
use Loviit\Loviit\Helper\Payment;

abstract class LoviitPaymentMethodAbstract extends \Magento\Payment\Model\Method\AbstractMethod {
	
	/**
	* Payment Method feature
	*
	* @var bool
	*/
	protected $_isInitializeNeeded = true;
	protected $_canRefund = true; //to allow online refunds, default AbstractMethod value is false
	protected $_canRefundInvoicePartial = true;
	
	const IS_DOCOMO = true;
	
	protected $paymentHelper;
	
	//these must be overriden by the real docomo payment method
	protected $is_secure = false;
	protected $is_installment = false;
	protected $is_installment_with_deposit = false;
	protected $is_iban_required = false;
	protected $is_server_to_server = true;
	protected $can_do_psp = false;
	protected $gateway_brand = '';
	protected $docomo_method_id = '';
	
	/**
	* @param \Magento\Framework\Model\Context $context
	* @param \Magento\Framework\Registry $registry
	* @param \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory
	* @param \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory
	* @param \Magento\Payment\Helper\Data $paymentData
	* @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
	* @param Logger $logger
	* @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
	* @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
	* @param array $data
	* @param Loviit\Loviit\Helper\Payment $data
	* @SuppressWarnings(PHPMD.ExcessiveParameterList)
	*/
	public function __construct(
		\Magento\Framework\Model\Context $context,
		\Magento\Framework\Registry $registry,
		\Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
		\Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
		\Magento\Payment\Helper\Data $paymentData,
		\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
		\Magento\Payment\Model\Method\Logger $logger,
		\Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
		\Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
		array $data = [],
		Payment $paymentHelper = null
		) {
			parent::__construct(
				$context,
				$registry,
				$extensionFactory,
				$customAttributeFactory,
				$paymentData,
				$scopeConfig,
				$logger,
				$resource,
				$resourceCollection,
				$data
			);
			
			$this->paymentHelper = $paymentHelper;
			
			
		}
		
		
		
		/**
		* Method that will be executed instead of authorize or capture
		* if flag isInitializeNeeded set to true
		*
		* @param string $paymentAction        	
		* @param object $stateObject        	
		*
		* @return $this @SuppressWarnings(PHPMD.UnusedFormalParameter)
		*         @api
		*/
		public function initialize($paymentAction, $stateObject) {
			$orderState = Order::STATE_PROCESSING; // STATE_PROCESSING;
			
			$payment = $this->getInfoInstance ();
			$orderStatus = $payment->getConfigData('order_status');
			
			/** @var \Magento\Sales\Model\Order $order */
			$order = $payment->getOrder ();
			
			//user will be notified just if the OR is successful
			$order->setCanSendNewEmailFlag ( false );
			$order->setCustomerNoteNotify(false);
			
			$order->getPayment()->setAdditionalInformation('new_general_tax_logics','true');
			
			$stateObject->setState ( $orderState );
			$stateObject->setStatus ( $orderStatus );
			$stateObject->setIsNotified ( false );
			
		}
		
		
		/**
		* Return brand (concatenated brands for cc and dc) associated to the payment method
		*
		* @return string
		*/
		
		public function getBrand(){
			return $this->gateway_brand;
		}
		
		/**
		* Return the id of 
		*
		* @return string
		*/
		
		public function getDocomoMethodId(){
			return $this->docomo_method_id;
		}

		/**
        * Return true if payment use server to server implementation
        *
        * @return string
        */

        public function isServerToServer(){
            return $this->is_server_to_server;
		}
		
		/**
        * Return true if payment method is secure
        *
        * @return string
        */
        public function isSecure(){
            return $this->is_secure;
		}
		
		/**
        * Return true if payment is psp
        *
        * @return string
        */
        public function isPSP(){
            if ($this->can_do_psp) {
                return $this->getConfigData('psp');
            }
            return false;
        }

        public function getPSPType(){
            if ($this->can_do_psp && $this->isPSP()) {
                return $this->getConfigData('psp_type');
			}
			else{
				return 'PA';
			}
		}
		
		/**
        * Returns true if method is loviirp and cart amount is higher than credit limit. Implemented in Installment.php
        */
        public function pushDeposit(){
            return false;
        }
		
		
		
	}
	