<?php
namespace Loviit\Loviit\Model;

class Registration extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'loviit_loviit_registration';
    protected $_cacheTag = 'loviit_loviit_registration';
    protected $_eventPrefix = 'loviit_loviit_registration';

    protected function _construct()
    {
        $this->_init('Loviit\Loviit\Model\ResourceModel\Registration');
    }
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }
}