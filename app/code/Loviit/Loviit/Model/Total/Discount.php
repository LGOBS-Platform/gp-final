<?php

/**
 * Copyright � 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Loviit\Loviit\Model\Total;

use Loviit\Loviit\Helper\Payment;

class Discount extends \Magento\Quote\Model\Quote\Address\Total\AbstractTotal {

    /**
     * Collect grand total address amount
     *
     * @param \Magento\Quote\Model\Quote $quote        	
     * @param \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment        	
     * @param \Magento\Quote\Model\Quote\Address\Total $total        	
     * @return $this
     */
    protected $quoteValidator = null;
    protected $config;
    protected $paymentHelper;

    public function __construct(
            \Magento\Quote\Model\QuoteValidator $quoteValidator, 
            \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, 
            Payment $paymentHelper) {
        $this->quoteValidator = $quoteValidator;
        $this->config = $scopeConfig;
        $this->paymentHelper = $paymentHelper;
    }

    public function collect(
            \Magento\Quote\Model\Quote $quote, 
            \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment, 
            \Magento\Quote\Model\Quote\Address\Total $total) {
        parent::collect($quote, $shippingAssignment, $total);

        $address = $this->_getAddress();
        if ($address->getAddressType() != $address::ADDRESS_TYPE_SHIPPING)
            return $this;

        try {
            $payment = $quote->getPayment()->getData();
            $payment = isset($payment ['method']) ? $payment ['method'] : null;
        } catch (\Exception $ex) {
            $this->paymentHelper->loviitLogger("Discount collect exc " . $ex->getMessage(), "error");
            return $this;
        }

        $discount = $this->paymentHelper->calculateDiscount($quote, $total, $payment, $quote->getPayment()->getData('number_of_installments'));

        $quote->addData(array(
            'loviit_discount' => $discount
        ));

        $total->setTotalAmount('loviit_discount', $discount);
        $total->setBaseTotalAmount('loviit_discount', $discount);

        return $this;
    }

    protected function clearValues(Address\Total $total) {
        $total->setTotalAmount('subtotal', 0);
        $total->setBaseTotalAmount('subtotal', 0);
        $total->setTotalAmount('tax', 0);
        $total->setBaseTotalAmount('tax', 0);
        $total->setTotalAmount('discount_tax_compensation', 0);
        $total->setBaseTotalAmount('discount_tax_compensation', 0);
        $total->setTotalAmount('shipping_discount_tax_compensation', 0);
        $total->setBaseTotalAmount('shipping_discount_tax_compensation', 0);
        $total->setSubtotalInclTax(0);
        $total->setBaseSubtotalInclTax(0);
    }

    /**
     *
     * @param \Magento\Quote\Model\Quote $quote        	
     * @param Address\Total $total        	
     * @return array|null
     */

    /**
     * Assign subtotal amount and label to address object
     *
     * @param \Magento\Quote\Model\Quote $quote        	
     * @param Address\Total $total        	
     * @return array @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function fetch(\Magento\Quote\Model\Quote $quote, \Magento\Quote\Model\Quote\Address\Total $total) {
        return [
            'code' => 'loviit_discount',
            'title' => 'Discount',
            'value' => $quote->getData('loviit_discount')
        ];
    }

    /**
     * Get Subtotal label
     *
     * @return \Magento\Framework\Phrase
     */
    public function getLabel() {
        return __('Discount');
    }

}
