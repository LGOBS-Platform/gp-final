<?php

namespace Loviit\Loviit\Model\Total\Invoice;

use Loviit\Loviit\Model\LoviitPaymentMethodAbstract;

class DocomoFeesInvoice extends \Magento\Sales\Model\Order\Invoice\Total\AbstractTotal
{
  

    public function collect(\Magento\Sales\Model\Order\Invoice $invoice)
    {
        $methodInstance = $invoice->getOrder()->getPayment()->getMethodInstance();
        if($methodInstance instanceof LoviitPaymentMethodAbstract){
            $adminFee = $invoice->getOrder()->getData('loviit_administration_fee');
            $interestFee = $invoice->getOrder()->getData('loviit_interest_fee');
            $docomoDiscount = $invoice->getOrder()->getData('loviit_discount');
            $invoice->setGrandTotal($invoice->getGrandTotal() + $adminFee +$interestFee+$docomoDiscount);
            $invoice->setBaseGrandTotal($invoice->getBaseGrandTotal() + $adminFee+$interestFee+$docomoDiscount);
        }
        
        return $this;
    }
}
