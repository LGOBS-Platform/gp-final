<?php

namespace Loviit\Loviit\Model;

use \Magento\Framework\Exception\InputException;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Quote\Model\QuoteAddressValidator;
use Psr\Log\LoggerInterface as Logger;
use Loviit\Loviit\Helper\Payment;

class BillingAddressManagement extends \Magento\Quote\Model\BillingAddressManagement
{
    /**
     * PaymentHelper
     *
     * @var Payment
     */
    protected $paymentHelper;
    
    /**
     * ScopeConfigInterface
     *
     * @var $config
     */
    protected $config;
    
    
    public function __construct(
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        QuoteAddressValidator $addressValidator,
        Logger $logger,
        \Magento\Customer\Api\AddressRepositoryInterface $addressRepository,
        ScopeConfigInterface $config,
        Payment $paymentHelper
    ) {
        parent::__construct($quoteRepository, $addressValidator, $logger, $addressRepository);
        $this->config = $config;
        $this->paymentHelper = $paymentHelper;
    }

    public function assign($cartId, \Magento\Quote\Api\Data\AddressInterface $address, $useForShipping = false)
    {
        $quote = $this->quoteRepository->getActive($cartId);
        $quote->removeAddress($quote->getBillingAddress()->getId());
        $quote->setBillingAddress($address);
   
        try {
            $groupId = $this->paymentHelper->setCustomerGroupId($quote);
            $quote->setCustomerGroupId($groupId);

            $quote->setDataChanges(true);
            $this->quoteRepository->save($quote);
            $quote->setTotalsCollectedFlag(false);
            $this->quoteRepository->save($quote);
        } catch (\Exception $e) {
            $this->logger->critical($e);
            throw new InputException(__('Unable to save address. Please check input data.'));
        }
        return $quote->getBillingAddress()->getId();

    }
}