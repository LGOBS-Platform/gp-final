<?php
/**
 * Copyright � 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Loviit\Loviit\Model;

/**
 * Pay In Store payment method model
 */
class Creditcard extends LoviitPaymentMethodAbstract
{

    /**
     * Payment code
     *
     * @var string
     */
    protected $_code = 'loviitcc';

    /**
     * Availability option
     *
     * @var bool
     */
    protected $_isOffline = true;

    protected $gateway_brand = '';
    protected $docomo_method_id = '6';
    protected $is_server_to_server = false;
    protected $is_secure = true;
    protected $can_do_psp = true;
    
    public function getBrand(){
        $brands = explode(',',$this->_scopeConfig->getValue ( 'loviit/settings/cc_types', \Magento\Store\Model\ScopeInterface::SCOPE_STORE ));
		return implode(' ',$brands);
    }
    
    public function getPSPType(){
        return $this->getConfigData('psp_type');
    }
    
}
