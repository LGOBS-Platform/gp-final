<?php
/**
 * Copyright � 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Loviit\Loviit\Model;



/**
 * Pay In Store payment method model
 */
class InstallmentPayPalDeposit extends LoviitPaymentMethodAbstract
{

    /**
     * Payment code
     *
     * @var string
     */
    protected $_code = 'loviitrpva';

    /**
     * Availability option
     *
     * @var bool
     */
    protected $_isOffline = true;
    
    protected $gateway_brand = 'PAYPAL';
    protected $docomo_method_id = '16';
    protected $is_server_to_server = false;
    protected $is_secure = false;

    public function getPSPType(){
        return $this->getConfigData('psp_type');
    }
}
