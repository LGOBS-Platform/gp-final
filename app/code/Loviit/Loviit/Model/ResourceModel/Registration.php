<?php
/**
 * Description of Registration
 *
 * @author samuel.garcia
 */

namespace Loviit\Loviit\Model\ResourceModel;

class Registration extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('loviit_customer_registration', 'registration_id');
    }
}