<?php

namespace Loviit\Loviit\Model\ResourceModel\Registration;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            'Loviit\Loviit\Model\Registration',
            'Loviit\Loviit\Model\ResourceModel\Registration'
        );
    }
}
