<?php
/**
 * Created by PhpStorm.
 * User: sonia
 * Date: 18. 12. 6
 * Time: 오전 9:22
 */

namespace Smile\ElasticsuiteCatalog\Helper;


use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class Configuration
 * @package Smile\ElasticsuiteCatalog\Helper
 */
class Configuration extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * Configuration constructor.
     * @param Context $context
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(Context $context, StoreManagerInterface $storeManager)
    {
        parent::__construct($context);
        $this->storeManager = $storeManager;
    }

    const WEBSITE_CONFIG_FILTER_INCLUDING_TAX_ENABLED = 'smile_elasticsuite_catalog_settings/price_filter_settings/including_tax_enabled';

    const WEBSITE_CONFIG_FILTER_INCLUDING_TAX_PERCENT = 'smile_elasticsuite_catalog_settings/price_filter_settings/including_tax_percent';


    /**
     * @return boolean
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function isEnabled()
    {
        return $this->scopeConfig->getValue(self::WEBSITE_CONFIG_FILTER_INCLUDING_TAX_ENABLED,
            'website',
            $this->storeManager->getWebsite()->getCode()
            );
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getTaxPercent()
    {
        return $this->scopeConfig->getValue(self::WEBSITE_CONFIG_FILTER_INCLUDING_TAX_PERCENT,
            'website',
            $this->storeManager->getWebsite()->getCode()
            );
    }

}