<?php
return [
    'scopes' => [
        'websites' => [
            'admin' => [
                'website_id' => '0',
                'code' => 'admin',
                'name' => 'Admin',
                'sort_order' => '0',
                'default_group_id' => '0',
                'is_default' => '0'
            ],
            'base' => [
                'website_id' => '1',
                'code' => 'base',
                'name' => 'Main Website',
                'sort_order' => '0',
                'default_group_id' => '1',
                'is_default' => '1'
            ],
            'pl' => [
                'website_id' => '2',
                'code' => 'pl',
                'name' => 'Poland Website',
                'sort_order' => '1',
                'default_group_id' => '2',
                'is_default' => '0'
            ],
            'it' => [
                'website_id' => '8',
                'code' => 'it',
                'name' => 'Italy Website',
                'sort_order' => '0',
                'default_group_id' => '8',
                'is_default' => '0'
            ],
            'br' => [
                'website_id' => '9',
                'code' => 'br',
                'name' => 'Brazil Website',
                'sort_order' => '0',
                'default_group_id' => '9',
                'is_default' => '0'
            ]
        ],
        'groups' => [
            0 => [
                'group_id' => '0',
                'website_id' => '0',
                'code' => 'default',
                'name' => 'Default',
                'root_category_id' => '0',
                'default_store_id' => '0'
            ],
            1 => [
                'group_id' => '1',
                'website_id' => '1',
                'code' => 'main_website_store',
                'name' => 'Main Website Store',
                'root_category_id' => '2',
                'default_store_id' => '1'
            ],
            2 => [
                'group_id' => '2',
                'website_id' => '2',
                'code' => 'pl_website_store',
                'name' => 'Poland Website Store',
                'root_category_id' => '3',
                'default_store_id' => '2'
            ],
            8 => [
                'group_id' => '8',
                'website_id' => '8',
                'code' => 'it_website_store',
                'name' => 'Italy Website Store',
                'root_category_id' => '92',
                'default_store_id' => '8'
            ],
            9 => [
                'group_id' => '9',
                'website_id' => '9',
                'code' => 'br_website_store',
                'name' => 'Brazil Website Store',
                'root_category_id' => '231',
                'default_store_id' => '9'
            ]
        ],
        'stores' => [
            'admin' => [
                'store_id' => '0',
                'code' => 'admin',
                'website_id' => '0',
                'group_id' => '0',
                'name' => 'Admin',
                'sort_order' => '0',
                'is_active' => '1'
            ],
            'default' => [
                'store_id' => '1',
                'code' => 'default',
                'website_id' => '1',
                'group_id' => '1',
                'name' => 'Default Store View',
                'sort_order' => '0',
                'is_active' => '1'
            ],
            'pl' => [
                'store_id' => '2',
                'code' => 'pl',
                'website_id' => '2',
                'group_id' => '2',
                'name' => 'Poland Default Store View',
                'sort_order' => '0',
                'is_active' => '1'
            ],
            'it' => [
                'store_id' => '8',
                'code' => 'it',
                'website_id' => '8',
                'group_id' => '8',
                'name' => 'Italy Default Store View',
                'sort_order' => '0',
                'is_active' => '1'
            ],
            'br' => [
                'store_id' => '9',
                'code' => 'br',
                'website_id' => '9',
                'group_id' => '9',
                'name' => 'Brazil Default Store View',
                'sort_order' => '0',
                'is_active' => '1'
            ]
        ]
    ],
    'system' => [
        'default' => [
            'general' => [
                'locale' => [
                    'code' => 'en_US'
                ]
            ],
            'dev' => [
                'static' => [
                    'sign' => '1'
                ],
                'front_end_development_workflow' => [
                    'type' => 'server_side_compilation'
                ],
                'template' => [
                    'minify_html' => '0'
                ],
                'js' => [
                    'merge_files' => 'false',
                    'minify_files' => 'false',
                    'minify_exclude' => '
                    /tiny_mce/
                ',
                    'session_storage_logging' => '0',
                    'translate_strategy' => 'dictionary'
                ],
                'css' => [
                    'minify_files' => 'false',
                    'minify_exclude' => '
                    /tiny_mce/
                ',
                    'merge_css_files' => 'false'
                ]
            ]
        ],
        'stores' => [
            'admin' => [
                'design' => [
                    'package' => [
                        'name' => 'default'
                    ],
                    'theme' => [
                        'default' => 'default'
                    ]
                ]
            ]
        ],
        'websites' => [
            'admin' => [
                'web' => [
                    'routers' => [
                        'frontend' => [
                            'disabled' => 'true'
                        ]
                    ],
                    'default' => [
                        'no_route' => 'admin/noroute/index'
                    ]
                ]
            ],
            'pl' => [
                'web' => [
                    'secure' => [
                        'use_in_frontend' => '1'
                    ]
                ],
                'general' => [
                    'locale' => [
                        'timezone' => 'Europe/Warsaw',
                        'weight_unit' => 'kgs',
                        'firstday' => '1',
                        'code' => 'pl_PL'
                    ],
                    'store_information' => [
                        'phone' => '801-54-54-54',
                        'hours' => 'Konsultanci dostępni są od poniedziałku do piątku w godzinach 8 - 20 i w soboty w godzinach 10.00 - 18.00',
                        'name' => 'Shop.lg.com',
                        'country_id' => 'PL',
                        'region_id' => 'Warszawa',
                        'postcode' => '02-675',
                        'city' => 'Warszawa',
                        'street_line1' => 'Mokotów Nova, Wołoska 22',
                        'street_line2' => NULL,
                        'merchant_vat_number' => NULL
                    ],
                    'country' => [
                        'default' => 'PL',
                        'allow' => 'PL',
                        'destinations' => 'PL'
                    ]
                ],
                'design' => [
                    'theme' => [
                        'theme_id' => 'frontend/LGOBS/pl'
                    ],
                    'pagination' => [
                        'pagination_frame_skip' => NULL,
                        'anchor_text_for_previous' => NULL,
                        'anchor_text_for_next' => NULL
                    ],
                    'head' => [
                        'title_prefix' => NULL,
                        'title_suffix' => NULL,
                        'default_description' => 'Witamy na stronie LG Polska. Elektronika LG to źródło prawdziwej przyjemności – aby brać z życia to, co najlepsze. Dowiedz się więcej na temat naszej elektroniki.',
                        'includes' => NULL,
                        'shortcut_icon' => 'websites/2/favicons.png',
                        'default_title' => 'LG Electronics Polska - sprzęt elektroniczny | LG Polska',
                        'default_keywords' => 'LG, LG PL, LG electronics, elektornika, elektronika użytkowa'
                    ],
                    'header' => [
                        'logo_width' => '105',
                        'logo_height' => '51',
                        'logo_alt' => 'LG',
                        'welcome' => NULL,
                        'logo_src' => 'websites/2/logo.png'
                    ],
                    'footer' => [
                        'absolute_footer' => NULL,
                        'copyright' => 'Copyright © 2019 LG Electronics. Wszelkie prawa zastrzeżone.'
                    ],
                    'search_engine_robots' => [
                        'custom_instructions' => NULL
                    ],
                    'watermark' => [
                        'image_size' => NULL,
                        'image_imageOpacity' => NULL,
                        'small_image_size' => NULL,
                        'small_image_imageOpacity' => NULL,
                        'thumbnail_size' => NULL,
                        'thumbnail_imageOpacity' => NULL,
                        'swatch_image_size' => NULL,
                        'swatch_image_imageOpacity' => NULL
                    ],
                    'email' => [
                        'logo_width' => NULL,
                        'logo_height' => NULL,
                        'logo' => 'websites/2/header-logo_1.png'
                    ]
                ],
                'currency' => [
                    'options' => [
                        'default' => 'PLN',
                        'allow' => 'PLN',
                        'base' => 'PLN'
                    ]
                ],
                'recommended_products' => [
                    'general' => [
                        'category_id' => '73'
                    ]
                ],
                'customer' => [
                    'address' => [
                        'company_show' => NULL
                    ]
                ],
                'wishlist' => [
                    'general' => [
                        'active' => '0'
                    ]
                ],
                'checkout' => [
                    'options' => [
                        'enable_agreements' => '1'
                    ]
                ],
                'tax' => [
                    'calculation' => [
                        'algorithm' => 'TOTAL_BASE_CALCULATION',
                        'based_on' => 'shipping',
                        'price_includes_tax' => '1',
                        'shipping_includes_tax' => '1',
                        'apply_after_discount' => '1',
                        'discount_tax' => '1',
                        'apply_tax_on' => '0'
                    ],
                    'defaults' => [
                        'country' => 'PL'
                    ],
                    'display' => [
                        'type' => '2',
                        'shipping' => '2'
                    ],
                    'cart_display' => [
                        'price' => '2',
                        'subtotal' => '2',
                        'shipping' => '2',
                        'gift_wrapping' => '2',
                        'printed_card' => '2'
                    ],
                    'sales_display' => [
                        'price' => '2',
                        'subtotal' => '2',
                        'shipping' => '2',
                        'gift_wrapping' => '2',
                        'printed_card' => '2'
                    ],
                    'weee' => [
                        'enable' => '0',
                        'display_list' => '1',
                        'display' => '1',
                        'display_sales' => '1',
                        'display_email' => '1',
                        'apply_vat' => '0',
                        'include_in_subtotal' => '0'
                    ]
                ],
                'loviit' => [
                    'settings' => [
                        'cc_types' => 'MASTER,VISA',
                        'legal_address' => 'Mokotów Nova, Wołoska 22, 02-675 Warszawa, Poland'
                    ]
                ],
                'carriers' => [
                    'flatrate' => [
                        'active' => '0'
                    ],
                    'freeshipping' => [
                        'active' => '1',
                        'title' => 'Darmowa dostawa',
                        'name' => 'Darmowa',
                        'sallowspecific' => '1',
                        'specificcountry' => 'PL'
                    ],
                    'tablerate' => [
                        'import' => '1536806295,,,,4,0'
                    ],
                    'dhl' => [
                        'doc_methods' => NULL
                    ],
                    'intelipost' => [
                        'active' => '0'
                    ]
                ],
                'payment' => [
                    'paypal_express' => [
                        'in_context' => '0',
                        'specificcountry' => NULL
                    ],
                    'paypal_express_bml' => [
                        'active' => '0'
                    ],
                    'paypal_billing_agreement' => [
                        'specificcountry' => NULL
                    ],
                    'payflow_express_bml' => [
                        'active' => '0'
                    ],
                    'payflow_advanced' => [
                        'specificcountry' => NULL
                    ],
                    'payflow_express' => [
                        'specificcountry' => NULL
                    ],
                    'payflowpro' => [
                        'specificcountry' => NULL
                    ],
                    'wps_express_bml' => [
                        'active' => '0'
                    ],
                    'payflow_link' => [
                        'specificcountry' => NULL
                    ],
                    'klarna_kp' => [
                        'specificcountry' => NULL
                    ],
                    'loviitcc' => [
                        'active' => '1',
                        'title' => 'karta kredytowa',
                        'allowspecific' => '1',
                        'specificcountry' => 'PL'
                    ],
                    'checkmo' => [
                        'active' => '0'
                    ],
                ],
                'url_rewrite_section' => [
                    'main_conf' => [
                        'categorytocustomurlredirect' => '{"_1542181613369_369":{"from":"227","to":"\\/pl\\/faq"}}'
                    ]
                ],
            ],
            'base' => [
                'design' => [
                    'theme' => [
                        'theme_id' => 'frontend/Eguana/lg'
                    ],
                    'pagination' => [
                        'pagination_frame_skip' => NULL,
                        'anchor_text_for_previous' => NULL,
                        'anchor_text_for_next' => NULL
                    ],
                    'head' => [
                        'title_prefix' => NULL,
                        'title_suffix' => NULL,
                        'default_description' => NULL,
                        'includes' => NULL
                    ],
                    'header' => [
                        'logo_width' => '105',
                        'logo_height' => '51',
                        'logo_alt' => 'LG',
                        'welcome' => NULL,
                        'logo_src' => 'websites/1/logo.png'
                    ],
                    'footer' => [
                        'absolute_footer' => NULL,
                        'copyright' => 'Copyright © 2018 LG Electronics. All Rights Reserved'
                    ],
                    'search_engine_robots' => [
                        'custom_instructions' => NULL
                    ],
                    'watermark' => [
                        'image_size' => NULL,
                        'image_imageOpacity' => NULL,
                        'small_image_size' => NULL,
                        'small_image_imageOpacity' => NULL,
                        'thumbnail_size' => NULL,
                        'thumbnail_imageOpacity' => NULL,
                        'swatch_image_size' => NULL,
                        'swatch_image_imageOpacity' => NULL
                    ],
                    'email' => [
                        'logo' => 'default/header-logo_1.png'
                    ]
                ],
            ],
            'it' => [
                'general' => [
                    'country' => [
                        'default' => 'IT',
                        'allow' => 'IT',
                        'destinations' => 'IT'
                    ],
                    'locale' => [
                        'timezone' => 'Europe/Rome',
                        'weight_unit' => 'kgs',
                        'code' => 'it_IT'
                    ],
                    'store_information' => [
                        'phone' => '800.699.707',
                        'hours' => 'Il servizio è attivo dal Lunedì al Venerdì dalle ore 09:00 alle ore 18:00.',
                        'country_id' => 'IT',
                        'region_id' => 'MI',
                        'postcode' => '20149',
                        'city' => 'Milano',
                        'street_line1' => 'Via Aldo Rossi, 4',
                        'street_line2' => NULL,
                        'merchant_vat_number' => NULL,
                        'name' => 'LG Online Shop'
                    ]
                ],
                'web' => [
                    'default' => [
                        'cms_home_page' => 'home|20'
                    ]
                ],
                'currency' => [
                    'options' => [
                        'base' => 'EUR',
                        'default' => 'EUR',
                        'allow' => 'EUR'
                    ]
                ],
                'payment' => [
                    'paypal_express' => [
                        'in_context' => '0',
                        'specificcountry' => NULL
                    ],
                    'paypal_express_bml' => [
                        'active' => '0'
                    ],
                    'paypal_billing_agreement' => [
                        'specificcountry' => NULL
                    ],
                    'payflow_express_bml' => [
                        'active' => '0'
                    ],
                    'payflow_advanced' => [
                        'specificcountry' => NULL
                    ],
                    'payflow_express' => [
                        'specificcountry' => NULL
                    ],
                    'payflowpro' => [
                        'specificcountry' => NULL
                    ],
                    'wps_express_bml' => [
                        'active' => '0'
                    ],
                    'payflow_link' => [
                        'specificcountry' => NULL
                    ],
                    'klarna_kp' => [
                        'specificcountry' => NULL
                    ],
                    'loviitcc' => [
                        'active' => '1',
                        'allowspecific' => '1',
                        'specificcountry' => 'IT',
                        'title' => 'Credit Card (Visa/Master)',
                        'channel_id' => '8ac9a4cc65a8a0670165e77f2de16ffa'
                    ],
                    'checkmo' => [
                        'active' => '0'
                    ]
                ],
                'shipping' => [
                    'origin' => [
                        'country_id' => 'IT',
                        'region_id' => 'MI',
                        'postcode' => '20149',
                        'city' => 'Milano',
                        'street_line1' => 'Via Aldo Rossi, 4',
                        'street_line2' => NULL
                    ],
                    'shipping_policy' => [
                        'enable_shipping_policy' => '1',
                        'shipping_policy_content' => 'Il servizio di Gold comprende:
<b>rimozione</b> dell’imballo;
<b>collegamento</b> del prodotto alla rete elettrica e/o idrica e/o del gas tramite rubinetteria preesistente ed agli scarichi mediante la rete in dotazione;
verifica del funzionamento del prodotto incluso il carico/scarico dell’acqua ove previsto.

<b style="text-decoration:underline">Per gli Elettrodomestici:</b>
• Rimozione dell’imballo;
• Allacciamento del tubo di carico acqua ad un rubinetto
• Allacciamento del tubo di scarico acqua ad uno scarico murale dotato di apposito sfiato in aria, oppure al sifone dello scarico del lavello
• Rimozione dei fermi di bloccaggio se presenti
• Collegamento alla presa elettrica del cavo di alimentazione in dotazione del prodotto
• Prova di scarico e carico acqua
• Ritiro e smaltimento usato <span style="text-decoration:underline">se selezionato il servizio al momento dell’acquisto</span>: secondo normativa RAEE, D.Lgs 49/2014.
Trasporto negli appositi Centri di Raccolta.

Sono esclusi in ogni caso lavori di falegnameria, di idraulica ecc e tutti quei servizi che rientrano tra i servizi accessori non inclusi nei costi base dell’installazione certificata.
I servizi di installazione ed allacciamento vengono eseguiti secondo le norme vigenti.
I suddetti servizi sono effettuati esclusivamente su impianti preesistenti (elettrico/idrico/gas), perfettamente funzionanti e rispondenti alle normative di sicurezza vigenti.

<b style="text-decoration:underline">Per i TV:</b>
• Rimozione dell’imballo;
• Collegamento alla presa elettrica del cavo di alimentazione in dotazione del prodotto
• Sintonizzazione dei Canali TV
• Ritiro e smaltimento usato <span style="text-decoration:underline">se selezionato il servizio al momento dell’acquisto</span>: secondo normativa RAEE, D.Lgs 49/2014.
Trasporto negli appositi Centri di Raccolta.

Gli accessori funzionali all’installazione del prodotto come staffe a muro, spine, adattatori, cavi e prolunghe, etc., dovranno essere messi a disposizione dal cliente al momento della consegna Sono esclusi in ogni caso lavori di falegnameria, di muratura ecc e tutti quei servizi che rientrano tra i servizi accessori non inclusi nei costi base dell’installazione certificata.
I servizi di installazione ed allacciamento vengono eseguiti secondo le norme vigenti.
Il personale addetto all’installazione del prodotto, potrà eseguirla solo su impianti preesistenti, perfettamente funzionanti e rispondenti alle normative di sicurezza vigenti;'
                    ]
                ],
                'sales_email' => [
                    'invoice' => [
                        'enabled' => '0'
                    ],
                    'order' => [
                        'enabled' => '1',
                        'identity' => 'sales',
                        'template' => '8',
                        'guest_template' => '8'
                    ],
                    'shipment' => [
                        'enabled' => '1',
                        'identity' => 'sales',
                        'template' => '11',
                        'guest_template' => '11'
                    ],
                    'creditmemo' => [
                        'enabled' => '1',
                        'identity' => 'sales',
                        'template' => '20',
                        'guest_template' => '20'
                    ],
                    'magento_rma' => [
                        'enabled' => '1',
                        'identity' => 'sales',
                        'template' => '14',
                        'guest_template' => '14'
                    ],
                    'magento_rma_auth' => [
                        'enabled' => '1',
                        'identity' => 'sales',
                        'template' => '17',
                        'guest_template' => '17'
                    ]
                ],
                'carriers' => [
                    'tablerate' => [
                        'import' => '1541599411,,,,4,0',
                        'active' => '1',
                        'title' => 'Flat Rate',
                        'name' => 'Fixed',
                        'condition_name' => 'package_weight',
                        'sallowspecific' => '1',
                        'specificcountry' => 'IT'
                    ],
                    'dhl' => [
                        'doc_methods' => NULL
                    ],
                    'flatrate' => [
                        'active' => '0',
                        'title' => 'Gold'
                    ],
                    'freeshipping' => [
                        'title' => 'Standard'
                    ],
                    'intelipost' => [
                        'active' => '0'
                    ]
                ],
                'design' => [
                    'theme' => [
                        'theme_id' => 'frontend/LGOBS/it'
                    ],
                    'pagination' => [
                        'pagination_frame_skip' => NULL,
                        'anchor_text_for_previous' => NULL,
                        'anchor_text_for_next' => NULL
                    ],
                    'head' => [
                        'shortcut_icon' => 'websites/8/favicons.png',
                        'default_title' => 'LG Italia',
                        'title_prefix' => NULL,
                        'title_suffix' => '| LG Italia',
                        'default_description' => 'Benvenuti in LG Electronics: elettronica di consumo ed elettrodomestici che rendono la vita più semplice e consentono di vivere grandi emozioni.',
                        'default_keywords' => 'LG Italia',
                        'includes' => NULL
                    ],
                    'header' => [
                        'logo_src' => 'websites/8/logo_bak.png',
                        'logo_width' => '105',
                        'logo_height' => '51',
                        'logo_alt' => 'LG',
                        'welcome' => NULL
                    ],
                    'footer' => [
                        'copyright' => 'Copyright © 2009-2018 LG Electronics. All Rights Reserved',
                        'absolute_footer' => NULL
                    ],
                    'search_engine_robots' => [
                        'custom_instructions' => NULL
                    ],
                    'watermark' => [
                        'image_size' => NULL,
                        'image_imageOpacity' => NULL,
                        'small_image_size' => NULL,
                        'small_image_imageOpacity' => NULL,
                        'thumbnail_size' => NULL,
                        'thumbnail_imageOpacity' => NULL,
                        'swatch_image_size' => NULL,
                        'swatch_image_imageOpacity' => NULL
                    ],
                    'email' => [
                        'logo' => 'websites/8/header-logo_1.png'
                    ]
                ],
                'recommended_products' => [
                    'general' => [
                        'category_id' => '212'
                    ]
                ],
                'tax' => [
                    'calculation' => [
                        'shipping_includes_tax' => '1',
                        'price_includes_tax' => '0',
                        'discount_tax' => '1'
                    ],
                    'defaults' => [
                        'country' => 'IT',
                        'postcode' => '*'
                    ],
                    'display' => [
                        'type' => '2',
                        'shipping' => '2'
                    ],
                    'cart_display' => [
                        'price' => '2',
                        'shipping' => '2',
                        'grandtotal' => '0',
                        'gift_wrapping' => '2',
                        'printed_card' => '2',
                        'subtotal' => '2',
                        'full_summary' => '0'
                    ],
                    'sales_display' => [
                        'price' => '2',
                        'subtotal' => '2',
                        'shipping' => '2',
                        'gift_wrapping' => '2',
                        'printed_card' => '2',
                        'grandtotal' => '0'
                    ]
                ],
                'wishlist' => [
                    'general' => [
                        'active' => '0'
                    ]
                ],
                'checkout' => [
                    'options' => [
                        'enable_agreements' => '1'
                    ]
                ],
                'url_rewrite_section' => [
                    'main_conf' => [
                        'categorytocustomurlredirect' => '{"_1542181678442_442":{"from":"218","to":"\\/it\\/faq"},"_1542181687097_97":{"from":"221","to":"\\/it\\/faq"},"_1542181696809_809":{"from":"224","to":"\\/it\\/contact"}}'
                    ]
                ],
                'loviit' => [
                    'settings' => [
                        'merchant_id' => '2196416',
                        'soap_user' => 'LG_IT',
                        'soap_pw' => 'LG_P78faAF',
                        'cc_types' => 'MASTER,VISA',
                        'legal_address' => 'Via Aldo Rossi, 4, 20149 Milano MI, Italy'
                    ]
                ],
                'returnemail' => [
                    'general' => [
                        'contactemail' => 'lgitaly.obs@lge.com'
                    ]
                ]
            ],
            'br' => [
                'carriers' => [
                    'intelipost' => [
                        'active' => '1',
                        'sallowspecific' => '1',
                        'specificcountry' => 'BR',
                        'height_attribute' => 'ts_dimensions_height',
                        'width_attribute' => 'ts_dimensions_width',
                        'length_attribute' => 'ts_dimensions_length'
                    ],
                    'tablerate' => [
                        'import' => '1542675355,,,,4,0'
                    ],
                    'dhl' => [
                        'doc_methods' => NULL
                    ],
                    'flatrate' => [
                        'name' => 'Frete',
                        'active' => '0'
                    ]
                ],
                'general' => [
                    'country' => [
                        'default' => 'BR',
                        'allow' => 'BR',
                        'destinations' => 'BR'
                    ],
                    'locale' => [
                        'timezone' => 'America/Sao_Paulo',
                        'weight_unit' => 'kgs',
                        'code' => 'pt_BR'
                    ],
                    'store_information' => [
                        'country_id' => 'BR',
                        'region_id' => '508',
                        'postcode' => '04583-110',
                        'city' => 'Sao Paulo-SP',
                        'street_line1' => 'Av Chucri Zaidan, No 940-3th AndarCEP 04583-110 Vila Cordeiro',
                        'phone' => '0800 726 8020',
                        'hours' => 'Segunda a Sexta, das 9h às 20h'
                    ]
                ],
                'web' => [
                    'default' => [
                        'cms_home_page' => 'home|46'
                    ],
                    'session' => [
                        'use_frontend_sid' => '0'
                    ]
                ],
                'currency' => [
                    'options' => [
                        'base' => 'BRL',
                        'default' => 'BRL',
                        'allow' => 'BRL'
                    ]
                ],
                'shipping' => [
                    'origin' => [
                        'country_id' => 'BR',
                        'region_id' => '508',
                        'postcode' => '04583-110',
                        'city' => 'Sau Paulo-SP',
                        'street_line1' => 'Av Chucri Zaidan, No 940-3th AndarCEP 04583-110 Vila Cordeiro',
                        'street_line2' => NULL
                    ]
                ],
                'mundipagg' => [
                    'general' => [
                        'is_active' => '1'
                    ]
                ],
                'payment' => [
                    'paypal_express' => [
                        'in_context' => '0',
                        'specificcountry' => NULL
                    ],
                    'paypal_express_bml' => [
                        'active' => '0'
                    ],
                    'paypal_billing_agreement' => [
                        'specificcountry' => NULL
                    ],
                    'payflow_express_bml' => [
                        'active' => '0'
                    ],
                    'payflow_advanced' => [
                        'specificcountry' => NULL
                    ],
                    'payflow_express' => [
                        'specificcountry' => NULL
                    ],
                    'payflowpro' => [
                        'specificcountry' => NULL
                    ],
                    'wps_express_bml' => [
                        'active' => '0'
                    ],
                    'payflow_link' => [
                        'specificcountry' => NULL
                    ],
                    'klarna_kp' => [
                        'specificcountry' => NULL
                    ],
                    'mundipagg_creditcard' => [
                        'order_status' => 'processing',
                        'reject_order_status' => 'fraud',
                        'review_order_status' => 'payment_review',
                        'cctypes' => 'Visa,Mastercard',
                        'installments_active' => '0'
                    ],
                    'mundipagg_billet' => [
                        'active' => '1'
                    ],
                    'mundipagg_two_creditcard' => [
                        'active' => '0'
                    ],
                    'mundipagg_billet_creditcard' => [
                        'active' => '0'
                    ],
                    'mundipagg_customer_address' => [
                        'street_attribute' => 'street_1',
                        'number_attribute' => 'street_2',
                        'complement_attribute' => 'street_3',
                        'district_attribute' => 'street_4'
                    ],
                    'checkmo' => [
                        'title' => 'Cartão de Crédito / Boleto Bancário'
                    ]
                ],
                'mundipagg_mundipagg' => [
                    'global' => [
                        'ative' => '1',
                        'test_mode' => '1',
                        'secret_key_test' => 'sk_test_Kb8E5rgUWgfMY1re',
                        'public_key_test' => 'pk_test_z5AlB9sw7HWXLNpk'
                    ]
                ],
                'tax' => [
                    'defaults' => [
                        'country' => 'BR'
                    ]
                ],
                'design' => [
                    'theme' => [
                        'theme_id' => 'frontend/LGOBS/br'
                    ],
                    'pagination' => [
                        'pagination_frame_skip' => NULL,
                        'anchor_text_for_previous' => NULL,
                        'anchor_text_for_next' => NULL
                    ],
                    'head' => [
                        'title_prefix' => NULL,
                        'title_suffix' => ' | Loja Oficial LG do Brasil',
                        'default_description' => 'Conheça toda a linha de produtos eletrônicos da LG Electronics. Na LG você encontra  soluções completas para Áudio, Vídeo, Eletrodomésticos, TVs, TVs 3D, Celulares e Informática.',
                        'includes' => NULL,
                        'default_title' => 'Loja Oficial LG do Brasil',
                        'default_keywords' => 'lg, lg brasil, lg eletronicos, eletrodomesticos lg, audio, video, tv,  celular, acessorios, eletrodomestivos, cinema 3d, smart tv, optimus',
                        'shortcut_icon' => 'websites/2/favicons.png'
                    ],
                    'header' => [
                        'logo_width' => '105',
                        'logo_height' => '51',
                        'logo_alt' => 'LG',
                        'logo_src' => 'websites/2/logo.png',
                        'welcome' => NULL
                    ],
                    'footer' => [
                        'absolute_footer' => NULL,
                        'copyright' => 'Copyright © 2009-2019 LG Electronics. Todos os Direitos Reservados'
                    ],
                    'search_engine_robots' => [
                        'custom_instructions' => NULL
                    ],
                    'watermark' => [
                        'image_size' => NULL,
                        'image_imageOpacity' => NULL,
                        'small_image_size' => NULL,
                        'small_image_imageOpacity' => NULL,
                        'thumbnail_size' => NULL,
                        'thumbnail_imageOpacity' => NULL,
                        'swatch_image_size' => NULL,
                        'swatch_image_imageOpacity' => NULL
                    ],
                    'email' => [
                        'logo' => 'websites/9/header-logo_1.png'
                    ]
                ],
                'recommended_products' => [
                    'general' => [
                        'category_id' => '463',
                        'product_size' => '8'
                    ]
                ],
                'url_rewrite_section' => [
                    'main_conf' => [
                        'categorytocustomurlredirect' => '{"_1542768201498_498":{"from":"352","to":"\\/br\\/faq"}}'
                    ]
                ],
                'customer' => [
                    'create_account' => [
                        'vat_frontend_visibility' => '1'
                    ],
                    'address' => [
                        'company_show' => NULL
                    ]
                ],
                'wishlist' => [
                    'general' => [
                        'active' => '0'
                    ]
                ],
                'intelipost_basic' => [
                    'settings' => [
                        'api_key' => 'bc73ef6db9bc24515be8fb00ea4bb51766c831a96df7987b83f6048b575e6bc0﻿'
                    ]
                ],
                'checkout' => [
                    'options' => [
                        'enable_agreements' => '1'
                    ]
                ]
            ]
        ]
    ],
    'modules' => [
        'Magento_Store' => 1,
        'Amasty_Geoip' => 1,
        'Magento_AdminNotification' => 1,
        'Magento_Directory' => 1,
        'Magento_Theme' => 1,
        'Magento_Eav' => 1,
        'Magento_Customer' => 1,
        'Magento_Indexer' => 1,
        'Magento_Cms' => 1,
        'Eguana_Base' => 1,
        'Magento_Catalog' => 1,
        'Eguana_CustomCheckout' => 1,
        'Eguana_CustomCustomer' => 1,
        'Eguana_CustomSales' => 1,
        'Eguana_CustomURLRewriter' => 1,
        'Magento_ImportExport' => 1,
        'Eguana_Faq' => 1,
        'Magento_Payment' => 1,
        'Magento_Rule' => 1,
        'Eguana_Security' => 1,
        'Eguana_LoginRedirect' => 0,
        'Eguana_MiniCartRefresh' => 1,
        'Eguana_ProductTab' => 1,
        'Eguana_RecommendedProducts' => 0,
        'Eguana_ReturnEmail' => 1,
        'Magento_CatalogInventory' => 1,
        'Magento_Backend' => 1,
        'Eguana_InventoryManagement' => 1,
        'Eguana_Share' => 1,
        'Eguana_Theme' => 1,
        'Magento_Config' => 1,
        'Eguana_Terms' => 1,
        'Eguana_Slider' => 1,
        'Magento_Ui' => 1,
        'Magento_CacheInvalidate' => 1,
        'Magento_Quote' => 1,
        'Magento_SalesSequence' => 1,
        'Magento_Sales' => 1,
        'Magento_Checkout' => 1,
        'Magento_User' => 1,
        'Magento_Authorization' => 1,
        'Amasty_Base' => 1,
        'Magento_AdvancedCatalog' => 1,
        'Magento_AdvancedCheckout' => 1,
        'Magento_AdvancedPricingImportExport' => 1,
        'Magento_AdvancedRule' => 1,
        'Magento_SalesRule' => 1,
        'Magento_Search' => 1,
        'Magento_Amqp' => 1,
        'Magento_Security' => 1,
        'Magento_AsynchronousOperations' => 1,
        'Magento_AdminGws' => 1,
        'Magento_Authorizenet' => 1,
        'Amasty_MultiInventory' => 1,
        'Magento_Backup' => 1,
        'Magento_CatalogRule' => 1,
        'Magento_CustomerCustomAttributes' => 1,
        'Magento_Vault' => 1,
        'Magento_Bundle' => 1,
        'Magento_BundleImportExport' => 1,
        'Magento_BundleImportExportStaging' => 1,
        'Magento_CatalogUrlRewrite' => 1,
        'Magento_PageCache' => 1,
        'Magento_Captcha' => 1,
        'Amazon_Core' => 1,
        'Magento_Integration' => 1,
        'Magento_MediaStorage' => 1,
        'Magento_CatalogImportExport' => 1,
        'Magento_CatalogImportExportStaging' => 1,
        'Amazon_Login' => 1,
        'Magento_Staging' => 1,
        'Magento_CatalogSearch' => 1,
        'Magento_Cron' => 1,
        'Magento_Msrp' => 1,
        'Magento_Widget' => 1,
        'Magento_WebsiteRestriction' => 1,
        'Magento_Downloadable' => 1,
        'Magento_GiftCard' => 1,
        'Magento_UrlRewrite' => 1,
        'Magento_CatalogWidget' => 1,
        'Amazon_Payment' => 1,
        'Magento_CheckoutAgreements' => 1,
        'Magento_CheckoutStaging' => 1,
        'Eguana_Attributes' => 1,
        'Magento_CmsStaging' => 1,
        'Magento_CmsUrlRewrite' => 1,
        'Magento_LayeredNavigation' => 1,
        'Magento_ConfigurableImportExport' => 1,
        'Magento_ConfigurableProduct' => 1,
        'Magento_ConfigurableProductSales' => 1,
        'Magento_Wishlist' => 1,
        'Magento_Contact' => 1,
        'Magento_Cookie' => 1,
        'Magento_Email' => 1,
        'Magento_CurrencySymbol' => 1,
        'Magento_CustomAttributeManagement' => 1,
        'Eguana_SSO' => 1,
        'Magento_Analytics' => 1,
        'Magento_CustomerBalance' => 1,
        'Magento_CustomerSegment' => 1,
        'Magento_CustomerFinance' => 1,
        'Magento_CustomerImportExport' => 1,
        'Magento_VersionsCms' => 1,
        'Magento_Cybersource' => 1,
        'Magento_Deploy' => 1,
        'Magento_Developer' => 1,
        'Magento_Dhl' => 1,
        'Eguana_SecureAdmin' => 1,
        'Magento_ProductAlert' => 1,
        'Magento_DownloadableImportExport' => 1,
        'Magento_Reports' => 1,
        'Magento_Banner' => 1,
        'Magento_AdvancedSearch' => 1,
        'Magento_Newsletter' => 1,
        'Magento_EncryptionKey' => 1,
        'Magento_Enterprise' => 1,
        'Magento_Eway' => 1,
        'Magento_Fedex' => 1,
        'Magento_TargetRule' => 1,
        'Magento_GiftCardAccount' => 1,
        'Magento_GiftCardImportExport' => 1,
        'Magento_Tax' => 1,
        'Magento_GiftMessage' => 1,
        'Magento_GiftMessageStaging' => 1,
        'Magento_Weee' => 1,
        'Magento_GiftWrapping' => 1,
        'Magento_GiftWrappingStaging' => 1,
        'Magento_GoogleAdwords' => 1,
        'Magento_GoogleAnalytics' => 1,
        'Magento_GoogleOptimizer' => 1,
        'Magento_GoogleOptimizerStaging' => 1,
        'Magento_GoogleTagManager' => 1,
        'Magento_GroupedImportExport' => 1,
        'Magento_GroupedProduct' => 1,
        'Magento_GroupedProductStaging' => 1,
        'Eguana_EnhancedExport' => 1,
        'Magento_Paypal' => 1,
        'Magento_InstantPurchase' => 1,
        'Magento_CatalogAnalytics' => 1,
        'Magento_Invitation' => 1,
        'Magento_Swatches' => 1,
        'Magento_LayeredNavigationStaging' => 1,
        'Magento_Logging' => 1,
        'Magento_Marketplace' => 1,
        'Magento_CatalogEvent' => 1,
        'Magento_MessageQueue' => 1,
        'Magento_CatalogRuleConfigurable' => 1,
        'Magento_MsrpStaging' => 1,
        'Magento_MultipleWishlist' => 1,
        'Magento_Multishipping' => 1,
        'Magento_MysqlMq' => 1,
        'Magento_NewRelicReporting' => 1,
        'Magento_Review' => 1,
        'Magento_OfflinePayments' => 1,
        'Magento_OfflineShipping' => 1,
        'Fastly_Cdn' => 1,
        'Eguana_GERP' => 1,
        'Magento_PaymentStaging' => 1,
        'Klarna_Core' => 1,
        'Magento_PaypalOnBoarding' => 1,
        'Magento_Persistent' => 1,
        'Magento_PersistentHistory' => 1,
        'Magento_PricePermissions' => 1,
        'Magento_GiftRegistry' => 1,
        'Magento_ProductVideo' => 1,
        'Magento_CatalogStaging' => 1,
        'Magento_PromotionPermissions' => 1,
        'Klarna_Ordermanagement' => 1,
        'Magento_QuoteAnalytics' => 1,
        'Magento_ReleaseNotification' => 1,
        'Magento_Reminder' => 1,
        'Magento_ConfigurableProductStaging' => 1,
        'Magento_RequireJs' => 1,
        'Magento_ResourceConnections' => 1,
        'Magento_SendFriend' => 1,
        'Magento_ReviewAnalytics' => 1,
        'Magento_ReviewStaging' => 1,
        'Magento_Reward' => 1,
        'Magento_RewardStaging' => 1,
        'Magento_Rma' => 1,
        'Magento_RmaStaging' => 1,
        'Magento_Robots' => 1,
        'Magento_Rss' => 1,
        'Eguana_ISAC' => 1,
        'Magento_Braintree' => 1,
        'Magento_SalesAnalytics' => 1,
        'Magento_Signifyd' => 1,
        'Magento_SalesInventory' => 1,
        'Magento_AdvancedSalesRule' => 1,
        'Magento_SalesRuleStaging' => 1,
        'Loviit_Loviit' => 1,
        'Magento_SampleData' => 0,
        'Magento_ScalableCheckout' => 1,
        'Magento_ScalableInventory' => 1,
        'Magento_ScalableOms' => 1,
        'Magento_ScheduledImportExport' => 1,
        'Magento_Elasticsearch' => 1,
        'Magento_SearchStaging' => 1,
        'Magento_CustomerAnalytics' => 1,
        'Magento_Shipping' => 1,
        'Eguana_WasteCollection' => 1,
        'Magento_SalesArchive' => 1,
        'Magento_Sitemap' => 1,
        'Magento_Solr' => 1,
        'Magento_CatalogInventoryStaging' => 1,
        'Amasty_Gdpr' => 1,
        'Magento_Support' => 1,
        'Magento_Swagger' => 1,
        'Eguana_Swatches' => 1,
        'Magento_SwatchesLayeredNavigation' => 1,
        'Magento_GiftCardStaging' => 1,
        'Magento_BundleStaging' => 1,
        'Magento_TaxImportExport' => 1,
        'Eguana_CustomCatalog' => 1,
        'Magento_Translation' => 1,
        'Dotdigitalgroup_Email' => 1,
        'Magento_Ups' => 1,
        'Magento_CatalogUrlRewriteStaging' => 1,
        'MSP_TwoFactorAuth' => 1,
        'Magento_Usps' => 1,
        'Magento_Variable' => 1,
        'Klarna_Kp' => 1,
        'Magento_Version' => 1,
        'Magento_BannerCustomerSegment' => 1,
        'Magento_VisualMerchandiser' => 1,
        'Magento_Webapi' => 1,
        'Magento_WebapiSecurity' => 1,
        'Magento_CatalogPermissions' => 1,
        'Magento_DownloadableStaging' => 1,
        'Magento_WeeeStaging' => 1,
        'Magento_CatalogRuleStaging' => 1,
        'Magento_ProductVideoStaging' => 1,
        'Magento_WishlistAnalytics' => 1,
        'Magento_Worldpay' => 0,
        'MundiPagg_MundiPagg' => 1,
        'Shopial_Facebook' => 0,
        'Smile_ElasticsuiteCore' => 1,
        'Smile_ElasticsuiteCatalog' => 1,
        'Smile_ElasticsuiteSwatches' => 1,
        'Temando_Shipping' => 0,
        'Vertex_Tax' => 0,
        'Xtento_XtCore' => 1,
        'Xtento_OrderExport' => 1
    ],
    'admin_user' => [
        'locale' => [
            'code' => [
                'en_US',
                'pt_BR',
                'pt_PT'
            ]
        ]
    ]
];
