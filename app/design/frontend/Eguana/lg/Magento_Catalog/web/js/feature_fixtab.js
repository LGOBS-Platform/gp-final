require(['jquery'], function($){
    $(function(){
        var cont = [
                $('.detail_guide_anchors'),
                $('.detail_an_contents .feature'),
                $('.detail_an_contents .tech'),
                $('.detail_an_contents .support'),
                $('.detail_an_contents .review')
            ],
            position = {},
            delta = 300,
            timer = null,
            speed = 500,
            contH;

        initPosition(true);

        // 컨텐츠 위치 설정
        function initPosition(boo) {
            for( var i = 0; i < cont.length - 1; i++ ){
                position['p'+i] = Math.floor(cont[i].offset().top);
            }
            contH = cont[0].height();
        }

        // 이동위치 설정
        function gotoScroll(e) {
            switch ($(e.target).attr('class')) {
                case 'anchor p1':
                    return position.p1;
                    break;
                case 'anchor p2':
                    return position.p2;
                    break;
                case 'anchor p3':
                    return position.p3;
                    break;
                case 'anchor p4':
                    return position.p4;
                    break;
                default:
                    break;
            }
        }

        function changTabName(n) {
            $('.detail_guide_anchors .anchors li').removeClass('on').eq(n-1).addClass('on');;
        }

        // 리사이즈
        // 상품 이미지 로딩 후 resize 이벤트 강제 발생.
        $( window ).resize(function() {
            clearTimeout( timer );
            //$(window).off('scroll');
            timer = setTimeout( resizeDone, delta );
        });

        function resizeDone() {
            //console.log('Feature Fix Tab');
            initPosition(false);

            $(window).scroll(function () {
                if ( $(this).scrollTop() > position.p1  -  contH ) {
                    cont[0].addClass('isFixed');

                    if ( $(this).scrollTop() > position.p2 -  contH && $(this).scrollTop() < position.p3 -  contH ) {
                        changTabName(2);
                    } else if ( $(this).scrollTop() >= position.p3 -  contH ) {
                        changTabName(3);
                    } else {
                        changTabName(1);
                    }

                } else {
                    cont[0].removeClass('isFixed');
                }
            });
        }

        // 이벤트
        $('.detail_content .tech div[data-role="trigger"]').click(function(){
            $( window ).trigger('resize');
        });
        cont[0].find('.openner').click(function(e){
            cont[0].addClass('open');
        });
        cont[0].find('a.anchor').click(function(e){
            cont[0].find('li').removeClass('on');
            $(this).parent().addClass('on');
            cont[0].removeClass('open');

            $('body,html').animate({
                scrollTop: gotoScroll(e) - (contH-2)
            }, speed);
        });

    })
});