require(['jquery'], function($){
    $(function(){
        
        var f = '.filter-option';

        // Filter 클릭 설정.
        $('.toggle_filter').click(function(e){
            $('.filter-title [data-role="title"]').trigger( "click" );

            $(f+'-item.allow').removeClass('active').find(f+'s-title').attr({
                'aria-selected': false,
                'aria-expanded': false
            }).end().find(f+'s-content').attr('aria-hidden', true).hide();
        })

        // 검색결과 노출 여부 확인.
        if( $('.filter-current').length > 0 ) {
            $('.in-filter').addClass('on');
        }
    })
});