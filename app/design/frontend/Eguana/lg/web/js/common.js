/**
 * Created by Eguana
 */
require(['jquery'],function($){
//<![CDATA[
    $(document).ready(function() {
        $('.common-left-nav .title').click(function(){
            $(this).parent().toggleClass('active');
        });

        if($('.common-left-nav').offset()) {
            $('.common-left-nav .title').append($('.common-left-nav .current').html());
        }

        $('ul.submenu > li').addClass('ui-menu-item');

        /*
        * Footer Mobile Title
        */
        $('.footer-links strong').click(function(){
            if(isApplication()){
                $(this).next().slideToggle();
                $(this).toggleClass('active');
            }
        });

        /*
        * Configurable Product List
        */
        if($('ol.product-items').offset()) {
            if($('li').hasClass('product-configurable')) {
                $('ol.product-items').addClass('products-configurable');
            }
        }

        /*
        * Mobile layered Filter
        */
        if($('#layered-filter-block').offset()) {
            if(isApplication() && $('.column .sidebar-main').length == 0) {
                $('.toolbar-top .toolbar-sorter').before($('.sidebar-main'));
                $('.block.filter').css('display','block');
            }
        }

        $(window).resize(function(){
            if($('#layered-filter-block').offset()) {
                if(isApplication() && $('.column .sidebar-main').length == 0) {
                    $('.toolbar-top .toolbar-sorter').before($('.sidebar-main'));
                    $('.block.filter').css('display','block');
                }else if(!isApplication() && $('.columns > .sidebar-main').length == 0) {
                    $('.columns .column.main').before($('.sidebar-main'));
                }
            }
        });

        /*
        * Product Detail Tab
        */
        if ($('.product.info').offset()) {
            $('.product.data.items .titles .title:first-child').attr('data-role','active');
            $('.product.data.items .titles .title').click(function(event){
                event.preventDefault();
                $(this).parent().toggleClass('open');

                var cntObj = $($(this).children('a').attr('href'));
                var activeTop = cntObj.offset().top;
                var stickyHeight = $('.titles').height();

                if(!$('.product.data.items .titles').hasClass('sticky')) {
                    activeTop = activeTop - stickyHeight;
                }

                $('.product.data.items .titles .title').removeAttr('data-role');

                if(!isApplication()) {
                    $('html,body').animate({scrollTop : activeTop},400);
                }else if(!$(this).parent().hasClass('open') && !$(this).is('[data-role]')) {
                    $('html,body').animate({scrollTop : activeTop},400);
                }

                $(this).attr('data-role','active');
            });

            var stickyNav = function () {
                $('.product.info .titles').removeClass('sticky');
                $('.product.info.detailed').removeClass('info-sticky');

                var scrollTop = $(window).scrollTop();
                var stickyNavTop = $('.product.info').offset().top;

                if (scrollTop > stickyNavTop) {
                    $('.product.info .titles').addClass('sticky');
                    $('.product.info.detailed').addClass('info-sticky');
                }
            };
            stickyNav();

            $(window).scroll(function () {
                stickyNav();
                var scrollPosition = $(this).scrollTop();

                $('.contents >.data.item.content').each(function(index,element){
                    var offset = $(element).offset();
                    var offsetTop = offset.top;
                    var stickyHeight = $('.titles').height();
                    var top = offsetTop - stickyHeight;
                    if(scrollPosition > top && scrollPosition > 0) {
                        var id = $(element).attr('id');
                        $('.titles >.data.item.title').removeAttr('data-role');
                        $("a[href='#"+id+"']").parent().attr('data-role','active');
                    }
                });
            });
        }


        /*
        * Header Search
        */
        $('[data-role=minisearch-label]').click(function(){
            $(this).parent().toggleClass('active');
        });

        //close search control
        // $('#search').blur(function() {
            //$('[data-role=minisearch-label]').parent().removeClass('active');
        // });

        /**
         * Back Top Button
         */
        // hide #back-top first
        $('.back-top').hide();

        // scroll body to 0px on click
        $('.back-top').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });

        $(window).scroll(function () {
            // fade in #back-top
            if ($(this).scrollTop() > 100) {
                $('.back-top').fadeIn();
            } else {
                $('.back-top').fadeOut();
            }

            var CheckHtml;

            if(isApplication()) {
               CheckHtml = $('.page-bottom');

                /*
                 * Mobile layered Filter
                 */
                if($('#layered-filter-block').offset()) {
                    if($('.column .sidebar-main').length == 0) {
                        $('.toolbar-top .toolbar-sorter').before($('.sidebar-main'));
                        $('.block.filter').css('display','block');
                    }
                }
            }else {
                CheckHtml = $('.bottom.content');

                /*
                 * Mobile layered Filter
                 */
                if($('#layered-filter-block').offset()) {
                    if($('.column .sidebar-main').length != 0) {
                        $('.column.main').before($('.sidebar-main'));
                    }
                }
            }

            if (checkVisible(CheckHtml)) {
                $('.back-top').addClass('end');
            }else {
                $('.back-top').removeClass('end');
            }
        });

        /**
         * menu close
         */
        $('.menu-close').click(function(){
            $('html').removeClass('nav-open');
            setTimeout(function () {
                $('html').removeClass('nav-before-open');
            }, 300);
        });
    });

    /****************************
     * Mobile Y/N
     * @returns {boolean}
     ****************************/
    function isApplication()
    {
        var isMobile = false;
        if(navigator.userAgent.match(/Android|Mobile|iP(hone|od|ad)|BlackBerry|IEMobile|Kindle|NetFront|Silk-Accelerated|(hpw|web)OS|Fennec|Minimo|Opera M(obi|ini)|Blazer|Dolfin|Dolphin|Skyfire|Zune/)){
            if(jQuery(window).width() < 769){
                isMobile = true;
            }
        }
        return isMobile;
    }

    /****************************
     * Check Object Visible
     * @returns
     ****************************/
    function checkVisible( elm, eval ) {
        eval = eval || "object visible";
        var viewportHeight = $(window).height(), // Viewport Height
            scrolltop = $(window).scrollTop(), // Scroll Top
            y = $(elm).offset().top,
            elementHeight = $(elm).height();

        if (eval == "object visible") return ((y < (viewportHeight + scrolltop)) && (y > (scrolltop - elementHeight)));
        if (eval == "above") return ((y < (viewportHeight + scrolltop)));
    }
//]]>
});