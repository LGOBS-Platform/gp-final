/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'uiComponent'
], function (Component) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Magento_Checkout/summary/item/details'
        },

        /**
         * @param {Object} quoteItem
         * @return {String}
         */
        getValue: function (quoteItem) {
            return quoteItem.name;
        },

        getSku: function(itemId) {
            var itemsData = window.checkoutConfig.quoteItemData;
            var prodSku = null;
            itemsData.forEach(function(item) {
                if (item.item_id == itemId) {
                    prodSku = item.sku;
                }
            });
            if (prodSku != null) {
                return prodSku;
            } else {
                return '';
            }
        },

        getSalesModelCode: function(itemId) {
            var itemsData = window.checkoutConfig.quoteItemData;
            var saleModelCode = null;
            itemsData.forEach(function(item) {
                if (item.item_id == itemId) {
                    saleModelCode = item.sales_model_code;
                }
            });
            if (saleModelCode != null) {
                return saleModelCode;
            } else {
                return '';
            }
        },

        getUserFriendlyName: function(itemId) {
            var itemsData = window.checkoutConfig.quoteItemData;
            var userFriendlyName = null;
            itemsData.forEach(function(item) {
                if (item.item_id == itemId) {
                    userFriendlyName = item.user_friendly_name;
                }
            });
            if (userFriendlyName != null) {
                return userFriendlyName;
            } else {
                return '';
            }
        },

        getModelName: function(itemId) {
            var itemsData = window.checkoutConfig.quoteItemData;
            var modelName = null;
            itemsData.forEach(function(item) {
                if (item.item_id == itemId) {
                    modelName = item.model_name;
                }
            });
            if (modelName != null) {
                return modelName;
            } else {
                return '';
            }
        }
    });
});
