/**
 * Created by Eguana
 */
require(['jquery'],function($){
//<![CDATA[
    $(document).ready(function() {
        /**
         * Back Top Button
         */
        // hide #back-top first
        $('.back-top').hide();

        // scroll body to 0px on click
        $('.back-top').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });

        $(window).scroll(function () {
            // fade in #back-top
            if ($(this).scrollTop() > 100) {
                $('.back-top').fadeIn();
            } else {
                $('.back-top').fadeOut();
            }
        });
    });

    /****************************
     * Mobile Y/N
     * @returns {boolean}
     ****************************/
    function isApplication()
    {
        var isMobile = false;
        if(navigator.userAgent.match(/Android|Mobile|iP(hone|od|ad)|BlackBerry|IEMobile|Kindle|NetFront|Silk-Accelerated|(hpw|web)OS|Fennec|Minimo|Opera M(obi|ini)|Blazer|Dolfin|Dolphin|Skyfire|Zune/)){
            if(jQuery(window).width() < 768){
                isMobile = true;
            }
        }
        return isMobile;
    }
//]]>
});